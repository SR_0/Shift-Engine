workspace "ShiftEngine"
	location "Source"
	architecture "x64"
	startproject "SClient_Win64"

	configurations
	{
		"Debug",
		"Release",
		"Final"
	}
	
	flags
	{
		"MultiProcessorCompile"
	}
	
binaryDir = "../Bin/"
libraryDir = "../Libraries/"

project "SRender"
	location "Source/SRender"
	kind "StaticLib"
	language "C++"
	cppdialect "C++17"
	staticruntime "on"

	targetdir (libraryDir)
	objdir (libraryDir)

	pchheader "pch.h"
	pchsource "pch.cpp"

	files
	{
		"Source/%{prj.name}/**.h",
		"Source/%{prj.name}/**.cpp"
	}

	defines
	{
		"_CRT_SECURE_NO_WARNINGS"
	}

	includedirs
	{
	}

	links 
	{ 
	}

	filter "system:windows"
		systemversion "10.0.18362.0"

		defines
		{
		}

	filter "configurations:Debug"
		defines "IS_DEBUG"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		defines "IS_RELEASE"
		runtime "Release"
		optimize "on"

	filter "configurations:Final"
		defines "IS_FINAL"
		runtime "Release"
		optimize "on"