#pragma once
#include "Component.h"
#include "SC_Matrix44.hpp"

namespace Shift
{
	class CTransformComponent : public CComponent
	{
	public:
		CTransformComponent();
		~CTransformComponent();

		const SC_Matrix44& GetTransform() const;
		void SetTransform(const SC_Matrix44& aTransform);

		static constexpr EComponentType ComponentType = EComponentType::Transform;
		static CTransformComponent DefaultObject;
	private:
		SC_Matrix44 myMatrix;

		/*
		Transform-matrix or quaternion?
		- position
		- rotation
		- scale
		*/
	};
}
