#pragma once
#include "Component.h"

namespace Shift
{
	class CScriptComponent : public CComponent
	{
	public:
		CScriptComponent();
		~CScriptComponent();

		static constexpr EComponentType ComponentType = EComponentType::Script;
		static CScriptComponent DefaultObject;
	};
}
