#include "SEntitySystem_Precompiled.h"
#include "EntityManager.h"
#include "GameObject.h"
#include "SG_SceneGraph.h"

#define CHECK_IF_VAILD(handle) assert("Invalid Handle." && handle != UINT_MAX)
namespace Shift
{
	CEntityManager::CEntityManager()
	{
	}


	CEntityManager::~CEntityManager()
	{
	}
	const bool CEntityManager::Init()
	{
		myGameObjectPool.resize(MaxEntities);
		for (uint i = 0; i < MaxEntities; ++i)
		{
			myFreeIndices.push(i);
		}
		return true;
	}
	void CEntityManager::Destroy()
	{
		// Invalidate all gameobjects.

		for (auto& gameObject : myGameObjectPool)
		{
			if (gameObject.IsActive() == false)
			{
				continue;
			}
			unsigned int compType = 0;
			for (auto& comp : gameObject.myComponents)
			{
				if (comp == CComponent::ComponentHandle_Invalid)
				{
					++compType;
					continue;
				}
				switch (compType)
				{
				case 0:
					if (myTransformComponentPool[comp].IsActive())
					{
						myTransformComponentPool[comp].SetActiveState(false);
					}
					break;
				case 1:
					if (myRenderableComponentPool[comp].IsActive())
					{
						myRenderableComponentPool[comp].SetActiveState(false);
					}
					break;
				case 2:
					if (myPhysicsComponentPool[comp].IsActive())
					{
						myPhysicsComponentPool[comp].SetActiveState(false);
					}
					break;
				case 3:
					if (myScriptComponentPool[comp].IsActive())
					{
						myScriptComponentPool[comp].SetActiveState(false);
					}
					break;
				case 4:
					if (myInputComponentPool[comp].IsActive())
					{
						myInputComponentPool[comp].SetActiveState(false);
					}
					break;
				}
			}

			gameObject.SetActiveState(false);
			++compType;
		}

	}
	void CEntityManager::Update()
	{
		// UPDATE INPUT
		// UPDATE SCRIPTS
		// UPDATE PHYSICS
		// UPDATE TRANSFORMS
		// RENDER
	}
	CGameObject& CEntityManager::CreateGameObject()
{
		if (myFreeIndices.empty())
			assert(false);

		uint handle = myFreeIndices.top();
		myFreeIndices.pop();
		myGameObjectPool[handle].SetActiveState(true);
		myGameObjectPool[handle].myHandle = handle;
		myGameObjectPool[handle].myEntityManager = this;
		myGameObjectPool[handle].AddComponent(EComponentType::Transform);
		return myGameObjectPool[handle];
	}
	const CGameObject& CEntityManager::GetGameObject(const GameObjectHandle aHandle)
	{
		CHECK_IF_VAILD(aHandle);
		return myGameObjectPool[aHandle];
	}

	const ComponentHandle CEntityManager::GetComponentHandle(const EComponentType aType, const GameObjectHandle aHandle)
	{
		ComponentHandle handle = CComponent::ComponentHandle_Invalid;

		switch (aType)
		{
		case EComponentType::Input:
			myInputComponentPool.insert(std::make_pair(aHandle, CInputComponent()));
			break;
		case EComponentType::Renderable:
			myRenderableComponentPool.insert(std::make_pair(aHandle, CRenderableComponent()));
			break;
		case EComponentType::Transform:
			myTransformComponentPool.insert(std::make_pair(aHandle, CTransformComponent()));
			break;
		case EComponentType::Script:
			myScriptComponentPool.insert(std::make_pair(aHandle, CScriptComponent()));
			break;
		case EComponentType::Physics:
			myPhysicsComponentPool.insert(std::make_pair(aHandle, CPhysicsComponent()));
			break;
		case EComponentType::Network:
			break;
		default:
			return handle;
		}

		return aHandle;
	}

	const CTransformComponent& CEntityManager::GetTransformComponent(const GameObjectHandle aGOHandle)
	{
		CHECK_IF_VAILD(aGOHandle);
		auto& gameObject = myGameObjectPool[aGOHandle];

		CTransformComponent& compHandle = gameObject.GetComponent<CTransformComponent>();

		return compHandle;
	}

	void CEntityManager::DestroyGameObject(const GameObjectHandle aHandle)
	{
		myGameObjectPool[aHandle].SetActiveState(false);
		myFreeIndices.push(aHandle);
	}
	void CEntityManager::PrepareView(SG_View* aView)
	{
		// wait for physics to complete.
		//CPhysicsEngine::FetchResults(true);
		//CSceneGraph& scene = CEngineInterface::GetScene();
		SG_RenderData& prepareData = aView->GetPrepareData();

		for (auto& comp : myRenderableComponentPool)
		{
			if (comp.second.IsActive() == false)
			{
				continue;
			}
			auto& transformComp = GetTransformComponent(comp.first);

			SG_RenderObject renderObject;
			renderObject.myTransform = transformComp.GetTransform();
			renderObject.myTransform.Transpose();
			comp.second.Render(renderObject);

			//scene.SubmitDrawCommand(renderObject);
			prepareData.myOpaqeObjects.Add(renderObject);
		}
	}
}