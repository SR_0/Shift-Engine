#pragma once
#include "Component.h"

namespace Shift
{
	class CInputComponent : public CComponent
	{
	public:
		CInputComponent();
		~CInputComponent();

		static constexpr EComponentType ComponentType = EComponentType::Input;
		static CInputComponent DefaultObject;
	};
}
