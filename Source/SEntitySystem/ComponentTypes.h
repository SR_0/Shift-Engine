#pragma once

namespace Shift
{
	enum class EComponentType : unsigned char
	{
		Transform = 0,
		Renderable,
		Physics,	
		Script,	
		Input,
		Network,
		COUNT,
		NONE,
	};
}