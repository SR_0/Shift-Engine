#pragma once
#include "ComponentTypes.h"

namespace Shift
{
	using GameObjectHandle = unsigned int;
	using ComponentHandle = unsigned int;
	class CComponent
	{
		friend class CEntityManager;
	public:
		CComponent();
		virtual ~CComponent();

		const bool IsActive() const;
		void SetActiveState(const bool aBool);

		static constexpr unsigned int ComponentHandle_Invalid = UINT_MAX;

	protected:
		ComponentHandle myHandle;
		GameObjectHandle myGameObjectHandle;
		bool myIsActive;
	};

}