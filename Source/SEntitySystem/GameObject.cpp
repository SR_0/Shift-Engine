#include "SEntitySystem_Precompiled.h"
#include "GameObject.h"
#include "EntityManager.h"
namespace Shift
{
	CGameObject::CGameObject()
		: myHandle(UINT_MAX)
		, myIsActive(false)
	{
		for (auto& comp : myComponents)
		{
			comp = CComponent::ComponentHandle_Invalid;
		}
	}


	CGameObject::~CGameObject()
	{
	}
	const bool CGameObject::IsActive() const
	{
		return myIsActive;
	}
	void CGameObject::SetActiveState(const bool aBool)
	{
		myIsActive = aBool;
	}
	const bool CGameObject::HasComponent(const EComponentType aType) const
	{
		if (aType >= EComponentType::COUNT)
		{
			return false;
		}
		else
		{
			return (myComponents[static_cast<unsigned int>(aType)] != CComponent::ComponentHandle_Invalid);
		}
	}
	const bool CGameObject::AddComponent(const EComponentType aType)
	{
		if (myEntityManager)
		{
			myComponents[uint(aType)] = myEntityManager->GetComponentHandle(aType, myHandle);
			return true;
		}

		return false;
	}
}