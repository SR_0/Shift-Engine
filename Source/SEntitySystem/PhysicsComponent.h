#pragma once
#include "Component.h"

namespace Shift
{
	class CPhysicsComponent : public CComponent
	{
	public:
		CPhysicsComponent();
		~CPhysicsComponent();

		static constexpr EComponentType ComponentType = EComponentType::Physics;
		static CPhysicsComponent DefaultObject;
	};

}