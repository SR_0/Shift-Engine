#include "SEntitySystem_Precompiled.h"
#include "TransformComponent.h"

namespace Shift
{
	CTransformComponent CTransformComponent::DefaultObject;
	CTransformComponent::CTransformComponent()
		: CComponent()
	{
	}


	CTransformComponent::~CTransformComponent()
	{
	}
	const SC_Matrix44& CTransformComponent::GetTransform() const
	{
		return myMatrix;
	}
	void CTransformComponent::SetTransform(const SC_Matrix44& aTransform)
	{
		myMatrix = aTransform;
	}
}