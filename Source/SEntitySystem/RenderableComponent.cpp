#include "SEntitySystem_Precompiled.h"
#include "RenderableComponent.h"
#include "SG_Model.h"

namespace Shift
{
	CRenderableComponent CRenderableComponent::DefaultObject;
	CRenderableComponent::CRenderableComponent()
		: CComponent()
	{
	}


	CRenderableComponent::~CRenderableComponent()
	{
	}

	void CRenderableComponent::Init(const SRenderableDesc& aDesc)
	{
		myModel = new SG_Model();
		myModel->FromFile(aDesc.myModelFile);

		myMaterial = new SG_Material();
		myMaterial->Init(aDesc.myMaterialDesc);
	}
	void CRenderableComponent::Render(SG_RenderObject& /*aRenderObjectOut*/)
	{
		//aRenderObjectOut.myMaterial = myMaterial;
		//aRenderObjectOut.myModel = myModel;
	}
}