#pragma once
#include "Component.h"
#include "EntityManager.h"
#include <array>

namespace Shift
{
	using GameObjectHandle = unsigned int;
	class CGameObject
	{
		friend class CEntityManager;
	public:
		CGameObject();
		~CGameObject();

		const bool IsActive() const;
		void SetActiveState(const bool aBool);

		const bool HasComponent(const EComponentType aType) const;
		const bool AddComponent(const EComponentType aType);

		template<class Type>
		const Type& GetComponent() const; // Returns UINT_MAX if objects doesn't have component type
		template<class Type>
		Type& GetComponent(); // Returns UINT_MAX if objects doesn't have component type

		const GameObjectHandle GetParentHandle() const;
		const std::vector<GameObjectHandle>& GetChildrenHandles() const;

	private:
		///////////////////////////////////////////////////////////////////////////////////////
		std::array<ComponentHandle, (unsigned int)EComponentType::COUNT> myComponents;
		std::vector<GameObjectHandle> myChildren;
		GameObjectHandle myHandle;
		GameObjectHandle myParent;
		CEntityManager* myEntityManager;
		bool myIsActive;
		///////////////////////////////////////////////////////////////////////////////////////

		//CNodeGraph* myGraph;

	};

	template<class Type>
	inline const Type& CGameObject::GetComponent() const
	{
		const EComponentType type = Type::ComponentType;
		if (HasComponent(type))
		{
			return *(static_cast<Type*>(myEntityManager->GetComponent<Type>(myComponents[static_cast<unsigned int>(type)])));
		}
		return Type::DefaultObject;
	}

	template<class Type>
	inline Type& CGameObject::GetComponent()
	{
		const EComponentType type = Type::ComponentType; 
		if (HasComponent(type))
		{
			return *(static_cast<Type*>(myEntityManager->GetComponent<Type>(myComponents[static_cast<unsigned int>(type)])));
		}
		return Type::DefaultObject;
	}
}
