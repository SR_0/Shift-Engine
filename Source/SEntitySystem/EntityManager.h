#pragma once
#include <array>
#include "TransformComponent.h"
#include "RenderableComponent.h"
#include "PhysicsComponent.h"
#include "InputComponent.h"
#include "ScriptComponent.h"
#include <stack>

namespace Shift
{
	class SG_RenderData;
	class CGameObject;
	class SG_View;
	class CEntityManager
	{
	public:
		CEntityManager();
		~CEntityManager();

		const bool Init();
		void Destroy();

		void Update();
		void PrepareView(SG_View* aView);

		CGameObject& CreateGameObject();
		const CGameObject& GetGameObject(const GameObjectHandle aHandle);

		const ComponentHandle GetComponentHandle(const EComponentType aType, const GameObjectHandle aHandle);

		template<class Type> 
		CComponent* GetComponent(const ComponentHandle aHandle);

		const CTransformComponent& GetTransformComponent(const GameObjectHandle aGOHandle);

		void DestroyGameObject(const GameObjectHandle aHandle);

		static constexpr unsigned int MaxEntities = (10000);
	private:
		void UpdatePhysics(const float aDeltaTime);
		void UpdateScripts(const float aDeltaTime);
		void UpdateInput(const float aDeltaTime);

		std::vector<CGameObject> myGameObjectPool;

		std::map<GameObjectHandle, CTransformComponent> myTransformComponentPool;
		std::map<GameObjectHandle, CRenderableComponent> myRenderableComponentPool;
		std::map<GameObjectHandle, CPhysicsComponent> myPhysicsComponentPool;
		std::map<GameObjectHandle, CScriptComponent> myScriptComponentPool;
		std::map<GameObjectHandle, CInputComponent> myInputComponentPool;

		// CQuadTree myStaticObjectVolume; // Static Objects
		// CGrid myDynamicObjectVolume; // Dynamic Objects

		std::stack<uint> myFreeIndices;
	};

	template<class Type>
	inline CComponent* CEntityManager::GetComponent(const ComponentHandle aHandle)
	{
		const EComponentType type = Type::ComponentType;
		switch (type)
		{
		case EComponentType::Input:
			return &myInputComponentPool[aHandle];
			break;
		case EComponentType::Transform:
			return &myTransformComponentPool[aHandle];
			break;
		case EComponentType::Renderable:
			return &myRenderableComponentPool[aHandle];
			break;
		case EComponentType::Physics:
			return &myPhysicsComponentPool[aHandle];
			break;
		case EComponentType::Script:
			return &myScriptComponentPool[aHandle];
			break;
		case EComponentType::Network:
			break;
		}
		return nullptr;
	}
}
