#pragma once
#include "Component.h"
#include "SG_Mesh.h"
#include "SG_Material.h"

namespace Shift
{
	struct SG_RenderObject;
	class SG_Model;
	class SG_Material;

	struct SRenderableDesc
	{
		SG_MaterialDesc myMaterialDesc;
		const char* myModelFile;
	};

	class CRenderableComponent : public CComponent
	{
	public:
		CRenderableComponent();
		~CRenderableComponent();

		void Init(const SRenderableDesc& aDesc);
		void Render(SG_RenderObject& aRenderObjectOut);

		SG_Model* GetModel() { return myModel; }

		static constexpr EComponentType ComponentType = EComponentType::Renderable;
		static CRenderableComponent DefaultObject;
	private:
		/* Render settings
		- Material
		- Mesh
		- Special Settings
		*/
		SG_Material* myMaterial;
		SG_Model* myModel;
	};

}