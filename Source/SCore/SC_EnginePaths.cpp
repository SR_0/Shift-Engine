#include "SCore_Precompiled.h"
#include "SC_EnginePaths.h"

namespace Shift
{
	static constexpr const char* locRelativeDataPath = "../Data/";
	static constexpr const char* locRelativeDataPathDblSlash = "..\\Data\\";
	static constexpr const char* locShadersDirectory = "../Data/Shaders/";
	static constexpr const char* locDocumentsDirectory = "NOT IMPLEMENTED YET!";

	static constexpr const wchar_t* locRelativeDataPathW = L"../Data/";
	static constexpr const wchar_t* locDocumentsDirectoryW = L"NOT IMPLEMENTED YET!";

	const char* SC_EnginePaths::GetRelativeDataPath()
	{
		return locRelativeDataPath;
	}

	const char* SC_EnginePaths::GetRelativeDataPathDblSlash()
	{
		return locRelativeDataPathDblSlash;
	}

	const char* SC_EnginePaths::GetShadersDirectory()
	{
		return locShadersDirectory;
	}

	const char* SC_EnginePaths::GetDocumentsDirectory()
	{
		return locDocumentsDirectory;
	}
	const wchar_t* SC_EnginePaths::GetRelativeDataPathW()
	{
		return locRelativeDataPathW;
	}

	const wchar_t* SC_EnginePaths::GetDocumentsDirectoryW()
	{
		return locDocumentsDirectoryW;
	}
	std::string SC_EnginePaths::GetExecutableName()
	{
		std::string outName;

#if IS_WINDOWS
		CHAR exeFileName[MAX_PATH];
		GetModuleFileNameA(NULL, exeFileName, MAX_PATH);
		outName = exeFileName;
#endif

		return outName;
	}

	void SC_EnginePaths::SetWorkingDirectory(const char* aWorkingDir)
	{
#if IS_WINDOWS
		SetCurrentDirectoryA(aWorkingDir);
#elif IS_LINUX
		chdir(aWorkingDir);
#else
#error Platform not supported!
#endif
	}
}