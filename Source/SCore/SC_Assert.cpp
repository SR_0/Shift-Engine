#include "SCore_Precompiled.h"
#include "SC_Assert.h"
#include <sstream>

namespace Shift
{
	void SC_Assert_Internal(const char* aFile, const int aLine, const char* aMessage, const char* aFmtString, ...)
	{
		SC_UniquePtr<char> formattedString = new char[2048];
		if (aFmtString)
		{
			va_list paramList;

			va_start(paramList, aFmtString);
			vsnprintf(formattedString.Get(), sizeof(char) * 2048, aFmtString, paramList);
			va_end(paramList);
		}

		std::stringstream outString;
		outString << "[" << aFile << "]: Line " << aLine << " - Assert Failed (" << aMessage << ") : " << formattedString << std::endl;

		SC_ERROR_LOG(outString.str().c_str());

#if USE_LOGGING
		SC_Logger::GetInstance()->Flush();
#endif
		assert(false);
	}
}
