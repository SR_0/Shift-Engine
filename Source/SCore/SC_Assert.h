#pragma once
#include <cassert>

namespace Shift
{
	void SC_Assert_Internal(const char* aFile, const int aLine, const char* aMessage, const char* aFmtString, ...);
}

#if ENABLE_ASSERTS
#	define SC_ASSERT_IMPL(aBool, ...) if (!(aBool)) { Shift::SC_Assert_Internal(__FILE__, __LINE__, #aBool, ##__VA_ARGS__); }
#	define SC_ASSERT(aBool, ...) SC_ASSERT_IMPL(aBool, ##__VA_ARGS__)
#else
#	define SC_ASSERT(aBool, ...) (void)aBool;
#endif

