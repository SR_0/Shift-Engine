#pragma once
#if IS_WINDOWS
#include "SC_Window.h"

namespace Shift
{
	class SC_Window_Win64 : public SC_Window
	{
		friend class SC_Window;
	public:
		virtual ~SC_Window_Win64();

		bool Init() override final;
		bool HandleMessages() override final;

		HWND GetHandle() const;
	private:
		SC_Window_Win64();

		HWND myWindowHandle;
	};
}

#endif