#pragma once
#include <string>
#include "SR_RenderEnums.h"

namespace Shift
{
	class SC_Config
	{
		friend class SC_Engine;
	public:
		static void Load();
		static void Save();

		static void SetWindowWidth(const float aWidth);
		static void SetWindowHeight(const float aHeight);

		static const std::string& GetAppName();
		static const float GetWindowWidth();
		static const float GetWindowHeight();
		static const bool IsFullscreen();
		static const SR_GraphicsAPI GetGraphicsAPI();

	private:
		struct ConfigLayout
		{
			ConfigLayout()
				: myWindowWidth(1280.f)
				, myWindowHeight(720.f)
				, myIsFullscreen(false)
				, myGraphicsAPI(SR_GraphicsAPI::DirectX12)
				, myApplicationName("ShiftEngine")
			{}

			void operator=(const ConfigLayout& aOther)
			{
				myApplicationName = aOther.myApplicationName;
				myWindowWidth = aOther.myWindowWidth;
				myWindowHeight = aOther.myWindowHeight;
				myIsFullscreen = aOther.myIsFullscreen;
				myGraphicsAPI = aOther.myGraphicsAPI;
			}

			std::string myApplicationName;
			float myWindowWidth;
			float myWindowHeight;
			bool myIsFullscreen;
			SR_GraphicsAPI myGraphicsAPI;
		};

		static ConfigLayout* ourConfig;
	};

}