#pragma once

namespace Shift
{  
	// Protection from unintended ADL
	namespace Noncopyable_
	{
		class SC_Noncopyable
		{
		protected:
			SC_Noncopyable() {}
			~SC_Noncopyable() {}
		private:
			SC_Noncopyable(const SC_Noncopyable&);
			const SC_Noncopyable& operator=(const SC_Noncopyable&);
		};
	}
	typedef Noncopyable_::SC_Noncopyable SC_Noncopyable;
}