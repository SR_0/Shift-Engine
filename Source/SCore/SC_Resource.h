#pragma once
#include "SC_RefCounted.h"

namespace Shift
{
	enum SC_LoadState
	{
		Unloaded,
		Loading,
		Loaded
	};

	class SC_Resource : public SC_RefCounted
	{
	public:
		SC_Resource();
		virtual ~SC_Resource();

		void SetState(const SC_LoadState& aState) { myState = aState; }
		bool IsLoaded() const { return myState == Loaded; }
		bool IsUnloaded() const { return myState == Unloaded; }
		bool IsLoading() const { return myState == Loading; }

	protected:
		SC_LoadState myState;
	};
}