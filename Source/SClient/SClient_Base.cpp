#include "SClient_Precompiled.h"
#include "SClient_Base.h"
#include "SClient_GameObject.h"

#include "SG_World.h"
#include "SG_ModelLoader.h"
#include "SG_RenderData.h"

#include "SP_PhysicsDevice.h"
#include "SP_RigidBody.h"

SClient_Base* SClient_Base::ourInstance = nullptr;

SClient_Base::SClient_Base()
{
	assert(ourInstance == nullptr);
	ourInstance = this;
}


SClient_Base::~SClient_Base()
{
	delete myTestObj;
	delete myMainCamera;
}

bool SClient_Base::Init()
{
	myGraphicsWorld = new Shift::SG_World();
	myMainView = myGraphicsWorld->CreateView();

	if (myGraphicsWorld->Init() == false)
		return false;

	myMainCamera = new Shift::SG_Camera();
	const Shift::SC_Vector2f resolution = Shift::SC_EngineInterface::GetResolution();
	myMainCamera->InitAsPerspective(resolution, 75.0f, 0.01f, 5000.f);
	myMainCamera->SetPosition(Shift::SC_Vector3f(0.0f, 0.0f, 0.0f));

	myTestObj = new SClient_GameObject();
	myTestObj->TEMP_LoadFromFile("SponzaPBR/sponzaPBR.obj");
	myTestObj->SetScale({ 0.01f, 0.01f, 0.01f });

	Shift::SG_ModelLoader loader;
	loader.Import2("Assets/Imported/Models/sponzaPBR.smf");
	loader.Import2("Assets/Imported/Models/sponzaPBR.smf");
	
	//_rigidbdy = Shift::CPhysicsDevice::GetInstance()->CreateStaticRigidBody();

	return true;
}

void SClient_Base::Update()
{
	SC_CPU_PROFILER_FUNCTION();
	const float aDeltaTime = Shift::SC_Timer::GetDeltaTime();
	//myTestObj->SetPosition(_rigidbdy->GetPosition());


	Shift::SC_Matrix44 camTransform = myMainCamera->GetTransform();

	bool changedRotation = false;
	float speed = 4.0f;
	if (GetKeyState(0x10) & 0x8000)
	{
		speed *= 3.0f;
	}

	if (GetKeyState('Q') & 0x8000)
	{
		camTransform.RotateAroundY(-aDeltaTime * 2.f);
		changedRotation = true;
	}
	if (GetKeyState('E') & 0x8000)
	{
		camTransform.RotateAroundY(aDeltaTime * 2.f);
		changedRotation = true;
	}
	if (GetKeyState('R') & 0x8000)
	{
		camTransform.RotateAroundX(-aDeltaTime * 2.f);
		changedRotation = true;
	}
	if (GetKeyState('F') & 0x8000)
	{
		camTransform.RotateAroundX(aDeltaTime * 2.f);
		changedRotation = true;
	}
	if (changedRotation)
	{
		myMainCamera->SetTransform(camTransform);
	}

	if (GetKeyState(0x11) & 0x8000) // Down
	{
		myMainCamera->Move(myMainCamera->GetUp(), -aDeltaTime * speed);
	}
	if (GetKeyState(0x20) & 0x8000) // Up
	{
		myMainCamera->Move(myMainCamera->GetUp(), aDeltaTime * speed);
	}

	if (GetKeyState('W') & 0x8000)
	{
		myMainCamera->Move(myMainCamera->GetForward(), aDeltaTime * speed);
	}
	if (GetKeyState('A') & 0x8000)
	{
		myMainCamera->Move(myMainCamera->GetRight(), -aDeltaTime * speed);
	}
	if (GetKeyState('S') & 0x8000)
	{
		myMainCamera->Move(myMainCamera->GetForward(), -aDeltaTime * speed);
	}
	if (GetKeyState('D') & 0x8000)
	{
		myMainCamera->Move(myMainCamera->GetRight(), aDeltaTime * speed);
	}

	myGraphicsWorld->Update();
	myTestObj->Interact();
	myTestObj->Render();
	myMainView->SetCamera(*myMainCamera);
}

void SClient_Base::Render()
{
	// TODO: Redo this, should not be dependent on ImGui.
	// Render straight into backbuffer.
	Shift::SC_Vector2f resolution = Shift::SC_EngineInterface::GetResolution();
	bool open = true;
	ImGuiViewport* viewport = ImGui::GetMainViewport();
	ImGui::SetNextWindowPos(viewport->Pos);
	ImGui::SetNextWindowSize(viewport->Size);
	ImGui::SetNextWindowViewport(viewport->ID);
	ImGui::Begin("GameClient", &open, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove);
	ImGui::Image(GetView()->GetLastFinishedFrame(), resolution);
	ImGui::End(); // Viewport
}

Shift::SG_View* SClient_Base::GetView()
{
	return myMainView;
}

Shift::SG_Camera* SClient_Base::GetCamera()
{
	return myMainCamera;
}
