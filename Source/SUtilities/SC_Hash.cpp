#include "SC_Hash.h"
namespace Shift
{
	uint Shift::SC_Hash(const void* aValue, uint aSize)
	{
		const unsigned char* data = reinterpret_cast<const unsigned char*>(aValue);

		if (!data || aSize == 0)
			return 0;

		uint hash = 0x811c9dc5;
		uint prime = 0x1000193;

		for (uint i = 0; i < aSize; ++i) 
		{
			unsigned char value = data[i];
			hash = hash ^ value;
			hash *= prime;
		}
		return hash;
	}
}