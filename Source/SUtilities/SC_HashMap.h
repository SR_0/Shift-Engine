#pragma once
namespace Shift
{

	template<class Key, class Value>
	class SC_HashMap
	{
	public:

		template<class Key, class Value>
		struct Node
		{
			Node(const Key& key, const Value& value) :
				myKey(key), myValue(value), myNext(nullptr) {}

			const Key& GetKey() const { return myKey; }
			Key& GetKey() { return myKey; }

			const Value& GetValue() const { return myValue; }
			Value& GetValue() { return myValue; }
			
			void SetKey(const Key& aKey) { myKey = aKey; }
			void SetValue(const Value& aValue) { myValue = aValue; }

			Node* GetNext() const { return myNext; }
			void SetNext(Node* aNext) { myNext = aNext; }
		private:
			Value myValue;
			Key myKey;
			Node* myNext;
		};

	private:
		Node* myStart;
	};

}