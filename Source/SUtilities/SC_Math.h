#pragma once
#include <math.h>
#include <float.h>
#include "SC_Types.h"

#define SC_Truncf(aFloat) ((float)((int)(aFloat)))
#define SC_Modf_Macro(aX, aY) ((aX) - SC_Truncf((aX) * (1.0f / (aY))) * (aY))

#define SC_Pi (3.1415926535897932384626433832795)

namespace Shift
{

	static constexpr float SC_PI_FLOAT = float(SC_Pi);
	static constexpr double SC_PI_DOUBLE = double(SC_Pi);

	float SC_DegreesToRadians(const float aDegree);
	float SC_RadiansToDegrees(const float aRadian);

	float SC_Cos(float aValue);
	float SC_Sin(float aValue);
	void SC_SinCos(float aValue, float& aOutSin, float& aOutCos);

	bool SC_IsPowerOfTwo(const unsigned int aValue);
	bool SC_IsPowerOfTwo(const float aValue);

	float SC_Ceil(float aFloat);
	float SC_Floor(float aFloat);

	inline float SC_InvSqrt(const float aValue)
	{
		if (aValue == 0.0f)
			return 0.0f;

		return 1.0f / sqrtf(aValue);
	}

	enum SC_NoInit
	{
		SC_NO_INIT
	};

}