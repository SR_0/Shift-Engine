#pragma once
#include "SC_Vector.h"

namespace Shift
{
	class SG_Frustum;
	class SC_Sphere
	{
		SC_Sphere();
		~SC_Sphere();

		bool IsInside(const SC_Vector3f& aPoint);

		bool Intersects(const SG_Frustum& aFrustum);
		void Merge(const SC_Sphere& aSphere);

		SC_Vector3f myCenter;
		float myRadius;
	};
}