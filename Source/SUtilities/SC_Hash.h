#pragma once

using uint = unsigned int;
namespace Shift
{
	uint SC_Hash(const void* aValue, uint aSize);
	template<class Type>
	uint SC_Hash(const Type& aValue)
	{
		const uint size = sizeof(Type);
		return SC_Hash(&aValue, size);
	}

}