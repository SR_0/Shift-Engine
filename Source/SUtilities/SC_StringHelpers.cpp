#include "SC_StringHelpers.h"

#pragma warning(push)
#pragma warning(disable : 4505) 
#pragma warning(disable : 4244) 

std::wstring Shift::ToWString(const char* aString)
{
	std::string str(aString);
	std::wstring wstr(str.begin(), str.end());
	return wstr;
}
std::wstring Shift::ToWString(const std::string& aString)
{
	std::wstring wstr(aString.begin(), aString.end());
	return wstr;
}

std::string Shift::ToString(const wchar_t* aWString)
{
	std::wstring wstr(aWString);
	std::string str(wstr.begin(), wstr.end());
	return str;
}
std::string Shift::ToString(const std::wstring& aWString)
{
	std::string str(aWString.begin(), aWString.end());
	return str;
}

std::string Shift::GetFileExtension(const std::string & aPath)
{
	size_t idx = aPath.rfind('.');
	return std::string(aPath.begin() + (++idx), aPath.end());
}
std::string Shift::GetFileWithExtension(const std::string& aPath)
{
	size_t idxNameStart = aPath.rfind('\\');
	if (idxNameStart == std::string::npos)
		idxNameStart = aPath.rfind('/');

	if (idxNameStart == std::string::npos)
		return std::string(aPath.begin(), aPath.end());
	else
		return std::string(aPath.begin() + (idxNameStart+1), aPath.end());
}
std::string Shift::GetFilename(const std::string& aPath)
{
	size_t idxExt = aPath.rfind('.');
	size_t idxNameStart = aPath.rfind('\\');
	if (idxNameStart == std::string::npos)
		idxNameStart = aPath.rfind('/');

	return std::string(aPath.begin() + (++idxNameStart), aPath.begin() + (idxExt));
}
#pragma warning(pop)