#include "SC_Sphere.h"

namespace Shift
{
	SC_Sphere::SC_Sphere()
		: myRadius(0.f)
	{
	}

	SC_Sphere::~SC_Sphere()
	{
	}

	bool SC_Sphere::IsInside(const SC_Vector3f& aPoint)
	{
		if ((myCenter - aPoint).Length2() < (myRadius * myRadius))
			return true;

		return false;
	}

	bool SC_Sphere::Intersects(const SG_Frustum& aFrustum)
	{
		aFrustum;
		return false;
	}

	void SC_Sphere::Merge(const SC_Sphere& aSphere)
	{
		if (!aSphere.myRadius)
			return;

		if (!myRadius)
		{
			*this = aSphere;
			return;
		}

		SC_Vector3f centerDiff = aSphere.myCenter - myCenter;
		float centerDiffLen = centerDiff.Length();
		float objectRadius = aSphere.myRadius;

		if ((centerDiffLen + myRadius) <= objectRadius)
		{
			*this = aSphere;
			return;
		}

		float sum = centerDiffLen + objectRadius;
		if (sum <= myRadius)
			return;

		myCenter += centerDiff * ((sum - myRadius) / (2.f * centerDiffLen));
		myRadius = (sum + myRadius) * 0.5f;
	}
}