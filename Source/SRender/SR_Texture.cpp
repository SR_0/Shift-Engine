#include "SRender_Precompiled.h"
#include "SR_Texture.h"

namespace Shift
{
	SR_Texture::SR_Texture()
		: myBuffer(nullptr)
	{
	}


	SR_Texture::~SR_Texture()
	{
		delete myBuffer; // TODO: [Smart Pointer]
	}
}