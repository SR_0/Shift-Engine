#include "SRender_Precompiled.h"
#if ENABLE_DX12

#include "SR_GraphicsContext_DX12.h"
#include "SR_GraphicsDevice_DX12.h"
#include "SR_Buffer_DX12.h"
#include "SR_Texture_DX12.h"
#include "SR_TextureBuffer_DX12.h"
#include "SR_RenderTarget_DX12.h"
#include "SR_CommandList_DX12.h"
#include "SR_ShaderState_DX12.h"
#include "SR_RootSignature_DX12.h"
#include "d3dx12.h"

#define ENABLE_PIX 1
#if ENABLE_PIX

#	define USE_PIX
#	include "pix3.h"
#	pragma comment(lib,"WinPixEventRuntime.lib")

inline void PIXSetMarkerOnContext(_In_ ID3D12GraphicsCommandList* commandList, _In_reads_bytes_(size) void* data, UINT size)
{
	commandList->SetMarker(D3D12_EVENT_METADATA, data, size);
}

inline void PIXSetMarkerOnContext(_In_ ID3D12CommandQueue* commandQueue, _In_reads_bytes_(size) void* data, UINT size)
{
	commandQueue->SetMarker(D3D12_EVENT_METADATA, data, size);
}

inline void PIXBeginEventOnContext(_In_ ID3D12GraphicsCommandList* commandList, _In_reads_bytes_(size) void* data, UINT size)
{
	commandList->BeginEvent(D3D12_EVENT_METADATA, data, size);
}

inline void PIXBeginEventOnContext(_In_ ID3D12CommandQueue* commandQueue, _In_reads_bytes_(size) void* data, UINT size)
{
	commandQueue->BeginEvent(D3D12_EVENT_METADATA, data, size);
}
inline void PIXEndEventOnContext(_In_ ID3D12GraphicsCommandList* commandList)
{
	commandList->EndEvent();
}

inline void PIXEndEventOnContext(_In_ ID3D12CommandQueue* commandQueue)
{
	commandQueue->EndEvent();
}
#endif

namespace Shift
{

	static const float globalClearColor[] = { 0.0f, 1.f, 0.0f, 1.0f };
	static constexpr uint globalHeapOffsetCBV = 0;
	static constexpr uint globalHeapOffsetSRV = GPU_RESOURCE_HEAP_CBV_COUNT;
	static constexpr uint globalHeapOffsetUAV = GPU_RESOURCE_HEAP_CBV_COUNT + GPU_RESOURCE_HEAP_SRV_COUNT;

	SR_GraphicsContext_DX12::SR_GraphicsContext_DX12(const SR_QueueType& aType, SR_GraphicsDevice_DX12* aDevice)
		: SR_GraphicsContext(aType)
		, myDevice(aDevice)
	{

		HRESULT hr = S_OK;
		D3D12_COMMAND_LIST_TYPE type;
		std::wstring name;
		switch (aType)
		{
		default:
		case SR_QueueType_Render:
			name = (L"Graphics Context");
			type = D3D12_COMMAND_LIST_TYPE_DIRECT;
			break;
		case SR_QueueType_Compute:
			name = (L"Compute Context");
			type = D3D12_COMMAND_LIST_TYPE_COMPUTE;
			break;
		case SR_QueueType_Copy:
			name = (L"Copy Context");
			type = D3D12_COMMAND_LIST_TYPE_COPY;
			break;

		}
		ID3D12Device* device = myDevice->GetNativeDevice();

		myResourceDescriptorIncrementSize = myDevice->GetNativeDevice()->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

		D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
		rtvHeapDesc.NumDescriptors = 8;
		rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
		rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		hr = device->CreateDescriptorHeap(&rtvHeapDesc, SR_IID_PPV_ARGS(&myRenderTargetHeap));
		assert(hr == S_OK && "Couldn't create render target heap.");
		myRenderTargetHeap->SetName(L"Context RTV-Heap");

		myRenderTargetDescriptorIncrementSize = myDevice->GetNativeDevice()->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
		D3D12_CPU_DESCRIPTOR_HANDLE rtvHandleCPU(myRenderTargetHeap->GetCPUDescriptorHandleForHeapStart());
		D3D12_GPU_DESCRIPTOR_HANDLE rtvHandleGPU(myRenderTargetHeap->GetGPUDescriptorHandleForHeapStart());
		myRenderTargetHeapHandles.myCPUAddress = rtvHandleCPU.ptr;
		myRenderTargetHeapHandles.myGPUAddress = rtvHandleGPU.ptr;

		D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc = {};
		dsvHeapDesc.NumDescriptors = 1;
		dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
		dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		hr = device->CreateDescriptorHeap(&dsvHeapDesc, SR_IID_PPV_ARGS(&myDepthBufferHeap));
		assert(hr == S_OK && "Couldn't create depth buffer heap.");
		myRenderTargetHeap->SetName(L"Context DSV-Heap");

		myDepthBufferDescriptorIncrementSize = myDevice->GetNativeDevice()->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
		D3D12_CPU_DESCRIPTOR_HANDLE dsvHandleCPU(myDepthBufferHeap->GetCPUDescriptorHandleForHeapStart());
		D3D12_GPU_DESCRIPTOR_HANDLE dsvHandleGPU(myDepthBufferHeap->GetGPUDescriptorHandleForHeapStart());
		myDepthBufferHeapHandles.myCPUAddress = dsvHandleCPU.ptr;
		myDepthBufferHeapHandles.myGPUAddress = dsvHandleGPU.ptr;

		myUploadHeap = new CDynamicUploadHeap_DX12(true, myDevice->GetNativeDevice(), 4*MB);
	}
	SR_GraphicsContext_DX12::~SR_GraphicsContext_DX12()
	{
		delete myUploadHeap;
	}
	void SR_GraphicsContext_DX12::Begin()
	{
		SC_Ref<SR_CommandList> cmdList;
		if (!SR_GraphicsQueueManager::GetCommandList(cmdList, myContextType))
		{
			assert(false);
		}
		myCommandList = cmdList;
		myNativeCommandList = myCommandList->GetCommandList();
#if ENABLE_DX12_19H2
		myNativeCommandList4 = myCommandList->GetCommandList4();
#endif
		myNumCommandsSinceReset = 0;
	}
	void SR_GraphicsContext_DX12::End()
	{
		myNativeCommandList = nullptr;
#if ENABLE_DX12_19H2
		myNativeCommandList4 = nullptr;
#endif
		myDevice->InactivateContext(this);
		SetCurrentContext(nullptr);
		myCurrentTopology = SR_Topology_Unknown;
		myCurrentBlendFactor = SC_Vector4f(0);
		myCurrentBlendState = nullptr;
		myCurrentDepthState = nullptr;
		myCurrentRasterizerState = nullptr;
		myCurrentRenderTargetFormats = nullptr;
		myCurrentRootSignature = nullptr;
		myCurrentStencilRef = 0;
		myResourceHeap = nullptr;
	}
	void SR_GraphicsContext_DX12::BeginRecording()
	{
		if (GetType() != SR_QueueType_Copy)
		{
			myNativeCommandList->SetGraphicsRootSignature(myDevice->GetGraphicsRootSignature());
			myNativeCommandList->SetComputeRootSignature(myDevice->GetComputeRootSignature());
			BindResourceTables();
		}
	}
	void SR_GraphicsContext_DX12::EndRecording()
	{
		myCommandList->myNumCommands += myNumCommandsSinceReset;

		for (auto& transition : myIntermediateTransitions)
		{
			myCommandList->myPendingTransitions[transition.first] = SC_Move(transition.second);
		}
		myIntermediateTransitions.clear();

		myCommandList->FinishRecording();
		myLastBoundPSO = nullptr;
	}
	bool SR_GraphicsContext_DX12::IsReady()
	{
		return true;
	}
	void SR_GraphicsContext_DX12::BeginEvent(const char* aID)
	{
#if ENABLE_PIX
		if (myUseDebugMarkers)
			PIXBeginEvent(myNativeCommandList, PIX_COLOR_DEFAULT, aID);
#else
		SC_UNUSED(aID);
#endif
	}
	void SR_GraphicsContext_DX12::EndEvent()
	{
#if ENABLE_PIX
		if (myUseDebugMarkers)
			PIXEndEvent(myNativeCommandList);
#endif
	}
	void SR_GraphicsContext_DX12::InsertFence(uint64 aFence)
	{
		myCommandList->SetLastFence(aFence, myContextType);

		if (GetType() != SR_QueueType_Copy && (myLastFence != aFence))
			myUploadHeap->FinishFrame(aFence, SR_GraphicsQueueManager::GetLastKnownCompletedFence(myContextType));

		myLastFence = aFence;
	}

	void SR_GraphicsContext_DX12::InsertWait(SR_Waitable* aEvent)
	{
		if (!aEvent)
			return;

		aEvent->myEventCPU.Wait();
	}

	void SR_GraphicsContext_DX12::Draw(const unsigned int aVertexCount, const unsigned int aStartVertexLocation)
	{
		if (!myLastBoundPSO)
			return;

		PreDraw();

		SetResources();
		myNativeCommandList->DrawInstanced(aVertexCount, 1, aStartVertexLocation, 0);
		RequestNewDescriptorRange();
		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}
	void SR_GraphicsContext_DX12::DrawIndexed(const unsigned int aIndexCount, const unsigned int aStartIndexLocation, const unsigned int aBaseVertexLocation)
	{
		if (!myLastBoundPSO)
			return;

		PreDraw();

		SetResources();
		myNativeCommandList->DrawIndexedInstanced(aIndexCount, 1, aStartIndexLocation, aBaseVertexLocation, 0); 
		IncrementTrianglesDrawn(aIndexCount / 3);
		RequestNewDescriptorRange();
		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}
	void SR_GraphicsContext_DX12::DrawInstanced(const unsigned int aVertexCount, const unsigned int aInstanceCount, const unsigned int aStartVertexLocation, const unsigned int aStartInstanceLocation)
	{
		if (!myLastBoundPSO)
			return;

		PreDraw();

		SetResources();
		myNativeCommandList->DrawInstanced(aVertexCount, aInstanceCount, aStartVertexLocation, aStartInstanceLocation);
		RequestNewDescriptorRange();
		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}
	void SR_GraphicsContext_DX12::DrawIndexedInstanced(const unsigned int aIndexCount, const unsigned int aInstanceCount, const unsigned int aStartIndexLocation, const unsigned int aBaseVertexLocation, const unsigned int aStartInstanceLocation)
	{
		//if (!myLastBoundPSO)
		//	return;

		PreDraw();

		SetResources();
		myNativeCommandList->DrawIndexedInstanced(aIndexCount, aInstanceCount, aStartIndexLocation, aBaseVertexLocation, aStartInstanceLocation);
		IncrementTrianglesDrawn(aIndexCount / 3);
		RequestNewDescriptorRange();
		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}
	void SR_GraphicsContext_DX12::Dispatch(const unsigned int aThreadGroupCountX, const unsigned int aThreadGroupCountY, const unsigned int aThreadGroupCountZ)
	{
		PreDispatch();

		SetResources();
		myNativeCommandList->Dispatch(aThreadGroupCountX, aThreadGroupCountY, aThreadGroupCountZ);
		RequestNewDescriptorRange();
		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}
	void SR_GraphicsContext_DX12::DispatchRays(const SDispatchRaysDesc& aDispatchDesc)
	{
#if ENABLE_DX12_19H2
		D3D12_DISPATCH_RAYS_DESC desc;

		desc.Width = aDispatchDesc.myX;
		desc.Height = aDispatchDesc.myY;
		desc.Depth = aDispatchDesc.myZ;

		SetResources();
		myNativeCommandList4->DispatchRays(&desc);
		SetNeedsFlush();
		++myNumCommandsSinceReset;
#endif
	}
	void SR_GraphicsContext_DX12::ClearRenderTarget(uint aRenderTargetSlot, SC_Float4 aClearColor, bool aClearDepth)
	{
		assert(aRenderTargetSlot <= MAX_NUM_RENDERTARGETS && "Max 8 render-target can be bound at the time.");

		SR_DescriptorCPU_DX12 handle;
		handle.ptr = myRenderTargetHeapHandles.myCPUAddress;
		SR_DescriptorCPU_DX12 rtvHandle = handle;
		rtvHandle.ptr += aRenderTargetSlot * myRenderTargetDescriptorIncrementSize;

		const float clearColor[] = { aClearColor.x, aClearColor.y, aClearColor.z, aClearColor.w };
		myNativeCommandList->ClearRenderTargetView(rtvHandle, clearColor, 0, nullptr);

		if (aClearDepth)
			ClearDepthTarget(false);

		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}
	void SR_GraphicsContext_DX12::ClearRenderTargets(uint aNumActiveTargets, SC_Float4 aClearColor, bool aClearDepth)
	{
		SC_Float4 clearColors[8];
		clearColors[0] = aClearColor;
		clearColors[1] = aClearColor;
		clearColors[2] = aClearColor;
		clearColors[3] = aClearColor;
		clearColors[4] = aClearColor;
		clearColors[5] = aClearColor;
		clearColors[6] = aClearColor;
		clearColors[7] = aClearColor;
		ClearRenderTargets(aNumActiveTargets, clearColors, aClearDepth);
		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}
	void SR_GraphicsContext_DX12::ClearRenderTargets(uint aNumActiveTargets, SC_Float4 aClearColors[MAX_NUM_RENDERTARGETS], bool aClearDepth)
	{
		assert(aNumActiveTargets <= MAX_NUM_RENDERTARGETS && "Max 8 render-target can be bound at the time.");

		for (uint i = 0; i < aNumActiveTargets; ++i)
		{
			if (aClearDepth && i == 0)
				ClearRenderTarget(i, aClearColors[i], true);
			else
				ClearRenderTarget(i, aClearColors[i], false);
		}

		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}
	void SR_GraphicsContext_DX12::ClearDepthTarget(bool aReverseDepth, bool aClearStencil)
	{
		D3D12_CPU_DESCRIPTOR_HANDLE handle;
		handle.ptr = myDepthBufferHeapHandles.myCPUAddress;

		float clearVal = 1.f;
		if (aReverseDepth)
			clearVal = 0.0f;

		D3D12_CLEAR_FLAGS clearFlag = D3D12_CLEAR_FLAG_DEPTH;
		if (aClearStencil)
			clearFlag |= D3D12_CLEAR_FLAG_STENCIL;

		myNativeCommandList->ClearDepthStencilView(handle, clearFlag, clearVal, 0, 0, nullptr);
		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}

	void SR_GraphicsContext_DX12::ClearRenderTarget(SR_RenderTarget* aTarget, const SC_Vector4f& aClearColor)
	{
		const SR_RenderTarget_DX12* target = static_cast<const SR_RenderTarget_DX12*>(aTarget);

		myNativeCommandList->ClearRenderTargetView(target->myRenderTargetView, &aClearColor.x, 0, nullptr);
		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}

	void SR_GraphicsContext_DX12::ClearDepthTarget(SR_RenderTarget* aTarget, float aDepthValue, uint8 aStencilValue, uint aClearFlags)
	{
		SC_UNUSED(aClearFlags);
		D3D12_CLEAR_FLAGS clearFlag = D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL;

		const SR_RenderTarget_DX12* target = static_cast<const SR_RenderTarget_DX12*>(aTarget);

		myNativeCommandList->ClearDepthStencilView(target->myDepthStencilView, clearFlag, aDepthValue, aStencilValue, 0, nullptr);
		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}

	void SR_GraphicsContext_DX12::BindGraphicsPSO(const CGraphicsPSO& aPSO)
	{
		myNativeCommandList->SetPipelineState((ID3D12PipelineState*)aPSO.myPipelineState);
		myLastBoundPSO = &aPSO;
	}
	void SR_GraphicsContext_DX12::BindComputePSO(const CComputePSO& aPSO)
	{
		myNativeCommandList->SetPipelineState((ID3D12PipelineState*)aPSO.myPipelineState);
	}
	void SR_GraphicsContext_DX12::BindConstantBufferRef(const CConstantBuffer& /*aConstantBuffer*/, const uint /*aBindingIndex*/)
	{
	}
	void SR_GraphicsContext_DX12::BindConstantBuffer(void* aData, uint aSize, const uint aBindingIndex)
	{
		if (!aData)
			return;

		DynamicAllocation tempBuffer = myUploadHeap->Allocate(aSize);

		D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc = {};
		cbvDesc.BufferLocation = tempBuffer.myGPUAddress;
		cbvDesc.SizeInBytes = (tempBuffer.mySize + 255) & ~255;    // CB size is required to be 256-byte aligned.

		SR_DescriptorCPU_DX12 handle;
		handle.ptr = myResourceHeapHandles.myCPUAddress;
		handle.ptr += (aBindingIndex + globalHeapOffsetCBV) * myResourceDescriptorIncrementSize;
		myDevice->GetNativeDevice()->CreateConstantBufferView(&cbvDesc, handle);

		SC_Memcpy(tempBuffer.myCPUAddress, aData, aSize);
	}
	void SR_GraphicsContext_DX12::BindBuffer(SR_BufferView* /*aBufferView*/, const uint /*aBindingIndex*/)
	{
	}
	void SR_GraphicsContext_DX12::BindBufferRW(SR_BufferView* /*aBufferView*/, const uint /*aBindingIndex*/)
	{
	}
	
	void SR_GraphicsContext_DX12::BindTexture(SR_Texture* aTexture, const uint aBindingIndex)
	{
		SR_DescriptorCPU_DX12 cpuHandle;
		cpuHandle.ptr = myResourceHeapHandles.myCPUAddress;
		cpuHandle = cpuHandle + uint64((aBindingIndex + globalHeapOffsetSRV) * myResourceDescriptorIncrementSize);

		SR_Texture_DX12* tex = static_cast<SR_Texture_DX12*>(aTexture);

		ID3D12Device* device = myDevice->GetNativeDevice();
		device->CopyDescriptorsSimple(1, cpuHandle, tex->myDescriptorCPU, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

		//SR_DescriptorCPU_DX12 stagedHandle = myDevice->GetStagedHandle();
		//stagedHandle = stagedHandle + uint64((aBindingIndex + globalHeapOffsetSRV) * myResourceDescriptorIncrementSize);
		//device->CopyDescriptorsSimple(1, stagedHandle, tex->myDescriptorCPU, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	}
	void SR_GraphicsContext_DX12::BindTexture(SR_Texture* aTexture, const uint aBindingIndex, const uint aHeapOffset)
	{
		SR_DescriptorCPU_DX12 cpuHandle;
		cpuHandle.ptr = myResourceHeapHandles.myCPUAddress;
		cpuHandle = cpuHandle + uint64((aBindingIndex + aHeapOffset) * myResourceDescriptorIncrementSize);

		SR_Texture_DX12* tex = static_cast<SR_Texture_DX12*>(aTexture);

		ID3D12Device* device = myDevice->GetNativeDevice();
		device->CopyDescriptorsSimple(1, cpuHandle, tex->myDescriptorCPU, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

		//SR_DescriptorCPU_DX12 stagedHandle = myDevice->GetStagedHandle();
		//stagedHandle = stagedHandle + uint64((aBindingIndex + globalHeapOffsetSRV) * myResourceDescriptorIncrementSize);
		//device->CopyDescriptorsSimple(1, stagedHandle, tex->myDescriptorCPU, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	}
	
	void SR_GraphicsContext_DX12::BindTextureRW(SR_Texture* aTexture, const uint aBindingIndex)
	{
		SR_DescriptorCPU_DX12 cpuHandle;
		cpuHandle.ptr = myResourceHeapHandles.myCPUAddress;
		cpuHandle = cpuHandle + uint64((aBindingIndex + globalHeapOffsetUAV) * myResourceDescriptorIncrementSize);

		SR_Texture_DX12* tex = static_cast<SR_Texture_DX12*>(aTexture);

		ID3D12Device* device = myDevice->GetNativeDevice();
		device->CopyDescriptorsSimple(1, cpuHandle, tex->myDescriptorCPU, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

		//SR_DescriptorCPU_DX12 stagedHandle = myDevice->GetStagedHandle();
		//stagedHandle = stagedHandle + uint64((aBindingIndex + globalHeapOffsetSRV) * myResourceDescriptorIncrementSize);
		//device->CopyDescriptorsSimple(1, stagedHandle, tex->myDescriptorCPU, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	}

	void SR_GraphicsContext_DX12::BindAccelerationStructure(const SR_AccelerationStructure& /*aAccStruct*/, const uint aBindingIndex)
	{
		D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};

		srvDesc.Format = DXGI_FORMAT_UNKNOWN;
		srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		srvDesc.ViewDimension = D3D12_SRV_DIMENSION_RAYTRACING_ACCELERATION_STRUCTURE;
		//srvDesc.RaytracingAccelerationStructure.Location = m_topLevelASBuffers.pResult->GetGPUVirtualAddress();

		SR_DescriptorCPU_DX12 cpuHandle;
		cpuHandle.ptr = myResourceHeapHandles.myCPUAddress;
		cpuHandle.ptr += (aBindingIndex + globalHeapOffsetSRV) * myResourceDescriptorIncrementSize;
		myDevice->GetNativeDevice()->CreateShaderResourceView(nullptr, &srvDesc, cpuHandle);
	}

	void SR_GraphicsContext_DX12::Transition(const SR_ResourceState& aTransition, SR_TrackedResource* aResource)
	{
		SC_Pair pair(aTransition, aResource);
		Transition(&pair, 1);
	}

	void SR_GraphicsContext_DX12::Transition(const SC_Pair<SR_ResourceState, SR_TrackedResource*>* aPairs, uint aCount)
	{
		SC_GrowingArray<D3D12_RESOURCE_BARRIER> barriers;
		barriers.PreAllocate(aCount);
		for (uint i = 0; i < aCount; ++i)
		{
			const SR_ResourceState& transitionState = aPairs[i].myFirst;
			SR_TrackedResource* trackedResource = aPairs[i].mySecond;
			auto iter = myIntermediateTransitions.find(trackedResource);
			if (iter != myIntermediateTransitions.end())
			{
				if (iter->second.mySecond == transitionState)
					continue;

				D3D12_RESOURCE_BARRIER& barrier = barriers.Add();
				barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
				barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	
				barrier.Transition.pResource = trackedResource->myDX12Resource;
				barrier.Transition.StateBefore = static_cast<D3D12_RESOURCE_STATES>(iter->second.mySecond);
				barrier.Transition.StateAfter = static_cast<D3D12_RESOURCE_STATES>(transitionState);
				barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;

				myIntermediateTransitions[trackedResource].mySecond = transitionState;
			}
			else
			{
				myIntermediateTransitions[trackedResource].myFirst = transitionState;
				myIntermediateTransitions[trackedResource].mySecond = transitionState;
			}
		}

		if (barriers.Count())
		{
			myNativeCommandList->ResourceBarrier(barriers.Count(), barriers.GetBuffer());
			++myNumCommandsSinceReset;
		}
	}

	void SR_GraphicsContext_DX12::SetBlendState(SR_BlendState* aBlendState, const SC_Vector4f& aBlendFactor)
	{
		if (myCurrentBlendState->myKey != aBlendState->myKey)
		{
			myCurrentBlendState = aBlendState;
			myCurrentPSOKey.myBlendStateKey = myCurrentBlendState->myKey;
		}

		if (myCurrentBlendFactor != aBlendFactor)
		{
			float blendFactor[] = {
				aBlendFactor.x,
				aBlendFactor.y,
				aBlendFactor.z,
				aBlendFactor.w
			};

			myNativeCommandList->OMSetBlendFactor(blendFactor);
			myCurrentBlendFactor = aBlendFactor;
		}
	}

	void SR_GraphicsContext_DX12::SetDepthState(SR_DepthState* aDepthState, uint aStencilRef)
	{
		if (myCurrentDepthState->myKey != aDepthState->myKey)
		{
			myCurrentDepthState = aDepthState;
			myCurrentPSOKey.myDepthStateKey = myCurrentDepthState->myKey;
		}

		if (myCurrentStencilRef != aStencilRef)
		{
			myNativeCommandList->OMSetStencilRef(aStencilRef);
			myCurrentStencilRef = aStencilRef;
		}
	}

	void SR_GraphicsContext_DX12::SetRasterizerState(SR_RasterizerState* aRasterizerState)
	{
		if (myCurrentRasterizerState->myKey != aRasterizerState->myKey)
		{
			myCurrentRasterizerState = aRasterizerState;
			myCurrentPSOKey.myRasterStateKey = myCurrentRasterizerState->myKey;
		}
	}

	void SR_GraphicsContext_DX12::SetViewport(const SViewport& aViewport)
	{
		const SViewport nullViewport;
		if (aViewport == nullViewport)
		{
			myNativeCommandList->RSSetViewports(0, nullptr);
			return;
		}
		D3D12_VIEWPORT viewport;
		viewport.TopLeftX = aViewport.topLeftX;
		viewport.TopLeftY = aViewport.topLeftY;
		viewport.Width = aViewport.width;
		viewport.Height = aViewport.height;
		viewport.MinDepth = aViewport.minDepth;
		viewport.MaxDepth = aViewport.maxDepth;
		myNativeCommandList->RSSetViewports(1, &viewport);
	}
	void SR_GraphicsContext_DX12::SetScissorRect(const SScissorRect& aScissorRect)
	{
		D3D12_RECT scissor;
		scissor.top = aScissorRect.top;
		scissor.left = aScissorRect.left;
		scissor.right = aScissorRect.right;
		scissor.bottom = aScissorRect.bottom;
		myNativeCommandList->RSSetScissorRects(1, &scissor);
	}
	void SR_GraphicsContext_DX12::SetTopology(const SR_Topology& aTopology)
	{
		if (myCurrentTopology == aTopology)
			return;

		myCurrentPSOKey.myTopologyKey = SC_Hash(&aTopology, sizeof(aTopology));

		D3D_PRIMITIVE_TOPOLOGY topology;
		switch (aTopology)
		{
		default:
		case SR_Topology_TriangleList:
			topology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
			break;
		case SR_Topology_LineList:
			topology = D3D_PRIMITIVE_TOPOLOGY_LINELIST;
			break;
		case SR_Topology_PointList:
			topology = D3D_PRIMITIVE_TOPOLOGY_POINTLIST;
			break;
		}

		myNativeCommandList->IASetPrimitiveTopology(topology);
		myCurrentTopology = aTopology;
	}
	void SR_GraphicsContext_DX12::SetVertexBuffer(uint aStartVertex, SR_Buffer* aBuffer)
	{
		SetVertexBuffer(aStartVertex, &aBuffer, 1);
	}
	void SR_GraphicsContext_DX12::SetVertexBuffer(uint aStartVertex, SR_Buffer** aBuffer, uint aCount)
	{
		SC_GrowingArray<D3D12_VERTEX_BUFFER_VIEW> views;
		views.PreAllocate(aCount);

		SC_GrowingArray<SC_Pair<SR_ResourceState, SR_TrackedResource*>> transitions;
		transitions.PreAllocate(aCount);

		for (uint i = 0; i < aCount; ++i)
		{
			SR_Buffer_DX12* buffer = static_cast<SR_Buffer_DX12*>(aBuffer[i]);
			const SR_BufferDesc& bufferDesc = buffer->GetProperties();

			if ((bufferDesc.myBindFlag & SBindFlag_VertexBuffer) == 0)
			{
				SC_ERROR_LOG("Tried to bind non-vertex buffer as vertex buffer (buffer: %p)", buffer);
				continue;
			}

			transitions.Add(SC_Pair(SR_ResourceState_VertexConstantBuffer, buffer));

			D3D12_VERTEX_BUFFER_VIEW& vbView = views.Add();
			vbView.BufferLocation = buffer->myDX12Resource->GetGPUVirtualAddress();
			vbView.SizeInBytes = buffer->GetProperties().mySize;
			vbView.StrideInBytes = buffer->GetProperties().myStructSize;
		}
		Transition(transitions.GetBuffer(), transitions.Count());
		myNativeCommandList->IASetVertexBuffers(aStartVertex, views.Count(), views.GetBuffer());
	}
	void SR_GraphicsContext_DX12::SetVertexBuffer(const unsigned int aStartVertexIndex, CVertexBuffer& aVertexBuffer)
	{
		SR_Buffer_DX12* buffer = static_cast<SR_Buffer_DX12*>(aVertexBuffer.myBuffer.Get());
			
		Transition(SR_ResourceState_VertexConstantBuffer, buffer);
		myNativeCommandList->IASetVertexBuffers(aStartVertexIndex, 1, (D3D12_VERTEX_BUFFER_VIEW*)(aVertexBuffer.myVertexBufferView));
	}

	void SR_GraphicsContext_DX12::SetVertexBuffer(const unsigned int aStartVertexIndex, SC_GrowingArray<CVertexBuffer>& aVertexBuffers)
	{
		SC_GrowingArray<D3D12_VERTEX_BUFFER_VIEW> views;
		SC_GrowingArray<SC_Pair<SR_ResourceState, SR_TrackedResource*>> transitions;
		views.PreAllocate(aVertexBuffers.Count());
		transitions.PreAllocate(aVertexBuffers.Count());
		for (uint i = 0; i < aVertexBuffers.Count(); ++i)
		{
			views.Add(*(static_cast<D3D12_VERTEX_BUFFER_VIEW*>(aVertexBuffers[i].myVertexBufferView)));
			transitions.Add(SC_Pair(SR_ResourceState_VertexConstantBuffer, aVertexBuffers[i].myBuffer));
		}
		Transition(transitions.GetBuffer(), transitions.Count());
		myNativeCommandList->IASetVertexBuffers(aStartVertexIndex, views.Count(), views.GetBuffer());
	}
	void SR_GraphicsContext_DX12::SetIndexBuffer(SR_Buffer* aIndexBuffer)
	{
		SR_Buffer_DX12* buffer = static_cast<SR_Buffer_DX12*>(aIndexBuffer); 
		const SR_BufferDesc& bufferDesc = buffer->GetProperties();

		if ((bufferDesc.myBindFlag & SBindFlag_IndexBuffer) == 0)
		{
			SC_ERROR_LOG("Tried to bind non-index buffer as index buffer (buffer: %p)", buffer);
			assert(false);
		}

		Transition(SR_ResourceState_IndexBuffer, buffer);
		D3D12_INDEX_BUFFER_VIEW view;
		view.BufferLocation = buffer->myDX12Resource->GetGPUVirtualAddress();
		view.Format = DXGI_FORMAT_R32_UINT;
		view.SizeInBytes = bufferDesc.mySize;
		myNativeCommandList->IASetIndexBuffer(&view);
	}

	void SR_GraphicsContext_DX12::SetIndexBuffer(CIndexBuffer& aIndexBuffer)
	{
		SR_Buffer_DX12* buffer = static_cast<SR_Buffer_DX12*>(aIndexBuffer.myBuffer.Get());

		Transition(SR_ResourceState_IndexBuffer, buffer);
		myNativeCommandList->IASetIndexBuffer((D3D12_INDEX_BUFFER_VIEW*)aIndexBuffer.myIndexBufferView);
	}

	void SR_GraphicsContext_DX12::SetShaderState(SR_ShaderState* aShaderState)
	{
		SR_ShaderState_DX12* shaderState = static_cast<SR_ShaderState_DX12*>(aShaderState);

		BindRootSignature(shaderState->myRootSignature);

		ID3D12PipelineState* pso = shaderState->GetPSO(myCurrentPSOKey);

		if (!pso)
		{
			// TODO: on the fly creation of PSO here.
			assert(false);
		}

		myNativeCommandList->SetPipelineState(pso);
	}

	void SR_GraphicsContext_DX12::SetRenderTargets(SR_RenderTarget** aTargets, uint aNumTargets, SR_RenderTarget* aDepthStencil, uint /*aDepthAccess*/)
	{
		assert(aNumTargets <= 8 && "Cannot bind more than 8 render-targets.");

		SR_DescriptorCPU_DX12 rtvHandles[8];
		for (uint i = 0; i < aNumTargets; ++i)
		{
			if (aTargets[i])
			{
				SR_RenderTarget_DX12* target = static_cast<SR_RenderTarget_DX12*>(aTargets[i]);

				assert(target->myRenderTargetView && "Trying to bind invalid RenderTargetView");

				rtvHandles[i] = target->myRenderTargetView;
			}
		}

		SR_DescriptorCPU_DX12* dsvHandle = nullptr;
		if (aDepthStencil)
		{
			SR_RenderTarget_DX12* target = static_cast<SR_RenderTarget_DX12*>(aDepthStencil);

			assert(target->myDepthStencilView && "Trying to bind invalid DepthStencilView");

			dsvHandle = &target->myDepthStencilView;
		}


		myNativeCommandList->OMSetRenderTargets(aNumTargets, rtvHandles, false, dsvHandle);
	}
	void SR_GraphicsContext_DX12::CopyBuffer(SR_Buffer* aDestination, SR_Buffer* aSource)
	{
		SR_Buffer_DX12* sourceBuffer = static_cast<SR_Buffer_DX12*>(aSource);
		SR_Buffer_DX12* destBuffer = static_cast<SR_Buffer_DX12*>(aDestination);
		ID3D12Resource* src = sourceBuffer->myDX12Resource;
		ID3D12Resource* dst = destBuffer->myDX12Resource;

		SC_Pair<SR_ResourceState, SR_TrackedResource*> transitions[2];

		transitions[0] = SC_Pair(SR_ResourceState_CopyDest, destBuffer);
		transitions[1] = SC_Pair(SR_ResourceState_CopySrc, sourceBuffer);

		Transition(transitions, 2);
		myNativeCommandList->CopyResource(dst, src);
		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}
	void SR_GraphicsContext_DX12::CopyTexture(SR_TextureBuffer* aDestination, SR_TextureBuffer* aSource)
	{
		SR_TextureBuffer_DX12* sourceBuffer = static_cast<SR_TextureBuffer_DX12*>(aSource);
		SR_TextureBuffer_DX12* destBuffer = static_cast<SR_TextureBuffer_DX12*>(aDestination);
		ID3D12Resource* src = sourceBuffer->myDX12Resource;
		ID3D12Resource* dst = destBuffer->myDX12Resource;

		SC_Pair<SR_ResourceState, SR_TrackedResource*> transitions[2];

		transitions[0] = SC_Pair(SR_ResourceState_CopyDest, destBuffer);
		transitions[1] = SC_Pair(SR_ResourceState_CopySrc, sourceBuffer);

		Transition(transitions, 2);
		myNativeCommandList->CopyResource(dst, src);
		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}

	void SR_GraphicsContext_DX12::UpdateBufferData(uint aStride, uint aCount, void* aData, CVertexBuffer& aOutBuffer)
	{
		bool needsInitialTransition = true;
		const uint actualNewByteSize = aStride * aCount;
		const uint currentByteSize = aOutBuffer.myDescription.myStride * aOutBuffer.myDescription.myNumVertices;
		SR_Buffer_DX12* bufferDX12 = static_cast<SR_Buffer_DX12*>(aOutBuffer.myBuffer.Get());
		ID3D12Resource* buffer = bufferDX12->myDX12Resource;
		if (actualNewByteSize > currentByteSize)
		{
			buffer->Release();
			ID3D12Resource* newBuffer;
			HRESULT hr = S_OK;
			{
				D3D12_HEAP_PROPERTIES heapProps;
				heapProps.Type = D3D12_HEAP_TYPE_DEFAULT;
				heapProps.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
				heapProps.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
				heapProps.CreationNodeMask = 1;
				heapProps.VisibleNodeMask = 1;

				D3D12_RESOURCE_DESC resourceDesc;
				resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
				resourceDesc.Alignment = 0;
				resourceDesc.Width = actualNewByteSize;
				resourceDesc.Height = 1;
				resourceDesc.DepthOrArraySize = 1;
				resourceDesc.MipLevels = 1;
				resourceDesc.Format = DXGI_FORMAT_UNKNOWN;
				resourceDesc.SampleDesc.Count = 1;
				resourceDesc.SampleDesc.Quality = 0;
				resourceDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
				resourceDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

				hr = myDevice->GetNativeDevice()->CreateCommittedResource(
					&heapProps,
					D3D12_HEAP_FLAG_NONE,
					&resourceDesc,
					D3D12_RESOURCE_STATE_COPY_DEST,
					nullptr,
					SR_IID_PPV_ARGS(&newBuffer));

				if (FAILED(hr))
					return;

				newBuffer->SetName(L"Vertex Buffer Resource Heap");
				buffer = newBuffer;
				bufferDX12->myDX12Resource = buffer;
				needsInitialTransition = false;
			}
		}

		ID3D12Resource* uploadHeap;

		D3D12_HEAP_PROPERTIES heapProps;
		heapProps.Type = D3D12_HEAP_TYPE_UPLOAD;
		heapProps.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
		heapProps.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
		heapProps.CreationNodeMask = 1;
		heapProps.VisibleNodeMask = 1;

		D3D12_RESOURCE_DESC resourceDesc;
		resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
		resourceDesc.Alignment = 0;
		resourceDesc.Width = actualNewByteSize;
		resourceDesc.Height = 1;
		resourceDesc.DepthOrArraySize = 1;
		resourceDesc.MipLevels = 1;
		resourceDesc.Format = DXGI_FORMAT_UNKNOWN;
		resourceDesc.SampleDesc.Count = 1;
		resourceDesc.SampleDesc.Quality = 0;
		resourceDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
		resourceDesc.Flags = D3D12_RESOURCE_FLAG_NONE;
		HRESULT hr = myDevice->GetNativeDevice()->CreateCommittedResource(
			&heapProps,
			D3D12_HEAP_FLAG_NONE,
			&resourceDesc,
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			SR_IID_PPV_ARGS(&uploadHeap));

		if (FAILED(hr))
			return;

		D3D12_SUBRESOURCE_DATA vertexData = {};
		vertexData.pData = reinterpret_cast<BYTE*>(aData);
		vertexData.RowPitch = actualNewByteSize;
		vertexData.SlicePitch = actualNewByteSize;

		if (needsInitialTransition && bufferDX12->myDX12Tracking.myState != SR_ResourceState_CopyDest)
		{
			D3D12_RESOURCE_BARRIER barrier;
			barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
			barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
			barrier.Transition.pResource = buffer;
			barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER;
			barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_COPY_DEST;
			barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
			myNativeCommandList->ResourceBarrier(1, &barrier);
			bufferDX12->myDX12Tracking.myState = static_cast<SR_ResourceState>(D3D12_RESOURCE_STATE_COPY_DEST);
		}
		UpdateSubresources(myNativeCommandList, buffer, uploadHeap, 0, 0, 1, &vertexData);

		if (bufferDX12->myDX12Tracking.myState == SR_ResourceState_CopyDest)
		{
			D3D12_RESOURCE_BARRIER barrier;
			barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
			barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
			barrier.Transition.pResource = buffer;
			barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
			barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER;
			barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
			myNativeCommandList->ResourceBarrier(1, &barrier);
			bufferDX12->myDX12Tracking.myState = static_cast<SR_ResourceState>(D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
		}

		aOutBuffer.myDescription.myNumVertices = aCount;
		aOutBuffer.myDescription.myStride = aStride;
		aOutBuffer.myDescription.myVertexData = nullptr;
		aOutBuffer.myNumVertices = aCount;

		D3D12_VERTEX_BUFFER_VIEW* vBufferView = new D3D12_VERTEX_BUFFER_VIEW;
		vBufferView->BufferLocation = buffer->GetGPUVirtualAddress();
		vBufferView->StrideInBytes = (UINT)aStride;
		vBufferView->SizeInBytes = actualNewByteSize;
		delete (D3D12_VERTEX_BUFFER_VIEW*)aOutBuffer.myVertexBufferView;
		aOutBuffer.myVertexBufferView = vBufferView;
		bufferDX12->myDX12Tracking.myState = static_cast<SR_ResourceState>(D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);

		myDevice->DeleteResource(uploadHeap);

		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}

	void SR_GraphicsContext_DX12::UpdateBufferData(uint aByteSize, void* aData, SR_Buffer* aOutBuffer)
	{
		aByteSize;
		aData;
		aOutBuffer;
	}

	void SR_GraphicsContext_DX12::GenerateMips(SR_Texture& aTexture)
	{
		aTexture;
		//const STextureDesc& desc = aTexture.myDescription;
		//if (desc.myMips == 1)
		//	return;
		//if (!IsPowerOfTwo(desc.myWidth) && !IsPowerOfTwo(desc.myHeight))
		//{
		//	S_ERROR_LOG("Trying to generate mips for non-power-of-two texture.");
		//	return;
		//}
		//
		//CBuffer_DX12* bufferDX12 = static_cast<CBuffer_DX12*>(aTexture.GetBuffer());
		//ID3D12Resource* buffer = bufferDX12->myDX12Resource;
		//if (!buffer)
		//{
		//	S_ERROR_LOG("Invalid resource when generating mips.");
		//	return;
		//}
		//
		//Transition(SR_ResourceState_UnorderedAccess, bufferDX12);
		//
		//CComputePSO* generationShader = nullptr;
		//
		//if (desc.myDimension == SR_Dimension_Texture2D)
		//	generationShader = myDevice->GetGenerateMips2DShader();
		//else if (desc.myDimension == SR_Dimension_Texture3D)
		//	generationShader = myDevice->GetGenerateMips3DShader();
		//
		//if (generationShader == nullptr)
		//{
		//	S_ERROR_LOG("Failed to get mip-generation shader.");
		//	return;
		//}
		//
		//BindComputePSO(*generationShader);
		//
		////CTexture* writableTexture;
		//
		//if ((aTexture.myDescription.myFlags & SR_ResourceFlag_AllowWrites) == 0)
		//{
		//	// Handle non-uav resources here
		//}
		//
		//DXGI_FORMAT format = _ConvertFormatDX(desc.myFormat);
		//for (uint topMip = 0, numMips = desc.myMips - 1; (topMip < numMips); ++topMip)
		//{
		//	uint dstWidth = std::max((uint)desc.myWidth >> (topMip + 1), 1u);
		//	uint dstHeight = std::max((uint)desc.myHeight >> (topMip + 1), 1u);
		//	uint dstDepth = std::max((uint)desc.myDepth >> (topMip + 1), 1u);
		//
		//	{
		//		D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
		//		srvDesc.Format = format;
		//		srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		//		if (desc.myDimension == SR_Dimension_Texture2D)
		//		{
		//			srvDesc.Texture2D.MipLevels = 1;
		//			srvDesc.Texture2D.MostDetailedMip = topMip;
		//			srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
		//		}
		//		else if (desc.myDimension == SR_Dimension_Texture3D)
		//		{
		//			srvDesc.Texture3D.MipLevels = 1;
		//			srvDesc.Texture3D.MostDetailedMip = topMip;
		//			srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE3D;
		//		}
		//
		//		D3D12_CPU_DESCRIPTOR_HANDLE handle;
		//		handle.ptr = myResourceHeapHandles.myCPUAddress;
		//		CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandle(handle, 0 + globalHeapOffsetSRV, myResourceDescriptorIncrementSize);
		//		myDevice->GetNativeDevice()->CreateShaderResourceView(buffer, &srvDesc, cpuHandle);
		//	}
		//	{
		//		D3D12_UNORDERED_ACCESS_VIEW_DESC uavDesc = {};
		//		uavDesc.Format = format;
		//		if (desc.myDimension == SR_Dimension_Texture2D)
		//		{
		//			uavDesc.Texture2D.MipSlice = topMip + 1;
		//			uavDesc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE2D;
		//		}
		//		else if (desc.myDimension == SR_Dimension_Texture3D)
		//		{
		//			uavDesc.Texture3D.MipSlice = topMip + 1;
		//			uavDesc.Texture3D.WSize = uint(-1); 
		//			uavDesc.Texture3D.FirstWSlice = 0;
		//			uavDesc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE3D;
		//		}
		//		D3D12_CPU_DESCRIPTOR_HANDLE handle;
		//		handle.ptr = myResourceHeapHandles.myCPUAddress;
		//		CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandle(handle, 0 + globalHeapOffsetUAV, myResourceDescriptorIncrementSize);
		//		myDevice->GetNativeDevice()->CreateUnorderedAccessView(buffer, nullptr, &uavDesc, cpuHandle);
		//	}
		//
		//	// Bind Constants
		//
		//	struct Constants
		//	{
		//		float3 myTexelSizes;
		//	} constants;
		//
		//	constants.myTexelSizes.x = float(dstWidth);
		//	constants.myTexelSizes.y = float(dstHeight);
		//	constants.myTexelSizes.z = float(dstDepth);
		//
		//	BindConstantBuffer(&constants, sizeof(constants), 0);
		//
		//
		//	if (desc.myDimension == SR_Dimension_Texture2D)
		//		Dispatch(std::max(dstWidth / 8, 1u), std::max(dstHeight / 8, 1u), 1u);
		//	else if (desc.myDimension == SR_Dimension_Texture3D)
		//		Dispatch(std::max(dstWidth / 4, 1u), std::max(dstHeight / 4, 1u), std::max(dstDepth / 4, 1u));
		//	
		//	CD3DX12_RESOURCE_BARRIER bar = CD3DX12_RESOURCE_BARRIER::UAV(buffer);
		//	myNativeCommandList->ResourceBarrier(1, &bar);
		//}
		//
		//Transition(SR_ResourceState_GenericRead, bufferDX12);
		//SetNeedsFlush();
		//++myNumCommandsSinceReset;
	}

	void SR_GraphicsContext_DX12::ResolveQuery(const EQueryType& aType, uint aStartIndex, uint aNumQueries, SR_Buffer* aOutBuffer)
	{
		SR_Buffer_DX12* buffer = static_cast<SR_Buffer_DX12*>(aOutBuffer);
		ID3D12Resource* resource = buffer->myDX12Resource;

		D3D12_QUERY_TYPE type = static_cast<D3D12_QUERY_TYPE>(aType);

		myNativeCommandList->ResolveQueryData(myDevice->GetQueryHeap(), type, aStartIndex, aNumQueries, resource, 0);
		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}

	void SR_GraphicsContext_DX12::EndQuery(const EQueryType& aType, uint aIndex)
	{
		D3D12_QUERY_TYPE type = static_cast<D3D12_QUERY_TYPE>(aType);
		myNativeCommandList->EndQuery(myDevice->GetQueryHeap(), type, aIndex);
	}

	void SR_GraphicsContext_DX12::BuildAccelerationStructure()
	{
		D3D12_RAYTRACING_GEOMETRY_DESC geometryDesc = {}; 
		geometryDesc.Type = D3D12_RAYTRACING_GEOMETRY_TYPE_TRIANGLES;
		//geometryDesc.Triangles.IndexBuffer = m_indexBuffer->GetGPUVirtualAddress();
		//geometryDesc.Triangles.IndexCount = static_cast<UINT>(m_indexBuffer->GetDesc().Width) / sizeof(Index);
		geometryDesc.Triangles.IndexFormat = DXGI_FORMAT_R16_UINT;
		geometryDesc.Triangles.Transform3x4 = 0;
		geometryDesc.Triangles.VertexFormat = DXGI_FORMAT_R32G32B32_FLOAT;
		//geometryDesc.Triangles.VertexCount = static_cast<UINT>(m_vertexBuffer->GetDesc().Width) / sizeof(Vertex);
		//geometryDesc.Triangles.VertexBuffer.StartAddress = m_vertexBuffer->GetGPUVirtualAddress();
		//geometryDesc.Triangles.VertexBuffer.StrideInBytes = sizeof(Vertex);


		geometryDesc.Flags = (true /*isOpaque*/) ? D3D12_RAYTRACING_GEOMETRY_FLAG_OPAQUE : D3D12_RAYTRACING_GEOMETRY_FLAG_NONE;

		D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS buildFlags = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE;
		D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS topLevelInputs = {};
		topLevelInputs.DescsLayout = D3D12_ELEMENTS_LAYOUT_ARRAY;
		topLevelInputs.Flags = buildFlags;
		topLevelInputs.NumDescs = 1;
		topLevelInputs.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL;

		D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO topLevelPrebuildInfo = {};
		myDevice->GetNativeDevice5()->GetRaytracingAccelerationStructurePrebuildInfo(&topLevelInputs, &topLevelPrebuildInfo);
		assert(topLevelPrebuildInfo.ResultDataMaxSizeInBytes > 0);

		D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO bottomLevelPrebuildInfo = {};
		D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS bottomLevelInputs = topLevelInputs;
		bottomLevelInputs.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL;
		bottomLevelInputs.pGeometryDescs = &geometryDesc;
		myDevice->GetNativeDevice5()->GetRaytracingAccelerationStructurePrebuildInfo(&bottomLevelInputs, &bottomLevelPrebuildInfo);
		assert(bottomLevelPrebuildInfo.ResultDataMaxSizeInBytes > 0);
		SetNeedsFlush();
		++myNumCommandsSinceReset;
	}

	SR_CommandList* SR_GraphicsContext_DX12::GetCommandList()
	{
		return myCommandList;
	}

	ID3D12GraphicsCommandList* SR_GraphicsContext_DX12::GetNativeCommandList() const
	{
		return myNativeCommandList;
	}

	void SR_GraphicsContext_DX12::PreDraw()
	{
		//SetResourceTables();
	}

	void SR_GraphicsContext_DX12::PreDispatch()
	{
		//SetResourceTables();
	}

	void SR_GraphicsContext_DX12::SetResourceTables()
	{
		uint numDescriptors = myCurrentRootSignature->GetMaxNumDescriptors();
		SR_Descriptor_DX12 tableStart = myDevice->AllocateDescriptorRange(numDescriptors);

		SR_DescriptorCPU_DX12 srcHandle = myDevice->GetStagedHandle();
		if (srcHandle.ptr != 0)
			myDevice->GetNativeDevice()->CopyDescriptorsSimple(numDescriptors, tableStart.myCPUHandle, srcHandle, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

		myResourceHeapDescriptor = tableStart;

		uint offset = 0;
		for (uint i = 0; i < SR_RootSignature_DX12::Tables::Tables_Count; ++i)
		{
			SR_DescriptorGPU_DX12 handle = myResourceHeapDescriptor.myGPUHandle;
			handle.ptr += offset * myResourceDescriptorIncrementSize;

			if (myCurrentRootSignature->IsCompute())
				myNativeCommandList->SetComputeRootDescriptorTable(i, handle);
			else
				myNativeCommandList->SetGraphicsRootDescriptorTable(i, handle);

			offset += myCurrentRootSignature->GetNumDescriptors(SR_RootSignature_DX12::Tables(i));
		}
	}

	void SR_GraphicsContext_DX12::BindRootSignature(SR_RootSignature_DX12* aRootSignature)
	{
		const SR_DescriptorHeap_DX12& heap = myDevice->GetGPUVisibleDescriptorHeap();
		ID3D12DescriptorHeap* descriptorHeap = heap.GetHeap();
		if (descriptorHeap != myResourceHeap)
		{
			myNativeCommandList->SetDescriptorHeaps(1, &descriptorHeap);
			myResourceHeap = descriptorHeap;
		}

		if (myCurrentRootSignature != aRootSignature)
		{
			if (aRootSignature->IsCompute())
				myNativeCommandList->SetComputeRootSignature(aRootSignature->myRootSignature);
			else
				myNativeCommandList->SetGraphicsRootSignature(aRootSignature->myRootSignature);

			myCurrentRootSignature = aRootSignature;
		}
	}


	void SR_GraphicsContext_DX12::BindResourceTables()
	{
		// TODO: Deprecate this!

		ID3D12DescriptorHeap* heap = myDevice->GetResourceHeap();
		myNativeCommandList->SetDescriptorHeaps(1, &heap);

		RequestNewDescriptorRange();
	}

	void SR_GraphicsContext_DX12::RequestNewDescriptorRange()
	{

		// TODO: Deprecate this!
		SR_DescriptorCPU_DX12 stagedHandle = myDevice->GetStagedHandle();

		myResourceHeapHandles = myDevice->GetDescriptorRange(GPU_RESOURCE_MAX_COUNT_PER_CONTEXT);
		D3D12_GPU_DESCRIPTOR_HANDLE gpuHandle;
		gpuHandle.ptr = myResourceHeapHandles.myGPUAddress;

		if (stagedHandle.ptr != 0)
		{
			SR_DescriptorCPU_DX12 dstHandle;
			dstHandle.ptr = myResourceHeapHandles.myCPUAddress;
			myDevice->GetNativeDevice()->CopyDescriptorsSimple(GPU_RESOURCE_MAX_COUNT_PER_CONTEXT, dstHandle, stagedHandle, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
		}


		SR_DescriptorGPU_DX12 cbvHandle;
		cbvHandle.ptr = myResourceHeapHandles.myGPUAddress;
		cbvHandle.ptr += globalHeapOffsetCBV * myResourceDescriptorIncrementSize;
		myNativeCommandList->SetGraphicsRootDescriptorTable(ERootParamIndex::CBV, cbvHandle);
		myNativeCommandList->SetComputeRootDescriptorTable(ERootParamIndex::CBV, cbvHandle);

		SR_DescriptorGPU_DX12 srvHandle;
		srvHandle.ptr = myResourceHeapHandles.myGPUAddress;
		srvHandle.ptr += globalHeapOffsetSRV * myResourceDescriptorIncrementSize;
		myNativeCommandList->SetGraphicsRootDescriptorTable(ERootParamIndex::SRV, srvHandle);
		myNativeCommandList->SetComputeRootDescriptorTable(ERootParamIndex::SRV, srvHandle);

		SR_DescriptorGPU_DX12 uavHandle;
		uavHandle.ptr = myResourceHeapHandles.myGPUAddress;
		uavHandle.ptr += globalHeapOffsetUAV * myResourceDescriptorIncrementSize;
		myNativeCommandList->SetGraphicsRootDescriptorTable(ERootParamIndex::UAV, uavHandle);
		myNativeCommandList->SetComputeRootDescriptorTable(ERootParamIndex::UAV, uavHandle);
	}

	void SR_GraphicsContext_DX12::SetResources()
	{
		// Everything below should only be done on demand and if needed
		// Set DescriptorHeaps
		// Set Root Signature, diff on Graphics vs Compute etc.
		// Bind resources from the RootTables to DescriptorTables
		// Set the DescriptorTables

		//ID3D12Device* device = myDevice->GetNativeDevice();
		//
		//device->CopyDescriptorsSimple
		return;
	}

}
#endif