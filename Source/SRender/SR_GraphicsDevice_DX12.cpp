#include "SRender_Precompiled.h"
#if ENABLE_DX12

#include "SR_GraphicsDevice_DX12.h"
#include "SR_GraphicsContext_DX12.h"
#include "SR_SwapChain_DX12.h"
#include "SR_Converters_DX12.h"
#include "SR_Buffer_DX12.h"
#include "SR_BufferView_DX12.h"
#include "SR_TextureBuffer_DX12.h"
#include "SR_Texture_DX12.h"
#include "SR_RenderTarget_DX12.h"
#include "SR_ShaderState_DX12.h"
#include "SR_ShaderStateCache.h"
#include "SR_CommandList_DX12.h"
#include "SR_DDSTextureLoader_DX12.h"
#include "SR_WICTextureLoader_DX12.h"
#include "SR_RootSignature_DX12.h"
#include "SR_ShaderCompiler_DX12.h"
#include "DirectXTex.h"
#include "DirectXTex.inl"

#include "SC_Window_Win64.h"

#include "d3dx12.h"


namespace Shift
{
	static std::string globalShaderModel = "_5_1";
	SR_GraphicsDevice_DX12* SR_GraphicsDevice_DX12::ourGraphicsDevice_DX12 = nullptr;

	namespace CGraphicsDevice_DX12_Private
	{
		static std::mutex myResourceDecriptorLock;
	}

	const char* GetFeatureLevelString(const D3D_FEATURE_LEVEL& aFeatureLevel)
	{
		switch (aFeatureLevel)
		{
		case D3D_FEATURE_LEVEL_12_1:
			return "12.1";
		case D3D_FEATURE_LEVEL_12_0:
			return "12.0";
		case D3D_FEATURE_LEVEL_11_1:
			return "11.1";
		case D3D_FEATURE_LEVEL_11_0:
			return "11.0";
		}
		return "<not supported>";
	}

	SR_GraphicsDevice_DX12::SR_GraphicsDevice_DX12()
		: SR_GraphicsDevice()
		, myDevice(nullptr)
		, myInfoQueue(nullptr)
		, myResourceDescriptorIncrementSize(0)
		, mySamplerDescriptorIncrementSize(0)
		, myResourceCtxOffset(0)
		, myFeatureLevel(D3D_FEATURE_LEVEL_11_0)
		, myIsUsingPlacementHeaps(false)
	{
		myAPIType = SR_GraphicsAPI::DirectX12;
		ourGraphicsDevice_DX12 = this;
	}


	SR_GraphicsDevice_DX12::~SR_GraphicsDevice_DX12()
	{
		ourGraphicsDevice_DX12 = nullptr;
	}
	SR_GraphicsDevice_DX12* SR_GraphicsDevice_DX12::GetDX12Device()
	{
		return ourGraphicsDevice_DX12;
	}

	void SR_GraphicsDevice_DX12::InactivateContext(SR_GraphicsContext_DX12* aCtx)
	{
		SR_QueueType type = aCtx->GetType();
		SC_MutexLock lock(myContextsMutex);

		//SR_DescriptorCPU_DX12 stagedHandle = GetStagedHandle();
		//if (stagedHandle.ptr != 0)
		//{
		//	SR_DescriptorCPU_DX12 srcHandle;
		//	srcHandle.ptr = aCtx->myResourceHeapDescriptor.myCPUHandle.ptr;
		//	myDevice->CopyDescriptorsSimple(aCtx->myCurrentRootSignature->GetMaxNumDescriptors(), stagedHandle, stagedHandle, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
		//}

		myInactiveContexts[type].Add(aCtx);
	}
	CComputePSO* SR_GraphicsDevice_DX12::GetGenerateMips2DShader() const
	{
		return myGenerateMips2D;
	}
	CComputePSO* SR_GraphicsDevice_DX12::GetGenerateMips3DShader() const
	{
		return myGenerateMips3D;
	}
	SR_DescriptorCPU_DX12 SR_GraphicsDevice_DX12::GetStagedHandle() const
	{
		return myResourceHeapStagedCPU.GetStartHandleCPU();
	}
	void SR_GraphicsDevice_DX12::CopyDescriptors(uint aNumDescriptors, D3D12_CPU_DESCRIPTOR_HANDLE aHandleDest, D3D12_CPU_DESCRIPTOR_HANDLE aHandleSrc)
	{
		myDevice->CopyDescriptorsSimple(aNumDescriptors, aHandleDest, aHandleSrc, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	}

	ID3D12Device * SR_GraphicsDevice_DX12::GetNativeDevice()
	{
		return myDevice;
	}

	ID3D12Device5* SR_GraphicsDevice_DX12::GetNativeDevice5()
	{
		return myDevice5;
	}

	ID3D12DescriptorHeap * SR_GraphicsDevice_DX12::GetResourceHeap()
	{
		return myResourceHeapCtx;
	}

	ID3D12DescriptorHeap * SR_GraphicsDevice_DX12::GetScreenTargetHeap()
	{
		return nullptr;
	}

	ID3D12DescriptorHeap * SR_GraphicsDevice_DX12::GetDepthStencilHeap()
	{
		return myDepthStencilHeap;
	}

	ID3D12RootSignature * SR_GraphicsDevice_DX12::GetGraphicsRootSignature()
	{
		return myGraphicsRootSignature;
	}

	ID3D12RootSignature * SR_GraphicsDevice_DX12::GetComputeRootSignature()
	{
		return myComputeRootSignature;
	}

	ID3D12GraphicsCommandList* SR_GraphicsDevice_DX12::GetCommandList()
	{
		return myContext->myNativeCommandList;
	}

	ID3D12QueryHeap* SR_GraphicsDevice_DX12::GetQueryHeap()
	{
		return myQueryHeap;
	}
	
	void SR_GraphicsDevice_DX12::DeleteResource(ID3D12Resource* aResource)
	{
		myDeletedResources.Add(aResource);
	}

	void SR_GraphicsDevice_DX12::BeginFrame()
	{
		ReleaseResources();
		for (auto& resource : myDeletedResources)
		{
			resource->Release();
		}
		myDeletedResources.RemoveAll();

		std::unique_lock<std::mutex> lock(CGraphicsDevice_DX12_Private::myResourceDecriptorLock);
		myResourceCtxOffset = 0;

	}

	void SR_GraphicsDevice_DX12::EndFrame()
	{
	}

	void SR_GraphicsDevice_DX12::Present()
	{
		mySwapChain->Present(false);
	}

	SC_Ref<SR_GraphicsContext> SR_GraphicsDevice_DX12::GetGraphicsContext(const SR_QueueType& aType)
	{
		SC_Ref<SR_GraphicsContext> ctx = new SR_GraphicsContext_DX12(aType, this);

		if (aType != SR_QueueType_Copy)
		{
			std::unique_lock<std::mutex> lock(CGraphicsDevice_DX12_Private::myResourceDecriptorLock);
			SR_GraphicsContext::SResourceHeapHandles handles;
			CD3DX12_CPU_DESCRIPTOR_HANDLE handleCPU(myResourceHeapCtx->GetCPUDescriptorHandleForHeapStart(), myResourceCtxOffset, myResourceDescriptorIncrementSize);
			CD3DX12_GPU_DESCRIPTOR_HANDLE handleGPU(myResourceHeapCtx->GetGPUDescriptorHandleForHeapStart(), myResourceCtxOffset, myResourceDescriptorIncrementSize);

			handles.myCPUAddress = handleCPU.ptr;
			handles.myGPUAddress = handleGPU.ptr;
			ctx->SetResourceHeapHandles(handles);

			myResourceCtxOffset += GPU_RESOURCE_MAX_COUNT_PER_CONTEXT;
		}
		return ctx;
	}

	SR_GraphicsContext* SR_GraphicsDevice_DX12::GetContext(const SR_QueueType& aType)
	{
		SC_Ref<SR_GraphicsContext> ctx;
		if (myInactiveContexts[aType].Count())
		{
			SC_MutexLock lock(myContextsMutex);
			if (myInactiveContexts[aType].Count())
			{
				ctx = myInactiveContexts[aType].GetLast();
				myInactiveContexts[aType].RemoveLast();
			}
		}

		if (!ctx)
			ctx = new SR_GraphicsContext_DX12(aType, this);

		// TODO: Init Context here

		SR_GraphicsContext::SetCurrentContext(ctx);
		return ctx;
	}

	SC_Ref<SR_TextureBuffer> SR_GraphicsDevice_DX12::CreateTextureBuffer(const SR_TextureBufferDesc& aDesc, const char* aID)
	{
		bool isRenderTarget = (aDesc.myFlags & SR_ResourceFlag_AllowRenderTarget);
		bool isDepth = (aDesc.myFlags & SR_ResourceFlag_AllowDepthStencil);

		D3D12_RESOURCE_DESC texDesc = {};
		texDesc.Width = static_cast<UINT64>(aDesc.myWidth);
		texDesc.Height = static_cast<UINT64>(aDesc.myHeight);
		texDesc.Format = _ConvertFormatDX(aDesc.myFormat);
		texDesc.MipLevels = aDesc.myMips;
		texDesc.DepthOrArraySize = UINT16(SC_Max(aDesc.myArraySize, 1U));
		texDesc.Dimension = static_cast<D3D12_RESOURCE_DIMENSION>(aDesc.myDimension);
		if (aDesc.myDimension == SR_Dimension_Texture3D)
			UINT16(aDesc.myDepth);
		else if (aDesc.myDimension == SR_Dimension_TextureCube)
		{
			texDesc.DepthOrArraySize = aDesc.myArraySize * 6;
			texDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
		}
		texDesc.SampleDesc.Count = 1;
		texDesc.SampleDesc.Quality = 0;
		texDesc.Flags = static_cast<D3D12_RESOURCE_FLAGS>(aDesc.myFlags);

		SC_Ref<SR_TextureBuffer_DX12> textureBuffer(new SR_TextureBuffer_DX12());

		HRESULT result = S_OK;
		if (myIsUsingPlacementHeaps)
		{
			// Use Placed Resources instead of allocating new space for each resource.
		}
		else
		{
			D3D12_HEAP_PROPERTIES heapProps = {};
			heapProps.Type = D3D12_HEAP_TYPE_DEFAULT;
			heapProps.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
			heapProps.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
			heapProps.CreationNodeMask = 1;
			heapProps.VisibleNodeMask = 1;

			D3D12_CLEAR_VALUE* clearValue = nullptr;
			if (isRenderTarget || isDepth)
			{
				D3D12_CLEAR_VALUE _clearValue;
				_clearValue.Color[0] = 0.0f;
				_clearValue.Color[1] = 0.0f;
				_clearValue.Color[2] = 0.0f;
				_clearValue.Color[3] = 0.0f;
				_clearValue.Format = isDepth ? GetDepthFormatFromTypelessDX(aDesc.myFormat) : texDesc.Format;
				clearValue = &_clearValue;
			}

			result = myDevice->CreateCommittedResource(
				&heapProps,
				D3D12_HEAP_FLAG_NONE,
				&texDesc,
				(isDepth) ? D3D12_RESOURCE_STATE_DEPTH_WRITE : D3D12_RESOURCE_STATE_COMMON,
				clearValue,
				SR_IID_PPV_ARGS(&textureBuffer->myDX12Resource));


		}
		if (FAILED(result))
		{
			SC_ERROR_LOG("Could not create texture with ID [%s]", aID);
			return nullptr;
		}

		if (aID)
		{
			textureBuffer->myDX12Resource->SetName(ToWString(aID).c_str());
#if !IS_FINAL_BUILD
			textureBuffer->myName = aID;
#endif
		}

		textureBuffer->myProperties = aDesc;

		if (textureBuffer->myProperties.myDimension == SR_Dimension_TextureCube)
			textureBuffer->myProperties.myArraySize = 6;

		textureBuffer->myDX12Tracking.myState = (isDepth) ? SR_ResourceState_DepthWrite : SR_ResourceState_Common;

		uint bytesFromFormat = GetFormatByteSize_DX12(texDesc.Format);
		uint allocatedVRAM = 0;
		allocatedVRAM = (uint)texDesc.Width * texDesc.Height * (uint)texDesc.DepthOrArraySize * bytesFromFormat;
		myUsedVRAM += allocatedVRAM;

		return textureBuffer;
	}
	SC_Ref<SR_Texture> SR_GraphicsDevice_DX12::CreateTexture(const SR_TextureDesc& aTextureDesc, SR_TextureBuffer* aTextureBuffer)
	{
		SR_TextureBuffer_DX12* textureBuffer = static_cast<SR_TextureBuffer_DX12*>(aTextureBuffer);
		const SR_TextureBufferDesc& bufferProps = textureBuffer->GetProperties();

		bool isArray = bufferProps.myArraySize > 1;

		uint bufferArraySize = bufferProps.myDimension == SR_Dimension_Texture3D ? (uint)bufferProps.myDepth : SC_Max(bufferProps.myArraySize, 1U);
		uint arraySize = aTextureDesc.myArraySize ? aTextureDesc.myArraySize : bufferArraySize - aTextureDesc.myFirstArrayIndex;
		assert(arraySize <= bufferArraySize);

		SC_Ref<SR_Texture_DX12> texture(new SR_Texture_DX12());
		texture->myTextureBuffer = aTextureBuffer;
		texture->myDescriptorCPU = myResourceHeapCPU.AllocateCPU();
		texture->myDescription = aTextureDesc;

		uint mip = aTextureDesc.myMostDetailedMip;
		uint mipLevels = aTextureDesc.myMipLevels ? aTextureDesc.myMipLevels : bufferProps.myMips - mip;
		assert(mip + mipLevels <= bufferProps.myMips);

		D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
		srvDesc.Format = _ConvertFormatDX(aTextureDesc.myFormat);
		srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

		switch (bufferProps.myDimension)
		{
		case SR_Dimension_Texture1D:
			srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE1D;
			srvDesc.Texture1D.MostDetailedMip = mip;
			srvDesc.Texture1D.MipLevels = mipLevels;
			break;

		case SR_Dimension_Texture2D:
		{	
			if (isArray)
			{
				srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2DARRAY;
				srvDesc.Texture2DArray.MostDetailedMip = mip;
				srvDesc.Texture2DArray.MipLevels = mipLevels;
				srvDesc.Texture2DArray.FirstArraySlice = aTextureDesc.myFirstArrayIndex;
				srvDesc.Texture2DArray.ArraySize = arraySize;
			}
			else
			{
				srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
				srvDesc.Texture2DArray.MostDetailedMip = mip;
				srvDesc.Texture2DArray.MipLevels = mipLevels;
			}
			break;
		}
		case SR_Dimension_Texture3D:
			srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE3D;
			srvDesc.Texture3D.MostDetailedMip = mip;
			srvDesc.Texture3D.MipLevels = mipLevels;
			break;

		case SR_Dimension_TextureCube:
			srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURECUBE;
			srvDesc.TextureCube.MipLevels = mipLevels;
			srvDesc.TextureCube.MostDetailedMip = mip;
			break;
		}

		myDevice->CreateShaderResourceView(textureBuffer->myDX12Resource, &srvDesc, texture->myDescriptorCPU);

		return texture;
	}

	SC_Ref<SR_Texture> SR_GraphicsDevice_DX12::CreateRWTexture(const SR_TextureDesc& aTextureDesc, SR_TextureBuffer* aTextureBuffer)
	{
		SR_TextureBuffer_DX12* textureBuffer = static_cast<SR_TextureBuffer_DX12*>(aTextureBuffer);
		const SR_TextureBufferDesc& bufferProps = textureBuffer->GetProperties();

		const bool isArray = bufferProps.myArraySize > 1;
		assert(!aTextureDesc.myMipLevels || aTextureDesc.myMipLevels == 1);
		uint bufferArraySize = bufferProps.myDimension == SR_Dimension_Texture3D ? (uint)bufferProps.myDepth : SC_Max(bufferProps.myArraySize, 1U);
		uint arraySize = aTextureDesc.myArraySize ? aTextureDesc.myArraySize : bufferArraySize - aTextureDesc.myFirstArrayIndex;
		assert(arraySize <= bufferArraySize);

		SC_Ref<SR_Texture_DX12> texture(new SR_Texture_DX12());
		texture->myTextureBuffer = aTextureBuffer;
		texture->myDescriptorCPU = myResourceHeapCPU.AllocateCPU();
		texture->myDescription = aTextureDesc;

		D3D12_UNORDERED_ACCESS_VIEW_DESC uavDesc = {};
		uavDesc.Format = _ConvertFormatDX(bufferProps.myFormat);

		switch (bufferProps.myDimension)
		{
		case SR_Dimension_Texture1D:
			uavDesc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE1D;
			uavDesc.Texture1D.MipSlice = aTextureDesc.myMostDetailedMip;
			break;

		case SR_Dimension_Texture2D:
		{
			if (isArray)
			{
				uavDesc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE2DARRAY;
				uavDesc.Texture2DArray.MipSlice = aTextureDesc.myMostDetailedMip;
				uavDesc.Texture2DArray.FirstArraySlice = aTextureDesc.myFirstArrayIndex;
				uavDesc.Texture2DArray.ArraySize = arraySize;
			}
			else
			{
				uavDesc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE2D;
				uavDesc.Texture2D.MipSlice = aTextureDesc.myMostDetailedMip;
			}
			break;
		}
		case SR_Dimension_Texture3D:
			uavDesc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE3D;
			uavDesc.Texture3D.MipSlice = aTextureDesc.myMostDetailedMip;
			uavDesc.Texture3D.FirstWSlice = aTextureDesc.myFirstArrayIndex;
			uavDesc.Texture3D.WSize = arraySize;
			break;

		case SR_Dimension_TextureCube:
			uavDesc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE2DARRAY;
			uavDesc.Texture2DArray.MipSlice = aTextureDesc.myMostDetailedMip;
			uavDesc.Texture2DArray.FirstArraySlice = aTextureDesc.myFirstArrayIndex * 6;
			uavDesc.Texture2DArray.ArraySize = SC_Max(1U, arraySize) * 6;
			break;
		}

		myDevice->CreateUnorderedAccessView(textureBuffer->myDX12Resource, nullptr, &uavDesc, texture->myDescriptorCPU);

		return texture;
	}

	SC_Ref<SR_Texture> SR_GraphicsDevice_DX12::GetCreateTexture(const char* aFile)
	{
		if (!aFile)
		{
			SC_ERROR_LOG("No texture-path provided.");
			return nullptr;
		}
		SC_Ref<SR_TextureBuffer_DX12> textureBuffer(new SR_TextureBuffer_DX12());

		std::unique_ptr<uint8_t[]> ddsData;
		std::vector<D3D12_SUBRESOURCE_DATA> subresources;

		bool isCube = false; 
		HRESULT result = S_OK;
		if (GetFileExtension(aFile) == "dds")
			result = DirectX::LoadDDSTextureFromFile(myDevice, ToWString(aFile).c_str(), &textureBuffer->myDX12Resource, ddsData, subresources, 0ui64, nullptr, &isCube);
		else
		{
			subresources.resize(1);
			result = DirectX::LoadWICTextureFromFile(myDevice, ToWString(aFile).c_str(), &textureBuffer->myDX12Resource, ddsData, subresources[0]);
		}
		if (FAILED(result))
		{
			SC_ERROR_LOG("Could not load texture [%s]", aFile);
			return nullptr;
		}
		
		const UINT64 uploadBufferSize = GetRequiredIntermediateSize(textureBuffer->myDX12Resource, 0, static_cast<UINT>(subresources.size()));

		// Create the GPU upload buffer.
		ID3D12Resource* uploadHeap;
		auto heapProps = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
		auto resourceDesc = CD3DX12_RESOURCE_DESC::Buffer(uploadBufferSize);
		result = myDevice->CreateCommittedResource(
			&heapProps,
			D3D12_HEAP_FLAG_NONE,
			&resourceDesc,
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			SR_IID_PPV_ARGS(&uploadHeap));

		if (FAILED(result))
		{
			SC_ERROR_LOG("Could not load texture [%s]", aFile);
			return nullptr;
		}

		textureBuffer->myDX12Tracking.myState = SR_ResourceState_CopyDest;

		SR_GraphicsContext_DX12* ctx = static_cast<SR_GraphicsContext_DX12*>(SR_GraphicsDevice::GetDevice()->GetContext(SR_QueueType_Copy));
		ctx->Begin();
		ctx->BeginRecording();

		UpdateSubresources(ctx->GetNativeCommandList(), textureBuffer->myDX12Resource, uploadHeap, 0, 0, static_cast<UINT>(subresources.size()), subresources.data());
		ctx->Transition(SR_ResourceState_Common, textureBuffer);
		ctx->EndRecording();

		uint64 fence = SR_GraphicsQueueManager::GetNextExpectedFence(SR_QueueType_Copy);
		ctx->InsertFence(fence);

		SC_GrowingArray<SC_Ref<SR_CommandList>> cls;
		cls.Add(ctx->GetCommandList());
		SR_GraphicsQueueManager::ExecuteCommandLists(cls, SR_QueueType_Copy);
		SR_GraphicsQueueManager::Signal(fence, SR_QueueType_Copy);
		ctx->End();
		SR_GraphicsQueueManager::WaitForSignal(fence, SR_QueueType_Copy);
		
		D3D12_RESOURCE_DESC desc = textureBuffer->myDX12Resource->GetDesc();
		SR_TextureBufferDesc& textureBufferDesc = textureBuffer->myProperties;

		textureBufferDesc.myWidth = static_cast<float>(desc.Width);
		textureBufferDesc.myHeight = static_cast<float>(desc.Height);
		textureBufferDesc.myMips = desc.MipLevels;
		textureBufferDesc.myFormat = _ConvertFormatFromDX(desc.Format);
		textureBufferDesc.myFlags = desc.Flags;
		textureBufferDesc.myDimension = isCube ? SR_Dimension_TextureCube : static_cast<SR_Dimension>(desc.Dimension);
		textureBufferDesc.myIsCube = isCube;

		SR_TextureDesc textureDesc;
		textureDesc.myFormat = textureBufferDesc.myFormat;
		textureDesc.myMipLevels = textureBufferDesc.myMips;

		SC_Ref<SR_Texture> texture = CreateTexture(textureDesc, textureBuffer);
		return texture;
	}

	SC_Ref<SR_RenderTarget> SR_GraphicsDevice_DX12::CreateRenderTarget(const SR_RenderTargetDesc& aRenderTargetDesc, SR_TextureBuffer* aTextureBuffer)
	{
		SR_TextureBuffer_DX12* textureBuffer = static_cast<SR_TextureBuffer_DX12*>(aTextureBuffer);
		const SR_TextureBufferDesc& bufferProps = textureBuffer->GetProperties();

		SC_Ref<SR_RenderTarget_DX12> renderTarget(new SR_RenderTarget_DX12());
		renderTarget->myTextureBuffer = aTextureBuffer;
		renderTarget->myRenderTargetView = myRTVHeap.AllocateCPU();

		D3D12_RENDER_TARGET_VIEW_DESC rtvdesc = {};
		rtvdesc.Format = _ConvertFormatDX(aRenderTargetDesc.myFormat == SR_Format_Unknown ? bufferProps.myFormat : aRenderTargetDesc.myFormat);

		switch (bufferProps.myDimension)
		{
		case SR_Dimension_Texture1D:

			rtvdesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE1D;
			break;
		case SR_Dimension_Texture2D:

			rtvdesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
			break;
		case SR_Dimension_Texture3D:

			rtvdesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE3D;
			break;
		}

		myDevice->CreateRenderTargetView(textureBuffer->myDX12Resource, &rtvdesc, renderTarget->myRenderTargetView);

		return renderTarget;
	}

	SC_Ref<SR_RenderTarget> SR_GraphicsDevice_DX12::CreateDepthStencil(const SR_RenderTargetDesc& aRenderTargetDesc, SR_TextureBuffer* aTextureBuffer)
	{
		SR_TextureBuffer_DX12* textureBuffer = static_cast<SR_TextureBuffer_DX12*>(aTextureBuffer);
		const SR_TextureBufferDesc& bufferProps = textureBuffer->GetProperties();

		bool isArray = bufferProps.myArraySize > 1;

		SC_Ref<SR_RenderTarget_DX12> renderTarget(new SR_RenderTarget_DX12());
		renderTarget->myTextureBuffer = aTextureBuffer;
		renderTarget->myDepthStencilView = myDSVHeap.AllocateCPU();

		D3D12_DEPTH_STENCIL_VIEW_DESC dsvdesc = {};
		dsvdesc.Format = _ConvertFormatDX(aRenderTargetDesc.myFormat);

		switch (bufferProps.myDimension)
		{
		case SR_Dimension_Texture1D:
			dsvdesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE1D;
			break;
		case SR_Dimension_Texture2D:
		{
			if (isArray)
			{
				dsvdesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2DARRAY;
				dsvdesc.Texture2DArray.FirstArraySlice = aRenderTargetDesc.myArrayIndex;
				dsvdesc.Texture2DArray.ArraySize = aRenderTargetDesc.myArraySize;
				dsvdesc.Texture2DArray.MipSlice = aRenderTargetDesc.myMipLevel;
			}
			else
				dsvdesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
			break;
		}
		default:
			assert(false && "Dimension not supported.");
			break;
		}

		myDevice->CreateDepthStencilView(textureBuffer->myDX12Resource, &dsvdesc, renderTarget->myDepthStencilView);

		return renderTarget;
	}

	SC_Ref<SR_Buffer> SR_GraphicsDevice_DX12::_CreateBuffer(const SR_BufferDesc& aBufferDesc, void* aInitialData, const char* aIdentifier)
	{
		SC_UNUSED(aInitialData);
		SC_Ref<SR_Buffer_DX12> buffer(new SR_Buffer_DX12());

		D3D12_RESOURCE_DESC bufferDesc = {};
		bufferDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
		bufferDesc.Width = aBufferDesc.mySize;
		bufferDesc.Height = 1;
		bufferDesc.DepthOrArraySize = 1;
		bufferDesc.MipLevels = 1;
		bufferDesc.Format = DXGI_FORMAT_UNKNOWN;
		bufferDesc.SampleDesc.Count = 1;
		bufferDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
		bufferDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

		if (aBufferDesc.myIsWritable)
			bufferDesc.Flags |= D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;

		D3D12_RESOURCE_STATES initialState = D3D12_RESOURCE_STATE_COMMON;
		D3D12_HEAP_PROPERTIES heapProps = {};

		GetResourceHeapDesc_DX12(heapProps, initialState, aBufferDesc.myCPUAccess, aBufferDesc.myMemoryAccess);
		//uint alignment = SR_GetBufferAlignment_DX12(aBufferDesc);

		HRESULT result = S_OK;
		if (myIsUsingPlacementHeaps)
		{
			// Use Placed Resources instead of allocating new space for each resource.
		}
		else
		{
			result = myDevice->CreateCommittedResource(&heapProps, D3D12_HEAP_FLAG_NONE, &bufferDesc, initialState, nullptr, SR_IID_PPV_ARGS(&buffer->myDX12Resource));
		}

		if (FAILED(result))
		{
			SC_ERROR_LOG("Could not create buffer with id: %s", aIdentifier);
			return nullptr;
		}

#if !IS_XBOX_ONE_PLATFORM
		if (heapProps.Type == D3D12_HEAP_TYPE_UPLOAD)
		{
			D3D12_RANGE readRange = {};
			HRESULT hr = buffer->myDX12Resource->Map(0, &readRange, (void**)& buffer->myData);
			if (FAILED(hr))
				assert(false);
		}
#endif
		//
		// TODO: UPDATE INITIAL DATA HERE
		//
		//myContext->UpdateBufferData();
		if (aInitialData)
		{
			SR_BufferDesc uploadDesc(aBufferDesc);
			uploadDesc.myCPUAccess = SR_AccessCPU_Map_Write;

			SC_Ref<SR_Buffer> uploadBuffer = _CreateBuffer(uploadDesc);

			SR_GraphicsContext_DX12* ctx = static_cast<SR_GraphicsContext_DX12*>(SR_GraphicsDevice::GetDevice()->GetContext(SR_QueueType_Render));
			ctx->Begin();
			ctx->BeginRecording();

			D3D12_SUBRESOURCE_DATA vertexData = {};
			vertexData.pData = reinterpret_cast<BYTE*>(aInitialData);
			vertexData.RowPitch = aBufferDesc.mySize;
			vertexData.SlicePitch = aBufferDesc.mySize;
			UpdateSubresources(ctx->GetNativeCommandList(), buffer->myDX12Resource, uploadBuffer->myDX12Resource, 0, 0, 1, &vertexData);
			ctx->EndRecording();

			uint64 fence = SR_GraphicsQueueManager::GetNextExpectedFence(SR_QueueType_Render);
			ctx->InsertFence(fence);

			SC_GrowingArray<SC_Ref<SR_CommandList>> cls;
			cls.Add(ctx->GetCommandList());
			SR_GraphicsQueueManager::ExecuteCommandLists(cls, SR_QueueType_Render);
			SR_GraphicsQueueManager::Signal(fence, SR_QueueType_Render);
			ctx->End();
			SR_GraphicsQueueManager::WaitForSignal(fence, SR_QueueType_Render);
		}


		if (aIdentifier)
			buffer->myDX12Resource->SetName(ToWString(aIdentifier).c_str());

		// TODO: Reflect states from resource desc
		buffer->myDX12Tracking.myState = SR_ResourceState_GenericRead;
		return buffer;
	}

	SC_Ref<SR_BufferView> SR_GraphicsDevice_DX12::CreateBufferView(const SBufferViewDesc& aViewDesc, SR_Buffer* aBuffer)
	{
		SC_UNUSED(aViewDesc);
		SC_UNUSED(aBuffer);

		const SR_Buffer_DX12* buffer = static_cast<const SR_Buffer_DX12*>(aBuffer);
		const SR_BufferDesc& bufferProps = buffer->GetProperties();
		SC_UNUSED(bufferProps);

		SC_Ref<SR_BufferView_DX12> bufferView(new SR_BufferView_DX12());
		bufferView->_myBuffer = aBuffer; 
		bufferView->myDescriptorCPU = myResourceHeapCPU.AllocateCPU();

		if (aViewDesc.myIsShaderWritable)
		{
			D3D12_UNORDERED_ACCESS_VIEW_DESC uavdesc = {};
			uavdesc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;

			if (aViewDesc.myType == SBufferViewType_Bytes)
			{
				assert(!(aViewDesc.myFirstElement & 3));
				assert(!(aViewDesc.myNumElements & 3));

				uavdesc.Format = DXGI_FORMAT_R32_TYPELESS;
				uavdesc.Buffer.Flags = D3D12_BUFFER_UAV_FLAG_RAW;
				uavdesc.Buffer.FirstElement = aViewDesc.myFirstElement / 4;
				uavdesc.Buffer.NumElements = aViewDesc.myNumElements / 4;
			}
			else
			{
				if (aViewDesc.myType == SBufferViewType_Structured)
				{
					uavdesc.Format = DXGI_FORMAT_UNKNOWN;
					uavdesc.Buffer.StructureByteStride = buffer->GetProperties().myStructSize;
				}
				else
					uavdesc.Format = _ConvertFormatDX(aViewDesc.myFormat);

				uavdesc.Buffer.Flags = D3D12_BUFFER_UAV_FLAG_NONE;
				uavdesc.Buffer.FirstElement = aViewDesc.myFirstElement;
				uavdesc.Buffer.NumElements = aViewDesc.myNumElements;
			}

			myDevice->CreateUnorderedAccessView(buffer->myDX12Resource, nullptr, &uavdesc, bufferView->myDescriptorCPU);
		}
		else
		{
			D3D12_SHADER_RESOURCE_VIEW_DESC srvdesc = {};
			srvdesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
			srvdesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
			
			if (aViewDesc.myType == SBufferViewType_Bytes)
			{
				assert(!(aViewDesc.myFirstElement & 3));
				assert(!(aViewDesc.myNumElements & 3));

				srvdesc.Format = DXGI_FORMAT_R32_TYPELESS;
				srvdesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_RAW;
				srvdesc.Buffer.FirstElement = aViewDesc.myFirstElement / 4;
				srvdesc.Buffer.NumElements = aViewDesc.myNumElements / 4;
			}
			else
			{
				srvdesc.Buffer.NumElements = aViewDesc.myNumElements;

				if (aViewDesc.myType == SBufferViewType_Structured)
				{
					srvdesc.Format = DXGI_FORMAT_UNKNOWN;
					srvdesc.Buffer.StructureByteStride = buffer->GetProperties().myStructSize;
				}
				else
					srvdesc.Format = _ConvertFormatDX(aViewDesc.myFormat);

				srvdesc.Buffer.FirstElement = aViewDesc.myFirstElement;
			}

			myDevice->CreateShaderResourceView(buffer->myDX12Resource, &srvdesc, bufferView->myDescriptorCPU);
		}

		return bufferView;
	}

	SC_Ref<SR_ShaderState> SR_GraphicsDevice_DX12::CreateShaderState(const SR_ShaderStateDesc& aShaderStateDesc)
	{
		aShaderStateDesc;
		return nullptr;
		/*SC_Ref<SR_ShaderState_DX12> shaderState = new SR_ShaderState_DX12();
		shaderState->SetState(SC_LoadState::Loading);

		ID3D12PipelineState* pipelineState;
		if (aShaderStateDesc.myIsCompute)
		{
			D3D12_COMPUTE_PIPELINE_STATE_DESC psoDesc = {};
			psoDesc.CS.BytecodeLength = (SIZE_T)aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Compute]->GetByteCodeSize();
			psoDesc.CS.pShaderBytecode = aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Compute]->GetByteCode();
			psoDesc.pRootSignature = myRootSignatures[Compute]->myRootSignature;

			HRESULT hr = myDevice->CreateComputePipelineState(&psoDesc, SR_IID_PPV_ARGS(&pipelineState));
			if (FAILED(hr))
			{
				SC_ERROR_LOG("Could not create PSO");
				return nullptr;
			}

			shaderState->myRootSignature = myRootSignatures[Compute];
			shaderState->myComputePipelineState = pipelineState;
			shaderState->myIsCompute = true;
		}
		else
		{
			D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
			// Add Input Layout

			if (aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Vertex])
			{
				void* vsByteCode = aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Vertex]->GetByteCode();
				uint vsByteCodeSize = aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Vertex]->GetByteCodeSize();
				psoDesc.VS.BytecodeLength = (SIZE_T)vsByteCodeSize;
				psoDesc.VS.pShaderBytecode = vsByteCode;

				SC_GrowingArray<D3D12_INPUT_ELEMENT_DESC> inputElements;
				CVertexLayout vertexLayout;
				if (ReflectShaderVertexLayout(vsByteCode, vsByteCodeSize, &vertexLayout))
				{
					SC_GrowingArray<SVertexLayoutElement>& ilData = vertexLayout.myElements;
					inputElements.PreAllocate(ilData.Count());
					for (SVertexLayoutElement& element : ilData)
					{
						D3D12_INPUT_ELEMENT_DESC& elementDesc = inputElements.Add();
						elementDesc.SemanticName = element.mySemanticName.c_str();
						elementDesc.SemanticIndex = element.mySemanticIndex;
						elementDesc.InputSlot = element.myInputSlot;
						elementDesc.AlignedByteOffset = element.myAlignedByteOffset;
						elementDesc.InputSlotClass = _ConvertInputClassificationDX12(element.myInputSlotClass);
						elementDesc.Format = _ConvertFormatDX(element.myFormat);
						elementDesc.InstanceDataStepRate = element.myInstanceDataStepRate;
					}
					psoDesc.InputLayout.NumElements = inputElements.Count();
					psoDesc.InputLayout.pInputElementDescs = inputElements.GetBuffer();
				}
			}
#if SR_ENABLE_TESSELATION_SHADERS
			if (aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Hull])
			{
				psoDesc.HS.BytecodeLength = (SIZE_T)aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Hull]->GetByteCodeSize();
				psoDesc.HS.pShaderBytecode = aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Hull]->GetByteCode();
			}
			if (aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Domain])
			{
				psoDesc.DS.BytecodeLength = (SIZE_T)aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Domain]->GetByteCodeSize();
				psoDesc.DS.pShaderBytecode = aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Domain]->GetByteCode();
			}
#endif
#if SR_ENABLE_GEOMETRY_SHADERS
			if (aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Geometry])
			{
				psoDesc.GS.BytecodeLength = (SIZE_T)aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Geometry]->GetByteCodeSize();
				psoDesc.GS.pShaderBytecode = aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Geometry]->GetByteCode();
			}
#endif
			if (aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Pixel])
			{
				psoDesc.PS.BytecodeLength = (SIZE_T)aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Pixel]->GetByteCodeSize();
				psoDesc.PS.pShaderBytecode = aShaderStateDesc.myShaderByteCodes[SR_ShaderType_Pixel]->GetByteCode();
			}

			switch (aShaderStateDesc.myTopology)
			{
			default:
			case SR_Topology_TriangleList:
				psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
				break;
			case SR_Topology_PointList:
				psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT;
				break;
			case SR_Topology_LineList:
				psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_LINE;
				break;
			}
			psoDesc.NumRenderTargets = aShaderStateDesc.myRenderTargetFormats.myNumColorFormats;
			psoDesc.DSVFormat = _ConvertFormatDX(aShaderStateDesc.myRenderTargetFormats.myDepthFormat);
			for (uint i = 0; i < 8; ++i)
			{
				psoDesc.RTVFormats[i] = _ConvertFormatDX(aShaderStateDesc.myRenderTargetFormats.myColorFormats[i]);
			}

			const SR_DepthStateDesc& depthStateDesc = aShaderStateDesc.myDepthStateDesc;
			D3D12_DEPTH_STENCIL_DESC depthDesc;
			depthDesc.DepthEnable = (depthStateDesc.myDepthComparisonFunc != SR_ComparisonFunc_Always || depthStateDesc.myWriteDepth) ? true : false;
			depthDesc.DepthWriteMask = (depthStateDesc.myWriteDepth) ? D3D12_DEPTH_WRITE_MASK_ALL : D3D12_DEPTH_WRITE_MASK_ZERO;
			depthDesc.DepthFunc = GetComparisonFunc_DX12(depthStateDesc.myDepthComparisonFunc);
			depthDesc.StencilEnable = depthStateDesc.myEnableStencil;
			depthDesc.StencilReadMask = depthStateDesc.myStencilReadMask;
			depthDesc.StencilWriteMask = depthStateDesc.myStencilWriteMask;
			depthDesc.FrontFace.StencilFailOp = GetStencilOperator_DX12(depthStateDesc.myFrontFace.myFail);
			depthDesc.FrontFace.StencilDepthFailOp = GetStencilOperator_DX12(depthStateDesc.myFrontFace.myDepthFail);
			depthDesc.FrontFace.StencilPassOp = GetStencilOperator_DX12(depthStateDesc.myFrontFace.myPass);
			depthDesc.FrontFace.StencilFunc = GetComparisonFunc_DX12(depthStateDesc.myFrontFace.myStencilComparisonFunc);
			depthDesc.BackFace.StencilFailOp = GetStencilOperator_DX12(depthStateDesc.myBackFace.myFail);
			depthDesc.BackFace.StencilDepthFailOp = GetStencilOperator_DX12(depthStateDesc.myBackFace.myDepthFail);
			depthDesc.BackFace.StencilPassOp = GetStencilOperator_DX12(depthStateDesc.myBackFace.myPass);
			depthDesc.BackFace.StencilFunc = GetComparisonFunc_DX12(depthStateDesc.myBackFace.myStencilComparisonFunc);
			psoDesc.DepthStencilState = depthDesc;

			psoDesc.SampleDesc = DXGI_SAMPLE_DESC{ 1, 0 };
			psoDesc.SampleMask = 0xffffffff;

			const SR_RasterizerStateDesc& rasteriserStateDesc = aShaderStateDesc.myRasterizerStateDesc;
			D3D12_RASTERIZER_DESC rasterDesc;
			rasterDesc.FillMode = (rasteriserStateDesc.myWireframe) ? D3D12_FILL_MODE_WIREFRAME : D3D12_FILL_MODE_SOLID;
			rasterDesc.CullMode = GetRasterizerFaceCull_DX12(rasteriserStateDesc.myFaceCull);
			rasterDesc.FrontCounterClockwise = false;
			rasterDesc.DepthBias = rasteriserStateDesc.myDepthBias;
			rasterDesc.DepthBiasClamp = rasteriserStateDesc.myDepthBiasClamp;
			rasterDesc.SlopeScaledDepthBias = rasteriserStateDesc.mySlopedScaleDepthBias;
			rasterDesc.DepthClipEnable = rasteriserStateDesc.myEnableDepthClip;
			rasterDesc.MultisampleEnable = false;
			rasterDesc.AntialiasedLineEnable = false;
			rasterDesc.ForcedSampleCount = 0;
			rasterDesc.ConservativeRaster = (rasteriserStateDesc.myConservativeRaster && myFeatureSupport.myEnableConservativeRaster) ? D3D12_CONSERVATIVE_RASTERIZATION_MODE_ON : D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;
			psoDesc.RasterizerState = rasterDesc;

			const SR_BlendStateDesc& blendStateDesc = aShaderStateDesc.myBlendStateDesc;
			D3D12_BLEND_DESC blendDesc;
			blendDesc.AlphaToCoverageEnable = blendStateDesc.myAlphaToCoverage;
			blendDesc.IndependentBlendEnable = (blendStateDesc.myNumRenderTargets > 0);
			for (uint rtIndex = 0; rtIndex < 8  TODO: CHANGE TO MAX_RENDER_TARGETS DEFINE; ++rtIndex)
			{
				D3D12_RENDER_TARGET_BLEND_DESC& rtBlendDescDst = blendDesc.RenderTarget[rtIndex];
				const SR_RenderTargetBlendDesc& rtBlendDesc = blendStateDesc.myRenderTagetBlendDescs[rtIndex];
				rtBlendDescDst.BlendEnable = rtBlendDesc.myEnableBlend;
				rtBlendDescDst.BlendOp = GetBlendFunc_DX12(rtBlendDesc.myBlendFunc);
				rtBlendDescDst.BlendOpAlpha = GetBlendFunc_DX12(rtBlendDesc.myBlendFuncAlpha);
				rtBlendDescDst.SrcBlend = GetBlendMode_DX12(rtBlendDesc.mySrcBlend);
				rtBlendDescDst.SrcBlendAlpha = GetBlendMode_DX12(rtBlendDesc.mySrcBlendAlpha);
				rtBlendDescDst.DestBlend = GetBlendMode_DX12(rtBlendDesc.myDstBlend);
				rtBlendDescDst.DestBlendAlpha = GetBlendMode_DX12(rtBlendDesc.myDstBlendAlpha);
				rtBlendDescDst.RenderTargetWriteMask = GetColorWriteMask_DX12(rtBlendDesc.myWriteMask);
				rtBlendDescDst.LogicOp = D3D12_LOGIC_OP_NOOP;
				rtBlendDescDst.LogicOpEnable = false;

				if (!(rtBlendDescDst.RenderTargetWriteMask & D3D12_COLOR_WRITE_ENABLE_ALPHA))
				{
					rtBlendDescDst.SrcBlendAlpha = D3D12_BLEND_ZERO;
					rtBlendDescDst.DestBlendAlpha = D3D12_BLEND_ONE;
				}
			}
			psoDesc.BlendState = blendDesc;
			psoDesc.pRootSignature = myRootSignatures[Graphics]->myRootSignature;

			psoDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;

			HRESULT hr = myDevice->CreateGraphicsPipelineState(&psoDesc, SR_IID_PPV_ARGS(&pipelineState));
			if (FAILED(hr))
			{
				SC_ERROR_LOG("Could not create PSO");
				return nullptr;
			}
			shaderState->myRootSignature = myRootSignatures[Graphics];

			SR_PSOKey key;
			key.myBlendStateKey = aShaderStateDesc.myBlendStateDesc.GetKey();
			key.myDepthStateKey = aShaderStateDesc.myDepthStateDesc.GetKey();
			key.myRasterStateKey = aShaderStateDesc.myRasterizerStateDesc.GetKey();
			key.myRenderTargetsKey = aShaderStateDesc.myRenderTargetFormats.GetKey();
			key.myTopologyKey = SC_Hash(&aShaderStateDesc.myTopology, sizeof(aShaderStateDesc.myTopology));

			shaderState->myPipelineStates[key] = pipelineState;
		}

		shaderState->SetState(SC_LoadState::Loaded);
		return shaderState;*/
	}

	SC_Ref<SR_ShaderByteCode> SR_GraphicsDevice_DX12::CompileShader(const SR_ShaderDesc& aShaderDesc)
	{
		ID3DBlob* byteCode = nullptr;
		ID3DBlob* errorBuffer = nullptr;
		UINT flags = 0;

#ifndef _FINAL
		if (myDebugShaders)
			flags |= D3DCOMPILE_DEBUG;

		if (mySkipShaderOptimizations)
			flags |= D3DCOMPILE_SKIP_OPTIMIZATION;
		else
			switch (myShaderOptimizationLevel)
			{
			case 0:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL0;
				break;
			case 1:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL1;
				break;
			case 2:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL2;
				break;
			default:
			case 3:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
				break;
			}
#else
		flags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif

		std::string shaderType;
		switch (aShaderDesc.myShaderType)
		{
		case SR_ShaderType_Vertex:
			shaderType = "vs";
			break;
		case SR_ShaderType_Pixel:
			shaderType = "ps";
			break;

#if SR_ENABLE_GEOMETRY_SHADERS
		case SR_ShaderType_Geometry:
			shaderType = "gs";
			break;
#endif

#if SR_ENABLE_TESSELATION_SHADERS
		case SR_ShaderType_Hull:
			shaderType = "hs";
			break;
		case SR_ShaderType_Domain:
			shaderType = "ds";
			break;
#endif

		case SR_ShaderType_Compute:
			shaderType = "cs";
			break;
		default:
			assert(false && "Unknown shader type");
		}

		HRESULT hr = D3DCompileFromFile(
			std::wstring(aShaderDesc.myFilePath.begin(), aShaderDesc.myFilePath.end()).c_str(),
			nullptr,
			D3D_COMPILE_STANDARD_FILE_INCLUDE,
			aShaderDesc.myEntryPoint.c_str(),
			(shaderType + globalShaderModel).c_str(),
			flags,
			0,
			&byteCode,
			&errorBuffer
		);

		if (FAILED(hr))
		{
			char* errorMsg = nullptr;
			if (errorBuffer)
				errorMsg = (char*)(errorBuffer->GetBufferPointer());

			SC_ERROR_LOG("Could not compile shader from file (%s)", aShaderDesc.myFilePath.c_str());
			if (errorMsg)
				SC_ERROR_LOG("Shader compilation error: [%s]", errorMsg);
		}

		SC_Ref<SR_ShaderByteCode> shaderByteCode = new SR_ShaderByteCode();
		size_t bufferSize = byteCode->GetBufferSize();
		shaderByteCode->myByteCodeSize = (unsigned int)bufferSize;
		shaderByteCode->myByteCode = new uint8[bufferSize];
		memcpy(shaderByteCode->myByteCode, byteCode->GetBufferPointer(), bufferSize);

		byteCode->Release();
		errorBuffer->Release();

		return shaderByteCode;
	}
	SC_PRAGMA_DEOPTIMIZE
	bool SR_GraphicsDevice_DX12::ReflectShaderVertexLayout(void* aVertexShaderByteCode, uint aByteCodeSize, CVertexLayout* aOutLayout)
	{
		ID3D12ShaderReflection* shaderReflection = nullptr;
		HRESULT hr = D3DReflect(aVertexShaderByteCode, aByteCodeSize, SR_IID_PPV_ARGS(&shaderReflection));
		if (FAILED(hr))
		{
			SC_ERROR_LOG("Could not reflect vertex layout on shader");
			return false;
		}

		D3D12_SHADER_DESC shaderDesc;
		shaderReflection->GetDesc(&shaderDesc);

		for (unsigned int i = 0; i < shaderDesc.InputParameters; i++)
		{
			D3D12_SIGNATURE_PARAMETER_DESC paramDesc;
			shaderReflection->GetInputParameterDesc(i, &paramDesc);

			SVertexLayoutElement& elementDesc = aOutLayout->myElements.Add();
			elementDesc.mySemanticName = paramDesc.SemanticName;
			elementDesc.mySemanticIndex = paramDesc.SemanticIndex;
			if (i == 0)
				elementDesc.myAlignedByteOffset = 0;
			else
				elementDesc.myAlignedByteOffset = D3D12_APPEND_ALIGNED_ELEMENT;

			std::string semantic(elementDesc.mySemanticName);
			std::string instancePrefix("INSTANCE_");
			if (semantic.substr(0, instancePrefix.size()) == instancePrefix)
			{
				elementDesc.myInputSlotClass = SVertexLayoutElement::EInputClassification::INPUT_PER_INSTANCE_DATA;
				elementDesc.myInstanceDataStepRate = 1;
				elementDesc.myInputSlot = 1;
			}
			else
			{
				elementDesc.myInputSlotClass = SVertexLayoutElement::EInputClassification::INPUT_PER_VERTEX_DATA;
				elementDesc.myInstanceDataStepRate = 0;
				elementDesc.myInputSlot = 0;
			}

			if (paramDesc.Mask == 1)
			{
				if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) elementDesc.myFormat = SR_Format_R32_Uint;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) elementDesc.myFormat = SR_Format_R32_Sint;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) elementDesc.myFormat = SR_Format_R32_Float;
			}
			else if (paramDesc.Mask <= 3)
			{
				if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) elementDesc.myFormat = SR_Format_RG32_Uint;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) elementDesc.myFormat = SR_Format_RG32_Sint;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) elementDesc.myFormat = SR_Format_RG32_Float;
			}
			else if (paramDesc.Mask <= 7)
			{
				if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) elementDesc.myFormat = SR_Format_RGB32_Uint;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) elementDesc.myFormat = SR_Format_RGB32_Sint;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) elementDesc.myFormat = SR_Format_RGB32_Float;
			}
			else if (paramDesc.Mask <= 15)
			{
				if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32) elementDesc.myFormat = SR_Format_RGBA32_Uint;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32) elementDesc.myFormat = SR_Format_RGBA32_Sint;
				else if (paramDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32) elementDesc.myFormat = SR_Format_RGBA32_Float;
			}
		}

		shaderReflection->Release();
		return true;
	}

	CVertexLayout* SR_GraphicsDevice_DX12::CreateVertexLayout(const CVertexShader* aVS)
	{
		CVertexLayout* inputLayout = new CVertexLayout();
		ReflectShaderVertexLayout(aVS->GetByteCode(), aVS->GetByteCodeSize(), inputLayout);
		return inputLayout;
	}

	CVertexShader * SR_GraphicsDevice_DX12::CreateVertexShader(const std::string& aFilePath, const std::string& aEntryPoint)
	{
		CVertexShader* vs = new CVertexShader();

		ID3DBlob* vertexShader = nullptr;
		ID3DBlob* errorBuffer = nullptr;
		UINT flags = 0;

		if(myDebugShaders)
			flags |= D3DCOMPILE_DEBUG;

		if (mySkipShaderOptimizations)
			flags |= D3DCOMPILE_SKIP_OPTIMIZATION;
		else
			switch (myShaderOptimizationLevel)
			{
			case 0:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL0;
				break;
			case 1:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL1;
				break;
			case 2:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL2;
				break;
			default:
			case 3:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
				break;

			}
#ifdef _FINAL
		flags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif
		HRESULT hr = D3DCompileFromFile(
			std::wstring(aFilePath.begin(), aFilePath.end()).c_str(),
			nullptr,
			D3D_COMPILE_STANDARD_FILE_INCLUDE,
			aEntryPoint.c_str(),
			(std::string("vs") + globalShaderModel).c_str(),
			flags,
			0,
			&vertexShader,
			&errorBuffer
		);

		if (FAILED(hr))
		{
			char* errorMSG = (char*)errorBuffer->GetBufferPointer();
			OutputDebugStringA(errorMSG);
			SC_ERROR_LOG(errorMSG);
			delete vs;
			return nullptr;
		}

		vs->myByteCode = (unsigned char*)vertexShader->GetBufferPointer();
		vs->myByteCodeSize = (unsigned int)vertexShader->GetBufferSize();
		vs->myEntryPoint = aEntryPoint.c_str();
		return vs;
	}

	bool SR_GraphicsDevice_DX12::CreatePixelShader(const std::string& aShaderCode, CPixelShader* aOutShader)
	{
		ID3DBlob* vertexShader = nullptr;
		ID3DBlob* errorBuffer = nullptr;
		UINT flags = 0;

		if (myDebugShaders)
			flags |= D3DCOMPILE_DEBUG;

		if (mySkipShaderOptimizations)
			flags |= D3DCOMPILE_SKIP_OPTIMIZATION;
		else
			switch (myShaderOptimizationLevel)
			{
			case 0:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL0;
				break;
			case 1:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL1;
				break;
			case 2:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL2;
				break;
			default:
			case 3:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
				break;

			}
#ifdef _FINAL
		flags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif
		HRESULT hr = D3DCompile(
			aShaderCode.c_str(),
			aShaderCode.size(),
			"..\\Data\\Shaders\\",
			nullptr,
			D3D_COMPILE_STANDARD_FILE_INCLUDE,
			"main",
			(std::string("ps") + globalShaderModel).c_str(),
			flags,
			0,
			&vertexShader,
			&errorBuffer
		);

		if (FAILED(hr))
		{
			char* errorMSG = (char*)errorBuffer->GetBufferPointer();
			OutputDebugStringA(errorMSG);
			SC_ERROR_LOG(errorMSG);
			return false;
		}

		aOutShader->myByteCode = (unsigned char*)vertexShader->GetBufferPointer();
		aOutShader->myByteCodeSize = (unsigned int)vertexShader->GetBufferSize();
		aOutShader->myEntryPoint = "main";

		return true;
	}

	CPixelShader * SR_GraphicsDevice_DX12::CreatePixelShader(const std::string & aFilePath, const std::string & aEntryPoint)
	{
		CPixelShader* ps = new CPixelShader();

		ID3DBlob* pixelShader = nullptr;
		ID3DBlob* errorBuffer = nullptr;
		UINT flags = 0;

#ifndef _FINAL
		if (myDebugShaders)
			flags |= D3DCOMPILE_DEBUG;

		if (mySkipShaderOptimizations)
			flags |= D3DCOMPILE_SKIP_OPTIMIZATION;
		else
			switch (myShaderOptimizationLevel)
			{
			case 0:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL0;
				break;
			case 1:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL1;
				break;
			case 2:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL2;
				break;
			default:
			case 3:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
				break;
			}
#else
		flags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif

		HRESULT hr = D3DCompileFromFile(
			std::wstring(aFilePath.begin(), aFilePath.end()).c_str(),
			nullptr,
			D3D_COMPILE_STANDARD_FILE_INCLUDE,
			aEntryPoint.c_str(),
			(std::string("ps") + globalShaderModel).c_str(),
			flags,
			0,
			&pixelShader,
			&errorBuffer
		);

		if (FAILED(hr))
		{
			char* errorMSG = (char*)errorBuffer->GetBufferPointer();
			OutputDebugStringA(errorMSG);
			SC_ERROR_LOG(errorMSG);
			delete ps;
			return nullptr;
		}

		ps->myByteCode = (unsigned char*)pixelShader->GetBufferPointer();
		ps->myByteCodeSize = (unsigned int)pixelShader->GetBufferSize();
		ps->myEntryPoint = aEntryPoint.c_str();
		return ps;
	}

	CGeometryShader * SR_GraphicsDevice_DX12::CreateGeometryShader(const std::string& aFilePath, const std::string& aEntryPoint)
	{
		CGeometryShader* gs = new CGeometryShader();

		ID3DBlob* geometryShader = nullptr;
		ID3DBlob* errorBuffer = nullptr;
		UINT flags = 0;

#ifndef _FINAL
		if (myDebugShaders)
			flags |= D3DCOMPILE_DEBUG;

		if (mySkipShaderOptimizations)
			flags |= D3DCOMPILE_SKIP_OPTIMIZATION;
		else
			switch (myShaderOptimizationLevel)
			{
			case 0:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL0;
				break;
			case 1:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL1;
				break;
			case 2:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL2;
				break;
			default:
			case 3:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
				break;
			}
#else
		flags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif

		HRESULT hr = D3DCompileFromFile(
			std::wstring(aFilePath.begin(), aFilePath.end()).c_str(),
			nullptr,
			D3D_COMPILE_STANDARD_FILE_INCLUDE,
			aEntryPoint.c_str(),
			(std::string("gs") + globalShaderModel).c_str(),
			flags,
			0,
			&geometryShader,
			&errorBuffer
		);

		if (FAILED(hr))
		{
			char* errorMSG = (char*)errorBuffer->GetBufferPointer();
			OutputDebugStringA(errorMSG);
			SC_ERROR_LOG(errorMSG);
			delete gs;
			return nullptr;
		}

		gs->myByteCode = (unsigned char*)geometryShader->GetBufferPointer();
		gs->myByteCodeSize = (unsigned int)geometryShader->GetBufferSize();
		gs->myEntryPoint = aEntryPoint.c_str();
		return gs;
	}

	CComputeShader * SR_GraphicsDevice_DX12::CreateComputeShader(const std::string& aFilePath, const std::string& aEntryPoint)
	{
		CComputeShader* cs = new CComputeShader();

		ID3DBlob* computeShader = nullptr;
		ID3DBlob* errorBuffer = nullptr;
		UINT flags = 0;

#ifndef _FINAL
		if (myDebugShaders)
			flags |= D3DCOMPILE_DEBUG;

		if (mySkipShaderOptimizations)
			flags |= D3DCOMPILE_SKIP_OPTIMIZATION;
		else
			switch (myShaderOptimizationLevel)
			{
			case 0:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL0;
				break;
			case 1:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL1;
				break;
			case 2:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL2;
				break;
			default:
			case 3:
				flags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
				break;
			}
#else
		flags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif

		HRESULT hr = D3DCompileFromFile(
			std::wstring(aFilePath.begin(), aFilePath.end()).c_str(),
			nullptr,
			D3D_COMPILE_STANDARD_FILE_INCLUDE,
			aEntryPoint.c_str(),
			(std::string("cs") + globalShaderModel).c_str(),
			flags,
			0,
			&computeShader,
			&errorBuffer
		);

		if (FAILED(hr))
		{
			char* errorMSG = (char*)errorBuffer->GetBufferPointer();
			OutputDebugStringA(errorMSG);
			SC_ERROR_LOG(errorMSG);
			delete cs;
			return nullptr;
		}

		cs->myByteCode = (unsigned char*)computeShader->GetBufferPointer();
		cs->myByteCodeSize = (unsigned int)computeShader->GetBufferSize();
		cs->myEntryPoint = aEntryPoint.c_str();
		return cs;
	}

	CGraphicsPSO* SR_GraphicsDevice_DX12::CreateGraphicsPSO(const SGraphicsPSODesc & aPSODesc)
	{
		CGraphicsPSO* pso = new CGraphicsPSO();
		if (!CreateGraphicsPSO(aPSODesc, pso))
		{
			delete pso;
			return nullptr;
		}

		return pso;
	}

	bool SR_GraphicsDevice_DX12::CreateGraphicsPSO(const SGraphicsPSODesc& aPSODesc, CGraphicsPSO* aDestinationPSO)
	{
		aDestinationPSO->SetState(Loading);
		D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
		ZeroMemory(&psoDesc, sizeof(psoDesc));

		// Shaders
		CVertexShader* vs = nullptr;
		if (!aPSODesc.myVertexShader.empty())
		{
			vs = CreateVertexShader(aPSODesc.myVertexShader, "main");
			if (vs)
			{
				D3D12_SHADER_BYTECODE vertexShaderBytecode = {};
				vertexShaderBytecode.BytecodeLength = vs->myByteCodeSize;
				vertexShaderBytecode.pShaderBytecode = vs->myByteCode;
				psoDesc.VS = vertexShaderBytecode;
			}
		}
		if (!aPSODesc.myGeometryShader.empty())
		{
			CGeometryShader* gs = CreateGeometryShader(aPSODesc.myGeometryShader, "main");
			if (gs)
			{
				D3D12_SHADER_BYTECODE geometryShaderBytecode = {};
				geometryShaderBytecode.BytecodeLength = gs->myByteCodeSize;
				geometryShaderBytecode.pShaderBytecode = gs->myByteCode;
				psoDesc.GS = geometryShaderBytecode;
			}
		}
		if (!aPSODesc.myPixelShader.empty())
		{
			CPixelShader* ps = CreatePixelShader(aPSODesc.myPixelShader, "main");
			if (ps)
			{
				D3D12_SHADER_BYTECODE pixelShaderBytecode = {};
				pixelShaderBytecode.BytecodeLength = ps->myByteCodeSize;
				pixelShaderBytecode.pShaderBytecode = ps->myByteCode;
				psoDesc.PS = pixelShaderBytecode;
			}
		}

		// Input Layout

		SC_GrowingArray<std::string> inputSemantics;
		SC_GrowingArray<D3D12_INPUT_ELEMENT_DESC> inputElements;
		CVertexLayout* layout = CreateVertexLayout(vs);
		if (layout != nullptr)
		{
			SC_GrowingArray<SVertexLayoutElement>& ilData = layout->myElements;
			inputElements.PreAllocate(ilData.Count());
			inputSemantics.PreAllocate(ilData.Count());
			for (SVertexLayoutElement& element : ilData)
			{
				D3D12_INPUT_ELEMENT_DESC& elementDesc = inputElements.Add();
				inputSemantics.Add(element.mySemanticName); // TODO: Hack to not loose strings before creation happens
				elementDesc.SemanticName = inputSemantics.GetLast().c_str();
				elementDesc.SemanticIndex = element.mySemanticIndex;
				elementDesc.InputSlot = element.myInputSlot;
				elementDesc.AlignedByteOffset = element.myAlignedByteOffset;
				elementDesc.InputSlotClass = _ConvertInputClassificationDX12(element.myInputSlotClass);
				elementDesc.Format = _ConvertFormatDX(element.myFormat);
				elementDesc.InstanceDataStepRate = element.myInstanceDataStepRate;
			}
			psoDesc.InputLayout.NumElements = inputElements.Count();
			psoDesc.InputLayout.pInputElementDescs = inputElements.GetBuffer();
		}
		delete layout;

		// TOPOLOGY
		switch (aPSODesc.topology)
		{
		default:
		case SR_Topology_TriangleList:
			psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
			break;
		case SR_Topology_PointList:
			psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT;
			break;
		case SR_Topology_LineList:
			psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_LINE;
			break;
		}

		psoDesc.NumRenderTargets = aPSODesc.myRenderTargetFormats.myNumColorFormats;
		psoDesc.DSVFormat = _ConvertFormatDX(aPSODesc.myRenderTargetFormats.myDepthFormat);
		for (uint i = 0; i < 8; ++i)
		{
			psoDesc.RTVFormats[i] = _ConvertFormatDX(aPSODesc.myRenderTargetFormats.myColorFormats[i]);
		}

		D3D12_DEPTH_STENCIL_DESC depthDesc;
		depthDesc.DepthEnable = (aPSODesc.myDepthStateDesc.myDepthComparisonFunc != SR_ComparisonFunc_Always || aPSODesc.myDepthStateDesc.myWriteDepth) ? true : false;
		depthDesc.DepthWriteMask = (aPSODesc.myDepthStateDesc.myWriteDepth) ? D3D12_DEPTH_WRITE_MASK_ALL : D3D12_DEPTH_WRITE_MASK_ZERO;
		depthDesc.DepthFunc = GetComparisonFunc_DX12(aPSODesc.myDepthStateDesc.myDepthComparisonFunc);
		depthDesc.StencilEnable = aPSODesc.myDepthStateDesc.myEnableStencil;
		depthDesc.StencilReadMask = aPSODesc.myDepthStateDesc.myStencilReadMask;
		depthDesc.StencilWriteMask = aPSODesc.myDepthStateDesc.myStencilWriteMask;
		depthDesc.FrontFace.StencilFailOp = GetStencilOperator_DX12(aPSODesc.myDepthStateDesc.myFrontFace.myFail);
		depthDesc.FrontFace.StencilDepthFailOp = GetStencilOperator_DX12(aPSODesc.myDepthStateDesc.myFrontFace.myDepthFail);
		depthDesc.FrontFace.StencilPassOp = GetStencilOperator_DX12(aPSODesc.myDepthStateDesc.myFrontFace.myPass);
		depthDesc.FrontFace.StencilFunc = GetComparisonFunc_DX12(aPSODesc.myDepthStateDesc.myFrontFace.myStencilComparisonFunc);
		depthDesc.BackFace.StencilFailOp = GetStencilOperator_DX12(aPSODesc.myDepthStateDesc.myBackFace.myFail);
		depthDesc.BackFace.StencilDepthFailOp = GetStencilOperator_DX12(aPSODesc.myDepthStateDesc.myBackFace.myDepthFail);
		depthDesc.BackFace.StencilPassOp = GetStencilOperator_DX12(aPSODesc.myDepthStateDesc.myBackFace.myPass);
		depthDesc.BackFace.StencilFunc = GetComparisonFunc_DX12(aPSODesc.myDepthStateDesc.myBackFace.myStencilComparisonFunc);
		psoDesc.DepthStencilState = depthDesc;


		psoDesc.SampleDesc = DXGI_SAMPLE_DESC{ 1, 0 };
		psoDesc.SampleMask = 0xffffffff;

		D3D12_RASTERIZER_DESC rasterDesc;
		rasterDesc.FillMode = (aPSODesc.myRasterizerStateDesc.myWireframe) ? D3D12_FILL_MODE_WIREFRAME : D3D12_FILL_MODE_SOLID;
		rasterDesc.CullMode = GetRasterizerFaceCull_DX12(aPSODesc.myRasterizerStateDesc.myFaceCull);
		rasterDesc.FrontCounterClockwise = false;
		rasterDesc.DepthBias = aPSODesc.myRasterizerStateDesc.myDepthBias;
		rasterDesc.DepthBiasClamp = aPSODesc.myRasterizerStateDesc.myDepthBiasClamp;
		rasterDesc.SlopeScaledDepthBias = aPSODesc.myRasterizerStateDesc.mySlopedScaleDepthBias;
		rasterDesc.DepthClipEnable = aPSODesc.myRasterizerStateDesc.myEnableDepthClip;
		rasterDesc.MultisampleEnable = false;
		rasterDesc.AntialiasedLineEnable = false;
		rasterDesc.ForcedSampleCount = 0;
		rasterDesc.ConservativeRaster = (aPSODesc.myRasterizerStateDesc.myConservativeRaster && myFeatureSupport.myEnableConservativeRaster) ? D3D12_CONSERVATIVE_RASTERIZATION_MODE_ON : D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;
		psoDesc.RasterizerState = rasterDesc;

		D3D12_BLEND_DESC blendDesc;
		blendDesc.AlphaToCoverageEnable = aPSODesc.myBlendStateDesc.myAlphaToCoverage;
		blendDesc.IndependentBlendEnable = (aPSODesc.myBlendStateDesc.myNumRenderTargets > 0);
		for (uint rtIndex = 0; rtIndex < 8 /* TODO: CHANGE TO MAX_RENDER_TARGETS DEFINE*/; ++rtIndex)
		{
			D3D12_RENDER_TARGET_BLEND_DESC& rtBlendDescDst = blendDesc.RenderTarget[rtIndex];
			const SR_RenderTargetBlendDesc& rtBlendDesc = aPSODesc.myBlendStateDesc.myRenderTagetBlendDescs[rtIndex];
			rtBlendDescDst.BlendEnable = rtBlendDesc.myEnableBlend;
			rtBlendDescDst.BlendOp = GetBlendFunc_DX12(rtBlendDesc.myBlendFunc);
			rtBlendDescDst.BlendOpAlpha = GetBlendFunc_DX12(rtBlendDesc.myBlendFuncAlpha);
			rtBlendDescDst.SrcBlend = GetBlendMode_DX12(rtBlendDesc.mySrcBlend);
			rtBlendDescDst.SrcBlendAlpha = GetBlendMode_DX12(rtBlendDesc.mySrcBlendAlpha);
			rtBlendDescDst.DestBlend = GetBlendMode_DX12(rtBlendDesc.myDstBlend);
			rtBlendDescDst.DestBlendAlpha = GetBlendMode_DX12(rtBlendDesc.myDstBlendAlpha);
			rtBlendDescDst.RenderTargetWriteMask = GetColorWriteMask_DX12(rtBlendDesc.myWriteMask);
			rtBlendDescDst.LogicOp = D3D12_LOGIC_OP_NOOP;
			rtBlendDescDst.LogicOpEnable = false;

			if (!(rtBlendDescDst.RenderTargetWriteMask & D3D12_COLOR_WRITE_ENABLE_ALPHA))
			{
				rtBlendDescDst.SrcBlendAlpha = D3D12_BLEND_ZERO;
				rtBlendDescDst.DestBlendAlpha = D3D12_BLEND_ONE;
			}
		}
		psoDesc.BlendState = blendDesc; // a default blent state.
		psoDesc.pRootSignature = myGraphicsRootSignature;

		psoDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;

		ID3D12PipelineState* pipelineState;
		HRESULT hr = myDevice->CreateGraphicsPipelineState(&psoDesc, SR_IID_PPV_ARGS(&pipelineState));
		aDestinationPSO->myPipelineState = pipelineState;
		aDestinationPSO->myProperties = aPSODesc;
		if (FAILED(hr))
		{
			aDestinationPSO->SetState(Unloaded);
			return false;
		}
		return true;
	}

	bool SR_GraphicsDevice_DX12::CreateComputePSO(const SComputePSODesc& aPSODesc, CComputePSO* aDestinationPSO)
	{
		D3D12_COMPUTE_PIPELINE_STATE_DESC psoDesc = {};
		ZeroMemory(&psoDesc, sizeof(psoDesc));

		if (!aPSODesc.myComputeShader.empty())
		{
			CComputeShader* cs = CreateComputeShader(aPSODesc.myComputeShader, "main");

			if (cs)
			{
				D3D12_SHADER_BYTECODE vertexShaderBytecode = {};
				vertexShaderBytecode.BytecodeLength = cs->myByteCodeSize;
				vertexShaderBytecode.pShaderBytecode = cs->myByteCode;
				psoDesc.CS = vertexShaderBytecode;
			}
		}

		psoDesc.pRootSignature = myComputeRootSignature;

		// TODO: add additional description here


		ID3D12PipelineState* pipelineState;
		HRESULT hr = myDevice->CreateComputePipelineState(&psoDesc, SR_IID_PPV_ARGS(&pipelineState));
		aDestinationPSO->myPipelineState = pipelineState;
		aDestinationPSO->myProperties = aPSODesc;

		if (FAILED(hr))
		{
			aDestinationPSO->SetState(Unloaded);
			return false;
		}

		return true;
	}

	const bool SR_GraphicsDevice_DX12::CreateVertexBuffer(const SVertexBufferDesc& aVBDesc, CVertexBuffer* aDestinationBuffer)
	{
		if (aDestinationBuffer == nullptr)
		{
			return false;
		}

		int vBufferSize = aVBDesc.myStride * aVBDesc.myNumVertices;

		SR_Buffer_DX12* buffer = new SR_Buffer_DX12();
		ID3D12Resource* vertexBuffer;
		HRESULT hr = S_OK;
		{
			auto heapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
			auto buf = CD3DX12_RESOURCE_DESC::Buffer(vBufferSize);
			hr = myDevice->CreateCommittedResource(
				&heapProperties,
				D3D12_HEAP_FLAG_NONE,
				&buf,
				D3D12_RESOURCE_STATE_COPY_DEST,
				nullptr,
				SR_IID_PPV_ARGS(&vertexBuffer));

			if (FAILED(hr))
			{
				return false;
			}
			vertexBuffer->SetName(L"Vertex Buffer Resource Heap");
		}
		if (aVBDesc.myVertexData != nullptr)
		{
			ID3D12Resource* vBufferUploadHeap;
			{
				auto heapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
				auto buf = CD3DX12_RESOURCE_DESC::Buffer(vBufferSize);
				hr = myDevice->CreateCommittedResource(
					&heapProperties,
					D3D12_HEAP_FLAG_NONE,
					&buf,
					D3D12_RESOURCE_STATE_GENERIC_READ,
					nullptr,
					SR_IID_PPV_ARGS(&vBufferUploadHeap));

				if (FAILED(hr))
				{
					return false;
				}
				vBufferUploadHeap->SetName(L"Vertex Buffer Upload Resource Heap");
			}

			D3D12_SUBRESOURCE_DATA vertexData = {};
			vertexData.pData = reinterpret_cast<BYTE*>(aVBDesc.myVertexData);
			vertexData.RowPitch = vBufferSize;
			vertexData.SlicePitch = vBufferSize;

			SR_GraphicsContext_DX12* ctx = static_cast<SR_GraphicsContext_DX12*>(SR_GraphicsDevice::GetDevice()->GetContext(SR_QueueType_Copy));
			ctx->Begin();
			ctx->BeginRecording();

			UpdateSubresources(ctx->GetNativeCommandList(), vertexBuffer, vBufferUploadHeap, 0, 0, 1, &vertexData);
			++ctx->myNumCommandsSinceReset;
			ctx->EndRecording();

			uint64 fence = SR_GraphicsQueueManager::GetNextExpectedFence(SR_QueueType_Copy);
			ctx->InsertFence(fence);

			SC_GrowingArray<SC_Ref<SR_CommandList>> cls;
			cls.Add(ctx->GetCommandList());
			SR_GraphicsQueueManager::ExecuteCommandLists(cls, SR_QueueType_Copy);
			SR_GraphicsQueueManager::Signal(fence, SR_QueueType_Copy);
			ctx->End();
			SR_GraphicsQueueManager::WaitForSignal(fence, SR_QueueType_Copy);
			DeleteResource(vBufferUploadHeap);
		}

		auto vBufferView = new D3D12_VERTEX_BUFFER_VIEW;
		vBufferView->BufferLocation = vertexBuffer->GetGPUVirtualAddress();
		vBufferView->StrideInBytes = (UINT)aVBDesc.myStride;
		vBufferView->SizeInBytes = vBufferSize;
		aDestinationBuffer->myVertexBufferView = vBufferView;


		//aDestinationBuffer->myVertexBufferResource = vertexBuffer;
		aDestinationBuffer->myNumVertices = aVBDesc.myNumVertices;
		aDestinationBuffer->myDescription = aVBDesc;
		buffer->myDX12Tracking.myState = static_cast<SR_ResourceState>(D3D12_RESOURCE_STATE_COPY_DEST);
		buffer->myDX12Resource = vertexBuffer;
		buffer->myDescription.mySize = aVBDesc.myStride * aVBDesc.myNumVertices;
		aDestinationBuffer->myBuffer = buffer;
		myUsedVRAM += vBufferSize;
		return true;
	}

	const bool SR_GraphicsDevice_DX12::CreateIndexBuffer(const std::vector<unsigned int>& aIndexList, CIndexBuffer* aDestinationBuffer)
	{
		return CreateIndexBuffer(aIndexList.data(), static_cast<uint>(aIndexList.size()), aDestinationBuffer);
	}
	const bool SR_GraphicsDevice_DX12::CreateIndexBuffer(const uint* aIndexList, uint aNumIndices, CIndexBuffer* aDestinationBuffer)
	{
		if (aDestinationBuffer == nullptr)
		{
			return false;
		}

		SR_Buffer_DX12* buffer = new SR_Buffer_DX12();
		unsigned int iBufferSize = aNumIndices * sizeof(uint);
		HRESULT hr = S_OK; 
		ID3D12Resource* indexBuffer;
		{
			auto heapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
			auto buf = CD3DX12_RESOURCE_DESC::Buffer(iBufferSize);

			hr = myDevice->CreateCommittedResource(
				&heapProperties,
				D3D12_HEAP_FLAG_NONE,
				&buf,
				D3D12_RESOURCE_STATE_COPY_DEST,
				nullptr,
				SR_IID_PPV_ARGS(&indexBuffer));

			if (FAILED(hr))
			{
				delete buffer;
				return false;
			}

			indexBuffer->SetName(L"Index Buffer Resource Heap");
		}

		ID3D12Resource* indexUploadHeap;
		{
			auto heapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
			auto buf = CD3DX12_RESOURCE_DESC::Buffer(iBufferSize);

			hr = myDevice->CreateCommittedResource(
				&heapProperties,
				D3D12_HEAP_FLAG_NONE,
				&buf,
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				SR_IID_PPV_ARGS(&indexUploadHeap));

			if (FAILED(hr))
			{
				delete buffer;
				return false;
			}

			indexUploadHeap->SetName(L"Index Buffer Resource Heap");
		}

		D3D12_SUBRESOURCE_DATA indexData = {};
		indexData.pData = reinterpret_cast<const BYTE*>(aIndexList);
		indexData.RowPitch = iBufferSize;
		indexData.SlicePitch = iBufferSize;

		SR_GraphicsContext_DX12* ctx = static_cast<SR_GraphicsContext_DX12*>(SR_GraphicsDevice::GetDevice()->GetContext(SR_QueueType_Copy));
		ctx->Begin();
		ctx->BeginRecording();

		UpdateSubresources(ctx->GetNativeCommandList(), indexBuffer, indexUploadHeap, 0, 0, 1, &indexData);
		++ctx->myNumCommandsSinceReset;
		ctx->EndRecording();

		uint64 fence = SR_GraphicsQueueManager::GetNextExpectedFence(SR_QueueType_Copy);
		ctx->InsertFence(fence);

		SC_GrowingArray<SC_Ref<SR_CommandList>> cls;
		cls.Add(ctx->GetCommandList());
		SR_GraphicsQueueManager::ExecuteCommandLists(cls, SR_QueueType_Copy);
		SR_GraphicsQueueManager::Signal(fence, SR_QueueType_Copy);
		ctx->End();
		SR_GraphicsQueueManager::WaitForSignal(fence, SR_QueueType_Copy);

		auto indexBufferView = new D3D12_INDEX_BUFFER_VIEW;
		indexBufferView->BufferLocation = indexBuffer->GetGPUVirtualAddress();
		indexBufferView->Format = DXGI_FORMAT_R32_UINT;
		indexBufferView->SizeInBytes = iBufferSize;
		aDestinationBuffer->myIndexBufferView = indexBufferView;

		aDestinationBuffer->myNumIndices = aNumIndices;
		//aDestinationBuffer->myIndexBufferResource = indexBuffer;

		buffer->myDX12Tracking.myState = static_cast<SR_ResourceState>(D3D12_RESOURCE_STATE_COPY_DEST);
		buffer->myDX12Resource = indexBuffer;
		buffer->myDescription.mySize = iBufferSize;
		aDestinationBuffer->myBuffer = buffer;

		DeleteResource(indexUploadHeap);
		myUsedVRAM += iBufferSize;
		return true;
	}

	SR_Buffer* SR_GraphicsDevice_DX12::CreateBuffer(const SR_BufferDesc& aBufferDesc, const char* aIdentifier)
	{
		SR_Buffer_DX12* buffer = new SR_Buffer_DX12();

		HRESULT hr = S_OK;
		ID3D12Resource* resource;
		{
			D3D12_HEAP_TYPE type = (aBufferDesc.myCPUAccess == SR_AccessCPU_Map_Read) ? D3D12_HEAP_TYPE_READBACK : D3D12_HEAP_TYPE_DEFAULT;

			auto heapProperties = CD3DX12_HEAP_PROPERTIES(type);
			auto bufferDesc = CD3DX12_RESOURCE_DESC::Buffer(aBufferDesc.mySize);
			hr = myDevice->CreateCommittedResource(
				&heapProperties,
				D3D12_HEAP_FLAG_NONE,
				&bufferDesc,
				D3D12_RESOURCE_STATE_COPY_DEST,
				nullptr,
				SR_IID_PPV_ARGS(&resource)
			);

			if (FAILED(hr))
				return nullptr;

			if (aIdentifier != nullptr)
			{
				resource->SetName(ToWString(aIdentifier).c_str());
			}
			buffer->myDescription = aBufferDesc;
			buffer->myDX12Resource = resource;
		}

		return buffer;
	}

	SR_Buffer* SR_GraphicsDevice_DX12::CreateBuffer(const SR_BufferDesc& aBufferDesc, void* aDataPtr, const char* aIdentifier)
	{
		SR_Buffer* result = CreateBuffer(aBufferDesc, aIdentifier);
		SR_Buffer_DX12* buffer = static_cast<SR_Buffer_DX12*>(result);

		D3D12_SUBRESOURCE_DATA data = {};
		data.pData = reinterpret_cast<BYTE*>(aDataPtr);
		data.RowPitch = aBufferDesc.mySize;
		data.SlicePitch = aBufferDesc.mySize;

		ID3D12Resource* uploadHeap;
		{
			auto heapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
			auto buf = CD3DX12_RESOURCE_DESC::Buffer(aBufferDesc.mySize);
			HRESULT hr = E_FAIL;
			hr = myDevice->CreateCommittedResource(
				&heapProperties,
				D3D12_HEAP_FLAG_NONE,
				&buf,
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				SR_IID_PPV_ARGS(&uploadHeap));

			if (FAILED(hr))
				return nullptr;
		}

		SR_GraphicsContext_DX12* ctx = static_cast<SR_GraphicsContext_DX12*>(SR_GraphicsDevice::GetDevice()->GetContext(SR_QueueType_Copy));
		ctx->Begin();
		ctx->BeginRecording();

		UpdateSubresources(ctx->GetNativeCommandList(), buffer->myDX12Resource, uploadHeap, 0, 0, 1, &data);
		ctx->EndRecording();

		uint64 fence = SR_GraphicsQueueManager::Get()->GetNextExpectedFence(SR_QueueType_Copy);
		ctx->InsertFence(fence);

		SC_GrowingArray<SC_Ref<SR_CommandList>> cls;
		cls.Add(ctx->GetCommandList());
		SR_GraphicsQueueManager::ExecuteCommandLists(cls, SR_QueueType_Copy);
		SR_GraphicsQueueManager::Signal(fence, SR_QueueType_Copy);
		ctx->End();
		SR_GraphicsQueueManager::WaitForSignal(fence, SR_QueueType_Copy);
		buffer->myDX12Tracking.myState = SR_ResourceState_CopyDest;
		DeleteResource(uploadHeap);
		return result;
	}


	bool SR_GraphicsDevice_DX12::CreateBufferView(const SBufferViewDesc & /*aViewDesc*/, const SR_Buffer * /*aBuffer*/, SR_BufferView * /*aDstView*/)
	{
		return false;
	}

	void SR_GraphicsDevice_DX12::GenerateMips(SR_TextureBuffer* /*aTextureBuffer*/)
	{
	}

	const SC_Vector2f& SR_GraphicsDevice_DX12::GetResolution() const
	{
		return myWindow->GetResolution();
	}

	SR_GraphicsContext::SResourceHeapHandles SR_GraphicsDevice_DX12::GetDescriptorRange(uint aNumDescriptors)
	{
		std::unique_lock<std::mutex> lock(CGraphicsDevice_DX12_Private::myResourceDecriptorLock);
		CD3DX12_CPU_DESCRIPTOR_HANDLE handleCPU(myResourceHeapCtx->GetCPUDescriptorHandleForHeapStart(), myResourceCtxOffset, myResourceDescriptorIncrementSize);
		CD3DX12_GPU_DESCRIPTOR_HANDLE handleGPU(myResourceHeapCtx->GetGPUDescriptorHandleForHeapStart(), myResourceCtxOffset, myResourceDescriptorIncrementSize);
		myResourceCtxOffset += aNumDescriptors;
		lock.unlock();

		SR_GraphicsContext::SResourceHeapHandles handles;
		handles.myCPUAddress = handleCPU.ptr;
		handles.myGPUAddress = handleGPU.ptr;

		return handles;
	}

	SR_Descriptor_DX12 SR_GraphicsDevice_DX12::AllocateDescriptorRange(uint aNumDescriptors)
	{
		uint offset = 0;
		{
			SC_MutexLock lock(myDescriptorsLock);
			offset = myDescriptorsOffset;
			myDescriptorsOffset += aNumDescriptors;
		}
		offset *= myResourceDescriptorIncrementSize;

		SR_Descriptor_DX12 tableStart;
		tableStart.myGPUHandle = myResourceHeapGPU.GetStartHandleGPU();
		tableStart.myGPUHandle.ptr += offset;
		tableStart.myCPUHandle = myResourceHeapGPU.GetStartHandleCPU();
		tableStart.myCPUHandle.ptr += offset;

		return tableStart;
	}

	const SR_DescriptorHeap_DX12& SR_GraphicsDevice_DX12::GetGPUVisibleDescriptorHeap()
	{
		return myResourceHeapGPU;
	}

	const bool SR_GraphicsDevice_DX12::PostInit()
	{
		if (!SR_GraphicsDevice::PostInit())
			return false;

		mySwapChain = new SR_SwapChain_DX12();

		SR_SwapChainDesc swapChainDesc;
		swapChainDesc.myBackbufferFormat = SR_Format_RGBA8_Unorm;
		swapChainDesc.myWindow = myWindow;
		if (!mySwapChain->Init(swapChainDesc))
			return false;

		return true;
	}

	void SR_GraphicsDevice_DX12::CreateDefaultPSOs()
	{
		{
			SComputePSODesc desc;
			desc.myComputeShader = "..\\Data\\Shaders\\GenerateMips_Texture2D.hlsl";
			myGenerateMips2D = SR_ShaderStateCache::Get().GetPSO(desc);
		} 
		{
			SComputePSODesc desc;
			desc.myComputeShader = "..\\Data\\Shaders\\GenerateMips_Texture3D.hlsl";
			myGenerateMips3D = SR_ShaderStateCache::Get().GetPSO(desc);
		}
	}

	bool SR_GraphicsDevice_DX12::CreateResourceHeap()
	{
		myResourceDescriptorIncrementSize = myDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
		mySamplerDescriptorIncrementSize = myDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER);

		D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};
		heapDesc.NumDescriptors = 999999;//TODO: CHANGE THIS!
		heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;

		HRESULT hr = myDevice->CreateDescriptorHeap(&heapDesc, SR_IID_PPV_ARGS(&myResourceHeapCtx));
		if (FAILED(hr))
			return false;


		static const uint ResourceDescriptorHeapSize = 64 * 1024;
		static const uint RTVDescriptorHeapSize = 4 * 1024;
		static const uint DSVDescriptorHeapSize = 4 * 1024;

		if (!myResourceHeapCPU.Init(SR_DescriptorHeapType_CBV_SRV_UAV, ResourceDescriptorHeapSize, false, "Main CPU-Visible DescriptorHeap"))
			return false;

		if (!myResourceHeapStagedCPU.Init(SR_DescriptorHeapType_CBV_SRV_UAV, 512, false, "Staged CPU-Visible DescriptorHeap"))
			return false;

		if (!myResourceHeapGPU.Init(SR_DescriptorHeapType_CBV_SRV_UAV, ResourceDescriptorHeapSize, true, "Main GPU-Visible DescriptorHeap"))
			return false;

		if (!myRTVHeap.Init(SR_DescriptorHeapType_RTV, RTVDescriptorHeapSize, false, "Main RTV DescriptorHeap"))
			return false;

		if (!myDSVHeap.Init(SR_DescriptorHeapType_DSV, DSVDescriptorHeapSize, false, "Main DSV DescriptorHeap"))
			return false;

		return true;
	}

	void SR_GraphicsDevice_DX12::SetFeatureSupport()
	{
		D3D12_FEATURE_DATA_D3D12_OPTIONS dx12Options = {};
		HRESULT hr = myDevice->CheckFeatureSupport(D3D12_FEATURE_D3D12_OPTIONS, &dx12Options, sizeof(dx12Options));
		if (FAILED(hr))
		{
			SC_ERROR_LOG("Could not check options from device");
			return;
		}

		// Conservative Rasterization
		myFeatureSupport.myConservativeRasterTier = dx12Options.ConservativeRasterizationTier;
		myFeatureSupport.myEnableConservativeRaster = myFeatureSupport.myConservativeRasterTier > D3D12_CONSERVATIVE_RASTERIZATION_TIER_NOT_SUPPORTED;
		SC_LOG("Conservative Rasterization Tier: %i", myFeatureSupport.myConservativeRasterTier);

		myFeatureSupport.myTiledResourcesTier = dx12Options.TiledResourcesTier;
		myFeatureSupport.myEnableTiledResources = myFeatureSupport.myTiledResourcesTier > D3D12_TILED_RESOURCES_TIER_NOT_SUPPORTED;
		SC_LOG("Tiled Resources Tier: %i", myFeatureSupport.myTiledResourcesTier);

		myFeatureSupport.myResourceBindingTier = dx12Options.ResourceBindingTier;
		SC_LOG("Resource Binding Tier: %i", myFeatureSupport.myResourceBindingTier);

		myFeatureSupport.myResourceHeapTier = dx12Options.ResourceHeapTier;
		SC_LOG("Resource Heap Tier: %i", myFeatureSupport.myResourceHeapTier);

		myFeatureSupport.myEnableTypedLoadAdditionalFormats = dx12Options.TypedUAVLoadAdditionalFormats;
		SC_LOG("Additional Typed Load Formats: %s", myFeatureSupport.myEnableTypedLoadAdditionalFormats ? "true" : "false");

		D3D12_FEATURE_DATA_D3D12_OPTIONS5 dx12Options5 = {};
		hr = myDevice->CheckFeatureSupport(D3D12_FEATURE_D3D12_OPTIONS5, &dx12Options5, sizeof(dx12Options5));
		if (FAILED(hr))
		{
			SC_ERROR_LOG("Could not check Options5 from device");
			return;
		}

		myFeatureSupport.myEnableRaytracing = dx12Options5.RaytracingTier > D3D12_RAYTRACING_TIER_NOT_SUPPORTED;
		myFeatureSupport.myRaytracingTier = (myFeatureSupport.myEnableRaytracing) ? 1 : 0; // 2019-09-28 Hampus: D3D12_RAYTRACING_TIER_1 is defined as 10. Force this to 1 for now.
		SC_LOG("Enable Hardware Raytracing: %s", myFeatureSupport.myEnableRaytracing ? "true" : "false");

		myFeatureSupport.myRenderPassTier = dx12Options5.RenderPassesTier;
		myFeatureSupport.myEnableRenderPass = myFeatureSupport.myRenderPassTier > D3D12_RENDER_PASS_TIER_0; // We will not enable software-based render-passes.
		SC_LOG("Render Pass Tier: %i", myFeatureSupport.myRenderPassTier);
	}

	void SR_GraphicsDevice_DX12::GetStaticSamplerDesc(D3D12_STATIC_SAMPLER_DESC(&aSamplersOut)[SAMPLERSLOT_COUNT]) const
	{
		// Linear Clamp
		aSamplersOut[0].Filter = D3D12_FILTER_MIN_MAG_LINEAR_MIP_POINT;
		aSamplersOut[0].MaxAnisotropy = 0;
		aSamplersOut[0].AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[0].AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[0].AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[0].BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
		aSamplersOut[0].ComparisonFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
		aSamplersOut[0].MipLODBias = 0.f;
		aSamplersOut[0].MinLOD = 0.f;
		aSamplersOut[0].MaxLOD = 3.402823466e+38f;
		aSamplersOut[0].MipLODBias = 0.f;
		aSamplersOut[0].RegisterSpace = 0;
		aSamplersOut[0].ShaderRegister = SAMPLERSLOT_LINEAR_CLAMP;
		aSamplersOut[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

		// Linear WRAP
		aSamplersOut[1].Filter = D3D12_FILTER_MIN_MAG_LINEAR_MIP_POINT;
		aSamplersOut[1].MaxAnisotropy = 0;
		aSamplersOut[1].AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
		aSamplersOut[1].AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
		aSamplersOut[1].AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
		aSamplersOut[1].BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
		aSamplersOut[1].ComparisonFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
		aSamplersOut[1].MipLODBias = 0.f;
		aSamplersOut[1].MinLOD = 0.f;
		aSamplersOut[1].MaxLOD = 3.402823466e+38f;
		aSamplersOut[1].MipLODBias = 0.f;
		aSamplersOut[1].RegisterSpace = 0;
		aSamplersOut[1].ShaderRegister = SAMPLERSLOT_LINEAR_WRAP;
		aSamplersOut[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

		// Linear MIRROR
		aSamplersOut[2].Filter = D3D12_FILTER_MIN_MAG_LINEAR_MIP_POINT;
		aSamplersOut[2].MaxAnisotropy = 0;
		aSamplersOut[2].AddressU = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
		aSamplersOut[2].AddressV = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
		aSamplersOut[2].AddressW = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
		aSamplersOut[2].BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
		aSamplersOut[2].ComparisonFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
		aSamplersOut[2].MipLODBias = 0.f;
		aSamplersOut[2].MinLOD = 0.f;
		aSamplersOut[2].MaxLOD = 3.402823466e+38f;
		aSamplersOut[2].MipLODBias = 0.f;
		aSamplersOut[2].RegisterSpace = 0;
		aSamplersOut[2].ShaderRegister = SAMPLERSLOT_LINEAR_MIRROR;
		aSamplersOut[2].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

		// Point Clamp
		aSamplersOut[3].Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
		aSamplersOut[3].MaxAnisotropy = 0;
		aSamplersOut[3].AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[3].AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[3].AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[3].BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
		aSamplersOut[3].ComparisonFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
		aSamplersOut[3].MipLODBias = 0.f;
		aSamplersOut[3].MinLOD = 0.f;
		aSamplersOut[3].MaxLOD = 3.402823466e+38f;
		aSamplersOut[3].MipLODBias = 0.f;
		aSamplersOut[3].RegisterSpace = 0;
		aSamplersOut[3].ShaderRegister = SAMPLERSLOT_POINT_CLAMP;
		aSamplersOut[3].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

		// Linear WRAP
		aSamplersOut[4].Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
		aSamplersOut[4].MaxAnisotropy = 0;
		aSamplersOut[4].AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
		aSamplersOut[4].AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
		aSamplersOut[4].AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
		aSamplersOut[4].BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
		aSamplersOut[4].ComparisonFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
		aSamplersOut[4].MipLODBias = 0.f;
		aSamplersOut[4].MinLOD = 0.f;
		aSamplersOut[4].MaxLOD = 3.402823466e+38f;
		aSamplersOut[4].MipLODBias = 0.f;
		aSamplersOut[4].RegisterSpace = 0;
		aSamplersOut[4].ShaderRegister = SAMPLERSLOT_POINT_WRAP;
		aSamplersOut[4].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

		// Linear MIRROR
		aSamplersOut[5].Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
		aSamplersOut[5].MaxAnisotropy = 0;
		aSamplersOut[5].AddressU = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
		aSamplersOut[5].AddressV = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
		aSamplersOut[5].AddressW = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
		aSamplersOut[5].BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
		aSamplersOut[5].ComparisonFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
		aSamplersOut[5].MipLODBias = 0.f;
		aSamplersOut[5].MinLOD = 0.f;
		aSamplersOut[5].MaxLOD = 3.402823466e+38f;
		aSamplersOut[5].MipLODBias = 0.f;
		aSamplersOut[5].RegisterSpace = 0;
		aSamplersOut[5].ShaderRegister = SAMPLERSLOT_POINT_MIRROR;
		aSamplersOut[5].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

		// Anisotropic CLAMP
		aSamplersOut[6].Filter = D3D12_FILTER_ANISOTROPIC;
		aSamplersOut[6].MaxAnisotropy = 16;
		aSamplersOut[6].AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[6].AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[6].AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[6].BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
		aSamplersOut[6].ComparisonFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
		aSamplersOut[6].MipLODBias = 0.f;
		aSamplersOut[6].MinLOD = 0.f;
		aSamplersOut[6].MaxLOD = 3.402823466e+38f;
		aSamplersOut[6].MipLODBias = 0.f;
		aSamplersOut[6].RegisterSpace = 0;
		aSamplersOut[6].ShaderRegister = SAMPLERSLOT_ANISO16_CLAMP;
		aSamplersOut[6].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

		// Anisotropic Wrap
		aSamplersOut[7].Filter = D3D12_FILTER_ANISOTROPIC;
		aSamplersOut[7].MaxAnisotropy = 16;
		aSamplersOut[7].AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
		aSamplersOut[7].AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
		aSamplersOut[7].AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
		aSamplersOut[7].BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
		aSamplersOut[7].ComparisonFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
		aSamplersOut[7].MipLODBias = 0.f;
		aSamplersOut[7].MinLOD = 0.f;
		aSamplersOut[7].MaxLOD = 3.402823466e+38f;
		aSamplersOut[7].MipLODBias = 0.f;
		aSamplersOut[7].RegisterSpace = 0;
		aSamplersOut[7].ShaderRegister = SAMPLERSLOT_ANISO16_WRAP;
		aSamplersOut[7].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

		// Anisotropic Mirror
		aSamplersOut[8].Filter = D3D12_FILTER_ANISOTROPIC;
		aSamplersOut[8].MaxAnisotropy = 16;
		aSamplersOut[8].AddressU = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
		aSamplersOut[8].AddressV = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
		aSamplersOut[8].AddressW = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
		aSamplersOut[8].BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
		aSamplersOut[8].ComparisonFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
		aSamplersOut[8].MipLODBias = 0.f;
		aSamplersOut[8].MinLOD = 0.f;
		aSamplersOut[8].MaxLOD = 3.402823466e+38f;
		aSamplersOut[8].MipLODBias = 0.f;
		aSamplersOut[8].RegisterSpace = 0;
		aSamplersOut[8].ShaderRegister = SAMPLERSLOT_ANISO16_MIRROR;
		aSamplersOut[8].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

		// Linear Cmp Clamp
		aSamplersOut[9].Filter = D3D12_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
		aSamplersOut[9].MaxAnisotropy = 16;
		aSamplersOut[9].AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[9].AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[9].AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[9].BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
		aSamplersOut[9].ComparisonFunc = D3D12_COMPARISON_FUNC_GREATER_EQUAL;
		aSamplersOut[9].MipLODBias = 0.f;
		aSamplersOut[9].MinLOD = 0.f;
		aSamplersOut[9].MaxLOD = 3.402823466e+38f;
		aSamplersOut[9].MipLODBias = 0.f;
		aSamplersOut[9].RegisterSpace = 0;
		aSamplersOut[9].ShaderRegister = SAMPLERSLOT_CMP_LINEAR_CLAMP;
		aSamplersOut[9].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;
		
		// Point Cmp Clamp
		aSamplersOut[10].Filter = D3D12_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
		aSamplersOut[10].MaxAnisotropy = 0;
		aSamplersOut[10].AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[10].AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[10].AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[10].BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
		aSamplersOut[10].ComparisonFunc = D3D12_COMPARISON_FUNC_GREATER_EQUAL;
		aSamplersOut[10].MipLODBias = 0.f;
		aSamplersOut[10].MinLOD = 0.f;
		aSamplersOut[10].MaxLOD = 3.402823466e+38f;
		aSamplersOut[10].MipLODBias = 0.f;
		aSamplersOut[10].RegisterSpace = 0;
		aSamplersOut[10].ShaderRegister = SAMPLERSLOT_CMP_POINT_CLAMP;
		aSamplersOut[10].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

		// Anisotropic Cmp CLAMP
		aSamplersOut[11].Filter = D3D12_FILTER_COMPARISON_ANISOTROPIC;
		aSamplersOut[11].MaxAnisotropy = 16;
		aSamplersOut[11].AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[11].AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[11].AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
		aSamplersOut[11].BorderColor = D3D12_STATIC_BORDER_COLOR_OPAQUE_WHITE;
		aSamplersOut[11].ComparisonFunc = D3D12_COMPARISON_FUNC_GREATER_EQUAL;
		aSamplersOut[11].MipLODBias = 0.f;
		aSamplersOut[11].MinLOD = 0.f;
		aSamplersOut[11].MaxLOD = 3.402823466e+38f;
		aSamplersOut[11].MipLODBias = 0.f;
		aSamplersOut[11].RegisterSpace = 0;
		aSamplersOut[11].ShaderRegister = SAMPLERSLOT_CMP_ANISO16_CLAMP;
		aSamplersOut[11].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;
	}

	void SR_GraphicsDevice_DX12::CreateDefaultRootSignatures()
	{
		SR_RootSignatureDesc_DX12 rootSignatureDesc;

		SR_RootSignatureDesc_DX12::Table& locConstantsTable = rootSignatureDesc.myTableDescs[SR_RootSignature_DX12::Tables::LocalConstants];
		locConstantsTable.myNumDescriptors = SR_ConstantBufferRef_NumLocals;
		locConstantsTable.myType = SR_RootTableType_CBV;

		SR_RootSignatureDesc_DX12::Table& locBuffersTable = rootSignatureDesc.myTableDescs[SR_RootSignature_DX12::Tables::LocalBuffers];
		locBuffersTable.myNumDescriptors = SR_BufferRef_NumLocals;
		locBuffersTable.myType = SR_RootTableType_SRV;

		SR_RootSignatureDesc_DX12::Table& locTexturesTable = rootSignatureDesc.myTableDescs[SR_RootSignature_DX12::Tables::LocalTextures];
		locTexturesTable.myNumDescriptors = SR_TextureRef_NumLocals;
		locTexturesTable.myType = SR_RootTableType_SRV;

		SR_RootSignatureDesc_DX12::Table& locTextureRWTable = rootSignatureDesc.myTableDescs[SR_RootSignature_DX12::Tables::LocalRWTextures];
		locTextureRWTable.myNumDescriptors = SR_TextureRWRef_NumLocals;
		locTextureRWTable.myType = SR_RootTableType_UAV;

		SR_RootSignatureDesc_DX12::Table& locBufferRWTable = rootSignatureDesc.myTableDescs[SR_RootSignature_DX12::Tables::LocalRWBuffers];
		locBufferRWTable.myNumDescriptors = SR_BufferRWRef_NumLocals;
		locBufferRWTable.myType = SR_RootTableType_UAV;

		SR_RootSignatureDesc_DX12::Table& globalConstantsTable = rootSignatureDesc.myTableDescs[SR_RootSignature_DX12::Tables::GlobalConstants];
		globalConstantsTable.myNumDescriptors = SR_ConstantBufferRef_NumGlobals;
		globalConstantsTable.myType = SR_RootTableType_CBV;

		SR_RootSignatureDesc_DX12::Table& globalBuffersTable = rootSignatureDesc.myTableDescs[SR_RootSignature_DX12::Tables::GlobalBuffers];
		globalBuffersTable.myNumDescriptors = 0;
		globalBuffersTable.myType = SR_RootTableType_SRV;

		SR_RootSignatureDesc_DX12::Table& globalTexturesTable = rootSignatureDesc.myTableDescs[SR_RootSignature_DX12::Tables::GlobalTextures];
		globalTexturesTable.myNumDescriptors = SR_TextureRef_NumGlobals;
		globalTexturesTable.myType = SR_RootTableType_SRV;

		rootSignatureDesc.myRootCBV.myIsUsed = false;
		rootSignatureDesc.myRootSRV.myIsUsed = false;

		myRootSignatures[Graphics] = new SR_RootSignature_DX12();
		myRootSignatures[Graphics]->Init(rootSignatureDesc, false);
		myRootSignatures[Compute] = new SR_RootSignature_DX12();
		myRootSignatures[Compute]->Init(rootSignatureDesc, true);

	}

	const bool SR_GraphicsDevice_DX12::Init(SC_Window* aWindow)
	{
		SC_Window_Win64* window = static_cast<SC_Window_Win64*>(aWindow);
		if (!window)
			return false;

		myWindow = window;

		SC_LOG("Initializing Direct3D 12");
		HRESULT hr = S_OK;

		IDXGIFactory4* dxgiFactory;
		hr = CreateDXGIFactory1(SR_IID_PPV_ARGS(&dxgiFactory));
		if (FAILED(hr))
		{
			return false;
		}

		D3D_FEATURE_LEVEL featureLevels[] = { D3D_FEATURE_LEVEL_12_1, D3D_FEATURE_LEVEL_12_0, D3D_FEATURE_LEVEL_11_1, D3D_FEATURE_LEVEL_11_0 };
		constexpr uint numFeatureLevels = (sizeof(featureLevels) / sizeof(D3D_FEATURE_LEVEL));

		IDXGIAdapter1* deviceAdapter = nullptr;
		DXGI_ADAPTER_DESC1 deviceDesc = {};

		int adapterIndex = 0;
		uint64 maxVRAM = 0;
		IDXGIAdapter1* adapter;
		while (dxgiFactory->EnumAdapters1(adapterIndex, &adapter) != DXGI_ERROR_NOT_FOUND)
		{
			DXGI_ADAPTER_DESC1 desc;
			adapter->GetDesc1(&desc);
			if (desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
			{
				adapterIndex++;
				continue;
			}
			
			D3D_FEATURE_LEVEL currFeatureLevel = D3D_FEATURE_LEVEL_11_0;
			HRESULT deviceResult = E_FAIL;
			for (uint i = 0; i < numFeatureLevels; ++i)
			{
				deviceResult = D3D12CreateDevice(adapter, featureLevels[i], _uuidof(ID3D12Device), nullptr);
				if (SUCCEEDED(deviceResult))
				{
					currFeatureLevel = featureLevels[i];
					break;
				}
			}

			if (desc.DedicatedVideoMemory > maxVRAM && currFeatureLevel >= myFeatureLevel)
			{
				maxVRAM = static_cast<uint64>(desc.DedicatedVideoMemory);
				myFeatureLevel = currFeatureLevel;
				deviceAdapter = adapter;
				deviceDesc = desc;
			}
			adapterIndex++;
		}

		if (myFeatureLevel < D3D_FEATURE_LEVEL_12_0)
		{
			SC_ERROR_LOG("Your graphics-card doesn't support D3D_Feature_Level 12_0");
			return false;
		}

		{
			if (deviceDesc.VendorId == 0x10DE)
			{
				SC_LOG("Graphics Vendor: Nvidia");
			}
			else if (deviceDesc.VendorId == 0x1002)
			{
				SC_LOG("Graphics Vendor: AMD");
			}
			else if (deviceDesc.VendorId == 0x163C || deviceDesc.VendorId == 0x8086)
			{
				SC_LOG("Graphics Vendor: Intel");
			}

			std::string deviceName = ToString(deviceDesc.Description);

			SC_LOG("Graphics card: %s (id: %x rev: %x)", deviceName.c_str(), deviceDesc.DeviceId, deviceDesc.Revision);
			SC_LOG("Video Memory: %lluMB", uint64(maxVRAM >> 20));
		}

		//////////////////////
		// Create Device
		if (myUseDebugDevice)
		{
			SC_LOG("Creating D3D12Device_DEBUGGABLE");
			ID3D12Debug* debugger = nullptr;
			D3D12GetDebugInterface(SR_IID_PPV_ARGS(&debugger));
			debugger->EnableDebugLayer();
		}
		else
			SC_LOG("Creating D3D12Device");

		SC_LOG("Using D3D_FeatureLevel (%s)", GetFeatureLevelString(myFeatureLevel));

		hr = D3D12CreateDevice(deviceAdapter, myFeatureLevel, SR_IID_PPV_ARGS(&myDevice));
		if (FAILED(hr))
		{
			SC_ERROR_LOG("Could not create device.");
			return false;
		}

		// Query Interfaces
		{
			hr = myDevice->QueryInterface(SR_IID_PPV_ARGS(&myDevice5));
			if (FAILED(hr))
			{
			}
			if (myDevice5)
				SC_LOG("Using ID3D12Device5");
		}

		switch (myFeatureLevel)
		{
		default:
		case D3D_FEATURE_LEVEL_12_1:
		case D3D_FEATURE_LEVEL_12_0:
		case D3D_FEATURE_LEVEL_11_1:
		case D3D_FEATURE_LEVEL_11_0:
			globalShaderModel = "_5_1";
			break;
		}

		SetFeatureSupport();

		//////////////////////
		// Create debug-message filters
		if (myUseDebugDevice)
		{
			if (SUCCEEDED(myDevice->QueryInterface(SR_IID_PPV_ARGS(&myInfoQueue))))
			{
				D3D12_MESSAGE_SEVERITY severities[] =
				{
					D3D12_MESSAGE_SEVERITY_INFO
				};
				D3D12_MESSAGE_ID denyIds[] =
				{
					D3D12_MESSAGE_ID_CLEARRENDERTARGETVIEW_MISMATCHINGCLEARVALUE,
					D3D12_MESSAGE_ID_CLEARDEPTHSTENCILVIEW_MISMATCHINGCLEARVALUE
				};

				D3D12_INFO_QUEUE_FILTER newFilter = {};
				newFilter.DenyList.NumSeverities = _countof(severities);
				newFilter.DenyList.pSeverityList = severities;
				newFilter.DenyList.NumIDs = _countof(denyIds);
				newFilter.DenyList.pIDList = denyIds;
				myInfoQueue->PushStorageFilter(&newFilter);
				if (myDebugBreakOnError)
				{
					myInfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_CORRUPTION, true);
					myInfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_ERROR, true);
					if (myDebugBreakOnWarning)
						myInfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_WARNING, true);
				}
			}
		}

		//////////////////////
		// Create Font-texture
		D3D12_DESCRIPTOR_HEAP_DESC fontHeapDesc = {};
		fontHeapDesc.NumDescriptors = 1;
		fontHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		fontHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		hr = myDevice->CreateDescriptorHeap(&fontHeapDesc, SR_IID_PPV_ARGS(&myFontTargetDescHeap));
		if (FAILED(hr))
		{
			return false;
		}

		//D3D12_DESCRIPTOR_RANGE samplerRange = {};
		//samplerRange.RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER;
		//samplerRange.BaseShaderRegister = 0;
		//samplerRange.RegisterSpace = 0;
		//samplerRange.NumDescriptors = GPU_SAMPLER_HEAP_COUNT;
		//samplerRange.OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

		// Graphics Root Signature
		// SetupRootSignatures(); // use this func to create the root signatures instead.
		{
			D3D12_DESCRIPTOR_RANGE descriptorRanges[3];

			descriptorRanges[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
			descriptorRanges[0].BaseShaderRegister = 0;
			descriptorRanges[0].RegisterSpace = 0;
			descriptorRanges[0].NumDescriptors = GPU_RESOURCE_HEAP_CBV_COUNT;
			descriptorRanges[0].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

			descriptorRanges[1].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
			descriptorRanges[1].BaseShaderRegister = 0;
			descriptorRanges[1].RegisterSpace = 0;
			descriptorRanges[1].NumDescriptors = GPU_RESOURCE_HEAP_SRV_COUNT;
			descriptorRanges[1].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

			descriptorRanges[2].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_UAV;
			descriptorRanges[2].BaseShaderRegister = 0;
			descriptorRanges[2].RegisterSpace = 0;
			descriptorRanges[2].NumDescriptors = GPU_RESOURCE_HEAP_UAV_COUNT;
			descriptorRanges[2].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;


			D3D12_ROOT_PARAMETER params[3];

			// CBVs
			params[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
			params[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;
			params[0].DescriptorTable.NumDescriptorRanges = 1;
			params[0].DescriptorTable.pDescriptorRanges = &descriptorRanges[0];

			// SRVs
			params[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
			params[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;
			params[1].DescriptorTable.NumDescriptorRanges = 1;
			params[1].DescriptorTable.pDescriptorRanges = &descriptorRanges[1];

			// UAVs
			params[2].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
			params[2].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;
			params[2].DescriptorTable.NumDescriptorRanges = 1;
			params[2].DescriptorTable.pDescriptorRanges = &descriptorRanges[2];

			//// Samplers
			//params[3].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
			//params[3].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;
			//params[3].DescriptorTable.NumDescriptorRanges = 1;
			//params[3].DescriptorTable.pDescriptorRanges = &samplerRange;

			D3D12_STATIC_SAMPLER_DESC samplers[SAMPLERSLOT_COUNT];
			GetStaticSamplerDesc(samplers);
					 
			D3D12_ROOT_SIGNATURE_DESC rootSigDesc = {};
			rootSigDesc.NumStaticSamplers = SAMPLERSLOT_COUNT;
			rootSigDesc.NumParameters = 3;
			rootSigDesc.pParameters = params;
			rootSigDesc.pStaticSamplers = samplers;
			rootSigDesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS;

			ID3DBlob* rootSigBlob;
			ID3DBlob* rootSigError;
			hr = D3D12SerializeRootSignature(&rootSigDesc, D3D_ROOT_SIGNATURE_VERSION_1_0, &rootSigBlob, &rootSigError);
			if (FAILED(hr))
			{
				SC_ERROR_LOG((char*)rootSigError->GetBufferPointer());
				assert(false);
			}
			hr = myDevice->CreateRootSignature(0, rootSigBlob->GetBufferPointer(), rootSigBlob->GetBufferSize(), __uuidof(ID3D12RootSignature), (void**)& myGraphicsRootSignature);
			hr = myDevice->CreateRootSignature(0, rootSigBlob->GetBufferPointer(), rootSigBlob->GetBufferSize(), __uuidof(ID3D12RootSignature), (void**)& myComputeRootSignature);
			assert(SUCCEEDED(hr));

			if (rootSigBlob != nullptr)
			{
				rootSigBlob->Release();
			}
			if (rootSigError != nullptr)
			{
				rootSigError->Release();
			}
		}

		CreateDefaultRootSignatures();

		//////////////////////
		// Depth-stencil
		// TODO: Remove from device?

		D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc = {};
		dsvHeapDesc.NumDescriptors = 1;
		dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
		dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		hr = myDevice->CreateDescriptorHeap(&dsvHeapDesc, SR_IID_PPV_ARGS(&myDepthStencilHeap));
		if (FAILED(hr))
		{
			return false;
		}
		D3D12_DEPTH_STENCIL_VIEW_DESC depthStencilDesc = {};
		DXGI_FORMAT depthFormat = DXGI_FORMAT_D32_FLOAT;
		DXGI_FORMAT depthResourceFormat = DXGI_FORMAT_R32_TYPELESS;
		if (myIsUsing16BitDepth)
		{
			depthResourceFormat = DXGI_FORMAT_R16_TYPELESS;
			depthFormat = DXGI_FORMAT_D16_UNORM;
		}
		
		depthStencilDesc.Format = depthFormat;
		depthStencilDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
		depthStencilDesc.Flags = D3D12_DSV_FLAG_NONE; 
		
		D3D12_CLEAR_VALUE depthOptimizedClearValue = {};
		depthOptimizedClearValue.Format = depthFormat;
#if (USE_REVERSE_Z == 1)
		depthOptimizedClearValue.DepthStencil.Depth = 0.0f;
#else
		depthOptimizedClearValue.DepthStencil.Depth = 1.0f;
#endif
		depthOptimizedClearValue.DepthStencil.Stencil = 0;


		auto heapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
		auto resDesc = CD3DX12_RESOURCE_DESC::Tex2D(depthResourceFormat, (uint64)myWindow->GetResolution().x, (uint)myWindow->GetResolution().y, 1, 0, 1, 0, D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL);
		hr= myDevice->CreateCommittedResource(
			&heapProperties,
			D3D12_HEAP_FLAG_NONE,
			&resDesc,
			D3D12_RESOURCE_STATE_DEPTH_WRITE,
			&depthOptimizedClearValue,
			SR_IID_PPV_ARGS(&myDepthStencilBuffer)
		);
		if (FAILED(hr))
		{
			return false;
		}
		myDepthStencilHeap->SetName(L"Depth/Stencil Resource Heap"); 
		myDevice->CreateDepthStencilView(myDepthStencilBuffer, &depthStencilDesc, myDepthStencilHeap->GetCPUDescriptorHandleForHeapStart());

		SR_Buffer_DX12* depthBuffer = new SR_Buffer_DX12();
		depthBuffer->myDX12Resource = myDepthStencilBuffer;
		depthBuffer->myDX12Tracking.myState = static_cast<SR_ResourceState>(D3D12_RESOURCE_STATE_DEPTH_WRITE);
		myDepthTexture.myBuffer = depthBuffer;
		myDepthTexture.myDescription.myFormat = _ConvertFormatFromDX(depthFormat);

		CreateResourceHeap();

		CreateDefaultPSOs();

		//////////////////////
		// Query heaps

		D3D12_QUERY_HEAP_DESC queryDesc;
		queryDesc.Type = D3D12_QUERY_HEAP_TYPE_TIMESTAMP;
		queryDesc.Count = 2 * ourNumQueriesPerContext; // Double up for start + end timestamps, multiply by num contexts.
		queryDesc.NodeMask = 0;

		hr = myDevice->CreateQueryHeap(&queryDesc, SR_IID_PPV_ARGS(&myQueryHeap));
		assert(hr == S_OK && "Couldn't create query heap.");

		myContext_KeepAlive = GetGraphicsContext(SR_QueueType_Copy);
		myContext = static_cast<SR_GraphicsContext_DX12*>(myContext_KeepAlive.Get());

		SR_GraphicsContext::SetCurrentContext(GetGraphicsContext());

		SC_SUCCESS_LOG("Successfully initialized Direct3D 12.");

		myShaderCompiler = new SR_ShaderCompiler_DX12();

		if (!PostInit())
			return false;

		return true;
	}
}

#endif