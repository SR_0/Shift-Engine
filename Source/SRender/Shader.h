#pragma once

namespace Shift
{
	class CShader
	{
		friend class SR_GraphicsDevice;
		friend class CGraphicsDevice_DX11;
		friend class SR_GraphicsDevice_DX12;
		friend class CGraphicsDevice_Vulkan;
	public:
		CShader();
		virtual ~CShader();

		uint GetByteCodeSize() const;
		uint8* GetByteCode() const;
		const char* GetEntryPoint() const;

	protected:
		unsigned int myByteCodeSize;
		unsigned char* myByteCode;
		const char* myEntryPoint;
	};
}