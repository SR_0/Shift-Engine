#include "SRender_Precompiled.h"
#include "SR_GraphicsContext.h"
namespace Shift
{
	thread_local SC_Ref<SR_GraphicsContext> SR_GraphicsContext::ourCurrentContext = nullptr;
	void SR_GraphicsContext::SetResourceHeapHandles(const SResourceHeapHandles& aHandles)
	{
		myResourceHeapHandles = aHandles;
	}
	CGraphicsPSO const* SR_GraphicsContext::GetBoundPSO() const
	{
		return myLastBoundPSO;
	}
	SR_GraphicsContext* SR_GraphicsContext::GetCurrent()
	{
		// Get the current context of the thread.
		// Each call for a new GraphicsContext puts itself on top of the thread-stack.
		// Destroying the context removes it from the stack.

		return ourCurrentContext;
	}
	void SR_GraphicsContext::IncrementTrianglesDrawn(uint aIncrement)
	{
#if ENABLE_RENDER_STATS
		CRenderStats& stats = CRenderStats::GetMutableStats();
		stats.Lock();
		stats.myDrawnTriangles += aIncrement;
		stats.Unlock();
#else
		SC_UNUSED(aIncrement);
#endif
	}
	SR_GraphicsContext::SR_GraphicsContext(const SR_QueueType& aType)
		: myContextType(aType)
		, myUseDebugDevice(false)
		, myGraphicsDevice(nullptr)
		, myUseDebugMarkers(false)
		, myNeedsFlush(false)
		, myNumCommandsSinceReset(0)
		, myCurrentDepthState(nullptr)
		, myCurrentRasterizerState(nullptr)
		, myCurrentBlendState(nullptr)
		, myCurrentRenderTargetFormats(nullptr)
		, myCurrentTopology(SR_Topology_Unknown)
	{
		if (SC_CommandLineManager::HasCommand("debugrender"))
			myUseDebugDevice = true;

#if !IS_FINAL
		myUseDebugMarkers = true;
		if (SC_CommandLineManager::HasCommand("nodebugmarkers"))
			myUseDebugMarkers = false;
#endif
	}
	bool SR_GraphicsContext::NeedsFlush() const
	{
		return myNeedsFlush;
	}
	void SR_GraphicsContext::SetNeedsFlush()
	{
		myNeedsFlush = true;
	}
	void SR_GraphicsContext::ClearRenderTargets(SR_RenderTarget** aTargets, uint aNumTargets, const SC_Vector4f& aClearColor)
	{
		for (uint i = 0; i < aNumTargets; ++i)
			ClearRenderTarget(aTargets[i], aClearColor);
	}
	void SR_GraphicsContext::SetCurrentContext(SR_GraphicsContext* aCtx)
	{
		ourCurrentContext = aCtx;
	}
}