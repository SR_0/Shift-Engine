#pragma once
#if ENABLE_DX12
#include "SR_RingBuffer_DX12.h"
#include "SR_Descriptor_DX12.h"

namespace Shift
{
	class SR_GraphicsDevice_DX12;
	class SR_CommandList_DX12;
	class SR_RootSignature_DX12;
	class SR_GraphicsContext_DX12 final : public SR_GraphicsContext
	{
		friend class SR_GraphicsDevice_DX12;
		friend class SR_GraphicsQueueManager_DX12;
	public:

		SR_GraphicsContext_DX12(const SR_QueueType& aType, SR_GraphicsDevice_DX12* aDevice);
		~SR_GraphicsContext_DX12();

		void Begin() override;
		void End() override;

		void BeginRecording() override;
		void EndRecording() override;
		bool IsReady() override;

		void BeginEvent(const char* aID) override;
		void EndEvent() override;

		void InsertFence(uint64 aFence) override; 
		void InsertWait(SR_Waitable* aEvent) override;

		void Draw(const unsigned int aVertexCount, const unsigned int aStartVertexLocation) override;
		void DrawIndexed(const unsigned int aIndexCount, const unsigned int aStartIndexLocation, const unsigned int aBaseVertexLocation) override;
		void DrawInstanced(const unsigned int aVertexCount, const unsigned int aInstanceCount, const unsigned int aStartVertexLocation, const unsigned int aStartInstanceLocation) override;
		void DrawIndexedInstanced(const unsigned int aIndexCount, const unsigned int aInstanceCount, const unsigned int aStartIndexLocation, const unsigned int aBaseVertexLocation, const unsigned int aStartInstanceLocation) override;
		void Dispatch(const unsigned int aThreadGroupCountX, const unsigned int aThreadGroupCountY, const unsigned int aThreadGroupCountZ) override;
		void DispatchRays(const SDispatchRaysDesc& aDispatchDesc) override;

		void ClearRenderTarget(uint aRenderTargetSlot, SC_Float4 aClearColor, bool aClearDepth) override;
		void ClearRenderTargets(uint aNumActiveTargets, SC_Float4 aClearColor, bool aClearDepth) override;
		void ClearRenderTargets(uint aNumActiveTargets, SC_Float4 aClearColors[MAX_NUM_RENDERTARGETS], bool aClearDepth) override;
		void ClearDepthTarget(bool aReverseDepth, bool aClearStencil = true) override;

		void ClearRenderTarget(SR_RenderTarget* aTarget, const SC_Vector4f& aClearColor) override;
		void ClearDepthTarget(SR_RenderTarget* aTarget, float aDepthValue, uint8 aStencilValue, uint aClearFlags) override;

		void BindGraphicsPSO(const CGraphicsPSO& aPSO) override;
		void BindComputePSO(const CComputePSO& aPSO) override;
		void BindConstantBufferRef(const CConstantBuffer& aConstantBuffer, const uint aBindingIndex) override;
		void BindConstantBuffer(void* aData, uint aSize, const uint aBindingIndex) override;
		void BindBuffer(SR_BufferView* aBufferView, const uint aBindingIndex) override;
		void BindBufferRW(SR_BufferView* aBufferView, const uint aBindingIndex) override;
		void BindTexture(SR_Texture* aTexture, const uint aBindingIndex) override;
		void BindTexture(SR_Texture* aTexture, const uint aBindingIndex, const uint aHeapOffset) override;
		void BindTextureRW(SR_Texture* aTexture, const uint aBindingIndex) override;
		void BindAccelerationStructure(const SR_AccelerationStructure& aAccStruct, const uint aBindingIndex) override;

		void Transition(const SR_ResourceState& aTransition, SR_TrackedResource* aResource) override;
		void Transition(const SC_Pair<SR_ResourceState, SR_TrackedResource*>* aPairs, uint aCount) override;

		void SetBlendState(SR_BlendState* aBlendState, const SC_Vector4f& aBlendFactor) override;
		void SetDepthState(SR_DepthState* aDepthState, uint aStencilRef) override;
		void SetRasterizerState(SR_RasterizerState* aRasterizerState) override;

		void SetViewport(const SViewport& aViewport) override;
		void SetScissorRect(const SScissorRect& aScissorRect) override;
		void SetTopology(const SR_Topology& aTopology) override;
		void SetVertexBuffer(uint aStartVertex, SR_Buffer* aBuffer) override;
		void SetVertexBuffer(uint aStartVertex, SR_Buffer** aBuffer, uint aCount) override;
		void SetVertexBuffer(const unsigned int aStartVertexIndex, CVertexBuffer& aVertexBuffer) override;
		void SetVertexBuffer(const unsigned int aStartVertexIndex, SC_GrowingArray<CVertexBuffer>& aVertexBuffers) override;
		void SetIndexBuffer(SR_Buffer* aIndexBuffer) override;
		void SetIndexBuffer(CIndexBuffer& aIndexBuffer) override;
		void SetShaderState(SR_ShaderState* aShaderState) override;
		void SetRenderTargets(SR_RenderTarget** aTargets, uint aNumTargets, SR_RenderTarget* aDepthStencil, uint aDepthAccess) override;

		void CopyBuffer(SR_Buffer* aDestination, SR_Buffer* aSource) override;
		void CopyTexture(SR_TextureBuffer* aDestination, SR_TextureBuffer* aSource) override;
		void UpdateBufferData(uint aStride, uint aCount, void* aData, CVertexBuffer& aOutBuffer) override;
		void UpdateBufferData(uint aByteSize, void* aData, SR_Buffer* aOutBuffer) override;
		void GenerateMips(SR_Texture& aTexture) override;

		void ResolveQuery(const EQueryType& aType, uint aStartIndex, uint aNumQueries, SR_Buffer* aOutBuffer) override;
		void EndQuery(const EQueryType& aType, uint aIndex) override;

		// Acceleration Structures
		void BuildAccelerationStructure() override;

		SR_CommandList* GetCommandList() override;

		// DX12 specific
		ID3D12GraphicsCommandList* GetNativeCommandList() const;
	private:

		void PreDraw();
		void PreDispatch();
		void SetResourceTables();


		void BindRootSignature(SR_RootSignature_DX12* aRootSignature);
		void BindResourceTables() override;
		void RequestNewDescriptorRange();
		void SetResources();

		std::mutex myWaitablesLock;
		SC_GrowingArray<SR_WaitEvent*> myWaitablesGPU;

		uint myResourceDescriptorIncrementSize;
		uint myRenderTargetDescriptorIncrementSize;
		uint myDepthBufferDescriptorIncrementSize;

		// Resource Heaps
		CDynamicUploadHeap_DX12* myUploadHeap;
		ID3D12DescriptorHeap* myResourceHeap;
		ID3D12DescriptorHeap* myRenderTargetHeap;
		ID3D12DescriptorHeap* myDepthBufferHeap;

		SR_Descriptor_DX12 myResourceHeapDescriptor;

		// Root Signature
		SR_RootSignature_DX12* myCurrentRootSignature;

		// Command Lists
		SC_Ref<SR_CommandList_DX12> myCommandList;
		ID3D12GraphicsCommandList* myNativeCommandList;
		ID3D12GraphicsCommandList4* myNativeCommandList4;

		SR_GraphicsDevice_DX12* myDevice;

		std::unordered_map<SR_TrackedResource*, SC_Pair<SR_ResourceState, SR_ResourceState>> myIntermediateTransitions;
	};
}
#endif