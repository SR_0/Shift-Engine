#pragma once

#if ENABLE_VULKAN

#include "SR_GraphicsDevice.h"

namespace Shift
{
	class CGraphicsDevice_Vulkan;
	class CGraphicsContext_Vulkan : public CGraphicsContext
	{
		friend CGraphicsDevice_Vulkan;
	public:

		CGraphicsContext_Vulkan(const EContextType& aType, CGraphicsDevice_Vulkan* aDevice);
		~CGraphicsContext_Vulkan();

		void BeginRecording() override;
		void EndRecording() override;

		void Draw(const unsigned int aVertexCount, const unsigned int aStartVertexLocation) override;
		void DrawIndexed(const unsigned int aIndexCount, const unsigned int aStartIndexLocation, const unsigned int aBaseVertexLocation) override;
		void DrawInstanced(const unsigned int aVertexCount, const unsigned int aInstanceCount, const unsigned int aStartVertexLocation, const unsigned int aStartInstanceLocation) override;
		void DrawIndexedInstanced(const unsigned int aIndexCount, const unsigned int aInstanceCount, const unsigned int aStartIndexLocation, const unsigned int aBaseVertexLocation, const unsigned int aStartInstanceLocation) override;
		void Dispatch(const unsigned int aThreadGroupCountX, const unsigned int aThreadGroupCountY, const unsigned int aThreadGroupCountZ) override;

		void ClearRenderTarget(uint aRenderTargetSlot, float4 aClearColor, bool aClearDepth) override;

		void BindGraphicsPSO(const CGraphicsPSO& aPSO) override;
		void BindConstantBufferRef(const CConstantBuffer& aConstantBuffer, const uint aBindingIndex) override;
		void BindConstantBuffer(void* aData, uint aSize, const uint aBindingIndex) override;
		void BindBuffer(const CBufferView& aBufferView, const uint aBindingIndex) override;
		void BindBufferRW(const CBufferView& aBufferView, const uint aBindingIndex) override;
		void BindTexture(const CTexture& aTexture, const uint aBindingIndex) override;
		void BindTextureRW(const CTexture& aTexture, const uint aBindingIndex) override;

		void Transition(CBuffer& aBuffer, const EStateTransition aTargetState) override;

		void SetViewport(const SViewport& aViewport) override;
		void SetScissorRect(const SScissorRect& aScissorRect) override;
		void SetTopology(const EPrimitiveTopology& aTopology) override;
		void SetVertexBuffer(const unsigned int aStartVertexIndex, const CVertexBuffer& aVertexBuffer) override;
		void SetVertexBuffer(const unsigned int aStartVertexIndex, const SC_GrowingArray<CVertexBuffer>& aVertexBuffers) override;
		void SetIndexBuffer(const CIndexBuffer& aIndexBuffer) override;
		void SetRenderTarget(const CTexture& aTarget) override;
		void SetRenderTarget(const CTexture& aTarget, const CTexture& aDepthStencil) override;

		void CopyBuffer(CBuffer& aDestination, CBuffer& aSource) override;
		void UpdateBufferData(uint aStride, uint aCount, void* aData, CVertexBuffer& aOutBuffer) override;

		// Vulkan specific
		//ID3D12GraphicsCommandList* GetCommandList() const;
	private:

		void WaitForPreviousFrame() override;
		void BindResourceTables() override;

		//uint myResourceDescriptorIncrementSize;
		//uint myRenderTargetDescriptorIncrementSize;
		//
		//uint myFenceValue;
		//ID3D12Fence* myFence;
		//HANDLE myFenceEvent;
		//
		//CDynamicUploadHeap_DX12* myUploadHeap;
		//ID3D12DescriptorHeap* myRenderTargetHeap;
		VkCommandBuffer myCommandBuffer;
		//ID3D12GraphicsCommandList4* myCommandList4;  //ID3D12GraphicsCommandList4 is for raytracing
		//ID3D12CommandAllocator* myCommandAllocator;
		CGraphicsDevice_Vulkan* myDevice;
	};

	struct SwapChainSupportDetails;
	class CGraphicsDevice_Vulkan : public CGraphicsDevice
	{
	public:
		CGraphicsDevice_Vulkan();
		~CGraphicsDevice_Vulkan();

		const bool Init(const SGraphicsDeviceStartParameters& aStartParams) override;
		void BeginFrame() override;
		void EndFrame() override;

		void Draw(const unsigned int aVertexCount, const unsigned int aStartVertexLocation) override;
		void DrawIndexed(const unsigned int aIndexCount, const unsigned int aStartIndexLocation, const unsigned int aBaseVertexLocation) override;
		void DrawInstanced(const unsigned int aVertexCount, const unsigned int aInstanceCount, const unsigned int aStartVertexLocation, const unsigned int aStartInstanceLocation) override;
		void DrawIndexedInstanced(const unsigned int aIndexCount, const unsigned int aInstanceCount, const unsigned int aStartIndexLocation, const unsigned int aBaseVertexLocation, const unsigned int aStartInstanceLocation) override;

		void Dispatch(const unsigned int aThreadGroupCountX, const unsigned int aThreadGroupCountY, const unsigned int aThreadGroupCountZ) override;

		void Present() override;
		void ExecuteCommandLists(CGraphicsContext* aContext) override;

		CGraphicsContext* GetGraphicsContext() override;

		// Textures
		bool CreateTexture(const STextureDesc& aTextureDesc, CTexture* aDestinationTexture) override;

		// Shaders
		CVertexLayout* CreateVertexLayout(const CVertexShader* aVS) override;
		CVertexShader* CreateVertexShader(const std::string& aFilePath, const std::string& aEntryPoint) override;
		CPixelShader* CreatePixelShader(const std::string& aFilePath, const std::string& aEntryPoint) override;
		CGeometryShader* CreateGeometryShader(const std::string& aFilePath, const std::string& aEntryPoint) override;
		CComputeShader* CreateComputeShader(const std::string& aFilePath, const std::string& aEntryPoint) override;
		CGraphicsPSO* CreateGraphicsPSO(const SGraphicsPSODesc& aPSODesc) override;
		bool CreateGraphicsPSO(const SGraphicsPSODesc& aPSODesc, CGraphicsPSO* aDestinationPSO) override;
		const bool CreateVertexBuffer(const SVertexBufferDesc& aVBDesc, CVertexBuffer* aDestinationBuffer) override;
		const bool CreateIndexBuffer(const std::vector<unsigned int>& aIndexList, CIndexBuffer* aDestinationBuffer) override;
		bool CreateBuffer(const SBufferDesc& aBufferDesc, CBuffer* aDstBuffer, const char* aIdentifier = nullptr) override;
		bool CreateBuffer(const SBufferDesc& aBufferDesc, void* aDataPtr, uint aDataSize, CBuffer* aDstBuffer, const char* aIdentifier = nullptr) override;
		bool CreateBufferView(const SBufferViewDesc& aViewDesc, const CBuffer* aBuffer, CBufferView* aDstView) override;

		void BindFontTexture(CGraphicsContext* aContext) override;

		CBuffer& GetCurrentFrameTarget() override;

	private:
		struct QueueFamilyIndices 
		{
			bool IsComplete() {	return (myGraphicsFamily >= 0) && (myPresentFamily >= 0); }
			int myGraphicsFamily = -1;
			int myPresentFamily = -1;
		};
		
	private:
		void CreateDefaultPSOs() override;
		void Destroy();
		const bool InitInstance(const SGraphicsDeviceStartParameters& aStartParams);
		const bool InitPhysicalDevice(const std::vector<const char*>& aDeviceExtensions);
		const bool InitLogicalDevice(const std::vector<const char*>& aDeviceExtensions);
		const bool CreateSurface(const SGraphicsDeviceStartParameters& aStartParams);
		const bool CreateSwapChain(const SGraphicsDeviceStartParameters& aStartParams);
		const bool CreateImageViews();
		const bool CreateGeneralFixedFunctionStates();

		void CreateGraphicsPipeline();

		const bool IsDeviceSuitable(VkPhysicalDevice aDevice, const std::vector<const char*>& aDeviceExtensions);
		QueueFamilyIndices FindQueueFamilies(VkPhysicalDevice aDevice);
		SwapChainSupportDetails QuerySwapChainSupport(VkPhysicalDevice aDevice);

		std::vector<VkImage> mySwapChainImages; 
		std::vector<VkImageView> mySwapChainImageViews;
		std::vector<VkFramebuffer> mySwapChainFramebuffers;
		std::vector<VkCommandBuffer> myCommandBuffers;
		VkFormat mySwapChainImageFormat;
		VkExtent2D mySwapChainExtent;
		VkInstance myInstance;
		VkPhysicalDevice myPhysicalDevice;
		VkDevice myLogicalDevice; 
		VkQueue myGraphicsQueue; 
		VkQueue myPresentQueue;
		VkSurfaceKHR mySurface;
		VkSwapchainKHR mySwapChain;
		VkRenderPass myRenderPass;
		VkPipelineLayout myPipelineLayout;
		VkPipeline myGraphicsPipeline;
		VkCommandPool myCommandPool;
		VkSemaphore myImageAvailableSemaphore;
		VkSemaphore myRenderFinishedSemaphore;
	};
}
#endif