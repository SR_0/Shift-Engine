#pragma once
#include "SR_RenderEnums.h"

namespace Shift
{
	struct SR_StencilDesc
	{
		SR_StencilOperator myFail;
		SR_StencilOperator myDepthFail;
		SR_StencilOperator myPass;
		SR_ComparisonFunc myStencilComparisonFunc;

		SR_StencilDesc()
			: myFail(SR_StencilOperator_Keep)
			, myDepthFail(SR_StencilOperator_Keep)
			, myPass(SR_StencilOperator_Keep)
			, myStencilComparisonFunc(SR_ComparisonFunc_Always)
		{}
	};

	struct SR_DepthStateDesc
	{
		SR_DepthStateDesc()
			: myWriteDepth(false)
			, myDepthComparisonFunc(SR_ComparisonFunc_Always)
			, myEnableStencil(false)
			, myStencilReadMask(0xff)
			, myStencilWriteMask(0xff)
		{}

		uint GetKey() const
		{
			return SC_Hash(this, sizeof(SR_DepthStateDesc));
		}

		bool myWriteDepth;
		SR_ComparisonFunc myDepthComparisonFunc;

		bool myEnableStencil;
		uint8 myStencilReadMask;
		uint8 myStencilWriteMask;
		SR_StencilDesc myFrontFace;
		SR_StencilDesc myBackFace;
	};

	struct SR_RasterizerStateDesc
	{
		SR_RasterizerStateDesc()
			: myDepthBias(0)
			, myDepthBiasClamp(0.0f)
			, mySlopedScaleDepthBias(0.0f)
			, myFaceCull(SR_RasterizerFaceCull_Back)
			, myEnableDepthClip(true)
			, myConservativeRaster(false)
			, myWireframe(false)
		{}

		uint GetKey() const
		{
			return SC_Hash(this, sizeof(SR_RasterizerStateDesc));
		}

		int myDepthBias;
		float myDepthBiasClamp;
		float mySlopedScaleDepthBias;

		SR_RasterizerFaceCull myFaceCull;

		bool myEnableDepthClip : 1;
		bool myConservativeRaster : 1;
		bool myWireframe : 1;
	};

	struct SR_RenderTargetFormats
	{
		SR_RenderTargetFormats()
			: myDepthFormat(SR_Format_D32_Float)
			, myNumColorFormats(0)
		{
			for (uint i = 0; i < 8; ++i)
				myColorFormats[i] = SR_Format_Unknown;
		}

		uint GetKey() const
		{
			uint key = SC_Hash(&myNumColorFormats, sizeof(uint8));
			key += SC_Hash(&myColorFormats, sizeof(SR_Format) * 8);
			key += SC_Hash(&myDepthFormat, sizeof(SR_Format));

			return key;
		}

		uint8 myNumColorFormats;
		SR_Format myColorFormats[8];
		SR_Format myDepthFormat;
	};

	struct SR_RenderTargetBlendDesc
	{
		SR_RenderTargetBlendDesc()
			: myEnableBlend(false)
			, myWriteMask(SR_ColorWriteMask_RGBA) 
			, mySrcBlend(SR_BlendMode_Src_Alpha)
			, myDstBlend(SR_BlendMode_One_Minus_Src_Alpha)
			, myBlendFunc(SR_BlendFunc_Add)
			, mySrcBlendAlpha(SR_BlendMode_Zero)
			, myDstBlendAlpha(SR_BlendMode_One)
			, myBlendFuncAlpha(SR_BlendFunc_Add)
		{}

		bool myEnableBlend;
		uint8 myWriteMask;

		SR_BlendMode mySrcBlend;
		SR_BlendMode myDstBlend;
		SR_BlendFunc myBlendFunc;

		SR_BlendMode mySrcBlendAlpha;
		SR_BlendMode myDstBlendAlpha;
		SR_BlendFunc myBlendFuncAlpha;
	};

	struct SR_BlendStateDesc
	{
		SR_BlendStateDesc()
			: myNumRenderTargets(0)
			, myAlphaToCoverage(false)
		{}

		uint GetKey() const
		{
			uint key = SC_Hash(&myNumRenderTargets, sizeof(uint8));
			key += SC_Hash(&myRenderTagetBlendDescs, sizeof(SR_RenderTargetBlendDesc) * 8);
			key += SC_Hash(&myAlphaToCoverage, sizeof(bool));

			return key;
		}

		uint8 myNumRenderTargets;
		SR_RenderTargetBlendDesc myRenderTagetBlendDescs[8];

		bool myAlphaToCoverage;
	};


}