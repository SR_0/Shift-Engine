#pragma once

struct SDepthStencilDesc
{
	enum class DepthStencilOp
	{
		DSV_OP_KEEP = 1,
		DSV_OP_ZERO = 2,
		DSV_OP_REPLACE = 3,
		DSV_OP_INCR_SAT = 4,
		DSV_OP_DECR_SAT = 5,
		DSV_OP_INVERT = 6,
		DSV_OP_INCR = 7,
		DSV_OP_DECR = 8
	};
	enum class DepthStencilComparisonFunc
	{
		DSV_COMPARISON_FUNC_NEVER = 1,
		DSV_COMPARISON_FUNC_LESS = 2,
		DSV_COMPARISON_FUNC_EQUAL = 3,
		DSV_COMPARISON_FUNC_LESS_EQUAL = 4,
		DSV_COMPARISON_FUNC_GREATER = 5,
		DSV_COMPARISON_FUNC_NOT_EQUAL = 6,
		DSV_COMPARISON_FUNC_GREATER_EQUAL = 7,
		DSV_COMPARISON_FUNC_ALWAYS = 8
	};
	enum class DepthStencilWriteMask
	{
		DSV_WRITE_MASK_ZERO = 0,
		DSV_WRITE_MASK_ALL = 1
	};
	struct DepthStencilOpDesc
	{
		DepthStencilOp StencilFailOp = DepthStencilOp::DSV_OP_KEEP;
		DepthStencilOp StencilDepthFailOp = DepthStencilOp::DSV_OP_KEEP;
		DepthStencilOp StencilPassOp = DepthStencilOp::DSV_OP_KEEP;
		DepthStencilComparisonFunc StencilFunc = DepthStencilComparisonFunc::DSV_COMPARISON_FUNC_ALWAYS;
	};
	DepthStencilOpDesc FrontFace;
	DepthStencilOpDesc BackFace;
	DepthStencilComparisonFunc myDepthFunction = DepthStencilComparisonFunc::DSV_COMPARISON_FUNC_LESS;
	DepthStencilWriteMask myWriteMask = DepthStencilWriteMask::DSV_WRITE_MASK_ALL;
	unsigned char myStencilReadMask = 0xff;
	unsigned char myStencilWriteMask = 0xff;
	bool myDepthTestEnabled = true;
	bool myStencilEnabled = false;
	bool myDepthBoundsTestEnable = false;
};