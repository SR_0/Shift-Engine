#pragma once
#include "SR_GraphicsDefinitions.h"

namespace Shift
{
	class SR_Buffer;
	class CIndexBuffer
	{
	public:
		CIndexBuffer();
		~CIndexBuffer();

		const unsigned int GetNumIndices() const { return myNumIndices; };

		unsigned int myNumIndices;
		SC_Handle myIndexBufferView;
		SC_Handle myIndexBufferResource;
		SC_Ref<SR_Buffer> myBuffer;
	};

}