#pragma once
#include "SR_RenderDefines.h"

#if ENABLE_DX12

#include SR_INCLUDE_FILE_D3D12 
#include SR_INCLUDE_FILE_DXGI
#pragma comment(lib,"d3d12.lib")
#pragma comment(lib,"dxgi.lib")

// For Shader compiler
#include SR_INCLUDE_FILE_D3DCOMPILER
#pragma comment(lib,"dxguid.lib")
#pragma comment(lib,"D3DCompiler.lib")



#endif