#pragma once

struct ID3D12RootSignature;
namespace Shift
{
	struct SR_RootSignatureDesc_DX12;

	enum SR_RootTableType
	{
		SR_RootTableType_CBV,
		SR_RootTableType_SRV,
		SR_RootTableType_UAV,
		SR_RootTableType_COUNT
	};

	class SR_RootSignature_DX12 : public SR_Resource
	{
		friend class SR_GraphicsDevice_DX12;
		friend class SR_GraphicsContext_DX12;
	public:
		enum Tables
		{
			LocalConstants,
			LocalBuffers,
			LocalTextures,
			LocalRWTextures,
			LocalRWBuffers,
			GlobalConstants,
			GlobalTextures,
			GlobalBuffers,
			Tables_Count,

			RootSRV,
			RootCBV,
			COUNT,
		};

		SR_RootSignature_DX12() : myRootSignature(nullptr), myMaxNumDescriptors(0), myIsCompute(false) {}
		~SR_RootSignature_DX12();

		void Init(const SR_RootSignatureDesc_DX12& aDesc, bool aIsCompute);
		uint GetActualTableIndex(const Tables& aTableIndex);
		uint GetNumDescriptors(const Tables& aTableIndex) const { return myTableInfos[aTableIndex].myNumDescriptors; }
		uint GetMaxNumDescriptors() const { return myMaxNumDescriptors; }
		bool IsCompute() const { return myIsCompute; }
	private:
		struct TableInfo
		{
			uint myHeapOffset;
			uint myNumDescriptors;
		};

		TableInfo myTableInfos[Tables::COUNT];
		uint myMaxNumDescriptors;
		ID3D12RootSignature* myRootSignature;
		bool myIsCompute : 1;
	};

	struct SR_RootSignatureDesc_DX12
	{
		struct Table
		{
			SR_RootTableType myType;
			uint myNumDescriptors;
		};

		struct RootParam
		{
			RootParam() : myIsUsed(false), myShaderRegister(0), myNum32BitValues(0) {}

			uint myShaderRegister;
			uint myNum32BitValues; // Only for RootCBV 
			bool myIsUsed : 1;
		};

		Table myTableDescs[SR_RootSignature_DX12::Tables::Tables_Count];
		RootParam myRootSRV;
		RootParam myRootCBV;

	};
}
