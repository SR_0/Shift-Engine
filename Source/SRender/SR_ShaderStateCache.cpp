#include "SRender_Precompiled.h"
#include "SR_ShaderStateCache.h"

#include <fstream>

namespace Shift
{
	SR_ShaderStateCache::SR_ShaderStateCache()
	{
	}

	const std::vector<std::string> SR_ShaderStateCache::GetIncludeFiles(const std::string& aFile)
	{
		std::vector<std::string> includes;
		std::ifstream vertexShaderFile(aFile);
		if (vertexShaderFile.good())
		{
			std::string line;
			std::string includeId("#include");
			while (std::getline(vertexShaderFile, line))
			{
				if (line.compare(0, includeId.length(), includeId) == 0)
				{
					const std::string delimiter = "\"";

					size_t position = line.find(delimiter) + 1;
					line = line.substr(position);
					line = line.substr(0, line.find(delimiter));
					line.insert(0, SC_EnginePaths::GetShadersDirectory());
					includes.push_back(line);
				}
			}
			vertexShaderFile.close();
		}
		return includes;
	}

	SR_ShaderStateCache& SR_ShaderStateCache::Get()
	{
		static SR_ShaderStateCache instance;
		return instance;
	}

	SR_ShaderStateCache::~SR_ShaderStateCache()
	{
	}

	CGraphicsPSO* SR_ShaderStateCache::GetPSO(SGraphicsPSODesc& aPSO)
	{
		replace(aPSO.myVertexShader.begin(), aPSO.myVertexShader.end(), '/', '\\');
		replace(aPSO.myGeometryShader.begin(), aPSO.myGeometryShader.end(), '/', '\\');
		replace(aPSO.myPixelShader.begin(), aPSO.myPixelShader.end(), '/', '\\');

		uint hash = SC_Hash(aPSO);
		if (myGraphicsStates.contains(hash))
			return &myGraphicsStates[hash];
		else
		{
			myGraphicsStates[hash] = CGraphicsPSO();
			myGraphicsStates[hash].SetState(SC_LoadState::Loading);

			SR_GraphicsDevice* device = SR_GraphicsDevice::GetDevice();
			if (!device->CreateGraphicsPSO(aPSO, &myGraphicsStates[hash]))
			{
				myGraphicsStates.erase(hash);
				return nullptr;
			}

#if IS_EDITOR_BUILD
			// Add to watcher
			auto reload = [&](const std::string& aFile, const EFileEvent& aEvent)
			{
				if (aEvent == Modified)
				{
					if (myGraphicsShaderConnections.contains(aFile))
					{
						for (auto& pso : myGraphicsShaderConnections[aFile])
						{
							pso->SetState(SC_LoadState::Loading);
							CGraphicsPSO newPSO;
							SR_GraphicsDevice* device = SR_GraphicsDevice::GetDevice();
							if (!device->CreateGraphicsPSO(pso->myProperties, &newPSO))
							{
								SC_ERROR_LOG("Couldn't reload shader. Falling back to old compilation.");
							}
							else
							{
								*pso = newPSO;
							}
							pso->SetState(SC_LoadState::Loaded);
						}
					}
				}
			};

			bool hasVertexShader = !aPSO.myVertexShader.empty();
			bool hasGeometryShader = !aPSO.myGeometryShader.empty();
			bool hasPixelShader = !aPSO.myPixelShader.empty();

			std::vector<std::string> includes;
			if (hasVertexShader)
			{
				myGraphicsShaderConnections[aPSO.myVertexShader].push_back(&myGraphicsStates[hash]);
				SC_EngineInterface::WatchFile(aPSO.myVertexShader, reload);

				std::vector<std::string> vIncludes = GetIncludeFiles(aPSO.myVertexShader);
				std::move(vIncludes.begin(), vIncludes.end(), std::back_inserter(includes));
			}
			if (hasGeometryShader)
			{
				myGraphicsShaderConnections[aPSO.myGeometryShader].push_back(&myGraphicsStates[hash]);
				SC_EngineInterface::WatchFile(aPSO.myGeometryShader, reload);

				std::vector<std::string> gIncludes = GetIncludeFiles(aPSO.myGeometryShader);
				std::move(gIncludes.begin(), gIncludes.end(), std::back_inserter(includes));
			}

			if (hasPixelShader)
			{
				myGraphicsShaderConnections[aPSO.myPixelShader].push_back(&myGraphicsStates[hash]);
				SC_EngineInterface::WatchFile(aPSO.myPixelShader, reload);

				std::vector<std::string> pIncludes = GetIncludeFiles(aPSO.myPixelShader);
				std::move(pIncludes.begin(), pIncludes.end(), std::back_inserter(includes));
			}

			for (auto& include : includes)
			{
				myGraphicsShaderConnections[include].push_back(&myGraphicsStates[hash]);
				SC_EngineInterface::WatchFile(include, reload);
			}
#endif
		}

		myGraphicsStates[hash].SetState(SC_LoadState::Loaded);
		return &myGraphicsStates[hash];
	}
	CComputePSO* SR_ShaderStateCache::GetPSO(SComputePSODesc& aPSO)
	{
		replace(aPSO.myComputeShader.begin(), aPSO.myComputeShader.end(), '/', '\\');

		uint hash = SC_Hash(aPSO);
		if (myComputeStates.contains(hash))
			return &myComputeStates[hash];
		else
		{
			myComputeStates[hash] = CComputePSO();
			myComputeStates[hash].SetState(SC_LoadState::Loading);

			SR_GraphicsDevice* device = SR_GraphicsDevice::GetDevice();
			if (!device->CreateComputePSO(aPSO, &myComputeStates[hash]))
			{
				myComputeStates.erase(hash);
				return nullptr;
			}

#if IS_EDITOR_BUILD
			// Add to watcher
			auto reload = [&](const std::string& aFile, const EFileEvent& aEvent)
			{
				if (aEvent == Modified)
				{
					if (myComputeShaderConnections.contains(aFile))
					{
						for (auto& pso : myComputeShaderConnections[aFile])
						{
							pso->SetState(SC_LoadState::Loading);
							CComputePSO newPSO;
							SR_GraphicsDevice* device = SR_GraphicsDevice::GetDevice();
							if (!device->CreateComputePSO(pso->myProperties, &newPSO))
							{
								SC_ERROR_LOG("Couldn't reload compute-shader. Falling back to old compilation.");
							}
							else
							{
								*pso = newPSO;
							}
							pso->SetState(SC_LoadState::Loaded);
						}
					}
				}

			};
			myComputeShaderConnections[aPSO.myComputeShader].push_back(&myComputeStates[hash]);
			SC_EngineInterface::WatchFile(aPSO.myComputeShader, reload);

			std::vector<std::string> includes = GetIncludeFiles(aPSO.myComputeShader);

			for (auto& include : includes)
			{
				myComputeShaderConnections[include].push_back(&myComputeStates[hash]);
				SC_EngineInterface::WatchFile(include, reload);
			}
#endif
		}

		myComputeStates[hash].SetState(SC_LoadState::Loaded);
		return &myComputeStates[hash];
	}
	SC_Ref<SR_ShaderState> SR_ShaderStateCache::GetShaderState(const SR_ShaderStateDesc& aDesc)
	{
		aDesc;
		return SC_Ref<SR_ShaderState>();
	}
}