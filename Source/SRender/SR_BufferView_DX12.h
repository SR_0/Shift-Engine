#pragma once
#include "SR_BufferView.h"
#include "SR_Descriptor_DX12.h"

namespace Shift
{
	class SR_BufferView_DX12 final : public SR_BufferView
	{
		friend class SR_GraphicsDevice_DX12;
	public:
		SR_BufferView_DX12();
		~SR_BufferView_DX12();

	private:
		SR_DescriptorCPU_DX12 myDescriptorCPU;
	};
}
