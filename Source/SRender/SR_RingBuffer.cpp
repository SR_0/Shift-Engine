#include "SRender_Precompiled.h"
#include "SR_RingBuffer.h"

namespace Shift
{
	SR_RingBuffer::SR_RingBuffer(OffsetType aMaxSize) noexcept
		: myMaxSize(aMaxSize)
		, myTail(0)
		, myHead(0)
		, myUsedSize(0)
		, myCurrFrameSize(0)
	{
	}
	SR_RingBuffer::SR_RingBuffer(SR_RingBuffer&& rhs) noexcept
		: myCompletedFrameTails(std::move(rhs.myCompletedFrameTails))
		, myTail(rhs.myTail)
		, myHead(rhs.myHead)
		, myMaxSize(rhs.myMaxSize)
		, myUsedSize(rhs.myUsedSize)
		, myCurrFrameSize(rhs.myCurrFrameSize)
	{
		rhs.myTail = 0;
		rhs.myHead = 0;
		rhs.myMaxSize = 0;
		rhs.myUsedSize = 0;
		rhs.myCurrFrameSize = 0;
	}
	SR_RingBuffer& SR_RingBuffer::operator=(SR_RingBuffer&& rhs) noexcept
	{
		myCompletedFrameTails = std::move(rhs.myCompletedFrameTails);
		myTail = rhs.myTail;
		myHead = rhs.myHead;
		myMaxSize = rhs.myMaxSize;
		myUsedSize = rhs.myUsedSize;
		myCurrFrameSize = rhs.myCurrFrameSize;

		rhs.myMaxSize = 0;
		rhs.myTail = 0;
		rhs.myHead = 0;
		rhs.myUsedSize = 0;
		rhs.myCurrFrameSize = 0;

		return *this;
	}

	SR_RingBuffer::~SR_RingBuffer()
	{
		assert(myUsedSize == 0 && "All space in the ring buffer must be released");
	}

	SR_RingBuffer::OffsetType SR_RingBuffer::Allocate(OffsetType aSize)
	{
		if (IsFull())
		{
			return InvalidOffset;
		}

		if (myTail >= myHead)
		{
			if (myTail + aSize <= myMaxSize)
			{
				auto Offset = myTail;
				myTail += aSize;
				myUsedSize += aSize;
				myCurrFrameSize += aSize;
				return Offset;
			}
			else if (aSize <= myHead)
			{
				// Allocate from the beginning of the buffer
				OffsetType AddSize = (myMaxSize - myTail) + aSize;
				myUsedSize += AddSize;
				myCurrFrameSize += AddSize;
				myTail = aSize;
				return 0;
			}
		}
		else if (myTail + aSize <= myHead)
		{
			auto Offset = myTail;
			myTail += aSize;
			myUsedSize += aSize;
			myCurrFrameSize += aSize;
			return Offset;
		}

		return InvalidOffset;
	}

	void SR_RingBuffer::FinishCurrentFrame(uint64 aFenceValue)
	{
		myCompletedFrameTails.emplace_back(aFenceValue, myTail, myCurrFrameSize);
		myCurrFrameSize = 0;
	}
	void SR_RingBuffer::ReleaseCompletedFrames(uint64 aCompletedFenceValue)
	{
		while (!myCompletedFrameTails.empty() &&
			myCompletedFrameTails.front().myFenceValue <= aCompletedFenceValue)
		{
			const auto &OldestFrameTail = myCompletedFrameTails.front();
			assert(OldestFrameTail.mySize <= myUsedSize);
			myUsedSize -= OldestFrameTail.mySize;
			myHead = OldestFrameTail.myOffset;
			myCompletedFrameTails.pop_front();
		}
	}
}
