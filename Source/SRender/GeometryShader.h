#pragma once
#include "Shader.h"

namespace Shift
{
	class CGeometryShader : public CShader
	{
		friend class SR_GraphicsDevice;
		friend class CGraphicsDevice_DX11;
		friend class SR_GraphicsDevice_DX12;
		friend class CGraphicsDevice_Vulkan;

	public:
		CGeometryShader();
		~CGeometryShader();

	private:
	};
}
