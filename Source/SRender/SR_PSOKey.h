#pragma once

namespace Shift
{
	struct SR_PSOKey
	{
		uint myTopologyKey;
		uint myDepthStateKey;
		uint myRasterStateKey;
		uint myBlendStateKey;
		uint myRenderTargetsKey;

		SR_PSOKey()
			: myTopologyKey(SC_UINT_MAX)
			, myDepthStateKey(SC_UINT_MAX)
			, myRasterStateKey(SC_UINT_MAX)
			, myBlendStateKey(SC_UINT_MAX)
			, myRenderTargetsKey(SC_UINT_MAX)
		{}

		bool operator==(const SR_PSOKey& aOther) const
		{
			if (myTopologyKey == aOther.myTopologyKey &&
				myDepthStateKey == aOther.myDepthStateKey &&
				myRasterStateKey == aOther.myRasterStateKey &&
				myBlendStateKey == aOther.myBlendStateKey &&
				myRenderTargetsKey == aOther.myRenderTargetsKey)
				return true;

			return false;
		}
	};
}

namespace std
{
	template <>
	struct hash<Shift::SR_PSOKey>
	{
		size_t operator()(const Shift::SR_PSOKey& aKey) const
		{
			size_t res = 17;
			res = res * 31 + hash<uint>()(aKey.myTopologyKey);
			res = res * 31 + hash<uint>()(aKey.myDepthStateKey);
			res = res * 31 + hash<uint>()(aKey.myRasterStateKey);
			res = res * 31 + hash<uint>()(aKey.myBlendStateKey);
			res = res * 31 + hash<uint>()(aKey.myRenderTargetsKey);
			return res;
		}
	};
}