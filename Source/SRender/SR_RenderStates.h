#pragma once
#include "SR_GraphicsResource.h"
#include "SR_RenderStateDescs.h"
namespace Shift
{
	class SR_RasterizerState : public SR_Resource
	{
	public:
		const SR_RasterizerStateDesc myProperties;
		const uint myKey;

		SR_RasterizerState(const SR_RasterizerStateDesc& aDesc) : myProperties(aDesc), myKey(aDesc.GetKey()) {}
	};

	class SR_BlendState : public SR_Resource
	{
	public:
		const SR_BlendStateDesc myProperties;
		const uint myKey;

		SR_BlendState(const SR_BlendStateDesc& aDesc) : myProperties(aDesc), myKey(aDesc.GetKey()) {}
	};

	class SR_DepthState : public SR_Resource
	{
	public:
		const SR_DepthStateDesc myProperties;
		const uint myKey;

		SR_DepthState(const SR_DepthStateDesc& aDesc) : myProperties(aDesc), myKey(aDesc.GetKey()) {}
	};

	class SR_RenderTargetFormatState : public SR_Resource
	{
	public:
		const SR_RenderTargetFormats myProperties;
		const uint myKey;

		SR_RenderTargetFormatState(const SR_RenderTargetFormats& aDesc) : myProperties(aDesc), myKey(aDesc.GetKey()) {}
	};
}