#pragma once
#include <deque>
namespace Shift
{

#	define DEFAULT_ALIGN 256

	class SR_RingBuffer
	{
	public:
		typedef uint64 OffsetType;
		struct FrameTailAttributes
		{
			FrameTailAttributes(uint64 aFenceValue, OffsetType aOffset, OffsetType aSize) :
				myFenceValue(aFenceValue),
				myOffset(aOffset),
				mySize(aSize)
			{}
			// Fence value associated with the command list in which
			// the allocation could have been referenced last time
			uint64 myFenceValue;
			OffsetType myOffset;
			OffsetType mySize;
		};
		static const OffsetType InvalidOffset = static_cast<OffsetType>(-1);

		SR_RingBuffer(OffsetType aMaxSize) noexcept;
		SR_RingBuffer(SR_RingBuffer&& rhs) noexcept;
		SR_RingBuffer& operator= (SR_RingBuffer&& rhs) noexcept;

		SR_RingBuffer(const SR_RingBuffer&) = delete;
		SR_RingBuffer& operator = (const SR_RingBuffer&) = delete;

		virtual ~SR_RingBuffer();

		OffsetType Allocate(OffsetType aSize);

		void FinishCurrentFrame(uint64 aFenceValue);
		void ReleaseCompletedFrames(uint64 aCompletedFenceValue);

		OffsetType GetMaxSize()const { return myMaxSize; }
		bool IsFull()const { return myUsedSize == myMaxSize; };
		bool IsEmpty()const { return myUsedSize == 0; };
		OffsetType GetUsedSize()const { return myUsedSize; }

	protected:
		std::deque< FrameTailAttributes > myCompletedFrameTails;
		OffsetType myHead;
		OffsetType myTail;
		OffsetType myMaxSize;
		OffsetType myUsedSize;
		OffsetType myCurrFrameSize;
	};

	struct DynamicAllocation
	{
		DynamicAllocation(SC_Handle aBuffer, uint64 aOffset, uint64 aSize)
			: myBuffer(aBuffer)
			, myOffset(aOffset)
			, mySize(aSize) 
			, myCPUAddress(0)
			, myGPUAddress(0)
		{}

		SC_Handle myBuffer;
		uint64 myOffset;
		uint64 mySize;
		void* myCPUAddress;
		uint64 myGPUAddress;
	};
}