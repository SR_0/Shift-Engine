#pragma once
#include "SR_GraphicsQueueManager.h"

#if ENABLE_DX12

namespace Shift
{
	class SR_CommandListPool_DX12;
	class SR_GraphicsQueueManager_DX12 : public SR_GraphicsQueueManager
	{
	public:
		SR_GraphicsQueueManager_DX12();
		~SR_GraphicsQueueManager_DX12();

		ID3D12CommandQueue* GetQueue(const SR_QueueType& aType) const;

		uint64 GetTimestampFreq(const SR_QueueType& aType) const override;

		static SR_GraphicsQueueManager_DX12* Get_DX12() { return static_cast<SR_GraphicsQueueManager_DX12*>(ourInstance); }

	private:
		void Init_Internal() override;
		void Execute_Internal(QueueData& aQueueData) override;
		void Signal_Internal(QueueData& aQueueData) override;
		void UpdateCommandListPool_Internal() override;

		uint64 Signal_Internal(const SR_QueueType& aQueue) override;
		void Signal_Internal(uint64 aFence, const SR_QueueType& aQueue) override;
		bool WaitForSignal_Internal(uint64 aFenceValue, const SR_QueueType& aQueue) override;
		bool IsPending_Internal(uint64 aFenceValue, const SR_QueueType& aQueue) override;
		uint64 GetNextExpectedFence_Internal(const SR_QueueType& aQueue) override;

		bool GetCommandList_Internal(SC_Ref<SR_CommandList>& aCmdListOut, SR_QueueType aContextType) override;

		SC_Ref<SR_CommandListPool_DX12> myCmdListPools[SR_QueueType::SR_QueueType_MaxQueues];
		ID3D12CommandQueue* myQueues[SR_QueueType_MaxQueues];
		ID3D12Fence* myFences[SR_QueueType_MaxQueues];
		HANDLE myFenceHandles[SR_QueueType_MaxQueues];
		SC_Mutex mylock;
	};

}

#endif