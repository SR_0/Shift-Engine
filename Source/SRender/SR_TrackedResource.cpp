#include "SRender_Precompiled.h"
#include "SR_TrackedResource.h"

namespace Shift
{
	SR_TrackedResource::SR_TrackedResource()
#if ENABLE_DX12
		: myDX12Resource(nullptr)
#endif
	{
		myWaitable = new SR_Waitable(SR_WaitableMode_CPU_GPU);
	}

	SR_TrackedResource::~SR_TrackedResource()
	{
	}
	void SR_TrackedResource::Reset()
	{
		myWaitable->myEventCPU.ResetSignal();
	}
	void SR_TrackedResource::Wait()
	{
		myWaitable->myEventCPU.Wait();
	}
	SR_Waitable* SR_TrackedResource::GetWaitable()
	{
		return myWaitable;
	}
}