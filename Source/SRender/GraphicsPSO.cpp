#include "SRender_Precompiled.h"
#include "GraphicsPSO.h"

namespace Shift
{

	CGraphicsPSO::CGraphicsPSO()
		:  myPipelineState(nullptr)
	{
	}

	CGraphicsPSO::CGraphicsPSO(const CGraphicsPSO& aOther)
		: myProperties(aOther.myProperties)
		, myPipelineState(aOther.myPipelineState)
	{
		SetState(aOther.myState);
	}


	CGraphicsPSO::~CGraphicsPSO()
	{
	}
	void CGraphicsPSO::operator=(const CGraphicsPSO& aOther)
	{
		myProperties = aOther.myProperties;
		myPipelineState = aOther.myPipelineState;
		SetState(aOther.myState);
	}
	uint SC_Hash(const SGraphicsPSODesc& aDesc)
	{
		uint hash = 0;

		hash += SC_Hash(aDesc.myVertexShader.c_str(), (uint)strlen(aDesc.myVertexShader.c_str()));
		hash += SC_Hash(aDesc.myPixelShader.c_str(), (uint)strlen(aDesc.myPixelShader.c_str()));
		hash += aDesc.myRenderTargetFormats.myNumColorFormats;

		for (uint i = 0; i < 8; ++i)
			hash += static_cast<uint>(aDesc.myRenderTargetFormats.myColorFormats[i]);

		hash += static_cast<uint>(aDesc.topology);

		return hash;
	}
	uint SC_Hash(const CGraphicsPSO& aPSO)
	{
		return SC_Hash(aPSO.myProperties);
	}
}