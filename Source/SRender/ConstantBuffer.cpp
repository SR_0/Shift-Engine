#include "SRender_Precompiled.h"
#include "ConstantBuffer.h"
#include "SR_BufferView.h"

namespace Shift
{
	CConstantBuffer::CConstantBuffer()
	{
	}
	CConstantBuffer::~CConstantBuffer()
	{
	}
	SR_Buffer * CConstantBuffer::GetBuffer() const
	{ 
		return myBufferView->GetBuffer();
	}
}