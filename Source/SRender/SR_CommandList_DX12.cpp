#include "SRender_Precompiled.h"
#include "SR_CommandList_DX12.h"

#if ENABLE_DX12
#include "SR_GraphicsDevice_DX12.h"
#include "SR_CommandAllocator_DX12.h"
#include "SR_CommandListPool_DX12.h"

namespace Shift
{
	SR_CommandList_DX12::SR_CommandList_DX12(D3D12_COMMAND_LIST_TYPE aType, SR_CommandAllocator_DX12* aCmdAllocator, SR_CommandListPool_DX12* aPool, const SR_QueueType& aContextType)
		: SR_CommandList(Recording)
		, myType(aType)
		, myCurrentContextType(aContextType)
		, myCommandList(nullptr)
#if ENABLE_DX12_19H2
		, myCommandList4(nullptr)
#endif
		, myAllocator(aCmdAllocator)
		, myPool(aPool)
		, myNumCommands(0)
		, myFence(0)
		, myFenceContext(SR_QueueType_Render)
	{
		SR_GraphicsDevice_DX12* device = SR_GraphicsDevice_DX12::GetDX12Device();
		ID3D12Device* nativeDevice = device->GetNativeDevice();

		HRESULT hr = nativeDevice->CreateCommandList(1, myType, myAllocator->myAllocator, nullptr, SR_IID_PPV_ARGS(&myCommandList));
		if (FAILED(hr))
		{
			SC_ERROR_LOG("Failed to create CommandList");
			assert(false);
		}

#if ENABLE_DX12_19H2
		if (myCommandList)
			myCommandList->QueryInterface(SR_IID_PPV_ARGS(&myCommandList4));
#endif

		//TODO: Set name

	}
	SR_CommandList_DX12::~SR_CommandList_DX12()
	{
		if (myCommandList)
			myCommandList->Release();

#if ENABLE_DX12_19H2
		if (myCommandList4)
			myCommandList4->Release();
#endif
	}
	ID3D12GraphicsCommandList* SR_CommandList_DX12::GetCommandList() const
	{
		return myCommandList;
	}
#if ENABLE_DX12_19H2
	ID3D12GraphicsCommandList4* SR_CommandList_DX12::GetCommandList4() const
	{
		return myCommandList4;
	}
#endif
	void SR_CommandList_DX12::FinishRecording()
	{
		assert(myState == Recording);
		SC_Atomic_CompareExchange(myState, Idle, Recording);
	}
	void SR_CommandList_DX12::Close()
	{
		assert(myState == Idle);
		HRESULT hr = myCommandList->Close();
		//S_LOG("Closing cmd-list (%p) with allocator (%p)", this, myAllocator);
		if (FAILED(hr))
		{
			SC_ERROR_LOG("Failed to close CommandList");
			assert(false);
		}

		SC_Atomic_Add(myAllocator->myCommandsSinceResetCount, myNumCommands);
		SC_Atomic_CompareExchange(myState, Closed, Idle);
		SC_Atomic_CompareExchange(myAllocator->myCommandListsActive, 0, 1);

		SC_MutexLock lock(myPool->myAllocatorsMutex);
		if (myAllocator->IsFilled())
			myPool->myInFlightAllocators.Add(myAllocator);
		else if (myAllocator->myCommandsSinceResetCount == 0)
			myPool->myIdleAllocators.Add(myAllocator);
		else
			myPool->myActiveAllocators.Add(myAllocator);

		myAllocator->RemoveRef();
	}
	void SR_CommandList_DX12::InactivateAndReturn()
	{
		assert(myState == Closed);
		if (myAllocator)
		{
			if (myFence && SR_GraphicsQueueManager::IsPending(myFence, myFenceContext))
			{
				SC_MutexLock lock(myAllocator->myMutex);
				if (myAllocator->myFence)
				{
					if (myFence > myAllocator->myFence)
					{
						myAllocator->myFence = myFence;
						myAllocator->myFenceContext = myFenceContext;
					}
				}
				else
				{
					myAllocator->myFence = myFence;
					myAllocator->myFenceContext = myFenceContext;
				}
			}

			SC_Atomic_Decrement(myAllocator->myCommandListsAlive);
			myAllocator = nullptr;
		}

		myPendingTransitions.clear();

		SC_MutexLock cmdLock(myPool->myCmdListsMutex);
		myPool->myIdleCmdLists.Add(this);
		myFence = 0;
	}
	void SR_CommandList_DX12::SetLastFence(uint64 aLastFence, const SR_QueueType& aFenceContext)
	{
		myFence = aLastFence;
		myFenceContext = aFenceContext;
	}

	void SR_CommandList_DX12::FlushPendingTransitions()
	{
		SC_GrowingArray<D3D12_RESOURCE_BARRIER> barriers;

		for (auto pair : myPendingTransitions)
		{
			SC_Pair<uint16, uint16> transitionStates = pair.second;

			if (pair.first->myDX12Tracking.myState == transitionStates.myFirst)
				continue;

			D3D12_RESOURCE_BARRIER& barrier = barriers.Add();
			barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
			barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
			barrier.Transition.pResource = pair.first->myDX12Resource;
			barrier.Transition.StateBefore = static_cast<D3D12_RESOURCE_STATES>(pair.first->myDX12Tracking.myState);
			barrier.Transition.StateAfter = static_cast<D3D12_RESOURCE_STATES>(transitionStates.myFirst);
			barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES; 
		}

		if (barriers.Count())
		{
			myCommandList->ResourceBarrier(barriers.Count(), barriers.GetBuffer());
			myNumCommands++;
		}
	}
	void SR_CommandList_DX12::UpdateResourceGlobalStates()
	{
		for (auto& pair : myPendingTransitions)
		{
			SR_TrackedResource* trackedResource = pair.first;
			SC_Pair<uint16, uint16> transitionStates = pair.second;

			trackedResource->myDX12Tracking.myState = transitionStates.mySecond;
		}
	}
	void SR_CommandList_DX12::Open(SR_CommandAllocator_DX12* aCmdAllocator)
	{
		assert(myState == Closed);
		assert(!myAllocator);
		assert(aCmdAllocator->myType == myType);

		myAllocator = aCmdAllocator;
		HRESULT hr = myCommandList->Reset(myAllocator->myAllocator, nullptr);
		//S_LOG("Opening cmd-list (%p) with allocator (%p)", this, myAllocator);
		if (FAILED(hr))
		{
			SC_ERROR_LOG("Failed to reset CommandList");
			assert(false);
		}

		SC_Atomic_CompareExchange(myState, Idle, Closed);
	}

	void SR_CommandList_DX12::BeginRecording(const SR_QueueType& aContextType)
	{
		assert(myState == Idle);

		myPendingTransitions.clear();
		myCurrentContextType = aContextType;
		myNumCommands = 0;
		myLastFrameOpened = SC_Timer::GetGlobalFrameIndex();

		SC_Atomic_CompareExchange(myState, Recording, Idle);
	}
}
#endif