#include "SRender_Precompiled.h"
#include "SR_GraphicsQueueManager.h"

#if ENABLE_VULKAN
// Include Vulkan stuff
#	include "SR_GraphicsQueueManager_Vulkan.h" 
#endif
#if ENABLE_PS4
// Include PS4 stuff
#	include "SR_GraphicsQueueManager_PS4.h" 
#endif
#if ENABLE_DX12
#	include "SR_GraphicsQueueManager_DX12.h" 
#endif

#include "SR_CommandList.h"

namespace Shift
{

	namespace CGraphicsQueueManager_Private
	{
		SC_ThreadSafeQueue<SR_GraphicsQueueManager::QueueData> ourQueueData;
	}
	SR_GraphicsQueueManager* SR_GraphicsQueueManager::ourInstance = nullptr;

	SR_GraphicsQueueManager::SR_GraphicsQueueManager()
	{
		for (uint i = 0; i< SR_QueueType_MaxQueues; ++i)
		{
			myLastKnownCompletedFences[i] = 0;
			myLastSignaledValue[i] = 0;
		}
	}

	SR_GraphicsQueueManager::~SR_GraphicsQueueManager()
	{
	}
	void SR_GraphicsQueueManager::Init()
	{
		if (ourInstance == nullptr)
		{
			switch (SR_GraphicsDevice::GetDevice()->APIType())
			{
#if ENABLE_VULKAN
			case SR_GraphicsAPI::::Vulkan:
				ourInstance = new SR_GraphicsQueueManager_Vulkan();
				break;
#endif
#if ENABLE_PS4
			case SR_GraphicsAPI::::PS4:
				ourInstance = new SR_GraphicsQueueManager_PS4();
				break;
#endif
#if ENABLE_DX12
			case SR_GraphicsAPI::DirectX12:
				ourInstance = new SR_GraphicsQueueManager_DX12();
				break;
#endif
			}

			SC_ThreadProperties threadProps;
			threadProps.myName = "Queue Manager";
			threadProps.myAffinity = SC_ThreadTools::ourRenderThreadAffinity | SC_ThreadTools::ourFrameTaskThreadAffinity;
			threadProps.myPriority = SC_ThreadPriority_Critical;
			ourInstance->myThread = SC_CreateThread(threadProps, ourInstance, &SR_GraphicsQueueManager::ExectueFunc);
		}
	}
	void SR_GraphicsQueueManager::Destroy()
	{
		QueueData qData;
		qData.myTaskType = SR_QueueTaskType_Quit;
		CGraphicsQueueManager_Private::ourQueueData.push(qData);

		ourInstance->myThread.Stop();
		SC_SAFE_DELETE(ourInstance);
	}

	void SR_GraphicsQueueManager::ExecuteCommandLists(SC_GrowingArray<SC_Ref<SR_CommandList>>& aCommandLists, const SR_QueueType& aQueue)
	{
		QueueData qData;
		qData.myCommandLists = SC_Move(aCommandLists);
		qData.myTaskType = SR_QueueTaskType_Execute;
		qData.myContextType = aQueue;

		//for (auto& cl : qData.myCommandLists)
		//	S_LOG("Queueing cmd-list (%p)", cl.Get());

		CGraphicsQueueManager_Private::ourQueueData.push(qData);
	}

	uint64 SR_GraphicsQueueManager::Signal(const SR_QueueType& aQueue)
	{
		return ourInstance->Signal_Internal(aQueue);
	}

	void SR_GraphicsQueueManager::Signal(uint64 aFence, const SR_QueueType& aQueue)
	{
		ourInstance->Signal_Internal(aFence, aQueue);
	}

	bool SR_GraphicsQueueManager::WaitForSignal(uint64 aFenceValue, const SR_QueueType& aQueue)
	{
		return ourInstance->WaitForSignal_Internal(aFenceValue, aQueue);
	}

	bool SR_GraphicsQueueManager::IsPending(uint64 aFenceValue, const SR_QueueType& aQueue)
	{
		if (aFenceValue <= ourInstance->myLastKnownCompletedFences[aQueue])
			return false;

		return ourInstance->IsPending_Internal(aFenceValue, aQueue);
	}

	uint64 SR_GraphicsQueueManager::GetNextExpectedFence(const SR_QueueType& aQueue)
	{
		return ourInstance->GetNextExpectedFence_Internal(aQueue);
	}

	uint64 SR_GraphicsQueueManager::GetLastKnownCompletedFence(const SR_QueueType& aQueue)
	{
		return ourInstance->myLastKnownCompletedFences[aQueue];
	}

	bool SR_GraphicsQueueManager::GetCommandList(SC_Ref<SR_CommandList>& aCmdListOut, SR_QueueType aContextType)
	{
		return ourInstance->GetCommandList_Internal(aCmdListOut, aContextType);
	}

	SR_GraphicsQueueManager* SR_GraphicsQueueManager::Get()
	{
		return ourInstance;
	}

	void SR_GraphicsQueueManager::ExectueFunc()
	{
		const uint updatePoolInterval = 20;
		uint currentPoolUpdateIndex = 0;
		bool shouldQuit = false;
		while (!shouldQuit)
		{
			SC_GrowingArray<QueueData> qData;
			if (CGraphicsQueueManager_Private::ourQueueData.TryPopAll(qData))
			{
				for (QueueData& data : qData)
				{
					switch (data.myTaskType)
					{
					case SR_QueueTaskType_Wait:
						//Wait_Internal(data);
						break;
					case SR_QueueTaskType_Signal:
						Signal_Internal(data);
						break;
					case SR_QueueTaskType_Execute:
						Execute_Internal(data);
						break;
					case SR_QueueTaskType_Quit:
						shouldQuit = true;
						break;
					}

					if (shouldQuit)
						break;
				}
			}
			else
			{
				if (currentPoolUpdateIndex == updatePoolInterval)
				{
					UpdateCommandListPool_Internal();
					currentPoolUpdateIndex = 0;
				}
				else
					++currentPoolUpdateIndex;
			}
		}
	}

	void SR_GraphicsQueueManager::InsertSignal_Internal(uint64 aFenceVal, const SR_QueueType& aQueue)
	{
		assert(ourInstance && "Needs to be created!");

		QueueData qData;
		qData.myFenceValue = aFenceVal;
		qData.myTaskType = SR_QueueTaskType_Signal;
		qData.myContextType = aQueue;
		CGraphicsQueueManager_Private::ourQueueData.push(qData);
	}

	bool SR_GraphicsQueueManager::HasWork() const
	{
		return !CGraphicsQueueManager_Private::ourQueueData.empty();
	}
}