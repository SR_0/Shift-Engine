#pragma once
#include "SR_RingBuffer.h"

#if ENABLE_DX12

namespace Shift
{
	class CGPURingBuffer_DX12 : public SR_RingBuffer
	{
	public:
		CGPURingBuffer_DX12(size_t aMaxSize, ID3D12Device *aGraphicsDevice, bool aAllowCPUAccess);

		CGPURingBuffer_DX12(CGPURingBuffer_DX12&& rhs);
		CGPURingBuffer_DX12& operator= (CGPURingBuffer_DX12&& rhs);
		~CGPURingBuffer_DX12();

		CGPURingBuffer_DX12(const CGPURingBuffer_DX12&) = delete;
		CGPURingBuffer_DX12& operator =(CGPURingBuffer_DX12&) = delete;

		DynamicAllocation Allocate(size_t aSizeInBytes);

	private:
		void Destroy();

		void* myCPUVirtualAddress;
		D3D12_GPU_VIRTUAL_ADDRESS myGPUVirtualAddress;
		ID3D12Resource* myBuffer;
	};

	class CDynamicUploadHeap_DX12
	{
	public:
		CDynamicUploadHeap_DX12(bool aIsCPUAccessible, ID3D12Device* aDevice, size_t aInitialSize);

		CDynamicUploadHeap_DX12(const CDynamicUploadHeap_DX12&) = delete;
		CDynamicUploadHeap_DX12(CDynamicUploadHeap_DX12&&) = delete;
		CDynamicUploadHeap_DX12& operator=(const CDynamicUploadHeap_DX12&) = delete;
		CDynamicUploadHeap_DX12& operator=(CDynamicUploadHeap_DX12&&) = delete;
		~CDynamicUploadHeap_DX12();

		DynamicAllocation Allocate(size_t aSizeInBytes, size_t aAlignment = DEFAULT_ALIGN);

		void FinishFrame(uint64 aFenceValue, uint64 aLastCompletedFenceValue);

	private:
		std::vector<CGPURingBuffer_DX12> myRingBuffers;
		ID3D12Device* myGraphicsDeviceDX12;
		const bool myIsCPUAccessible;
	};
}
#endif