#pragma once

namespace Shift
{
	struct SRenderTargetBlendDesc2
	{
		enum class EBlendOp
		{
			BLEND_OP_ADD,
			BLEND_OP_SUBTRACT,
			BLEND_OP_REV_SUBTRACT,
			BLEND_OP_MIN,
			BLEND_OP_MAX,
		};

		enum class EBlend
		{
			BLEND_ZERO,
			BLEND_ONE,
			BLEND_SRC_COLOR,
			BLEND_INV_SRC_COLOR,
			BLEND_SRC_ALPHA,
			BLEND_INV_SRC_ALPHA,
			BLEND_DEST_ALPHA,
			BLEND_INV_DEST_ALPHA,
			BLEND_DEST_COLOR,
			BLEND_INV_DEST_COLOR,
			BLEND_SRC_ALPHA_SAT,
			BLEND_BLEND_FACTOR,
			BLEND_INV_BLEND_FACTOR,
			BLEND_SRC1_COLOR,
			BLEND_INV_SRC1_COLOR,
			BLEND_SRC1_ALPHA,
			BLEND_INV_SRC1_ALPHA,
		};

		enum EColorWriteEnabled : unsigned char
		{
			DISABLE = 0,
			ENABLE_RED = 1,
			ENABLE_GREEN = 2,
			ENABLE_BLUE = 4,
			ENABLE_ALPHA = 8,
			ENABLE_ALL = (((ENABLE_RED | ENABLE_GREEN) | ENABLE_BLUE) | ENABLE_ALPHA)
		};

		bool myBlendEnable;
		EBlend mySrcBlend;
		EBlend myDestBlend;
		EBlendOp myBlendOp;
		EBlend mySrcBlendAlpha;
		EBlend myDestBlendAlpha;
		EBlendOp myBlendOpAlpha;
		unsigned char myColorWriteMask;
	};

	class CBlendState
	{
		friend class CGraphicsDevice_DX11;
		friend class SR_GraphicsDevice_DX12;
		friend class CGraphicsDevice_Vulkan;
	public:
		CBlendState();
		~CBlendState();

	private:
		SRenderTargetBlendDesc2 myRenderTargetBlendStates[8];
	};

}