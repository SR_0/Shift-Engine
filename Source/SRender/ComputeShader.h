#pragma once
#include "Shader.h"
namespace Shift
{
	struct SComputeProperties
	{
		SC_Vector3<uint> myNumThreads;
	};

	class CComputeShader : public CShader
	{
		friend class SR_GraphicsDevice;
		friend class CGraphicsDevice_DX11;
		friend class SR_GraphicsDevice_DX12;
		friend class CGraphicsDevice_Vulkan;

	public:
		CComputeShader();
		~CComputeShader();

		const SComputeProperties& GetProperties() const;

	private:
		SComputeProperties myProperties;
	};
}
