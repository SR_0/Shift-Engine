#pragma once
#include "SR_GraphicsResource.h"
#include "SR_Buffer.h"

namespace Shift
{
	enum EBufferViewType
	{
		SBufferViewType_None,
		SBufferViewType_Bytes,
		SBufferViewType_Structured,
		SBufferViewType_Append_Consume,

		SBufferViewType_Count,
	};

	struct SBufferViewDesc
	{
		EBufferViewType myType;
		SR_Format myFormat;
		uint myFirstElement; 
		uint myNumElements;
		bool myIsShaderWritable;

		SBufferViewDesc()
			: myType(SBufferViewType_None)
			, myFormat(SR_Format_Unknown)
			, myNumElements(0)
			, myFirstElement(0)
			, myIsShaderWritable(false)
		{}
	};

	class SR_BufferView : public SR_Resource
	{
		friend class SR_GraphicsDevice_DX12;
	public:
		SR_BufferView();
		~SR_BufferView();

		SR_Buffer* GetBuffer() const { return _myBuffer; }

		SBufferViewDesc myDescription;
	private:
		SR_Buffer* myBuffer;
		SC_Ref<SR_Buffer> _myBuffer;
	};
}