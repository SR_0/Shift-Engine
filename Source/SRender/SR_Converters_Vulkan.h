#pragma once
#if ENABLE_VULKAN

namespace Shift
{
	inline VkBlendFactor _ConvertBlendVk(const SRenderTargetBlendDesc::EBlend& value)
	{
		switch (value)
		{
		case SRenderTargetBlendDesc::EBlend::BLEND_ZERO:
			return VK_BLEND_FACTOR_ZERO;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_ONE:
			return VK_BLEND_FACTOR_ONE;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_SRC_COLOR:
			return VK_BLEND_FACTOR_SRC_COLOR;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_INV_SRC_COLOR:
			return VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_SRC_ALPHA:
			return VK_BLEND_FACTOR_SRC_ALPHA;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_INV_SRC_ALPHA:
			return VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_DEST_ALPHA:
			return VK_BLEND_FACTOR_DST_ALPHA;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_INV_DEST_ALPHA:
			return VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_DEST_COLOR:
			return VK_BLEND_FACTOR_DST_COLOR;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_INV_DEST_COLOR:
			return VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_SRC_ALPHA_SAT:
			return VK_BLEND_FACTOR_SRC_ALPHA_SATURATE;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_BLEND_FACTOR:
			return VK_BLEND_FACTOR_CONSTANT_COLOR;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_INV_BLEND_FACTOR:
			return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_SRC1_COLOR:
			return VK_BLEND_FACTOR_SRC1_COLOR;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_INV_SRC1_COLOR:
			return VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_SRC1_ALPHA:
			return VK_BLEND_FACTOR_SRC1_ALPHA;
			break;
		case SRenderTargetBlendDesc::EBlend::BLEND_INV_SRC1_ALPHA:
			return VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA;
			break;
		default:
			break;
		}
		return VK_BLEND_FACTOR_ZERO;
	}


	inline VkBlendOp _ConvertBlendOpVk(const SRenderTargetBlendDesc::EBlendOp& value)
	{
		switch (value)
		{
		case SRenderTargetBlendDesc::EBlendOp::BLEND_OP_ADD:
			return VK_BLEND_OP_ADD;
			break;
		case SRenderTargetBlendDesc::EBlendOp::BLEND_OP_SUBTRACT:
			return VK_BLEND_OP_SUBTRACT;
			break;
		case SRenderTargetBlendDesc::EBlendOp::BLEND_OP_REV_SUBTRACT:
			return VK_BLEND_OP_REVERSE_SUBTRACT;
			break;
		case SRenderTargetBlendDesc::EBlendOp::BLEND_OP_MIN:
			return VK_BLEND_OP_MIN;
			break;
		case SRenderTargetBlendDesc::EBlendOp::BLEND_OP_MAX:
			return VK_BLEND_OP_MAX;
			break;
		default:
			break;
		}
		return VK_BLEND_OP_ADD;
	}
}
#endif