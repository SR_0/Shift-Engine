#include "SRender_Precompiled.h"
#include "SR_RenderThreadPool.h"
#include "SR_RenderThread.h"
#include "SR_CommandList.h"

namespace Shift
{
	SR_RenderThreadPool::SR_RenderThreadPool(uint aNumThreads)
	{
		myThreads.PreAllocate(aNumThreads);
		for (uint i = 0; i < aNumThreads; ++i)
		{
			std::string name("Render Worker");
			SR_RenderThread* thread = myThreads.Add(new SR_RenderThread(this));
			thread->Start((name + std::to_string(i)).c_str());
		}
	}
	SR_RenderThreadPool::~SR_RenderThreadPool()
	{
		myQueueData.invalidate();
		for (uint i = 0; i < myThreads.Count(); ++i)
			delete myThreads[i];
	}
	SC_Ref<SR_Waitable> SR_RenderThreadPool::PostRenderTask(std::function<void()> aTask, uint aWaitMode)
	{
		return PushTask(aTask, aWaitMode, SR_QueueType_Render);
	}
	SC_Ref<SR_Waitable> SR_RenderThreadPool::PostComputeTask(std::function<void()> aTask, uint aWaitMode)
	{
		return PushTask(aTask, aWaitMode, SR_QueueType_Compute);
	}
	SC_Ref<SR_Waitable> SR_RenderThreadPool::PostCopyTask(std::function<void()> aTask, uint aWaitMode)
	{
		return PushTask(aTask, aWaitMode, SR_QueueType_Copy);
	}
	bool SR_RenderThreadPool::Flush(bool aBlock)
	{
		if (aBlock)
		{
			while (!myQueueData.empty())
				std::this_thread::sleep_for(std::chrono::microseconds(1));
		}
		else
		{
			return myQueueData.empty();
		}
		return true;
	}
	bool SR_RenderThreadPool::IsIdle() const
	{
		return myQueueData.empty();
	}
	bool SR_RenderThreadPool::WaitPop(std::function<void()>& aTaskOut)
	{
		return myQueueData.waitPop(aTaskOut);
	}
	SC_Ref<SR_Waitable> SR_RenderThreadPool::PushTask(std::function<void()> aTask, uint aWaitMode, SR_QueueType aContextType)
	{
		SC_Ref<SR_Waitable> event = new SR_Waitable(SR_WaitableMode(aWaitMode));

		SR_Waitable* eventPtr = event;
		eventPtr->AddRef();

		auto task = [&, aTask, eventPtr, aWaitMode, aContextType]() mutable
		{
			SC_CPU_PROFILER_BEGIN_SECTION(SC_PROFILER_TAG_VIEW_RENDER);

			SR_GraphicsContext* ctx = nullptr;
			if (aWaitMode > SR_WaitableMode_CPU)
			{
				ctx = SR_GraphicsDevice::GetDevice()->GetContext(aContextType);
				ctx->Begin();
				ctx->BeginRecording();
			}

			aTask();

			if (aWaitMode > SR_WaitableMode_CPU)
			{
				ctx->EndRecording();
				myPendingCLs[aContextType].push(ctx->GetCommandList());

				uint64 fence = SR_GraphicsQueueManager::Get()->GetNextExpectedFence(aContextType);
				ctx->InsertFence(fence);
				eventPtr->myFence = fence;
				eventPtr->myFenceContext = aContextType;

				SC_GrowingArray<SC_Ref<SR_CommandList>> cls;
				if (myPendingCLs[aContextType].TryPopAll(cls))
					SR_GraphicsQueueManager::ExecuteCommandLists(cls, aContextType);

				SR_GraphicsQueueManager::Signal(fence, aContextType);

				ctx->End();
				SR_GraphicsContext::SetCurrentContext(nullptr);
			}
			eventPtr->myEventCPU.Signal();

			eventPtr->RemoveRef();

			SC_CPU_PROFILER_END_SECTION();
		};

		myQueueData.push(task);
		return event;
	}
}