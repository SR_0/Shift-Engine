#include "SRender_Precompiled.h"
#include "Shader.h"

Shift::CShader::CShader()
	: myByteCodeSize(0)
	, myByteCode(nullptr)
	, myEntryPoint("main")
{
}

Shift::CShader::~CShader()
{
	delete myByteCode;
}

uint Shift::CShader::GetByteCodeSize() const
{
	return myByteCodeSize;
}

uint8* Shift::CShader::GetByteCode() const
{
	return myByteCode;
}

const char* Shift::CShader::GetEntryPoint() const
{
	return myEntryPoint;
}
