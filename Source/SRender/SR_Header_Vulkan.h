#pragma once
#include "SR_RenderDefines.h"

#if ENABLE_VULKAN

#include SR_INCLUDE_FILE_VULKAN

#if IS_WINDOWS
#	include SR_INCLUDE_FILE_VULKAN_WIN32
#endif

#pragma comment(lib,"vulkan-1.lib")

#endif