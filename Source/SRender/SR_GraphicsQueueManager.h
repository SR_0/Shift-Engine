#pragma once
#include "SR_GraphicsEngineEnums.h"

#include "SC_Future.h"

#define MAX_NUM_CONTEXTS_BATCH 15

namespace Shift
{
	class SC_Thread;
	class SR_GraphicsDevice;
	class SR_GraphicsContext;
	class SR_WaitEvent;
	class SR_Fence;
	enum SR_QueueTaskType
	{
		SR_QueueTaskType_Execute,
		SR_QueueTaskType_Signal,
		SR_QueueTaskType_Wait,
		SR_QueueTaskType_Quit
	};

	class SR_CommandList;
	class SR_GraphicsQueueManager
	{
	public:
		virtual ~SR_GraphicsQueueManager();

		static void Init();
		static void Destroy();
		static void ExecuteCommandLists(SC_GrowingArray<SC_Ref<SR_CommandList>>& aCommandLists, const SR_QueueType& aQueue);

		static uint64 Signal(const SR_QueueType& aQueue);
		static void Signal(uint64 aFence, const SR_QueueType& aQueue);
		static bool WaitForSignal(uint64 aFenceValue, const SR_QueueType& aQueue);
		static bool IsPending(uint64 aFenceValue, const SR_QueueType& aQueue);
		static uint64 GetNextExpectedFence(const SR_QueueType& aQueue);
		static uint64 GetLastKnownCompletedFence(const SR_QueueType& aQueue);

		static bool GetCommandList(SC_Ref<SR_CommandList>& aCmdListOut, SR_QueueType aContextType);

		virtual uint64 GetTimestampFreq(const SR_QueueType& aType) const = 0;

		static SR_GraphicsQueueManager* Get();

		struct QueueData
		{
			QueueData()
				: myFenceValue(0)
			{}
			QueueData(const QueueData& aOther)
				: myFenceValue(aOther.myFenceValue)
				, myCommandLists(aOther.myCommandLists)
				, myContextType(aOther.myContextType)
				, myTaskType(aOther.myTaskType)
				, myWaitEvent(aOther.myWaitEvent)
			{}

			SC_GrowingArray<SC_Ref<SR_CommandList>> myCommandLists;
			SR_QueueType myContextType; 
			SR_QueueTaskType myTaskType;
			SC_Ref<SR_WaitEvent> myWaitEvent;
			unsigned __int64 myFenceValue;
		};

	protected:

		SR_GraphicsQueueManager();
		void ExectueFunc();
		void InsertSignal_Internal(uint64 aFenceVal, const SR_QueueType& aQueue);
		virtual void Init_Internal() = 0;
		virtual void Execute_Internal(QueueData& aQueueData) = 0;
		virtual void Signal_Internal(QueueData& aQueueData) = 0;
		virtual void UpdateCommandListPool_Internal() = 0;

		virtual uint64 Signal_Internal(const SR_QueueType& aQueue) = 0;
		virtual void Signal_Internal(uint64 aFence, const SR_QueueType& aQueue) = 0;
		virtual bool WaitForSignal_Internal(uint64 aFenceValue, const SR_QueueType& aQueue) = 0;
		virtual bool IsPending_Internal(uint64 aFenceValue, const SR_QueueType& aQueue) = 0;
		virtual uint64 GetNextExpectedFence_Internal(const SR_QueueType& aQueue) = 0;

		bool HasWork() const;

		virtual bool GetCommandList_Internal(SC_Ref<SR_CommandList>& aCmdListOut, SR_QueueType aContextType) = 0;

		SC_FutureBase myThread;
		static SR_GraphicsQueueManager* ourInstance;
		volatile uint64 myLastKnownCompletedFences[SR_QueueType_MaxQueues];
		volatile uint64 myLastSignaledValue[SR_QueueType_MaxQueues];
	};

}