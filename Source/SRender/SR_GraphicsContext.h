#pragma once
#include "SC_RefCounted.h"
#include "SC_Pair.h"
#include "SR_GraphicsDefinitions.h"
#include "SR_GraphicsEngineEnums.h"
#include "SR_GraphicsResources.h"
#include "SR_RenderStates.h"


#define SE_INTERNAL_SCOPED_PROFILER_FUNCTION(aVarName, aID) CGraphicsProfilerScope aVarName(aID)

#define SE_PROFILER_FUNCTION() SE_INTERNAL_SCOPED_PROFILER_FUNCTION(__internalScopedProfilerVariable, __FUNCTION__) /* TODO: Integrate timestamps into this define.*/

#define SE_PROFILER_FUNCTION_TAGGED(aID) SE_INTERNAL_SCOPED_PROFILER_FUNCTION(__internalScopedProfilerVariableTagged, aID)

namespace Shift
{


	struct SDispatchRaysDesc
	{
		uint myX;
		uint myY;
		uint myZ;
		uint myRayGenByteSize;
		uint myMissByteSize;
		uint myHitGroupByteSize;
		void* myRayGenAddress;
		void* myMissAddress;
		void* myHitGroupAddress;
	};

	// CGraphicsContext is the primary interface used to record GPU-commands.
	// The context is separated from the actual CommandList to allow for better threading-structure.
	// One thread only holds a single context, but the context may change between frames.
	// It internally manages descriptor placements and temporary-resource descriptor-rings.
	class SR_GraphicsContext : public SC_RefCounted
	{
		friend class SR_GraphicsQueueManager;
	public:
		struct SResourceHeapHandles
		{
			SResourceHeapHandles()
				: myCPUAddress(0)
				, myGPUAddress(0)
			{
			}

			uint64 myGPUAddress;
			uint64 myCPUAddress;
		};

		SR_GraphicsContext(const SR_QueueType& aType);
		virtual ~SR_GraphicsContext() {}

		virtual void Begin() = 0;	// Starts the scope of this context
		virtual void End() = 0;		// Ends the scope of this context

		virtual void BeginRecording() = 0;
		virtual void EndRecording() = 0;
		bool NeedsFlush() const;
		void SetNeedsFlush();

		virtual void InsertFence(uint64 aFence) = 0;
		virtual void InsertWait(SR_Waitable* aEvent) = 0;

		virtual void BeginEvent(const char* aID) = 0;
		virtual void EndEvent() = 0;

		virtual bool IsReady() = 0;

		static void SetCurrentContext(SR_GraphicsContext* aCtx);

		virtual void Draw(const unsigned int aVertexCount, const unsigned int aStartVertexLocation) = 0;
		virtual void DrawIndexed(const unsigned int aIndexCount, const unsigned int aStartIndexLocation, const unsigned int aBaseVertexLocation) = 0;
		virtual void DrawInstanced(const unsigned int aVertexCount, const unsigned int aInstanceCount, const unsigned int aStartVertexLocation, const unsigned int aStartInstanceLocation) = 0;
		virtual void DrawIndexedInstanced(const unsigned int aIndexCount, const unsigned int aInstanceCount, const unsigned int aStartIndexLocation, const unsigned int aBaseVertexLocation, const unsigned int aStartInstanceLocation) = 0;
		virtual void Dispatch(const unsigned int aThreadGroupCountX, const unsigned int aThreadGroupCountY, const unsigned int aThreadGroupCountZ) = 0;
		virtual void DispatchRays(const SDispatchRaysDesc& aDispatchDesc) = 0;

		virtual void ClearRenderTarget(uint aRenderTargetSlot, SC_Float4 aClearColor, bool aClearDepth) = 0;
		virtual void ClearRenderTargets(uint aNumActiveTargets, SC_Float4 aClearColor, bool aClearDepth) = 0;
		virtual void ClearRenderTargets(uint aNumActiveTargets, SC_Float4 aClearColors[MAX_NUM_RENDERTARGETS], bool aClearDepth) = 0;
		virtual void ClearDepthTarget(bool aReverseDepth, bool aClearStencil = true) = 0;

		virtual void ClearRenderTarget(SR_RenderTarget* aTarget, const SC_Vector4f& aClearColor) = 0;
		void ClearRenderTargets(SR_RenderTarget** aTargets, uint aNumTargets, const SC_Vector4f& aClearColor);
		virtual void ClearDepthTarget(SR_RenderTarget* aTarget, float aDepthValue, uint8 aStencilValue, uint aClearFlags) = 0;

		virtual void BindGraphicsPSO(const CGraphicsPSO& aPSO) = 0;
		virtual void BindComputePSO(const CComputePSO& aPSO) = 0;
		virtual void BindConstantBufferRef(const CConstantBuffer& aConstantBuffer, const uint aBindingIndex) = 0;
		virtual void BindConstantBuffer(void* aData, uint aSize, const uint aBindingIndex) = 0;

		//////////////////////////
		// Buffers

		virtual void BindBuffer(SR_BufferView* aBufferView, const uint aBindingIndex) = 0;
		virtual void BindBufferRW(SR_BufferView* aBufferView, const uint aBindingIndex) = 0;

		//////////////////////////
		// Textures

		virtual void BindTexture(SR_Texture* aTexture, const uint aBindingIndex) = 0;
		virtual void BindTexture(SR_Texture* aTexture, const uint aBindingIndex, const uint aHeapOffset) = 0;
		virtual void BindTextureRW(SR_Texture* aTexture, const uint aBindingIndex) = 0;

		//////////////////////////
		// Acceleration Structure

		virtual void BindAccelerationStructure(const SR_AccelerationStructure& aAccStruct, const uint aBindingIndex) = 0;

		virtual void Transition(const SR_ResourceState& aTransition, SR_TrackedResource* aResource) = 0;
		virtual void Transition(const SC_Pair<SR_ResourceState, SR_TrackedResource*>* aPairs, uint aCount) = 0;

		virtual void SetBlendState(SR_BlendState* aBlendState, const SC_Vector4f& aBlendFactor) = 0;
		virtual void SetDepthState(SR_DepthState* aDepthState, uint aStencilRef) = 0;
		virtual void SetRasterizerState(SR_RasterizerState* aRasterizerState) = 0;

		virtual void SetViewport(const SViewport& aViewport) = 0;
		virtual void SetScissorRect(const SScissorRect& aScissorRect) = 0;
		virtual void SetTopology(const SR_Topology& aTopology) = 0;
		virtual void SetVertexBuffer(uint aStartVertex, SR_Buffer* aBuffer) = 0;
		virtual void SetVertexBuffer(uint aStartVertex, SR_Buffer** aBuffer, uint aCount) = 0;
		virtual void SetVertexBuffer(const unsigned int aStartVertexIndex, CVertexBuffer& aVertexBuffer) = 0;
		virtual void SetVertexBuffer(const unsigned int aStartVertexIndex, SC_GrowingArray<CVertexBuffer>& aVertexBuffers) = 0;
		virtual void SetIndexBuffer(SR_Buffer* aIndexBuffer) = 0;
		virtual void SetIndexBuffer(CIndexBuffer& aIndexBuffer) = 0;
		virtual void SetShaderState(SR_ShaderState* aShaderState) = 0;
		virtual void SetRenderTargets(SR_RenderTarget** aTargets, uint aNumTargets, SR_RenderTarget* aDepthStencil, uint aDepthAccess) = 0;

		virtual void CopyBuffer(SR_Buffer* aDestination, SR_Buffer* aSource) = 0;
		virtual void CopyTexture(SR_TextureBuffer* aDestination, SR_TextureBuffer* aSource) = 0;
		virtual void UpdateBufferData(uint aStride, uint aCount, void* aData, CVertexBuffer& aOutBuffer) = 0;
		virtual void UpdateBufferData(uint aByteSize, void* aData, SR_Buffer* aOutBuffer) = 0;

		virtual void GenerateMips(SR_Texture& aTexture) = 0;

		virtual void ResolveQuery(const EQueryType& aType, uint aStartIndex, uint aNumQueries, SR_Buffer* aOutBuffer) = 0;
		virtual void EndQuery(const EQueryType& aType, uint aIndex) = 0;

		// Acceleration Structures
		virtual void BuildAccelerationStructure() = 0;

		virtual SR_CommandList* GetCommandList() = 0;

		CGraphicsPSO const* GetBoundPSO() const;

		void SetResourceHeapHandles(const SResourceHeapHandles& aHandles);
		SR_GraphicsDevice* GetDevice() { return myGraphicsDevice; };
		const SR_QueueType& GetType() { return myContextType; }

		static SR_GraphicsContext* GetCurrent();

	protected:

		struct FrameResourceTable
		{

		};

	protected:
		void IncrementTrianglesDrawn(uint aIncrement);
		virtual void BindResourceTables() = 0;

		static thread_local SC_Ref<SR_GraphicsContext> ourCurrentContext;

		// Render States
		SC_Vector4f myCurrentBlendFactor;
		uint myCurrentStencilRef;
		SR_DepthState* myCurrentDepthState;
		SR_RasterizerState* myCurrentRasterizerState;
		SR_BlendState* myCurrentBlendState;
		SR_RenderTargetFormatState* myCurrentRenderTargetFormats;
		SR_Topology myCurrentTopology;
		SR_PSOKey myCurrentPSOKey;

		SResourceHeapHandles myResourceHeapHandles;
		SResourceHeapHandles myRenderTargetHeapHandles;
		SResourceHeapHandles myDepthBufferHeapHandles;
		SR_QueueType myContextType;
		SR_GraphicsDevice* myGraphicsDevice;
		CGraphicsPSO const* myLastBoundPSO;
		uint64 myLastFence;
		uint myNumCommandsSinceReset;
		bool myUseDebugDevice;
		bool myUseDebugMarkers;
		bool myNeedsFlush;

		// Also holds its own resource heap descriptor-table
	};


	class CGraphicsProfilerScope
	{
	public:
		explicit CGraphicsProfilerScope(const char* aID)
			: myContext(nullptr)
		{
			SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();
			if (ctx)
			{
				myContext = ctx;
				myContext->BeginEvent(aID);
			}
		}

		~CGraphicsProfilerScope()
		{
			if (myContext)
				myContext->EndEvent();
		}

	private:
		SR_GraphicsContext* myContext;
	};
}
