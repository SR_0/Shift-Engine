#include "SRender_Precompiled.h"
#if ENABLE_VULKAN
#include "SR_GraphicsDevice_Vulkan.h"

#include "VertexShader.h"
#include "PixelShader.h"
#include "GraphicsPSO.h"
#include "GeometryShader.h"
#include "RasterizerState.h"
#include "spirv_cross.hpp"
#include "spirv_hlsl.hpp"
#include "VulkanConverters.h"


#ifdef max
#undef max
#endif

#ifdef min
#undef min
#endif


namespace Shift
{
	struct SwapChainSupportDetails
	{
		VkSurfaceCapabilitiesKHR myCapabilities;
		std::vector<VkSurfaceFormatKHR> myFormats;
		std::vector<VkPresentModeKHR> myPresentModes;
	};

	CGraphicsDevice_Vulkan::CGraphicsDevice_Vulkan()
		: CGraphicsDevice()
		, myInstance(VK_NULL_HANDLE)
		, myPhysicalDevice(VK_NULL_HANDLE)
		, myLogicalDevice(VK_NULL_HANDLE)
		, mySurface(VK_NULL_HANDLE)
		, mySwapChain(VK_NULL_HANDLE)
	{
		myAPIType = EGraphicsAPI::Vulkan;
	}


	CGraphicsDevice_Vulkan::~CGraphicsDevice_Vulkan()
	{
		Destroy();
	}

	const bool CGraphicsDevice_Vulkan::Init(const SGraphicsDeviceStartParameters & aStartParams)
	{
		Init_Log("Initializing Vulkan...");
		Destroy();
		if (InitInstance(aStartParams) == false)
		{
			return false;
		}
		if (CreateSurface(aStartParams) == false)
		{
			return false;
		}

		const std::vector<const char*> deviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

		if (InitPhysicalDevice(deviceExtensions) == false)
		{
			return false;
		}
		if (InitLogicalDevice(deviceExtensions) == false)
		{
			return false;
		}
		if (CreateSwapChain(aStartParams) == false)
		{
			return false;
		}
		if (CreateImageViews() == false)
		{
			return false;
		}
		CreateGraphicsPipeline();
		CreateGeneralFixedFunctionStates();
		Init_Log("Successfully initialized Vulkan.");
		return true;
	}

	void CGraphicsDevice_Vulkan::BeginFrame()
	{
		
	}

	void CGraphicsDevice_Vulkan::Draw(const unsigned int aVertexCount, const unsigned int aStartVertexLocation)
	{
		aVertexCount;
		aStartVertexLocation;
		vkCmdDraw();
	}

	void CGraphicsDevice_Vulkan::DrawIndexed(const unsigned int aIndexCount, const unsigned int aStartIndexLocation, const unsigned int aBaseVertexLocation)
	{
		aIndexCount;
		aStartIndexLocation;
		aBaseVertexLocation;
	}

	void CGraphicsDevice_Vulkan::DrawInstanced(const unsigned int aVertexCount, const unsigned int aInstanceCount, const unsigned int aStartVertexLocation, const unsigned int aStartInstanceLocation)
	{
		aVertexCount;
		aInstanceCount;
		aStartVertexLocation;
		aStartInstanceLocation;
	}

	void CGraphicsDevice_Vulkan::DrawIndexedInstanced(const unsigned int aIndexCount, const unsigned int aInstanceCount, const unsigned int aStartIndexLocation, const unsigned int aBaseVertexLocation, const unsigned int aStartInstanceLocation)
	{
		aIndexCount;
		aInstanceCount;
		aStartIndexLocation;
		aBaseVertexLocation;
		aStartInstanceLocation;
	}

	void CGraphicsDevice_Vulkan::Dispatch(const unsigned int aThreadGroupCountX, const unsigned int aThreadGroupCountY, const unsigned int aThreadGroupCountZ)
	{
		aThreadGroupCountX;
		aThreadGroupCountY;
		aThreadGroupCountZ;
	}

	void CGraphicsDevice_Vulkan::EndFrame()
	{
		uint32_t imageIndex;
		vkAcquireNextImageKHR(myLogicalDevice, mySwapChain, std::numeric_limits<uint64_t>::max(), myImageAvailableSemaphore, VK_NULL_HANDLE, &imageIndex);
		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

		VkSemaphore waitSemaphores[] = { myImageAvailableSemaphore };
		VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = waitSemaphores;
		submitInfo.pWaitDstStageMask = waitStages;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &myCommandBuffers[imageIndex];

		VkSemaphore signalSemaphores[] = { myRenderFinishedSemaphore };
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = signalSemaphores;

		if (vkQueueSubmit(myGraphicsQueue, 1, &submitInfo, VK_NULL_HANDLE) != VK_SUCCESS)
		{
			throw std::runtime_error("failed to submit draw command buffer!");
		}


		VkPresentInfoKHR presentInfo = {};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = signalSemaphores;

		VkSwapchainKHR swapChains[] = { mySwapChain };
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = swapChains;
		presentInfo.pImageIndices = &imageIndex;
		presentInfo.pResults = nullptr; // Optional
		vkQueuePresentKHR(myPresentQueue, &presentInfo);
	}

	void CGraphicsDevice_Vulkan::OpenCommandLists()
	{
	}

	void CGraphicsDevice_Vulkan::ExecuteCommandLists(CGraphicsContext* /*aContext*/)
	{
	}

	bool CGraphicsDevice_Vulkan::CreateTexture(const STextureDesc & aTextureDesc, CTexture * aDestinationTexture)
	{
		aTextureDesc;
		aDestinationTexture;
		return false;
	}

	CVertexLayout * CGraphicsDevice_Vulkan::CreateVertexLayout(const CVertexShader * aVS)
	{
		aVS;
		return nullptr;
	}

	CPixelShader* CGraphicsDevice_Vulkan::CreatePixelShader(const std::string& aFilePath, const std::string& aEntryPoint)
	{
		CPixelShader* ps = new CPixelShader();

		std::ifstream file(aFilePath, std::ios::ate | std::ios::binary);
		size_t fileSize = (size_t)file.tellg();
		std::vector<char> buffer(fileSize);
		file.seekg(0);
		file.read(buffer.data(), fileSize);
		file.close();

		ps->myByteCode = reinterpret_cast<unsigned char*>(buffer.data());
		ps->myByteCodeSize = static_cast<unsigned int>(fileSize);
		ps->myEntryPoint = aEntryPoint.c_str();

		return ps;
	}

	CVertexShader* CGraphicsDevice_Vulkan::CreateVertexShader(const std::string& aFilePath, const std::string& aEntryPoint)
	{
		CVertexShader* vs = new CVertexShader();

		std::ifstream file(aFilePath, std::ios::ate | std::ios::binary);
		if (file.bad())
		{
			SC_ERROR_LOG("Couldn't create vertex shader [%s]", aFilePath.c_str());
			return nullptr;
		}

		size_t fileSize = (size_t)file.tellg();
		std::vector<char> buffer(fileSize);
		file.seekg(0);
		file.read(buffer.data(), fileSize);
		file.close();

		vs->myByteCode = reinterpret_cast<unsigned char*>(buffer.data());
		vs->myByteCodeSize = static_cast<unsigned int>(fileSize);
		vs->myEntryPoint = aEntryPoint.c_str();

		return vs;
	}
	CGeometryShader* CGraphicsDevice_Vulkan::CreateGeometryShader(const std::string& aFilePath, const std::string& aEntryPoint)
	{
		CGeometryShader* gs = new CGeometryShader();

		std::ifstream file(aFilePath, std::ios::ate | std::ios::binary);
		size_t fileSize = (size_t)file.tellg();
		std::vector<char> buffer(fileSize);
		file.seekg(0);
		file.read(buffer.data(), fileSize);
		file.close();

		gs->myByteCode = reinterpret_cast<unsigned char*>(buffer.data());
		gs->myByteCodeSize = static_cast<unsigned int>(fileSize);
		gs->myEntryPoint = aEntryPoint.c_str();

		return gs;
	}
	CComputeShader * CGraphicsDevice_Vulkan::CreateComputeShader(const std::string& aFilePath, const std::string& aEntryPoint)
	{
		aFilePath;
		aEntryPoint;
		return nullptr;
	}
	CGraphicsPSO * CGraphicsDevice_Vulkan::CreateGraphicsPSO(const SGraphicsPSODesc& aPSODesc)
	{
		CGraphicsPSO* pso = new CGraphicsPSO();

		VkGraphicsPipelineCreateInfo pipelineInfo = {};
		pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		pipelineInfo.layout = myPipelineLayout;
		pipelineInfo.renderPass = myRenderPass;
		pipelineInfo.subpass = 0;
		pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
		pipelineInfo.basePipelineIndex = -1;

		//------------------------------------------------------------------------------------//
		// SHADERS
		//------------------------------------------------------------------------------------//
		{
			std::vector<VkPipelineShaderStageCreateInfo> shaderStages;

			if (aPSODesc.vs != nullptr)
			{
				VkShaderModuleCreateInfo moduleInfo = {};
				moduleInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
				moduleInfo.codeSize = aPSODesc.vs->myByteCodeSize;
				moduleInfo.pCode = reinterpret_cast<const uint32_t*>(aPSODesc.vs->myByteCode);

				VkShaderModule shaderModule;
				if (vkCreateShaderModule(myLogicalDevice, &moduleInfo, nullptr, &shaderModule) != VK_SUCCESS)
				{
					throw std::runtime_error("failed to create shader module!");
				}

				VkPipelineShaderStageCreateInfo stageInfo = {};
				stageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
				stageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
				stageInfo.module = shaderModule;
				stageInfo.pName = aPSODesc.vs->myEntryPoint;

				shaderStages.push_back(stageInfo);
			}

			if (aPSODesc.hs != nullptr)
			{
			}

			if (aPSODesc.ds != nullptr)
			{
			}

			if (aPSODesc.gs != nullptr)
			{
				VkShaderModuleCreateInfo moduleInfo = {};
				moduleInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
				moduleInfo.codeSize = aPSODesc.gs->myByteCodeSize;
				moduleInfo.pCode = reinterpret_cast<const uint32_t*>(aPSODesc.gs->myByteCode);

				VkShaderModule shaderModule;
				if (vkCreateShaderModule(myLogicalDevice, &moduleInfo, nullptr, &shaderModule) != VK_SUCCESS)
				{
					throw std::runtime_error("failed to create shader module!");
				}

				VkPipelineShaderStageCreateInfo stageInfo = {};
				stageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
				stageInfo.stage = VK_SHADER_STAGE_GEOMETRY_BIT;
				stageInfo.module = shaderModule;
				stageInfo.pName = aPSODesc.gs->myEntryPoint;

				shaderStages.push_back(stageInfo);
			}

			if (aPSODesc.ps != nullptr)
			{
				VkShaderModuleCreateInfo moduleInfo = {};
				moduleInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
				moduleInfo.codeSize = aPSODesc.ps->myByteCodeSize;
				moduleInfo.pCode = reinterpret_cast<const uint32_t*>(aPSODesc.ps->myByteCode);

				VkShaderModule shaderModule;
				if (vkCreateShaderModule(myLogicalDevice, &moduleInfo, nullptr, &shaderModule) != VK_SUCCESS)
				{
					throw std::runtime_error("failed to create shader module!");
				}

				VkPipelineShaderStageCreateInfo stageInfo = {};
				stageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
				stageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
				stageInfo.module = shaderModule;
				stageInfo.pName = aPSODesc.ps->myEntryPoint;

				shaderStages.push_back(stageInfo);
			}

			pipelineInfo.pStages = shaderStages.data();
			pipelineInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
		}

		//------------------------------------------------------------------------------------//
		// INPUT LAYOUT
		//------------------------------------------------------------------------------------//
		{
			VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
			vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
			vertexInputInfo.vertexBindingDescriptionCount = 0;
			vertexInputInfo.pVertexBindingDescriptions = nullptr;
			vertexInputInfo.vertexAttributeDescriptionCount = 0;
			vertexInputInfo.pVertexAttributeDescriptions = nullptr;

			pipelineInfo.pVertexInputState = &vertexInputInfo;
		}

		//------------------------------------------------------------------------------------//
		// INPUT ASSEMBLY
		//------------------------------------------------------------------------------------//
		{
			VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
			inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
			inputAssembly.primitiveRestartEnable = VK_FALSE;

			if (aPSODesc.inputLayout != nullptr)
			{
				switch (aPSODesc.topology)
				{
				case EPrimitiveTopology::TriangleList:
					inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
					break;
				case EPrimitiveTopology::PointList:
					inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
					break;
				}
			}

			pipelineInfo.pInputAssemblyState = &inputAssembly;
		}

		//------------------------------------------------------------------------------------//
		// VIEWPORST & SCISSOR
		//------------------------------------------------------------------------------------//
		{
			VkViewport viewport = {};
			viewport.x = 0;
			viewport.y = 0;
			viewport.width = 65535;
			viewport.height = 65535;
			viewport.minDepth = 0;
			viewport.maxDepth = 1;

			VkRect2D scissor = {};
			scissor.extent.width = 65535;
			scissor.extent.height = 65535;

			VkPipelineViewportStateCreateInfo viewportState = {};
			viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
			viewportState.viewportCount = 1;
			viewportState.pViewports = &viewport;
			viewportState.scissorCount = 1;
			viewportState.pScissors = &scissor;

			pipelineInfo.pViewportState = &viewportState;
		}

		//------------------------------------------------------------------------------------//
		// RASTERIZER
		//------------------------------------------------------------------------------------//
		{
			VkPipelineRasterizationStateCreateInfo rasterizer = {};
			rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
			rasterizer.depthClampEnable = VK_FALSE;
			rasterizer.rasterizerDiscardEnable = VK_FALSE;
			rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
			rasterizer.lineWidth = 1.0f;
			rasterizer.cullMode = VK_CULL_MODE_NONE;
			rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
			rasterizer.depthBiasEnable = VK_FALSE;
			rasterizer.depthBiasConstantFactor = 0.0f;
			rasterizer.depthBiasClamp = 0.0f;
			rasterizer.depthBiasSlopeFactor = 0.0f;

			auto& rs = myRasterizerStates[(unsigned int)aPSODesc.rasterizerState];
			switch (rs.myCullMode)
			{
			case CRasterizerState::ECullMode::Back:
				rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
				break;
			case CRasterizerState::ECullMode::Front:
				rasterizer.cullMode = VK_CULL_MODE_FRONT_BIT;
				break;
			}
			switch (rs.myFillMode)
			{
			case CRasterizerState::EFillMode::Wireframe:
				rasterizer.polygonMode = VK_POLYGON_MODE_LINE;
				break;
			}
			switch (rs.myFrontFace)
			{
			case CRasterizerState::EFrontFace::Counter_Clockwise:
				rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
				break;
			}
			pipelineInfo.pRasterizationState = &rasterizer;
		}

		//------------------------------------------------------------------------------------//
		// MSAA
		//------------------------------------------------------------------------------------//
		{
			VkPipelineMultisampleStateCreateInfo multisampling = {};
			multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
			multisampling.sampleShadingEnable = VK_FALSE;
			multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
			multisampling.minSampleShading = 1.0f;
			multisampling.pSampleMask = nullptr;
			multisampling.alphaToCoverageEnable = VK_FALSE;
			multisampling.alphaToOneEnable = VK_FALSE;

			pipelineInfo.pMultisampleState = &multisampling;
		}

		//------------------------------------------------------------------------------------//
		// COLORBLENDING
		//------------------------------------------------------------------------------------//
		{
			std::vector<VkPipelineColorBlendAttachmentState> colorBlendAttachments(aPSODesc.numRTVs);

			for (unsigned char i = 0; i < aPSODesc.numRTVs; ++i)
			{
				auto& blendState = myBlendStates[(unsigned int)aPSODesc.blendState];

				VkPipelineColorBlendAttachmentState& colorBlendAttachment = colorBlendAttachments[i];

				colorBlendAttachments[i].colorWriteMask = 0;
				auto& rtBlendState = blendState.myRenderTargetBlendStates[i];
				if (rtBlendState.myColorWriteMask & SRenderTargetBlendDesc::EColorWriteEnabled::ENABLE_RED)
				{
					colorBlendAttachments[i].colorWriteMask |= VK_COLOR_COMPONENT_R_BIT;
				}
				if (rtBlendState.myColorWriteMask & SRenderTargetBlendDesc::EColorWriteEnabled::ENABLE_GREEN)
				{
					colorBlendAttachments[i].colorWriteMask |= VK_COLOR_COMPONENT_G_BIT;
				}
				if (rtBlendState.myColorWriteMask & SRenderTargetBlendDesc::EColorWriteEnabled::ENABLE_BLUE)
				{
					colorBlendAttachments[i].colorWriteMask |= VK_COLOR_COMPONENT_B_BIT;
				}
				if (rtBlendState.myColorWriteMask & SRenderTargetBlendDesc::EColorWriteEnabled::ENABLE_ALPHA)
				{
					colorBlendAttachments[i].colorWriteMask |= VK_COLOR_COMPONENT_A_BIT;
				}

				colorBlendAttachment.blendEnable = rtBlendState.myBlendEnable ? VK_TRUE : VK_FALSE;
				colorBlendAttachment.srcColorBlendFactor = _ConvertBlendVk(rtBlendState.mySrcBlend);
				colorBlendAttachment.dstColorBlendFactor = _ConvertBlendVk(rtBlendState.myDestBlend);
				colorBlendAttachment.colorBlendOp = _ConvertBlendOpVk(rtBlendState.myBlendOp);
				colorBlendAttachment.srcAlphaBlendFactor = _ConvertBlendVk(rtBlendState.mySrcBlendAlpha);
				colorBlendAttachment.dstAlphaBlendFactor = _ConvertBlendVk(rtBlendState.myDestBlendAlpha);
				colorBlendAttachment.alphaBlendOp = _ConvertBlendOpVk(rtBlendState.myBlendOpAlpha);
			}

			VkPipelineColorBlendStateCreateInfo colorBlending = {};
			colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
			colorBlending.logicOpEnable = VK_FALSE;
			colorBlending.logicOp = VK_LOGIC_OP_COPY;
			colorBlending.attachmentCount = static_cast<uint32_t>(colorBlendAttachments.size());
			colorBlending.pAttachments = colorBlendAttachments.data();
			colorBlending.blendConstants[0] = 1.0f;
			colorBlending.blendConstants[1] = 1.0f;
			colorBlending.blendConstants[2] = 1.0f;
			colorBlending.blendConstants[3] = 1.0f;

			pipelineInfo.pColorBlendState = &colorBlending;
		}

		//------------------------------------------------------------------------------------//
		// DYNAMIC STATE
		//------------------------------------------------------------------------------------//
		{
			VkDynamicState dynamicStates[] = {
				VK_DYNAMIC_STATE_VIEWPORT,
				VK_DYNAMIC_STATE_LINE_WIDTH
			};

			VkPipelineDynamicStateCreateInfo dynamicState = {};
			dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
			dynamicState.dynamicStateCount = 2;
			dynamicState.pDynamicStates = dynamicStates;

			pipelineInfo.pDynamicState = nullptr;
		}

		//------------------------------------------------------------------------------------//
		// DEPTH STENCIL
		//------------------------------------------------------------------------------------//
		{
			VkPipelineDepthStencilStateCreateInfo depthstencil = {};
			depthstencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
			//if (aPSODesc.depthStencilState != nullptr)
			//{
			//}
			pipelineInfo.pDepthStencilState = &depthstencil;
		}

		//------------------------------------------------------------------------------------//
		// TESSELATION
		//------------------------------------------------------------------------------------//
		{
			VkPipelineTessellationStateCreateInfo tessellationInfo = {};
			tessellationInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
			tessellationInfo.patchControlPoints = 3;

			pipelineInfo.pTessellationState = &tessellationInfo;
		}

		auto pipelineState = reinterpret_cast<VkPipeline>(pso->myPipelineState);
		if (vkCreateGraphicsPipelines(myLogicalDevice, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &pipelineState) != VK_SUCCESS)
		{
			SAFE_DELETE(pso);
			SC_ERROR_LOG("Could not create PSO.");
			return nullptr;
		}

		return pso;
	}
	bool CGraphicsDevice_Vulkan::CreateGraphicsPSO(const SGraphicsPSODesc & /*aPSODesc*/, CGraphicsPSO * /*aDestinationPSO*/)
	{
		return false;
	}
	const bool CGraphicsDevice_Vulkan::CreateVertexBuffer(const SVertexBufferDesc& aVBDesc, CVertexBuffer* aDestinationBuffer)
	{
		aVBDesc;
		aDestinationBuffer;
		return false;
	}
	const bool CGraphicsDevice_Vulkan::CreateIndexBuffer(std::vector<unsigned int>& aIndexList, CIndexBuffer* aDestinationBuffer)
	{
		aIndexList;
		aDestinationBuffer;
		return false;
	}

	bool CGraphicsDevice_Vulkan::CreateBuffer(const SBufferDesc & /*aBufferDesc*/, CBuffer * /*aDstBuffer*/, const char * /*aIdentifier*/)
	{
		return false;

	}

	bool CGraphicsDevice_Vulkan::CreateBuffer(const SBufferDesc & /*aBufferDesc*/, void * /*aDataPtr*/, uint /*aDataSize*/, CBuffer * /*aDstBuffer*/, const char * /*aIdentifier*/)
	{
		return false;
	}

	bool CGraphicsDevice_Vulkan::CreateBufferView(const SBufferViewDesc & /*aViewDesc*/, const CBuffer * /*aBuffer*/, CBufferView * /*aDstView*/)
	{
		return false;
	}

	void CGraphicsDevice_Vulkan::BindGraphicsPSO(const CGraphicsPSO & aPSO)
	{
		auto pipeline = reinterpret_cast<VkPipeline>(aPSO.myPipelineState);
		vkCmdBindPipeline(myCommandBuffers[0], VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
	}

	void CGraphicsDevice_Vulkan::BindConstantBuffer(void* aData, uint aSize, const uint aBindingIndex)
	{
		aData;
		aSize;
		aBindingIndex;
	}

	void CGraphicsDevice_Vulkan::BindConstantBufferRef(const CConstantBuffer & aConstantBuffer, const uint aBindingIndex)
	{
		aConstantBuffer;
		aBindingIndex;
	}

	void CGraphicsDevice_Vulkan::BindBuffer(const CBufferView & aBufferView, const uint aBindingIndex)
	{
		aBufferView;
		aBindingIndex;
	}

	void CGraphicsDevice_Vulkan::BindBufferRW(const CBufferView & aBufferView, const uint aBindingIndex)
	{
		aBufferView;
		aBindingIndex;
	}

	void CGraphicsDevice_Vulkan::BindTexture(const CTexture & aTexture, const uint aBindingIndex)
	{
		aTexture;
		aBindingIndex;
	}

	void CGraphicsDevice_Vulkan::BindTextureRW(const CTexture & aTexture, const uint aBindingIndex)
	{
		aTexture;
		aBindingIndex;
	}


	void CGraphicsDevice_Vulkan::SetViewport(const SViewport & aViewport)
	{
		aViewport;
	}

	void CGraphicsDevice_Vulkan::SetScissorRect(const SScissorRect & aScissorRect)
	{
		aScissorRect;
	}

	void CGraphicsDevice_Vulkan::SetTopology(const EPrimitiveTopology & aTopology)
	{
		aTopology;
	}

	void CGraphicsDevice_Vulkan::SetVertexBuffer(const unsigned int aStartVertexIndex, const CVertexBuffer & aVertexBuffer)
	{
		aStartVertexIndex;
		aVertexBuffer;
	}

	void CGraphicsDevice_Vulkan::SetIndexBuffer(const CIndexBuffer & aIndexBuffer)
	{
		aIndexBuffer;
	}

	CGraphicsDevice_Vulkan::QueueFamilyIndices CGraphicsDevice_Vulkan::FindQueueFamilies(VkPhysicalDevice aDevice)
	{
		QueueFamilyIndices indices;

		uint32_t queueFamilyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(aDevice, &queueFamilyCount, nullptr);

		std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(aDevice, &queueFamilyCount, queueFamilies.data());

		int i = 0;
		for (const auto& queueFamily : queueFamilies) 
		{
			if (indices.IsComplete())
			{
				break;
			}

			VkBool32 presentSupport = false;
			vkGetPhysicalDeviceSurfaceSupportKHR(aDevice, i, mySurface, &presentSupport);
			if (queueFamily.queueCount > 0 && presentSupport) 
			{
				indices.myPresentFamily = i;
			}

			if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) 
			{
				indices.myGraphicsFamily = i;
			}
			++i;
		}

		return indices;
	}

	SwapChainSupportDetails CGraphicsDevice_Vulkan::QuerySwapChainSupport(VkPhysicalDevice aDevice)
	{
		SwapChainSupportDetails details;

		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(aDevice, mySurface, &details.myCapabilities);

		uint32_t formatCount;
		vkGetPhysicalDeviceSurfaceFormatsKHR(aDevice, mySurface, &formatCount, nullptr);

		if (formatCount != 0) 
		{
			details.myFormats.resize(formatCount);
			vkGetPhysicalDeviceSurfaceFormatsKHR(aDevice, mySurface, &formatCount, details.myFormats.data());
		}

		uint32_t presentModeCount;
		vkGetPhysicalDeviceSurfacePresentModesKHR(aDevice, mySurface, &presentModeCount, nullptr);

		if (presentModeCount != 0) 
		{
			details.myPresentModes.resize(presentModeCount);
			vkGetPhysicalDeviceSurfacePresentModesKHR(aDevice, mySurface, &presentModeCount, details.myPresentModes.data());
		}


		return details;
	}

	void CGraphicsDevice_Vulkan::CreateDefaultPSOs()
	{
	}

	void CGraphicsDevice_Vulkan::Destroy()
	{
		if (myLogicalDevice != VK_NULL_HANDLE)
		{
			if (mySwapChain != VK_NULL_HANDLE)
			{
				vkDestroySwapchainKHR(myLogicalDevice, mySwapChain, nullptr);
			}

			for (auto& imageView : mySwapChainImageViews)
			{
				vkDestroyImageView(myLogicalDevice, imageView, nullptr);
			}
			for (auto& framebuffer : mySwapChainFramebuffers) 
			{
				vkDestroyFramebuffer(myLogicalDevice, framebuffer, nullptr);
			}
			vkDestroyPipelineLayout(myLogicalDevice, myPipelineLayout, nullptr);
			vkDestroyRenderPass(myLogicalDevice, myRenderPass, nullptr);
			vkDestroyPipeline(myLogicalDevice, myGraphicsPipeline, nullptr);
			vkDestroyCommandPool(myLogicalDevice, myCommandPool, nullptr);
			vkDestroySemaphore(myLogicalDevice, myRenderFinishedSemaphore, nullptr);
			vkDestroySemaphore(myLogicalDevice, myImageAvailableSemaphore, nullptr);
			vkDestroyDevice(myLogicalDevice, nullptr);
		}

		if (myInstance != VK_NULL_HANDLE)
		{
			if (mySurface != VK_NULL_HANDLE)
			{
				vkDestroySurfaceKHR(myInstance, mySurface, nullptr);
			}
			vkDestroyInstance(myInstance, nullptr);
		}
	}

	const bool CGraphicsDevice_Vulkan::InitInstance(const SGraphicsDeviceStartParameters& aStartParams)
	{
		VkApplicationInfo appInfo = {};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = aStartParams.applicationName;
		appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.pEngineName = "Shift Engine";
		appInfo.engineVersion = VK_MAKE_VERSION(2, 0, 0);
		appInfo.apiVersion = VK_API_VERSION_1_0;

		uint32_t glfwExtensionCount = 0;
		const char** glfwExtensions;
		glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

#ifdef NDEBUG
		const bool enableValidationLayers = false;
#else
		const bool enableValidationLayers = true; 
		const std::vector<const char*> validationLayers = 
		{
			"VK_LAYER_LUNARG_standard_validation"
		};
#endif

		VkInstanceCreateInfo createInstanceInfo = {};
		createInstanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInstanceInfo.pApplicationInfo = &appInfo; 
		if (enableValidationLayers) 
		{
			createInstanceInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
			createInstanceInfo.ppEnabledLayerNames = validationLayers.data();
		}
		else 
		{
			createInstanceInfo.enabledLayerCount = 0;
		}
		createInstanceInfo.enabledExtensionCount = glfwExtensionCount;
		createInstanceInfo.ppEnabledExtensionNames = glfwExtensions;

		Init_Log("\t- Creating VkInstance");
		VkResult result = vkCreateInstance(&createInstanceInfo, nullptr, &myInstance);
		if (result != VK_SUCCESS)
		{
			return false;
		}
		return true;
	}

	const bool CGraphicsDevice_Vulkan::InitPhysicalDevice(const std::vector<const char*>& aDeviceExtensions)
	{
		myPhysicalDevice = VK_NULL_HANDLE;

		uint32_t deviceCount = 0;
		Init_Log("\t- Listing physical devices");
		VkResult result = vkEnumeratePhysicalDevices(myInstance, &deviceCount, nullptr);
		if (deviceCount <= 0)
		{
			SC_ERROR_LOG("No device with Vulkan-support found");
			return false;
		}
		std::vector<VkPhysicalDevice> devices(deviceCount);
		result = vkEnumeratePhysicalDevices(myInstance, &deviceCount, devices.data());

		Init_Log("\t- Physical device found");
		for (auto& device : devices)
		{
			if (IsDeviceSuitable(device, aDeviceExtensions))
			{
				myPhysicalDevice = device;
				break;
			}
		}
		if (myPhysicalDevice == VK_NULL_HANDLE)
		{
			SC_ERROR_LOG("No device with Vulkan-support found");
			return false;
		}
		return true;
	}

	const bool CGraphicsDevice_Vulkan::InitLogicalDevice(const std::vector<const char*>& aDeviceExtensions)
	{
		Init_Log("\t- Creating VkDevice");

		QueueFamilyIndices indices = FindQueueFamilies(myPhysicalDevice);

		std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
		std::set<int> uniqueQueueFamilies = { indices.myGraphicsFamily, indices.myPresentFamily };

		float queuePriority = 1.0f;
		for (int queueFamily : uniqueQueueFamilies) 
		{
			VkDeviceQueueCreateInfo queueCreateInfo = {};
			queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueCreateInfo.queueFamilyIndex = queueFamily;
			queueCreateInfo.queueCount = 1;
			queueCreateInfo.pQueuePriorities = &queuePriority;
			queueCreateInfos.push_back(queueCreateInfo);
		}
		
		VkPhysicalDeviceFeatures deviceFeatures = {}; // Not implemented yet

		VkDeviceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		createInfo.pQueueCreateInfos = queueCreateInfos.data();
		createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
		createInfo.pEnabledFeatures = &deviceFeatures;
		createInfo.enabledLayerCount = 0;
		createInfo.enabledExtensionCount = static_cast<uint32_t>(aDeviceExtensions.size());
		createInfo.ppEnabledExtensionNames = aDeviceExtensions.data();

		if (vkCreateDevice(myPhysicalDevice, &createInfo, nullptr, &myLogicalDevice) != VK_SUCCESS)
		{
			SC_ERROR_LOG("Could not create logical device");
			return false;
		}

		vkGetDeviceQueue(myLogicalDevice, indices.myGraphicsFamily, 0, &myGraphicsQueue);
		vkGetDeviceQueue(myLogicalDevice, indices.myPresentFamily, 0, &myPresentQueue);
		return true;
	}

	const bool CGraphicsDevice_Vulkan::CreateSurface(const SGraphicsDeviceStartParameters & aStartParams)
	{
		Init_Log("\t- Creating [WIN32] window surface");
		VkWin32SurfaceCreateInfoKHR createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
		createInfo.hwnd = (HWND)aStartParams.windowHandle;
		createInfo.hinstance = GetModuleHandle(nullptr);

		auto CreateWin32SurfaceKHR = (PFN_vkCreateWin32SurfaceKHR)vkGetInstanceProcAddr(myInstance, "vkCreateWin32SurfaceKHR");

		if (!CreateWin32SurfaceKHR || CreateWin32SurfaceKHR(myInstance, &createInfo, nullptr, &mySurface) != VK_SUCCESS)
		{
			SC_ERROR_LOG("failed to create window surface!");
			return false;
		}
		return true;
	}

	static VkSurfaceFormatKHR ChooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) 
	{
		if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED) 
		{
			return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
		}

		for (const auto& availableFormat : availableFormats) 
		{
			if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) 
			{
				return availableFormat;
			}
		}

		return availableFormats[0];
	}

	static VkPresentModeKHR ChooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes) 
	{
		VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;

		for (const auto& availablePresentMode : availablePresentModes) 
		{
			if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) 
			{
				return availablePresentMode;
			}
			else if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR) 
			{
				bestMode = availablePresentMode;
			}
		}

		return bestMode;
	}

	static VkExtent2D ChooseSwapExtent(const float aWidth, const float aHeight, const VkSurfaceCapabilitiesKHR& capabilities) 
	{
		if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) 
		{
			return capabilities.currentExtent;
		}
		else 
		{
			VkExtent2D actualExtent = { (uint32_t)aWidth, (uint32_t)aHeight };

			actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
			actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

			return actualExtent;
		}
	}

	const bool CGraphicsDevice_Vulkan::CreateSwapChain(const SGraphicsDeviceStartParameters& aStartParams)
	{
		SwapChainSupportDetails swapChainSupport = QuerySwapChainSupport(myPhysicalDevice);

		VkSurfaceFormatKHR surfaceFormat = ChooseSwapSurfaceFormat(swapChainSupport.myFormats);
		VkPresentModeKHR presentMode = ChooseSwapPresentMode(swapChainSupport.myPresentModes);
		VkExtent2D extent = ChooseSwapExtent((float)aStartParams.windowWidth, (float)aStartParams.windowHeight, swapChainSupport.myCapabilities);

		uint32_t imageCount = swapChainSupport.myCapabilities.minImageCount + 1;
		if (swapChainSupport.myCapabilities.maxImageCount > 0 && imageCount > swapChainSupport.myCapabilities.maxImageCount)
		{
			imageCount = swapChainSupport.myCapabilities.maxImageCount;
		}

		VkSwapchainCreateInfoKHR createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		createInfo.surface = mySurface;
		createInfo.minImageCount = imageCount;
		createInfo.imageFormat = surfaceFormat.format;
		createInfo.imageColorSpace = surfaceFormat.colorSpace;
		createInfo.imageExtent = extent;
		createInfo.imageArrayLayers = 1;
		createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

		QueueFamilyIndices indices = FindQueueFamilies(myPhysicalDevice);
		uint32_t queueFamilyIndices[] = { (uint32_t)indices.myGraphicsFamily, (uint32_t)indices.myPresentFamily };

		if (indices.myGraphicsFamily != indices.myPresentFamily) 
		{
			createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
			createInfo.queueFamilyIndexCount = 2;
			createInfo.pQueueFamilyIndices = queueFamilyIndices;
		}
		else 
		{
			createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
			createInfo.queueFamilyIndexCount = 0;
			createInfo.pQueueFamilyIndices = nullptr;
		}

		createInfo.preTransform = swapChainSupport.myCapabilities.currentTransform;
		createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		createInfo.presentMode = presentMode;
		createInfo.clipped = VK_TRUE;
		createInfo.oldSwapchain = VK_NULL_HANDLE;

		if (vkCreateSwapchainKHR(myLogicalDevice, &createInfo, nullptr, &mySwapChain) != VK_SUCCESS)
		{
			SC_ERROR_LOG("SwapChain could not be created.");
			return false;
		}

		vkGetSwapchainImagesKHR(myLogicalDevice, mySwapChain, &imageCount, nullptr);
		mySwapChainImages.resize(imageCount);
		vkGetSwapchainImagesKHR(myLogicalDevice, mySwapChain, &imageCount, mySwapChainImages.data());

		mySwapChainImageFormat = surfaceFormat.format;
		mySwapChainExtent = extent;

		return true;
	}

	const bool CGraphicsDevice_Vulkan::CreateImageViews()
	{
		mySwapChainImageViews.resize(mySwapChainImages.size());

		for (size_t i = 0; i < mySwapChainImages.size(); i++) 
		{
			VkImageViewCreateInfo createInfo = {};
			createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			createInfo.image = mySwapChainImages[i]; 
			createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
			createInfo.format = mySwapChainImageFormat;
			createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
			createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
			createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
			createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
			createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			createInfo.subresourceRange.baseMipLevel = 0;
			createInfo.subresourceRange.levelCount = 1;
			createInfo.subresourceRange.baseArrayLayer = 0;
			createInfo.subresourceRange.layerCount = 1;
			if (vkCreateImageView(myLogicalDevice, &createInfo, nullptr, &mySwapChainImageViews[i]) != VK_SUCCESS)
			{
				return false;
			}
		}
		return true;
	}

	const bool CGraphicsDevice_Vulkan::CreateGeneralFixedFunctionStates()
	{
		// Rasterizers
		{
			auto& rs = myRasterizerStates[(unsigned int)ERasterizerState::Fill];
			rs.myCullMode = CRasterizerState::ECullMode::Back;
			rs.myFillMode = CRasterizerState::EFillMode::Fill;
			rs.myFrontFace = CRasterizerState::EFrontFace::Clockwise;
		}
		{
			auto& rs = myRasterizerStates[(unsigned int)ERasterizerState::Fill_FrontFaceCull];
			rs.myCullMode = CRasterizerState::ECullMode::Front;
			rs.myFillMode = CRasterizerState::EFillMode::Fill;
			rs.myFrontFace = CRasterizerState::EFrontFace::Clockwise;
		} 
		{
			auto& rs = myRasterizerStates[(unsigned int)ERasterizerState::Fill_NoFaceCull];
			rs.myCullMode = CRasterizerState::ECullMode::None;
			rs.myFillMode = CRasterizerState::EFillMode::Fill;
			rs.myFrontFace = CRasterizerState::EFrontFace::Clockwise;
		}
		{
			auto& rs = myRasterizerStates[(unsigned int)ERasterizerState::Wireframe];
			rs.myCullMode = CRasterizerState::ECullMode::Back;
			rs.myFillMode = CRasterizerState::EFillMode::Wireframe;
			rs.myFrontFace = CRasterizerState::EFrontFace::Clockwise;
		}
		{
			auto& rs = myRasterizerStates[(unsigned int)ERasterizerState::Wireframe_FrontFaceCull];
			rs.myCullMode = CRasterizerState::ECullMode::Front;
			rs.myFillMode = CRasterizerState::EFillMode::Wireframe;
			rs.myFrontFace = CRasterizerState::EFrontFace::Clockwise;
		}
		{
			auto& rs = myRasterizerStates[(unsigned int)ERasterizerState::Wireframe_NoFaceCull];
			rs.myCullMode = CRasterizerState::ECullMode::None;
			rs.myFillMode = CRasterizerState::EFillMode::Wireframe;
			rs.myFrontFace = CRasterizerState::EFrontFace::Clockwise;
		}



		return true;
	}

	void CGraphicsDevice_Vulkan::CreateGraphicsPipeline()
	{

		std::ifstream vfile("Data/Shaders/Object_SimpleVS.spv", std::ios::ate | std::ios::binary);
		size_t vfileSize = (size_t)vfile.tellg();
		std::vector<char> vbuffer(vfileSize);
		vfile.seekg(0);
		vfile.read(vbuffer.data(), vfileSize);
		vfile.close();


		VkShaderModuleCreateInfo vcreateInfo = {};
		vcreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		vcreateInfo.codeSize = vbuffer.size();
		vcreateInfo.pCode = reinterpret_cast<const uint32_t*>(vbuffer.data());

		VkShaderModule vshaderModule;
		if (vkCreateShaderModule(myLogicalDevice, &vcreateInfo, nullptr, &vshaderModule) != VK_SUCCESS)
		{
			return;
		}

		std::ifstream pfile("Data/Shaders/Object_SimplePS.spv", std::ios::ate | std::ios::binary);
		size_t pfileSize = (size_t)pfile.tellg();
		std::vector<char> pbuffer(pfileSize);
		pfile.seekg(0);
		pfile.read(pbuffer.data(), pfileSize);
		pfile.close();


		VkShaderModuleCreateInfo pcreateInfo = {};
		pcreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		pcreateInfo.codeSize = pbuffer.size();
		pcreateInfo.pCode = reinterpret_cast<const uint32_t*>(pbuffer.data());

		VkShaderModule pshaderModule;
		if (vkCreateShaderModule(myLogicalDevice, &pcreateInfo, nullptr, &pshaderModule) != VK_SUCCESS)
		{
			return;
		}

		VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
		vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
		vertShaderStageInfo.module = vshaderModule;
		vertShaderStageInfo.pName = "main";

		VkPipelineShaderStageCreateInfo pShaderStageInfo = {};
		pShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		pShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		pShaderStageInfo.module = pshaderModule;
		pShaderStageInfo.pName = "main";

		VkPipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, pShaderStageInfo };

		VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
		vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
		vertexInputInfo.vertexBindingDescriptionCount = 0;
		vertexInputInfo.pVertexBindingDescriptions = nullptr; // Optional
		vertexInputInfo.vertexAttributeDescriptionCount = 0;
		vertexInputInfo.pVertexAttributeDescriptions = nullptr; // Optional

		VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
		inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		inputAssembly.primitiveRestartEnable = VK_FALSE;

		VkViewport viewport = {};
		viewport.x = 0.0f;
		viewport.y = 0.0f;
		viewport.width = (float)mySwapChainExtent.width;
		viewport.height = (float)mySwapChainExtent.height;
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;

		VkRect2D scissor = {};
		scissor.offset = { 0, 0 };
		scissor.extent = mySwapChainExtent;

		VkPipelineViewportStateCreateInfo viewportState = {};
		viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		viewportState.viewportCount = 1;
		viewportState.pViewports = &viewport;
		viewportState.scissorCount = 1;
		viewportState.pScissors = &scissor;

		VkPipelineRasterizationStateCreateInfo rasterizer = {};
		rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		rasterizer.depthClampEnable = VK_FALSE;
		rasterizer.rasterizerDiscardEnable = VK_FALSE;
		rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
		rasterizer.lineWidth = 1.0f;
		rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
		rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
		rasterizer.depthBiasEnable = VK_FALSE;
		rasterizer.depthBiasConstantFactor = 0.0f; // Optional
		rasterizer.depthBiasClamp = 0.0f; // Optional
		rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

		VkPipelineMultisampleStateCreateInfo multisampling = {};
		multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		multisampling.sampleShadingEnable = VK_FALSE;
		multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
		multisampling.minSampleShading = 1.0f; // Optional
		multisampling.pSampleMask = nullptr; // Optional
		multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
		multisampling.alphaToOneEnable = VK_FALSE; // Optional

		VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
		colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		colorBlendAttachment.blendEnable = VK_FALSE;
		colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
		colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
		colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
		colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
		colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
		colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optional

		VkPipelineColorBlendStateCreateInfo colorBlending = {};
		colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		colorBlending.logicOpEnable = VK_FALSE;
		colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
		colorBlending.attachmentCount = 1;
		colorBlending.pAttachments = &colorBlendAttachment;
		colorBlending.blendConstants[0] = 0.0f; // Optional
		colorBlending.blendConstants[1] = 0.0f; // Optional
		colorBlending.blendConstants[2] = 0.0f; // Optional
		colorBlending.blendConstants[3] = 0.0f; // Optional

		VkDynamicState dynamicStates[] = {
			VK_DYNAMIC_STATE_VIEWPORT,
			VK_DYNAMIC_STATE_LINE_WIDTH
		};

		VkPipelineDynamicStateCreateInfo dynamicState = {};
		dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
		dynamicState.dynamicStateCount = 2;
		dynamicState.pDynamicStates = dynamicStates;

		VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
		pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		pipelineLayoutInfo.setLayoutCount = 0; // Optional
		pipelineLayoutInfo.pSetLayouts = nullptr; // Optional
		pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
		pipelineLayoutInfo.pPushConstantRanges = nullptr; // Optional

		if (vkCreatePipelineLayout(myLogicalDevice, &pipelineLayoutInfo, nullptr, &myPipelineLayout) != VK_SUCCESS) 
		{
			return;
		}

		// RENDER PASS
		VkAttachmentDescription colorAttachment = {};
		colorAttachment.format = mySwapChainImageFormat;
		colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		VkAttachmentReference colorAttachmentRef = {};
		colorAttachmentRef.attachment = 0;
		colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		VkSubpassDescription subpass = {};
		subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpass.colorAttachmentCount = 1;
		subpass.pColorAttachments = &colorAttachmentRef;

		VkRenderPassCreateInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		renderPassInfo.attachmentCount = 1;
		renderPassInfo.pAttachments = &colorAttachment;
		renderPassInfo.subpassCount = 1;
		renderPassInfo.pSubpasses = &subpass;

		VkSubpassDependency dependency = {};
		dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
		dependency.dstSubpass = 0;
		dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.srcAccessMask = 0;
		dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		renderPassInfo.dependencyCount = 1;
		renderPassInfo.pDependencies = &dependency;

		if (vkCreateRenderPass(myLogicalDevice, &renderPassInfo, nullptr, &myRenderPass) != VK_SUCCESS)
		{
			return;
		}

		VkGraphicsPipelineCreateInfo pipelineInfo = {};
		pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		pipelineInfo.stageCount = 2;
		pipelineInfo.pStages = shaderStages;
		pipelineInfo.pVertexInputState = &vertexInputInfo;
		pipelineInfo.pInputAssemblyState = &inputAssembly;
		pipelineInfo.pViewportState = &viewportState;
		pipelineInfo.pRasterizationState = &rasterizer;
		pipelineInfo.pMultisampleState = &multisampling;
		pipelineInfo.pDepthStencilState = nullptr; // Optional
		pipelineInfo.pColorBlendState = &colorBlending;
		pipelineInfo.pDynamicState = nullptr; // Optional
		pipelineInfo.layout = myPipelineLayout;
		pipelineInfo.renderPass = myRenderPass;
		pipelineInfo.subpass = 0;
		pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
		pipelineInfo.basePipelineIndex = -1; // Optional


		if (vkCreateGraphicsPipelines(myLogicalDevice, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &myGraphicsPipeline) != VK_SUCCESS)
		{
			return;
		}

		// FRAMEBUFFER
		mySwapChainFramebuffers.resize(mySwapChainImageViews.size());
		for (size_t i = 0; i < mySwapChainImageViews.size(); i++) 
		{
			VkImageView attachments[] = { mySwapChainImageViews[i] };
			VkFramebufferCreateInfo framebufferInfo = {};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.renderPass = myRenderPass;
			framebufferInfo.attachmentCount = 1;
			framebufferInfo.pAttachments = attachments;
			framebufferInfo.width = mySwapChainExtent.width;
			framebufferInfo.height = mySwapChainExtent.height;
			framebufferInfo.layers = 1;

			if (vkCreateFramebuffer(myLogicalDevice, &framebufferInfo, nullptr, &mySwapChainFramebuffers[i]) != VK_SUCCESS)
			{
				return;
			}
		}

		// COMMAND POOL
		QueueFamilyIndices queueFamilyIndices = FindQueueFamilies(myPhysicalDevice);

		VkCommandPoolCreateInfo poolInfo = {};
		poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		poolInfo.queueFamilyIndex = queueFamilyIndices.myGraphicsFamily;
		poolInfo.flags = 0; // Optional

		if (vkCreateCommandPool(myLogicalDevice, &poolInfo, nullptr, &myCommandPool) != VK_SUCCESS) 
		{
			throw std::runtime_error("failed to create command pool!");
		}

		myCommandBuffers.resize(mySwapChainFramebuffers.size());

		VkCommandBufferAllocateInfo allocInfo = {};
		allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		allocInfo.commandPool = myCommandPool;
		allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		allocInfo.commandBufferCount = (uint32_t)myCommandBuffers.size();

		if (vkAllocateCommandBuffers(myLogicalDevice, &allocInfo, myCommandBuffers.data()) != VK_SUCCESS) 
		{
			throw std::runtime_error("failed to allocate command buffers!");
		}

		for (size_t i = 0; i < myCommandBuffers.size(); i++) 
		{
			VkCommandBufferBeginInfo beginInfo = {};
			beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
			beginInfo.pInheritanceInfo = nullptr; // Optional

			if (vkBeginCommandBuffer(myCommandBuffers[i], &beginInfo) != VK_SUCCESS) 
			{
				throw std::runtime_error("failed to begin recording command buffer!");
			}

			VkRenderPassBeginInfo renderBeginPassInfo = {};
			renderBeginPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			renderBeginPassInfo.renderPass = myRenderPass;
			renderBeginPassInfo.framebuffer = mySwapChainFramebuffers[i];
			renderBeginPassInfo.renderArea.offset = { 0, 0 };
			renderBeginPassInfo.renderArea.extent = mySwapChainExtent;

			VkClearValue clearColor = { 0.0f, 0.0f, 0.0f, 1.0f };
			renderBeginPassInfo.clearValueCount = 1;
			renderBeginPassInfo.pClearValues = &clearColor;

			

			vkCmdBeginRenderPass(myCommandBuffers[i], &renderBeginPassInfo, VK_SUBPASS_CONTENTS_INLINE);

			// THIS IS THE PART THAT IS DONE EACH FRAME
			vkCmdBindPipeline(myCommandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, myGraphicsPipeline);
			vkCmdDraw(myCommandBuffers[i], 3, 1, 0, 0);
			/////////////////////////////////////////////

			vkCmdEndRenderPass(myCommandBuffers[i]);
			if (vkEndCommandBuffer(myCommandBuffers[i]) != VK_SUCCESS)
			{
				throw std::runtime_error("failed to record command buffer!");
			}
		}

		// CREATE SEMAPHORES
		VkSemaphoreCreateInfo semaphoreInfo = {};
		semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
		if (vkCreateSemaphore(myLogicalDevice, &semaphoreInfo, nullptr, &myImageAvailableSemaphore) != VK_SUCCESS ||
			vkCreateSemaphore(myLogicalDevice, &semaphoreInfo, nullptr, &myRenderFinishedSemaphore) != VK_SUCCESS) 
		{

			throw std::runtime_error("failed to create semaphores!");
		}
	}

	const bool CGraphicsDevice_Vulkan::IsDeviceSuitable(VkPhysicalDevice aDevice, const std::vector<const char*>& aDeviceExtensions)
	{
		QueueFamilyIndices indices = FindQueueFamilies(aDevice);
		uint32_t extensionCount = 0;
		vkEnumerateDeviceExtensionProperties(aDevice, nullptr, &extensionCount, nullptr);

		std::vector<VkExtensionProperties> availableExtensions(extensionCount);
		vkEnumerateDeviceExtensionProperties(aDevice, nullptr, &extensionCount, availableExtensions.data());

		std::set<std::string> requiredExtensions(aDeviceExtensions.begin(), aDeviceExtensions.end());

		for (const auto& extension : availableExtensions) 
		{
			requiredExtensions.erase(extension.extensionName);
		}

		bool canUseSwapChain = false;
		if (requiredExtensions.empty())
		{
			SwapChainSupportDetails swapChainSupport = QuerySwapChainSupport(aDevice);
			canUseSwapChain = !swapChainSupport.myFormats.empty() && !swapChainSupport.myPresentModes.empty();
		}
		return indices.IsComplete() && requiredExtensions.empty() && canUseSwapChain;
	}
	CGraphicsContext_Vulkan::CGraphicsContext_Vulkan(const EContextType& aType, CGraphicsDevice_Vulkan* aDevice)
		: CGraphicsContext(aType)
		, myDevice(aDevice)
	{
	}
	CGraphicsContext_Vulkan::~CGraphicsContext_Vulkan()
	{
	}
	void CGraphicsContext_Vulkan::BeginRecording()
	{
	}
	void CGraphicsContext_Vulkan::EndRecording()
	{
	}
	void CGraphicsContext_Vulkan::Draw(const unsigned int aVertexCount, const unsigned int aStartVertexLocation)
	{
		vkCmdDraw(myCommandBuffer, aVertexCount, 1, aStartVertexLocation, 0);
	}
	void CGraphicsContext_Vulkan::DrawIndexed(const unsigned int aIndexCount, const unsigned int aStartIndexLocation, const unsigned int aBaseVertexLocation)
	{
		vkCmdDrawIndexed(myCommandBuffer, aIndexCount, 1, aStartIndexLocation, aBaseVertexLocation, 0);
	}
	void CGraphicsContext_Vulkan::DrawInstanced(const unsigned int aVertexCount, const unsigned int aInstanceCount, const unsigned int aStartVertexLocation, const unsigned int aStartInstanceLocation)
	{
		vkCmdDraw(myCommandBuffer, aVertexCount, aInstanceCount, aStartVertexLocation, aStartInstanceLocation);
	}
	void CGraphicsContext_Vulkan::DrawIndexedInstanced(const unsigned int aIndexCount, const unsigned int aInstanceCount, const unsigned int aStartIndexLocation, const unsigned int aBaseVertexLocation, const unsigned int aStartInstanceLocation)
	{
		vkCmdDrawIndexed(myCommandBuffer, aIndexCount, aInstanceCount, aStartIndexLocation, aBaseVertexLocation, aStartInstanceLocation);
	}
	void CGraphicsContext_Vulkan::Dispatch(const unsigned int aThreadGroupCountX, const unsigned int aThreadGroupCountY, const unsigned int aThreadGroupCountZ)
	{
		vkCmdDispatch(myCommandBuffer, aThreadGroupCountX, aThreadGroupCountY, aThreadGroupCountZ);
	}
}

#endif