#pragma once
#include "SR_GraphicsDefinitions.h"
namespace Shift
{
	struct SVertexBufferDesc
	{
		SC_Handle myVertexData = nullptr;
		unsigned int myStride = 0;
		unsigned int myNumVertices = 0;
	};

	class SR_Buffer;
	class CVertexBuffer
	{
	public:
		CVertexBuffer();
		CVertexBuffer(const CVertexBuffer& aOther);
		~CVertexBuffer();

		void operator=(const CVertexBuffer& aOther);

		bool operator==(const CVertexBuffer& aOther)
		{
			if (myDescription.myNumVertices == aOther.myDescription.myNumVertices &&
				myDescription.myStride == aOther.myDescription.myStride &&
				myDescription.myVertexData == aOther.myDescription.myVertexData &&
				myVertexBufferView == aOther.myVertexBufferView)
			{
				return true;
			}
			return false;
		}

		const unsigned int GetNumVertices() const { return myNumVertices; };

		SVertexBufferDesc myDescription;
		unsigned int myNumVertices;
		SC_Handle myVertexBufferView;
		SC_Ref<SR_Buffer> myBuffer;
	};

}