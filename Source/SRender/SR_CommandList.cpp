#include "SRender_Precompiled.h"
#include "SR_CommandList.h"

namespace Shift
{
	SR_CommandList::SR_CommandList(uint8 aInitialState)
		: myState(aInitialState)
	{
	}
	SR_CommandList::~SR_CommandList()
	{
		myPendingTransitions.clear();
	}
}
