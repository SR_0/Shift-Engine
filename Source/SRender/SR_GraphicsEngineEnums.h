#pragma once
namespace Shift
{
	enum EWindowMode
	{
		Fullscreen,
		BorderlessFullscreen,
		Windowed,
	};

	enum EContextType
	{
		SContext_Graphics,
		SContext_Compute,
		SContext_Copy,

		SContext_Count,
	};

	enum SR_QueueType
	{
		SR_QueueType_Render,
		SR_QueueType_Compute,
		SR_QueueType_Copy,

		SR_QueueType_MaxQueues,
	};
}