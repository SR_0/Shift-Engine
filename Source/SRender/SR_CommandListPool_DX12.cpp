#include "SRender_Precompiled.h"
#include "SR_CommandListPool_DX12.h"
#if ENABLE_DX12
#include "SR_CommandAllocator_DX12.h"

namespace Shift
{
	SR_CommandListPool_DX12::SR_CommandListPool_DX12(D3D12_COMMAND_LIST_TYPE aType, const char* aDebugName)
		: myType(aType)
		, myAllocatorCount(0)
		, myCmdListCount(0)
		, myDebugName(aDebugName)
	{
	}

	SR_CommandListPool_DX12::~SR_CommandListPool_DX12()
	{
	}

	SC_Ref<SR_CommandList_DX12> SR_CommandListPool_DX12::GetCommandList(SR_QueueType aType)
	{
		SC_Ref<SR_CommandList_DX12> list;

		if (myIdleCmdLists.Count())
		{
			SC_MutexLock lock(myCmdListsMutex);
			if (myIdleCmdLists.Count())
			{
				list = myIdleCmdLists.GetLast();
				myIdleCmdLists.RemoveLast();
			}
		}

		SC_Ref<SR_CommandAllocator_DX12> allocator = GetAllocator();
		if (list)
		{
			list->Open(allocator);
			list->BeginRecording(aType);
		}
		else
		{
			list = new SR_CommandList_DX12(myType, allocator, this, aType);
			SC_Atomic_Increment(myCmdListCount);
		}
		return list;
	}

	void SR_CommandListPool_DX12::OpenCommandList(SR_CommandList_DX12* aCmdList)
	{
		SC_Ref<SR_CommandAllocator_DX12> allocator = GetAllocator();
		aCmdList->Open(allocator);
	}

	void SR_CommandListPool_DX12::Update()
	{
		if (!myAllocatorCount && !myIdleCmdLists.Count())
			return;

		const uint64 currentFrame = SC_Timer::GetGlobalFrameIndex();
		const uint64 removeAfterFrames = 100;

		SC_GrowingArray<SC_Ref<SC_RefCounted>> pendingRemoves;

		if (myAllocatorCount)
		{
			SC_MutexLock lock(myAllocatorsMutex);
			if (myIdleAllocators.Count())
			{
				SR_CommandAllocator_DX12* allocator = myIdleAllocators.GetFirst();
				if (allocator->myLastFrameOpened + removeAfterFrames < currentFrame)
				{
					SC_Atomic_Decrement(myAllocatorCount);
					pendingRemoves.Add(allocator);
					myIdleAllocators.RemoveByIndex(0);
				}
			}
			for (int i = int(myActiveAllocators.Count() - 1); i >= 0; --i)
			{
				SR_CommandAllocator_DX12* allocator = myActiveAllocators[i];
				if (allocator->IsIdle())
				{
					allocator->Reset();
					myIdleAllocators.Add(allocator);
					myActiveAllocators.RemoveByIndex(i);
				}
			}
			for (int i = (myInFlightAllocators.Count() - 1); i >= 0; --i)
			{
				SR_CommandAllocator_DX12* allocator = myInFlightAllocators[i];
				if (allocator->IsIdle())
				{
					allocator->Reset();
					myIdleAllocators.Add(allocator);
					myInFlightAllocators.RemoveByIndex(i);
				}
			}
		}

		if (myIdleCmdLists.Count())
		{
			SC_MutexLock lock(myCmdListsMutex);

			if (myIdleCmdLists.Count())
			{
				SR_CommandList_DX12* cmdList = myIdleCmdLists.GetFirst();
				if (cmdList->myLastFrameOpened + removeAfterFrames < currentFrame)
				{
					SC_Atomic_Decrement(myCmdListCount);
					pendingRemoves.Add(cmdList);
					myIdleCmdLists.RemoveByIndex(0);
				}
			}
		}
	}

	SC_Ref<SR_CommandAllocator_DX12> SR_CommandListPool_DX12::GetAllocator()
	{
		{
			SC_MutexLock lock(myAllocatorsMutex);

			if (myActiveAllocators.Count())
			{
				SR_CommandAllocator_DX12* allocator = myActiveAllocators.GetLast();

				allocator->AddRef();
				allocator->BeginCommandList();
				myActiveAllocators.RemoveLast();
				return allocator;
			}

			if (myIdleAllocators.Count())
			{
				SR_CommandAllocator_DX12* allocator = myIdleAllocators.GetLast();

				allocator->AddRef();
				allocator->BeginCommandList();
				myIdleAllocators.RemoveLast();
				return allocator;
			}

			if (!myInFlightAllocators.Empty())
			{
				SR_CommandAllocator_DX12* allocator = myInFlightAllocators.GetFirst();
				if (allocator->IsIdle())
				{
					allocator->AddRef();
					myInFlightAllocators.RemoveLast();
					lock.Unlock();
			
					allocator->Reset();
					allocator->BeginCommandList();
					return allocator;
				}
			}
		}

		SC_Ref<SR_CommandAllocator_DX12> allocator = new SR_CommandAllocator_DX12(myType, this);
		allocator->AddRef();
		allocator->BeginCommandList();
		return allocator;
	}
}
#endif