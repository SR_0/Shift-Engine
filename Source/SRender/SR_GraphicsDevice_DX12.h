#pragma once
#if ENABLE_DX12
#include "SC_Mutex.h"
#include "SR_GraphicsDevice.h"
#include "SR_DescriptorHeap_DX12.h"

struct ID3D12Device;
struct ID3D12Device5;
struct IDXGISwapChain4;
struct ID3D12CommandQueue;
struct ID3D12DescriptorHeap;
struct ID3D12Resource;
struct ID3D12CommandAllocator;
struct ID3D12GraphicsCommandList;
struct ID3D12GraphicsCommandList4;
struct ID3D12Fence;
struct ID3D12RootSignature;
struct D3D12_STATIC_SAMPLER_DESC;
struct D3D12_CPU_DESCRIPTOR_HANDLE;
struct D3D12_GPU_DESCRIPTOR_HANDLE;
namespace Shift
{
	class SR_GraphicsContext_DX12;
	class SC_Window_Win64;
	class SR_RootSignature_DX12;
	class SR_GraphicsDevice_DX12 final : public SR_GraphicsDevice
	{
	public:
		SR_GraphicsDevice_DX12();
		~SR_GraphicsDevice_DX12();

		const bool Init(SC_Window* aWindow) override;
		void BeginFrame() override;
		void EndFrame() override;

		void Present() override;

		SC_Ref<SR_GraphicsContext> GetGraphicsContext(const SR_QueueType& aType = SR_QueueType_Render) override;
		SR_GraphicsContext* GetContext(const SR_QueueType& aType) override;

		//////////////////////
		// Resources

		// Textures
		SC_Ref<SR_TextureBuffer> CreateTextureBuffer(const SR_TextureBufferDesc& aDesc, const char* aID = nullptr) override; 

		SC_Ref<SR_Texture> CreateTexture(const SR_TextureDesc& aTextureDesc, SR_TextureBuffer* aTextureBuffer) override;
		SC_Ref<SR_Texture> CreateRWTexture(const SR_TextureDesc& aTextureDesc, SR_TextureBuffer* aTextureBuffer) override;
		SC_Ref<SR_Texture> GetCreateTexture(const char* aFile) override;

		SC_Ref<SR_RenderTarget> CreateRenderTarget(const SR_RenderTargetDesc& aRenderTargetDesc, SR_TextureBuffer* aTextureBuffer) override;
		SC_Ref<SR_RenderTarget> CreateDepthStencil(const SR_RenderTargetDesc& aRenderTargetDesc, SR_TextureBuffer* aTextureBuffer) override;

		// Buffers
		SC_Ref<SR_Buffer> _CreateBuffer(const SR_BufferDesc& aBufferDesc, void* aInitialData = nullptr, const char* aIdentifier = nullptr) override;
		SC_Ref<SR_BufferView> CreateBufferView(const SBufferViewDesc& aViewDesc, SR_Buffer* aBuffer) override;
		   
		// Shaders
		SC_Ref<SR_ShaderState> CreateShaderState(const SR_ShaderStateDesc& aShaderStateDesc) override;
		SC_Ref<SR_ShaderByteCode> CompileShader(const SR_ShaderDesc& aShaderDesc) override;
		bool ReflectShaderVertexLayout(void* aVertexShaderByteCode, uint aByteCodeSize, CVertexLayout* aOutLayout);

		CVertexLayout* CreateVertexLayout(const CVertexShader* aVS) override;
		CVertexShader* CreateVertexShader(const std::string& aFilePath, const std::string& aEntryPoint) override;
		CPixelShader* CreatePixelShader(const std::string& aFilePath, const std::string& aEntryPoint) override;
		bool CreatePixelShader(const std::string& aShaderCode, CPixelShader* aOutShader) override;
		CGeometryShader* CreateGeometryShader(const std::string& aFilePath, const std::string& aEntryPoint) override;
		CComputeShader* CreateComputeShader(const std::string& aFilePath, const std::string& aEntryPoint) override;
		CGraphicsPSO* CreateGraphicsPSO(const SGraphicsPSODesc& aPSODesc) override;
		bool CreateGraphicsPSO(const SGraphicsPSODesc& aPSODesc, CGraphicsPSO* aDestinationPSO) override;
		bool CreateComputePSO(const SComputePSODesc& aPSODesc, CComputePSO* aDestinationPSO) override;
		const bool CreateVertexBuffer(const SVertexBufferDesc& aVBDesc, CVertexBuffer* aDestinationBuffer) override;
		const bool CreateIndexBuffer(const std::vector<unsigned int>& aIndexList, CIndexBuffer* aDestinationBuffer) override;
		const bool CreateIndexBuffer(const uint* aIndexList, uint aNumIndices, CIndexBuffer* aDestinationBuffer) override;
		SR_Buffer* CreateBuffer(const SR_BufferDesc& aBufferDesc, const char* aIdentifier = nullptr) override;
		SR_Buffer* CreateBuffer(const SR_BufferDesc& aBufferDesc, void* aDataPtr, const char* aIdentifier = nullptr) override;
		bool CreateBufferView(const SBufferViewDesc& aViewDesc, const SR_Buffer* aBuffer, SR_BufferView* aDstView) override;

		void GenerateMips(SR_TextureBuffer* aTextureBuffer) override;

		const SC_Vector2f& GetResolution() const override;

		SR_GraphicsContext::SResourceHeapHandles GetDescriptorRange(uint aNumDescriptors);

		SR_Descriptor_DX12 AllocateDescriptorRange(uint aNumDescriptors);
		const SR_DescriptorHeap_DX12& GetGPUVisibleDescriptorHeap();

		// D3D12 Specific

		static SR_GraphicsDevice_DX12* GetDX12Device();

		void InactivateContext(SR_GraphicsContext_DX12* aCtx);

		CComputePSO* GetGenerateMips2DShader() const;
		CComputePSO* GetGenerateMips3DShader() const;

		SR_DescriptorCPU_DX12 GetStagedHandle() const;
		void CopyDescriptors(uint aNumDescriptors, D3D12_CPU_DESCRIPTOR_HANDLE aHandleDest, D3D12_CPU_DESCRIPTOR_HANDLE aHandleSrc);
		ID3D12Device* GetNativeDevice();
		ID3D12Device5* GetNativeDevice5();
		ID3D12DescriptorHeap* GetResourceHeap();
		ID3D12DescriptorHeap* GetScreenTargetHeap();
		ID3D12DescriptorHeap* GetDepthStencilHeap();
		ID3D12RootSignature* GetGraphicsRootSignature();
		ID3D12RootSignature* GetComputeRootSignature();
		ID3D12GraphicsCommandList* GetCommandList();
		ID3D12QueryHeap* GetQueryHeap();

		void DeleteResource(ID3D12Resource* aResource);
	private:
		const bool PostInit() override;
		void CreateDefaultPSOs() override;
		bool CreateResourceHeap() override;
		void SetFeatureSupport() override;
		void GetStaticSamplerDesc(D3D12_STATIC_SAMPLER_DESC (&aSamplersOut)[SAMPLERSLOT_COUNT]) const;
		void CreateDefaultRootSignatures();

		static SR_GraphicsDevice_DX12* ourGraphicsDevice_DX12;

		// Contexts
		SC_Mutex myContextsMutex;
		SC_GrowingArray<SC_Ref<SR_GraphicsContext_DX12>> myInactiveContexts[SR_QueueType::SR_QueueType_MaxQueues];

		SC_GrowingArray<ID3D12Resource*> myDeletedResources;
		SC_Ref<SR_GraphicsContext> myContext_KeepAlive;
		SR_GraphicsContext_DX12* myContext;

		// Internal Shaders
		CComputePSO* myGenerateMips2D;
		CComputePSO* myGenerateMips3D;
		////////////////////

		SC_Window_Win64* myWindow;

		D3D_FEATURE_LEVEL myFeatureLevel;
		ID3D12Device* myDevice;
		ID3D12Device5* myDevice5; //ID3D12Device5 is for raytracing

		// Depth-stencil
		ID3D12Resource* myDepthStencilBuffer;
		ID3D12DescriptorHeap* myDepthStencilHeap;

		// Font-target
		ID3D12DescriptorHeap* myFontTargetDescHeap;

		enum { Graphics, Compute };
		SC_Ref<SR_RootSignature_DX12> myRootSignatures[2];
		ID3D12RootSignature* myGraphicsRootSignature;
		ID3D12RootSignature* myComputeRootSignature;

		// Resource Heap
		ID3D12DescriptorHeap* myResourceHeapCtx;
		SR_DescriptorHeap_DX12 myResourceHeapCPU;
		SR_DescriptorHeap_DX12 myResourceHeapStagedCPU;
		SR_DescriptorHeap_DX12 myResourceHeapGPU;
		SC_Mutex myDescriptorsLock;
		uint myDescriptorsOffset;

		SR_DescriptorHeap_DX12 myRTVHeap;
		SR_DescriptorHeap_DX12 myDSVHeap;

		// Query
		static constexpr uint ourNumQueriesPerContext = 128;
		ID3D12QueryHeap* myQueryHeap;

		// Debug Interfaces
		ID3D12InfoQueue* myInfoQueue;

		uint myResourceDescriptorIncrementSize;
		uint mySamplerDescriptorIncrementSize;
		uint myResourceCtxOffset;

		bool myIsUsingPlacementHeaps;
	};
}

#endif