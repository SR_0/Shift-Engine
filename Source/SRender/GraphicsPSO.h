#pragma once
#include "SR_RenderStateDescs.h"
#include "SR_GraphicsDefinitions.h"
#include "SR_GraphicsResources.h"
#include "SC_Resource.h"
#include <string>

namespace Shift
{
	class CHullShader;
	class CDomainShader;
	class CGeometryShader;
	class CBlendState;
	class CRasterizerState;
	class CVertexLayout;

	struct SGraphicsPSODesc
	{
		SGraphicsPSODesc()
			: topology(SR_Topology_TriangleList)
		{
		}

		~SGraphicsPSODesc()
		{
		}

		std::string myVertexShader;
		std::string myGeometryShader;
		std::string myPixelShader;

		SR_Topology topology;
		SR_DepthStateDesc myDepthStateDesc;
		SR_RasterizerStateDesc myRasterizerStateDesc;
		SR_BlendStateDesc myBlendStateDesc;
		SR_RenderTargetFormats myRenderTargetFormats;
	};

	class CGraphicsPSO : public SC_Resource
	{
	public:
		CGraphicsPSO();
		CGraphicsPSO(const CGraphicsPSO& aOther);
		~CGraphicsPSO();

		void operator=(const CGraphicsPSO& aOther);

		SGraphicsPSODesc myProperties;
		SC_Handle myPipelineState;
	};

	uint SC_Hash(const SGraphicsPSODesc& aDesc);
	uint SC_Hash(const CGraphicsPSO& aPSO);

}