#pragma once
#include "SC_Enum.h"

namespace Shift
{
	SC_ENUM(SR_ConstantBufferRef,
		SR_ConstantBufferRef_Local0 = 0,
		SR_ConstantBufferRef_Local1,
		SR_ConstantBufferRef_Local2,
		SR_ConstantBufferRef_Local3,
		SR_ConstantBufferRef_ViewConstants,
		SR_ConstantBufferRef_ShadowConstants,
		SR_ConstantBufferRef_EnvironmentConstants
	);
	static constexpr uint SR_ConstantBufferRef_LocalStart = SR_ConstantBufferRef::SR_ConstantBufferRef_Local0;
	static constexpr uint SR_ConstantBufferRef_LocalEnd = SR_ConstantBufferRef::SR_ConstantBufferRef_Local3;
	static constexpr uint SR_ConstantBufferRef_NumLocals = 4;
	static constexpr uint SR_ConstantBufferRef_GlobalStart = SR_ConstantBufferRef::SR_ConstantBufferRef_ViewConstants;
	static constexpr uint SR_ConstantBufferRef_GlobalEnd = SR_ConstantBufferRef::SR_ConstantBufferRef_EnvironmentConstants;
	static constexpr uint SR_ConstantBufferRef_NumGlobals = 3;

	SC_ENUM(SR_SamplerRef,
		SR_SamplerRef_Linear_Clamp = 0,
		SR_SamplerRef_Linear_Wrap,
		SR_SamplerRef_Linear_Mirror,
		SR_SamplerRef_Point_Clamp,
		SR_SamplerRef_Point_Wrap,
		SR_SamplerRef_Point_Mirror,
		SR_SamplerRef_Aniso_Clamp,
		SR_SamplerRef_Aniso_Wrap,
		SR_SamplerRef_Aniso_Mirror, 
		SR_SamplerRef_CmpGreater_Linear_Clamp,
		SR_SamplerRef_CmpGreater_Point_Clamp,
		SR_SamplerRef_CmpGreater_Aniso_Clamp
	);

	SC_ENUM(SR_TextureRef,
		SR_TextureRef_Local0 = 0,
		SR_TextureRef_Local1,
		SR_TextureRef_Local2,
		SR_TextureRef_Local3,
		SR_TextureRef_Local4,
		SR_TextureRef_Local5,
		SR_TextureRef_Local6,
		SR_TextureRef_Local7,
		SR_TextureRef_Local8,
		SR_TextureRef_Local9,
		SR_TextureRef_Local10,
		SR_TextureRef_Local11,
		SR_TextureRef_Local12,
		SR_TextureRef_Local13,
		SR_TextureRef_Local14,
		SR_TextureRef_Local15,
		SR_TextureRef_Local16,
		SR_TextureRef_Local17,
		SR_TextureRef_Local18,
		SR_TextureRef_Local19,
		SR_TextureRef_Local20,
		SR_TextureRef_Local21,
		SR_TextureRef_Local22,
		SR_TextureRef_Local23,
		SR_TextureRef_Local24,
		SR_TextureRef_Local25,
		SR_TextureRef_Local26,
		SR_TextureRef_Local27,
		SR_TextureRef_Local28,
		SR_TextureRef_Local29,
		SR_TextureRef_Local30,
		SR_TextureRef_Local31,
		SR_TextureRef_Depth,
		SR_TextureRef_Stencil,
		SR_TextureRef_GBuffer_Color,
		SR_TextureRef_GBuffer_Normal,
		SR_TextureRef_GBuffer_ARM,
		SR_TextureRef_GBuffer_Optional_Emission,
		SR_TextureRef_GBuffer_Optional_MotionVector,
		SR_TextureRef_ShadowMapCSM,
		SR_TextureRef_ShadowMapNoise,
		SR_TextureRef_AmbientOcclusion,
		SR_TextureRef_SkyProbe,
		SR_TextureRef_SkyDiffuse,
		SR_TextureRef_SkySpecular,
		SR_TextureRef_SkyBrdf
	);
	static constexpr uint SR_TextureRef_LocalStart = SR_TextureRef::SR_TextureRef_Local0;
	static constexpr uint SR_TextureRef_LocalEnd = SR_TextureRef::SR_TextureRef_Local31;
	static constexpr uint SR_TextureRef_NumLocals = 32;
	static constexpr uint SR_TextureRef_GlobalStart = SR_TextureRef::SR_TextureRef_Depth;
	static constexpr uint SR_TextureRef_GlobalEnd = SR_TextureRef::SR_TextureRef_SkyBrdf;
	static constexpr uint SR_TextureRef_NumGlobals = 14;


	SC_ENUM(SR_TextureRWRef,
		SR_TextureRWRef_Local0 = 0,
		SR_TextureRWRef_Local1,
		SR_TextureRWRef_Local2,
		SR_TextureRWRef_Local3,
		SR_TextureRWRef_Local4,
		SR_TextureRWRef_Local5,
		SR_TextureRWRef_Local6,
		SR_TextureRWRef_Local7
		);
	static constexpr uint SR_TextureRWRef_LocalStart = SR_TextureRWRef::SR_TextureRWRef_Local0;
	static constexpr uint SR_TextureRWRef_LocalEnd = SR_TextureRWRef::SR_TextureRWRef_Local7;
	static constexpr uint SR_TextureRWRef_NumLocals = 8;

	SC_ENUM(SR_BufferRef,
		SR_BufferRef_Local0 = 0,
		SR_BufferRef_Local1,
		SR_BufferRef_Local2,
		SR_BufferRef_Local3,
		SR_BufferRef_Local4,
		SR_BufferRef_Local5,
		SR_BufferRef_Local6,
		SR_BufferRef_Local7,
		SR_BufferRef_Local8,
		SR_BufferRef_Local9,
		SR_BufferRef_Local10,
		SR_BufferRef_Local11,
		SR_BufferRef_Local12,
		SR_BufferRef_Local13,
		SR_BufferRef_Local14,
		SR_BufferRef_Local15,
		SR_BufferRef_Local16,
		SR_BufferRef_Local17,
		SR_BufferRef_Local18,
		SR_BufferRef_Local19,
		SR_BufferRef_Local20,
		SR_BufferRef_Local21,
		SR_BufferRef_Local22,
		SR_BufferRef_Local23,
		SR_BufferRef_Local24,
		SR_BufferRef_Local25,
		SR_BufferRef_Local26,
		SR_BufferRef_Local27,
		SR_BufferRef_Local28,
		SR_BufferRef_Local29,
		SR_BufferRef_Local30,
		SR_BufferRef_Local31
		);
	static constexpr uint SR_BufferRef_LocalStart = SR_BufferRef::SR_BufferRef_Local0;
	static constexpr uint SR_BufferRef_LocalEnd = SR_BufferRef::SR_BufferRef_Local31;
	static constexpr uint SR_BufferRef_NumLocals = 32;
	static constexpr uint SR_BufferRef_NumGlobals = 0;

	SC_ENUM(SR_BufferRWRef,
		SR_BufferRWRef_Local0 = 0,
		SR_BufferRWRef_Local1,
		SR_BufferRWRef_Local2,
		SR_BufferRWRef_Local3,
		SR_BufferRWRef_Local4,
		SR_BufferRWRef_Local5,
		SR_BufferRWRef_Local6,
		SR_BufferRWRef_Local7
		);
	static constexpr uint SR_BufferRWRef_LocalStart = SR_BufferRWRef::SR_BufferRWRef_Local0;
	static constexpr uint SR_BufferRWRef_LocalEnd = SR_BufferRWRef::SR_BufferRWRef_Local7;
	static constexpr uint SR_BufferRWRef_NumLocals = 8; 
	
}