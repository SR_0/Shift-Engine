#pragma once
#include "SR_GraphicsDefinitions.h"
#include "SR_GraphicsEngineEnums.h"
#include "SR_GraphicsResources.h"
#include "SR_RenderThreadPool.h"

#define GPU_RESOURCE_HEAP_CBV_COUNT	14
#define GPU_RESOURCE_HEAP_SRV_COUNT	64
#define GPU_RESOURCE_HEAP_UAV_COUNT	64
#define GPU_RESOURCE_MAX_COUNT_PER_CONTEXT (GPU_RESOURCE_HEAP_CBV_COUNT + GPU_RESOURCE_HEAP_SRV_COUNT + GPU_RESOURCE_HEAP_UAV_COUNT)

#define GPU_SAMPLER_HEAP_COUNT	16

#define MAX_NUM_RENDERTARGETS 8

namespace Shift
{
	enum ERootParamIndex : unsigned char
	{
		CBV = 0,
		SRV,
		UAV,
		Sampler,
	};

	struct SViewport
	{
		SViewport()
			: topLeftX(0)
			, topLeftY(0)
			, width(0)
			, height(0)
			, minDepth(0)
			, maxDepth(1)
		{}

		bool operator==(const SViewport& aOther) const
		{
			if (topLeftX == aOther.topLeftX &&
				topLeftY == aOther.topLeftY &&
				width == aOther.width &&
				height == aOther.height &&
				minDepth == aOther.minDepth &&
				maxDepth == aOther.maxDepth)
				return true;

			return false;
		}

		float topLeftX;
		float topLeftY;
		float width;
		float height;
		float minDepth;
		float maxDepth;
	};
	struct SScissorRect
	{
		SScissorRect()
			: left(0)
			, top(0)
			, right(0)
			, bottom(0)
		{}

		long left;
		long top;
		long right;
		long bottom;
	};

	class SR_Texture;
	class CConstantBuffer;
	class CGeometryShader;
	class CComputeShader;
	class SR_WaitEvent;
	class SR_GraphicsContext;
	class SC_Window;
	class SR_SwapChain;
	class SR_ShaderStateCache;
	class SR_ShaderCompiler;

	struct SFeatureSupport
	{
		SFeatureSupport()
			: myEnableConservativeRaster(false)
			, myEnableTiledResources(false)
			, myEnableRaytracing(false)
			, myEnableRenderPass(false)
			, myEnableTypedLoadAdditionalFormats(false)
			, myResourceBindingTier(1)
			, myTiledResourcesTier(0)
			, myResourceHeapTier(1)
			, myConservativeRasterTier(0)
			, myRaytracingTier(0)
			, myRenderPassTier(0)
		{}

		uint myResourceBindingTier;
		uint myTiledResourcesTier;
		uint myResourceHeapTier;
		uint myConservativeRasterTier;
		uint myRaytracingTier;
		uint myRenderPassTier;
		bool myEnableTiledResources : 1;
		bool myEnableConservativeRaster : 1;
		bool myEnableRaytracing : 1;
		bool myEnableRenderPass : 1;
		bool myEnableTypedLoadAdditionalFormats : 1;
	};

	class SR_GraphicsDevice
	{
	public:
		static void Destroy();

		SR_GraphicsDevice();
		virtual ~SR_GraphicsDevice();

		virtual const bool Init(SC_Window* aWindow) = 0;
		virtual void BeginFrame() = 0;
		virtual void EndFrame() = 0;

		virtual void Present() = 0;

		virtual SC_Ref<SR_GraphicsContext> GetGraphicsContext(const SR_QueueType& aType = SR_QueueType_Render) = 0;
		virtual SR_GraphicsContext* GetContext(const SR_QueueType& aType) = 0;

		//////////////////////
		// Resources

		// Textures
		virtual SC_Ref<SR_TextureBuffer> CreateTextureBuffer(const SR_TextureBufferDesc& aDesc, const char* aID = nullptr) = 0;

		virtual SC_Ref<SR_Texture> CreateTexture(const SR_TextureDesc& aTextureDesc, SR_TextureBuffer* aTextureBuffer) = 0;
		virtual SC_Ref<SR_Texture> CreateRWTexture(const SR_TextureDesc& aTextureDesc, SR_TextureBuffer* aTextureBuffer) = 0;
		virtual SC_Ref<SR_Texture> GetCreateTexture(const char* aFile) = 0;

		virtual SC_Ref<SR_RenderTarget> CreateRenderTarget(const SR_RenderTargetDesc& aRenderTargetDesc, SR_TextureBuffer* aTextureBuffer) = 0;
		virtual SC_Ref<SR_RenderTarget> CreateDepthStencil(const SR_RenderTargetDesc& aRenderTargetDesc, SR_TextureBuffer* aTextureBuffer) = 0;

		// Buffers
		virtual SC_Ref<SR_Buffer> _CreateBuffer(const SR_BufferDesc& aBufferDesc, void* aInitialData = nullptr, const char* aIdentifier = nullptr) = 0;
		virtual SC_Ref<SR_BufferView> CreateBufferView(const SBufferViewDesc& aViewDesc, SR_Buffer* aBuffer) = 0;

		// Shaders
		virtual SC_Ref<SR_ShaderState> CreateShaderState(const SR_ShaderStateDesc& aShaderStateDesc) = 0;
		virtual SC_Ref<SR_ShaderByteCode> CompileShader(const SR_ShaderDesc& aShaderDesc) = 0;

		virtual CVertexLayout* CreateVertexLayout(const CVertexShader* aVS) = 0;
		virtual CPixelShader* CreatePixelShader(const std::string& aFilePath, const std::string& aEntryPoint);
		virtual bool CreatePixelShader(const std::string& aShaderCode, CPixelShader* aOutShader) = 0;
		virtual CVertexShader* CreateVertexShader(const std::string& aFilePath, const std::string& aEntryPoint);
		virtual CGeometryShader* CreateGeometryShader(const std::string& aFilePath, const std::string& aEntryPoint) = 0;
		virtual CComputeShader* CreateComputeShader(const std::string& aFilePath, const std::string& aEntryPoint) = 0;
		virtual CGraphicsPSO* CreateGraphicsPSO(const SGraphicsPSODesc& aPSODesc) = 0;
		virtual bool CreateGraphicsPSO(const SGraphicsPSODesc& aPSODesc, CGraphicsPSO* aDestinationPSO) = 0;
		virtual bool CreateComputePSO(const SComputePSODesc& aPSODesc, CComputePSO* aDestinationPSO) = 0;
		virtual const bool CreateVertexBuffer(const SVertexBufferDesc& aVBDesc, CVertexBuffer* aDestinationBuffer) = 0;
		virtual const bool CreateIndexBuffer(const std::vector<unsigned int>& aIndexList, CIndexBuffer* aDestinationBuffer) = 0;
		virtual const bool CreateIndexBuffer(const uint* aIndexList, uint aNumIndices, CIndexBuffer* aDestinationBuffer) = 0;
		virtual SR_Buffer* CreateBuffer(const SR_BufferDesc& aBufferDesc, const char* aIdentifier = nullptr) = 0;
		virtual SR_Buffer* CreateBuffer(const SR_BufferDesc& aBufferDesc, void* aDataPtr, const char* aIdentifier = nullptr) = 0;
		virtual bool CreateBufferView(const SBufferViewDesc& aViewDesc, const SR_Buffer* aBuffer, SR_BufferView* aDstView) = 0;

		virtual void GenerateMips(SR_TextureBuffer* aTextureBuffer) = 0;

		virtual const SC_Vector2f& GetResolution() const = 0;

		SC_Ref<SR_Waitable> PostRenderTask(std::function<void()> aTask, uint aWaitMode = SR_WaitMode_CPU);
		SC_Ref<SR_Waitable> PostComputeTask(std::function<void()> aTask, uint aWaitMode = SR_WaitMode_CPU);
		SC_Ref<SR_Waitable> PostCopyTask(std::function<void()> aTask, uint aWaitMode = SR_WaitMode_CPU);
		bool IsRenderPoolIdle() const;

		bool AddToReleasedResourceCache(SR_Resource* aResource);

		const SR_GraphicsAPI& APIType() const;
		const SR_Texture& GetDepthTexture() const { return myDepthTexture; };
		SR_Texture& GetDepthTexture() { return myDepthTexture; };
		uint GetDrawCallCount() const { return myNumDrawCalls; }
		uint GetDispatchCallCount() const { return myNumDispatchCalls; }

		SR_SwapChain* GetSwapChain() const { return mySwapChain; }
		SR_RenderThreadPool& GetThreadPool() { return myRenderThreadPool; }
		SR_ShaderCompiler* GetShaderCompiler() { return myShaderCompiler; }

		static SR_GraphicsDevice* GetDevice() { return ourGraphicsDevice; };
		static uint GetUsedVRAM() { return uint(ourGraphicsDevice->myUsedVRAM * 0.000001f); }
	protected:
		virtual const bool PostInit();
		virtual void CreateDefaultPSOs() = 0;
		virtual bool CreateResourceHeap() { return true; }
		virtual void SetFeatureSupport() = 0;
		void ReleaseResources();

		static SR_GraphicsDevice* ourGraphicsDevice;
		SR_RenderThreadPool myRenderThreadPool;

		SC_GrowingArray<SR_Resource*> myReleasedResourceCache;
		// Subsystems
		SC_UniquePtr<SR_SwapChain> mySwapChain;
		SC_UniquePtr<SR_ShaderStateCache> myShaderStateCache;
		SC_UniquePtr<SR_ShaderCompiler> myShaderCompiler;

		SC_Ref<SR_Texture> myBlack4x4;
		SC_Ref<SR_Texture> myWhite4x4;
		SC_Ref<SR_Texture> myGrey4x4;

		SR_Texture myDepthTexture;
		std::array<CGraphicsPSO*, EDefaultGraphicsPSO::EDGPSO_Count> myDefaultGraphicsPSOs = {nullptr};
		uint myNumDrawCalls;
		uint myNumDispatchCalls;

		SR_GraphicsAPI myAPIType;

		uint myUsedVRAM;

		SFeatureSupport myFeatureSupport;
		uint8 myShaderOptimizationLevel;
		bool myUseDebugDevice : 1;
		bool myDebugBreakOnError : 1;
		bool myDebugBreakOnWarning : 1;
		bool myDebugShaders : 1;
		bool mySkipShaderOptimizations : 1;
		bool myIsUsing16BitDepth : 1;
	};
}
