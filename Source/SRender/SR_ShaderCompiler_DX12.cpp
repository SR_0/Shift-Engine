#include "SRender_Precompiled.h"
#include "SR_ShaderCompiler_DX12.h"

#if ENABLE_DX12

#include "dxc/dxcapi.h"
#include <d3d12shader.h>
#include <fstream>
#include <sstream>

#pragma comment(lib,"dxcompiler.lib")

namespace Shift
{
	static const wchar_t* locGetHLSLTarget(const SR_ShaderType aShaderType)
	{
		switch (aShaderType)
		{
		case SR_ShaderType_Vertex:
			return L"vs_6_4";
		case SR_ShaderType_Pixel:
			return L"ps_6_4";

#if SR_ENABLE_TESSELATION_SHADERS
		case SR_ShaderType_Hull:
			return L"hs_6_4";
		case SR_ShaderType_Domain:
			return L"ds_6_4";
#endif

#if SR_ENABLE_GEOMETRY_SHADERS
		case SR_ShaderType_Geometry:
			return L"gs_6_4";
#endif
		case SR_ShaderType_Compute:
			return L"cs_6_4";
		case SR_ShaderType_Mesh:
			return L"ms_6_5";
		case SR_ShaderType_Amplification:
			return L"as_6_5";
		case SR_ShaderType_Raytracing:
		case SR_ShaderType_Raytracing_ClosestHit:
		case SR_ShaderType_Raytracing_AnyHit:
		case SR_ShaderType_Raytracing_Miss:
		case SR_ShaderType_Raytracing_Intersection:
			return L"lib_6_3";
		default:
			assert(false);
			return L"Unknown target";
		}
	}

	template<class T>
	uint locGetRefIndex(const std::string& aLineOfCode)
	{
		const int* values = T::Values();
		const char* const* refs = T::Names();
		const uint count = T::myCount;

		for (uint i = 0; i < count; ++i)
		{
			if (aLineOfCode.find(refs[i]) != aLineOfCode.npos)
				return values[i];
		}

		assert(false);
		return SC_UINT_MAX;
	}

	template<class T>
	uint locGetLocalRefIndex(const std::string& aLineOfCode, const char* aType)
	{
		const int* values = T::Values();
		const char* const* cbRefs = T::Names();
		const uint count = T::myCount;

		std::string registerString;
		registerString.reserve(128);
		registerString = T::TypeName;
		registerString += "_Local";

		size_t index = aLineOfCode.rfind(aType);
		std::string temp = aLineOfCode.substr(aLineOfCode.rfind(aType) + 17, (aLineOfCode.length() - 1) - index);
		registerString += temp;

		for (uint i = 0; i < count; ++i)
		{
			if (registerString == cbRefs[i])
				return values[i];
		}

		assert(false);
		return SC_UINT_MAX;
	}
	/*static uint locGetTextureRefIndex(const std::string& aLineOfCode)
	{
		const int* values = SR_TextureRef::Values();
		const char* const* textureRefs = SR_TextureRef::Names();
		const uint count = SR_TextureRef::myCount;

		for (uint i = 0; i < count; ++i)
		{
			if (aLineOfCode.find(textureRefs[i]) != aLineOfCode.npos)
				return values[i];
		}

		assert(false);
		return SC_UINT_MAX;
	}

	static uint locGetSamplerRefIndex(const std::string& aLineOfCode)
	{
		const int* values = SR_SamplerRef::Values();
		const char* const* samplerRefs = SR_SamplerRef::Names();
		const uint count = SR_SamplerRef::myCount;

		for (uint i = 0; i < count; ++i)
		{
			if (aLineOfCode.find(samplerRefs[i]) != aLineOfCode.npos)
				return values[i];
		}

		assert(false);
		return SC_UINT_MAX;
	}*/



	SR_ShaderCompiler_DX12::SR_ShaderCompiler_DX12()
	{
		HRESULT hr = DxcCreateInstance(CLSID_DxcCompiler, SR_IID_PPV_ARGS(&myCompiler));
		if (FAILED(hr))
			SC_ERROR_LOG("Failed to create compiler DXC");
		
		hr = DxcCreateInstance(CLSID_DxcUtils, SR_IID_PPV_ARGS(&myUtils));
		if (FAILED(hr))
			SC_ERROR_LOG("Failed to create instance DXC");
		
		hr = myUtils->CreateDefaultIncludeHandler(&myIncludeHandler);
		if (FAILED(hr))
			SC_ERROR_LOG("Failed to create default include handler DXC");
		
	}

	SR_ShaderCompiler_DX12::~SR_ShaderCompiler_DX12()
	{
	}

	SC_Ref<SR_ShaderByteCode> SR_ShaderCompiler_DX12::CompileShaderFromFile(const char* aShaderFile, const SR_ShaderType aShaderType)
	{
		std::string generatedHLSLFile;
		std::string hlslCode = LoadHLSL(aShaderFile, generatedHLSLFile);

		DxcBuffer sourceBuffer;
		sourceBuffer.Ptr = hlslCode.data();
		sourceBuffer.Size = hlslCode.size();
		sourceBuffer.Encoding = DXC_CP_ACP;


		if (SC_Ref<SR_ShaderByteCode> shaderByteCode = FindCompiledByteCode(sourceBuffer))
		{
			return shaderByteCode;
		}

		return CompileInternal(sourceBuffer, generatedHLSLFile.c_str(), locGetHLSLTarget(aShaderType));
	}

	SC_Ref<SR_ShaderByteCode> SR_ShaderCompiler_DX12::CompileShader()
	{
		return nullptr;
	}
	SC_Ref<SR_ShaderByteCode> SR_ShaderCompiler_DX12::CompileInternal(DxcBuffer& aDxcBuffer, const char* aShaderFile, const wchar_t* aTarget)
	{
		SC_GrowingArray<LPCWSTR> compileArgs;
		compileArgs.PreAllocate(13);

		// Filename (Optional)
		compileArgs.Add(ToWString(aShaderFile).c_str());

		// Entry Point
		compileArgs.Add(L"-E");
		compileArgs.Add(L"main");

		// Target
		compileArgs.Add(L"-T");
		compileArgs.Add(aTarget);

		// Debug Info
		if (myDebugShaders)
			compileArgs.Add(L"-Zi");

		// Defines
		//compileArgs.Add(L"-D");
		//compileArgs.Add(L"MYDEFINE=1");

		//// Stored in pdb (Optional)
		//compileArgs.Add(L"-Fo");
		//compileArgs.Add(L"myshader.bin");

		//// pdb filename
		//compileArgs.Add(L"-Fd");
		//compileArgs.Add(L"myshader.pdb");

		// Strip reflection into a separate blob
		compileArgs.Add(L"-Qstrip_reflect"); 
		compileArgs.Add(L"-flegacy-macro-expansion");

		IDxcResult* compileResults;
		HRESULT hr = myCompiler->Compile(
			&aDxcBuffer,					// Source buffer.
			compileArgs.GetBuffer(),		// Array of pointers to arguments.
			compileArgs.Count(),			// Number of arguments.
			nullptr,				// User-provided interface to handle #include directives (optional).
			IID_PPV_ARGS(&compileResults)	// Compiler output status, buffer, and errors.
		);

		SC_Ref<SR_ShaderByteCode> outShaderByteCode;
		compileResults->GetStatus(&hr);
		if (!FAILED(hr))
		{
			IDxcBlob* shaderByteCode = nullptr;
			IDxcBlobUtf16* shaderName = nullptr;
			hr = compileResults->GetOutput(DXC_OUT_OBJECT, SR_IID_PPV_ARGS(&shaderByteCode), &shaderName);

			outShaderByteCode = new SR_ShaderByteCode();
			outShaderByteCode->myByteCodeSize = static_cast<uint>(shaderByteCode->GetBufferSize());
			outShaderByteCode->myByteCode = new uint8[outShaderByteCode->myByteCodeSize];
			SC_Memcpy(outShaderByteCode->myByteCode, shaderByteCode->GetBufferPointer(), outShaderByteCode->myByteCodeSize);

			shaderByteCode->Release();
			if(shaderName)
				shaderName->Release();

			// TODO: output bytecode to file for faster loading later on.
			WriteByteCode(shaderByteCode, (void*)aDxcBuffer.Ptr, (uint)aDxcBuffer.Size);
		}
		else
		{
			SC_ERROR_LOG("Failed to compile shader [%s]", aShaderFile);
			IDxcBlobUtf16* name = nullptr;
			IDxcBlobUtf8* errors = nullptr;
			compileResults->GetOutput(DXC_OUT_ERRORS, SR_IID_PPV_ARGS(&errors), &name);
			if (errors != nullptr && errors->GetStringLength() != 0)
				SC_ERROR_LOG("Shader compilation error: %s", errors->GetStringPointer());

			errors->Release();
		}

		compileResults->Release();
		return outShaderByteCode;
	}

	void SR_ShaderCompiler_DX12::InsertHeaderHLSL(std::string& aShaderCode) const
	{
		static constexpr const char* header = 
			"#pragma warning(error: 3206)\n" // Implicit truncation of vector type
			"#pragma warning(error: 3556)\n" // Interger divides may be slower
			"#pragma warning(error: 1519)\n" // Macro redefinition

			"float2 SR_Transform(float2x2 aMat, float2 aVec) { return mul(aMat, aVec); }\n"
			"float3 SR_Transform(float3x3 aMat, float3 aVec) { return mul(aMat, aVec); }\n"
			"float4 SR_Transform(float4x4 aMat, float4 aVec) { return mul(aMat, aVec); }\n"
			"float4 SR_Transform(float4x4 aMat, float3 aVec) { return mul(aMat, float4(aVec, 1.0)); }\n"

			"#define SR_Sample(sampler, texture, uv) texture.Sample(sampler, uv)\n"
			"#define SR_SampleLod0(sampler, texture, uv) texture.SampleLevel(sampler, uv, 0)\n"
			"#define SR_SampleLod(sampler, texture, uv, lod) texture.SampleLevel(sampler, uv, lod)\n"
			"#define SR_SampleCmp(sampler, texture, uv, comperand) texture.SampleCmpLevel(sampler, uv, comperand)\n"
			"#define SR_SampleCmpLod0(sampler, texture, uv, comperand) texture.SampleCmpLevelZero(sampler, uv, comperand)\n"

			"#define SR_Load(res, location) res.Load(location)\n"
			"#define SR_Load2(res, location) res.Load2(location)\n"
			"#define SR_Load3(res, location) res.Load3(location)\n"
			"#define SR_Load4(res, location) res.Load4(location)\n"

			"#define SR_Sampler(name, slot) SamplerState name : register(s ## slot)\n"
			"#define SR_SamplerCmp(name, slot) SamplerComparisonState name : register(s ## slot)\n"

			"#define SR_Texture2D(name, slot) Texture2D name : register(t ## slot)\n" 
			"#define SR_TextureRW2D(name, slot) RWTexture2D<float4> name : register(u ## slot)\n"
			"#define SR_TextureRWTyped2D(name, slot, type) RWTexture2D<type> name : register(u ## slot)\n"
			"#define SR_TextureArray2D(name, slot, type) Texture2DArray<type> name : register(t ## slot)\n"
			"#define SR_TextureArrayRW2D(name, slot, type) RWTexture2DArray<type> name : register(u ## slot)\n"
			"#define SR_Texture3D(name, slot, type) Texture3D<type> name : register(t ## slot)\n"
			"#define SR_TextureRW3D(name, slot, type) RWTexture3D<type> name : register(u ## slot)\n"
			"#define SR_TextureCube(name, slot) TextureCube name : register(t ## slot)\n"

			"#define SR_Buffer(name, slot) Buffer name : register(t ## slot)\n"
			"#define SR_BufferRW(name, slot) RWBuffer name : register(u ## slot)\n"

			"#define SR_ByteBuffer(name, slot) ByteAddressBuffer name : register(t ## slot)\n"
			"#define SR_ByteBufferRW(name, slot) RWByteAddressBuffer name : register(u ## slot)\n"

			"#define SR_StructuredBuffer(name, slot) StructuredBuffer name : register(t ## slot)\n"
			"#define SR_StructuredBufferRW(name, slot) RWStructuredBuffer name : register(u ## slot)\n\n"

			"static const float SR_Pi = 3.14159265358979323846264338328;\n"
			;

		aShaderCode.insert(0, header);
	}

	std::string SR_ShaderCompiler_DX12::LoadHLSL(const char* aShaderFile, std::string& aOutHLSLFile) const
	{
		std::ifstream shaderFile(aShaderFile);

		SC_GrowingArray<std::string> insertedSemantics;
		SC_GrowingArray<std::string> includedFiles;
		std::string shaderCode;
		shaderCode.reserve(4096);

		if (shaderFile.is_open())
		{
			std::string part;
			while (std::getline(shaderFile, part))
			{
				if (part.rfind("#include", 0) == 0 || part.rfind("import", 0) == 0)
					HandleIncludeFile(part, shaderCode, includedFiles, insertedSemantics);
				else
				{
					if (!HandleSemantics(shaderFile, part, shaderCode, insertedSemantics))
						continue;

					shaderCode += part;
					shaderCode += "\n";
				}
			}
		}

		InsertHeaderHLSL(shaderCode);
		shaderFile.close();

		std::string outFilePath = "Assets/Generated/Shaders/HLSL/";
		outFilePath += GetFilename(aShaderFile);
		outFilePath += ".hlsl";
		std::ofstream outf(outFilePath);

		if (outf.is_open())
		{
			aOutHLSLFile = outFilePath;
			outf << shaderCode;
			outf.close();
		}

		return shaderCode;
	}

	void SR_ShaderCompiler_DX12::HandleIncludeFile(const std::string& aIncludeLine, std::string& aOutShaderCode, SC_GrowingArray<std::string>& aIncludedFiles, SC_GrowingArray<std::string>& aInsertedSemantics) const
	{
		std::string includeFile = SC_EnginePaths::GetRelativeDataPath() + std::string("ShiftEngine/Shaders/");

		static constexpr const char delimiter = '"';
		size_t delimiterStart = aIncludeLine.find_first_of(delimiter) + 1;
		size_t delimiterEnd = aIncludeLine.find_last_of(delimiter);
		includeFile += aIncludeLine.substr(delimiterStart, delimiterEnd - delimiterStart);

		if (aIncludedFiles.Find(includeFile) != aIncludedFiles.FoundNone)
			return;

		std::ifstream shaderFile(includeFile);
		if (shaderFile.is_open())
		{
			std::string part;
			while (std::getline(shaderFile, part))
			{
				if (part.rfind("#include", 0) == 0 || part.rfind("import", 0) == 0)
					HandleIncludeFile(part, aOutShaderCode, aIncludedFiles, aInsertedSemantics);
				else
				{
					if (!HandleSemantics(shaderFile, part, aOutShaderCode, aInsertedSemantics))
						continue;

					aOutShaderCode += part;
					aOutShaderCode += "\n";
				}
			}
		}
		aIncludedFiles.Add(includeFile);
		shaderFile.close();
	}

	bool SR_ShaderCompiler_DX12::HandleSemantics(std::ifstream& aShaderFile, const std::string& aLineOfCode, std::string& aOutShaderCode, SC_GrowingArray<std::string>& aInsertedSemantics) const
	{
		bool addLineOfCodeToShader = true;
		if (aLineOfCode.find("SR_TextureRef") != aLineOfCode.npos)
		{
			const int* values = SR_TextureRef::Values();
			const char* const* textureRefs = SR_TextureRef::Names();
			const uint count = SR_TextureRef::myCount;

			for (uint i = 0; i < count; ++i)
			{
				if (aLineOfCode.find(textureRefs[i]) != aLineOfCode.npos)
				{
					std::string defineTextureRefValue;
					defineTextureRefValue.reserve(128);
					defineTextureRefValue += "#define ";
					defineTextureRefValue += textureRefs[i];
					defineTextureRefValue += " ";
					defineTextureRefValue += std::to_string(values[i]);
					defineTextureRefValue += "\n";

					if (aInsertedSemantics.AddUnique(defineTextureRefValue))
						aOutShaderCode.insert(0, defineTextureRefValue);
				}
			}
		}

		if (aLineOfCode.find("SR_SamplerRef") != aLineOfCode.npos)
		{
			const int* values = SR_SamplerRef::Values();
			const char* const* samplerRefs = SR_SamplerRef::Names();
			const uint count = SR_SamplerRef::myCount;

			for (uint i = 0; i < count; ++i)
			{
				if (aLineOfCode.find(samplerRefs[i]) != aLineOfCode.npos)
				{
					std::string defineSamplerRefValue;
					defineSamplerRefValue.reserve(128);
					defineSamplerRefValue += "#define ";
					defineSamplerRefValue += samplerRefs[i];
					defineSamplerRefValue += " ";
					defineSamplerRefValue += std::to_string(values[i]);
					defineSamplerRefValue += "\n";

					if (aInsertedSemantics.AddUnique(defineSamplerRefValue))
						aOutShaderCode.insert(0, defineSamplerRefValue);
				}
			}
		}
		if (aLineOfCode.find("SR_ConstantBuffer ") != aLineOfCode.npos)
		{
			addLineOfCodeToShader = false;
			std::string variableName;
			variableName.reserve(64);

			uint registerSlot = SC_UINT_MAX;
			bool isLocal = false;
			if (aLineOfCode.rfind("SR_ConstantBufferRef") != aLineOfCode.npos)
			{
				registerSlot = locGetRefIndex<SR_ConstantBufferRef>(aLineOfCode);
			}
			else if (aLineOfCode.rfind("SR_ConstantBuffer") != aLineOfCode.npos)
			{
				registerSlot = locGetLocalRefIndex<SR_ConstantBufferRef>(aLineOfCode, "SR_ConstantBuffer");
				isLocal = true;
			}

			size_t firstNameCharIdx = aLineOfCode.find(" ") + 1;
			size_t lastNameCharIdx = aLineOfCode.rfind(':') - 1;
			variableName = aLineOfCode.substr(firstNameCharIdx, lastNameCharIdx - firstNameCharIdx);

			std::string structVariables;
			structVariables.reserve(4096);
			structVariables += "cbuffer ";
			structVariables += variableName;
			structVariables += " : ";
			structVariables += "register(b";
			structVariables += std::to_string(registerSlot);
			structVariables += ")\n";
			structVariables += "{\n";

			if (!isLocal)
			{
				structVariables += "\tstruct\n";
				structVariables += "\t{\n";
			}

			std::string nextLine;
			while (std::getline(aShaderFile, nextLine))
			{
				if (nextLine.find('{') != nextLine.npos)
					continue;
				if (nextLine.find('}') != nextLine.npos)
					break;

				if (!isLocal)
					structVariables += '\t';

				structVariables += nextLine;
				structVariables += '\n';
			}

			if (!isLocal)
			{
				structVariables += "\t} ";
				structVariables += variableName;
				structVariables += ";\n";
			}

			structVariables += "}\n";
			aOutShaderCode += structVariables;
		}
		return addLineOfCodeToShader;
	}

	void SR_ShaderCompiler_DX12::WriteByteCode(IDxcBlob* aByteCode, void* aHLSLCodeBuffer, uint aHLSLCodeBufferSize)
	{
		std::stringstream outFilePath;
		outFilePath << "Assets/Generated/Shaders/HLSL/ByteCode/";
		uint hash = SC_Hash(aHLSLCodeBuffer, aHLSLCodeBufferSize);
		outFilePath << std::hex << hash << ".ssbytecode";

		uint bytecodeSize = (uint)aByteCode->GetBufferSize();

		std::ofstream output(outFilePath.str(), std::ios::binary);
		if (output.is_open())
		{
			output.write((char*)&bytecodeSize, sizeof(uint));
			output.write((char*)aByteCode->GetBufferPointer(), aByteCode->GetBufferSize());
			output.close();
		}
	}

	SC_Ref<SR_ShaderByteCode> SR_ShaderCompiler_DX12::FindCompiledByteCode(DxcBuffer& aDxcBuffer)
	{
		std::stringstream inFilePath;
		inFilePath << "Assets/Generated/Shaders/HLSL/ByteCode/";
		uint hash = SC_Hash(aDxcBuffer.Ptr, (uint)aDxcBuffer.Size);
		inFilePath << std::hex << hash << ".ssbytecode";

		std::ifstream input(inFilePath.str(), std::ios::binary);
		if (input.is_open())
		{
			uint bytecodeSize = 0;
			input.read((char*)&bytecodeSize, sizeof(uint));
			input.seekg(sizeof(uint));
			char* bytecode = new char[bytecodeSize];
			input.read(bytecode, bytecodeSize);
			input.close();

			SC_Ref<SR_ShaderByteCode> shaderByteCode = new SR_ShaderByteCode();
			shaderByteCode->myByteCode = (uint8*)bytecode;
			shaderByteCode->myByteCodeSize = bytecodeSize;
			return shaderByteCode;
		}

		return nullptr;
	}
}

#endif