#pragma once

namespace Shift
{
	struct SR_ResourceTracking_DX12
	{
		SR_ResourceTracking_DX12()
			: myState(0)
			, myReadState(0)
			, myFirstWriteState(0)
		{}

		uint myState;
		uint16 myReadState;
		uint16 myFirstWriteState;
	};
}