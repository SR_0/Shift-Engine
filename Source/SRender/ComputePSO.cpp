#include "SRender_Precompiled.h"
#include "ComputePSO.h"

namespace Shift
{
	CComputePSO::CComputePSO()
		: myPipelineState(nullptr)
	{
	}
	CComputePSO::CComputePSO(const CComputePSO& aOther)
		: myProperties(aOther.myProperties)
		, myPipelineState(aOther.myPipelineState)
	{
		SetState(aOther.myState);
	}
	CComputePSO::~CComputePSO()
	{
	}
	void CComputePSO::operator=(const CComputePSO& aOther)
	{
		myProperties = aOther.myProperties;
		myPipelineState = aOther.myPipelineState;
		SetState(aOther.myState);
	}
	uint SC_Hash(const SComputePSODesc& aDesc)
	{
		uint hash = 0;

		hash += SC_Hash(aDesc.myComputeShader.c_str(), (uint)strlen(aDesc.myComputeShader.c_str()));

		return hash;
	}
	uint SC_Hash(const CComputePSO& aPSO)
	{
		return SC_Hash(aPSO.myProperties);
	}
}