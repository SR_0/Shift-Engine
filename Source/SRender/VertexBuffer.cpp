#include "SRender_Precompiled.h"
#include "VertexBuffer.h"

namespace Shift
{
	CVertexBuffer::CVertexBuffer()
		: myVertexBufferView(nullptr)
		, myNumVertices(0)
		, myBuffer(nullptr)
	{
	}

	CVertexBuffer::CVertexBuffer(const CVertexBuffer& aOther)
	{
		myDescription = aOther.myDescription;
		myNumVertices = aOther.myNumVertices;
		myVertexBufferView = aOther.myVertexBufferView;
		myBuffer = aOther.myBuffer;
	}


	CVertexBuffer::~CVertexBuffer()
	{
	}
	void CVertexBuffer::operator=(const CVertexBuffer& aOther)
	{
		myDescription = aOther.myDescription;
		myNumVertices = aOther.myNumVertices;
		myVertexBufferView = aOther.myVertexBufferView;
		myBuffer = aOther.myBuffer;
	}
}