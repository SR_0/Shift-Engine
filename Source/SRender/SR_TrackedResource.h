#pragma once
#include "SR_GraphicsResource.h"
#include "SR_RenderEnums.h"
#include "SR_WaitEvent.h"
#include "SR_GraphicsDefinitions.h"

#if ENABLE_DX12
#include "SR_ResourceTracking_DX12.h"
#endif

#if ENABLE_VULKAN
#include "SR_ResourceTracking_Vulkan.h"
#endif


struct ID3D12Resource;

namespace Shift
{
	class SR_WaitEvent;
	class SR_TrackedResource : public SR_Resource
	{			
	public:
		SR_TrackedResource();
		virtual ~SR_TrackedResource(); 

		void Reset();
		void Wait();
		SR_Waitable* GetWaitable();

		union {
#if ENABLE_DX12
			ID3D12Resource* myDX12Resource;
#endif
#if ENABLE_VULKAN
			VkObject* myVkResource;
#endif
		};

		union {
#if ENABLE_DX12
			SR_ResourceTracking_DX12 myDX12Tracking;
#endif
#if ENABLE_VULKAN
			SResourceTracking_Vulkan myVkTracking;
#endif
		};

	protected:
		SC_Ref<SR_Waitable> myWaitable;
	};
}
