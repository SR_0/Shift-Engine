#pragma once

namespace Shift
{
	class SR_CommandList : public SR_Resource
	{
	public:
		SR_CommandList(uint8 aInitialState);
		virtual ~SR_CommandList();

		enum RecordingState
		{
			Closed,
			Idle,
			Recording,
		};

		virtual void FlushPendingTransitions() = 0;
		virtual void UpdateResourceGlobalStates() = 0;

		volatile uint8 myState;

		std::unordered_map<SR_TrackedResource*, SC_Pair<uint, uint>> myPendingTransitions;
	};
}

