#include "SRender_Precompiled.h"
#include "SR_GraphicsDevice.h"

#include "SR_RenderInterface.h"
#include "SR_SwapChain.h"
#include "SR_ShaderStateCache.h"
#include "SR_ShaderCompiler.h"

namespace Shift
{
	SR_GraphicsDevice* SR_GraphicsDevice::ourGraphicsDevice = nullptr;

	void SR_GraphicsDevice::Destroy()
	{
		SC_SAFE_DELETE(ourGraphicsDevice);
	}

	SR_GraphicsDevice::SR_GraphicsDevice()
		: myUseDebugDevice(false)
		, myDebugBreakOnError(false)
		, myDebugBreakOnWarning(false)
		, myNumDrawCalls(0)
		, myNumDispatchCalls(0)
		, myShaderOptimizationLevel(3)
		, mySkipShaderOptimizations(false)
		, myIsUsing16BitDepth(false)
		, myUsedVRAM(0)
		, myRenderThreadPool(SC_Max(std::thread::hardware_concurrency() - 1, 1))
	{
		assert(ourGraphicsDevice == nullptr);
		ourGraphicsDevice = this;
		
		if (SC_CommandLineManager::HasCommand(L"debugrender"))
		{
			myUseDebugDevice = true;
		}
		if (SC_CommandLineManager::HasCommand(L"debugrenderbreak"))
		{
			myDebugBreakOnError = true;
			if (SC_CommandLineManager::HasArgument(L"debugrenderbreak", L"warning"))
				myDebugBreakOnWarning = true;
		}
		if (SC_CommandLineManager::HasCommand(L"debugshaders"))
		{
			myDebugShaders = true;
		}
		if (SC_CommandLineManager::HasCommand(L"skipshaderoptimization"))
		{
			mySkipShaderOptimizations = true;
		}
		if (SC_CommandLineManager::HasCommand(L"bitdepth16"))
		{
			myIsUsing16BitDepth = true;
		}
		if (!mySkipShaderOptimizations)
		{
			if (SC_CommandLineManager::HasArgument(L"shaderoptimizationlevel", L"0"))
			{
				myShaderOptimizationLevel = 0;
			}
			else if (SC_CommandLineManager::HasArgument(L"shaderoptimizationlevel", L"1"))
			{
				myShaderOptimizationLevel = 1;
			}
			else if (SC_CommandLineManager::HasArgument(L"shaderoptimizationlevel", L"2"))
			{
				myShaderOptimizationLevel = 2;
			}
			else if (SC_CommandLineManager::HasArgument(L"shaderoptimizationlevel", L"3"))
			{
				myShaderOptimizationLevel = 3;
			}
		}

		myShaderStateCache = new SR_ShaderStateCache();
	}


	SR_GraphicsDevice::~SR_GraphicsDevice()
	{
		SR_GraphicsQueueManager::Destroy();
		assert(ourGraphicsDevice != nullptr);
		ourGraphicsDevice = nullptr;
	}
	const bool SR_GraphicsDevice::PostInit()
	{
		SR_GraphicsQueueManager::Init();

		SR_RenderInterface::ourBlack4x4 = GetCreateTexture("DefaultTextures/Black4x4.dds");
		SR_RenderInterface::ourWhite4x4 = GetCreateTexture("DefaultTextures/White4x4.dds");
		SR_RenderInterface::ourGrey4x4 = GetCreateTexture("DefaultTextures/Grey4x4.dds");
		SR_RenderInterface::ourNormal4x4 = GetCreateTexture("DefaultTextures/Normal4x4.dds");

		return true;
	}

	void SR_GraphicsDevice::ReleaseResources()
	{
		for (SR_Resource* resource : myReleasedResourceCache)
			delete resource;

		myReleasedResourceCache.RemoveAll();
	}

	CPixelShader * SR_GraphicsDevice::CreatePixelShader(const std::string & aFilePath, const std::string & aEntryPoint)
	{
		// FOR WHEN THE READBLE FILE IS ALREADY COMPILED INTO BYTECODE

		CPixelShader* ps = new CPixelShader();
		std::ifstream file(aFilePath, std::ios::ate | std::ios::binary);
		size_t fileSize = (size_t)file.tellg();
		std::vector<char> buffer(fileSize);
		file.seekg(0);
		file.read(buffer.data(), fileSize);
		file.close();

		ps->myByteCode = reinterpret_cast<unsigned char*>(buffer.data());
		ps->myByteCodeSize = static_cast<unsigned int>(fileSize);
		ps->myEntryPoint = aEntryPoint.c_str();

		return ps;
	}
	CVertexShader * SR_GraphicsDevice::CreateVertexShader(const std::string & aFilePath, const std::string & aEntryPoint)
	{
		// FOR WHEN THE READBLE FILE IS ALREADY COMPILED INTO BYTECODE

		CVertexShader* vs = new CVertexShader();
		std::ifstream file(aFilePath, std::ios::ate | std::ios::binary);
		size_t fileSize = (size_t)file.tellg();
		std::vector<char> buffer(fileSize);
		file.seekg(0);
		file.read(buffer.data(), fileSize);
		file.close();

		vs->myByteCode = reinterpret_cast<unsigned char*>(buffer.data());
		vs->myByteCodeSize = static_cast<unsigned int>(fileSize);
		vs->myEntryPoint = aEntryPoint.c_str();

		return vs;
	}
	SC_Ref<SR_Waitable> SR_GraphicsDevice::PostRenderTask(std::function<void()> aTask, uint aWaitMode)
	{
		return myRenderThreadPool.PostRenderTask(aTask, aWaitMode);
	}
	SC_Ref<SR_Waitable> SR_GraphicsDevice::PostComputeTask(std::function<void()> aTask, uint aWaitMode)
	{
		return myRenderThreadPool.PostComputeTask(aTask, aWaitMode);
	}
	SC_Ref<SR_Waitable> SR_GraphicsDevice::PostCopyTask(std::function<void()> aTask, uint aWaitMode)
	{
		return myRenderThreadPool.PostCopyTask(aTask, aWaitMode);
	}
	bool SR_GraphicsDevice::IsRenderPoolIdle() const
	{
		return myRenderThreadPool.IsIdle();
	}
	bool SR_GraphicsDevice::AddToReleasedResourceCache(SR_Resource* aResource)
	{
		return myReleasedResourceCache.AddUnique(aResource);
	}
	const SR_GraphicsAPI& SR_GraphicsDevice::APIType() const
	{
		return myAPIType;
	}
}