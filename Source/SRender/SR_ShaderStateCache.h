#pragma once

namespace Shift
{
	struct SGraphicsPSODesc;
	class CGraphicsPSO;
	struct SComputePSODesc;
	class CComputePSO;
	struct SR_ShaderStateDesc;
	class SR_ShaderState;

	class SR_ShaderStateCache
	{
	public:
		static SR_ShaderStateCache& Get();
		SR_ShaderStateCache();
		~SR_ShaderStateCache();

		CGraphicsPSO* GetPSO(SGraphicsPSODesc& aPSO);
		CComputePSO* GetPSO(SComputePSODesc& aPSO);

		SC_Ref<SR_ShaderState> GetShaderState(const SR_ShaderStateDesc& aDesc);

	private:
		const std::vector<std::string> GetIncludeFiles(const std::string& aFile);

		std::map<uint, CGraphicsPSO> myGraphicsStates;
		std::map<uint, CComputePSO> myComputeStates;

		std::unordered_map<uint, SC_Ref<SR_ShaderState>> myShaderStates;

#if IS_EDITOR_BUILD
		std::map<std::string, std::vector<CGraphicsPSO*>> myGraphicsShaderConnections;
		std::map<std::string, std::vector<CComputePSO*>> myComputeShaderConnections;
#endif
	};
}
