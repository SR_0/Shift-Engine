#pragma once
#include "SR_ShaderCompiler.h"

#if ENABLE_DX12

struct IDxcCompiler3;
struct IDxcUtils;
struct IDxcIncludeHandler;
struct DxcBuffer;
struct IDxcBlob;

namespace Shift
{
	class SR_ShaderCompiler_DX12 final : public SR_ShaderCompiler
	{
	public:
		SR_ShaderCompiler_DX12();
		~SR_ShaderCompiler_DX12();

		SC_Ref<SR_ShaderByteCode> CompileShaderFromFile(const char* aShaderFile, const SR_ShaderType aShaderType) override;
		SC_Ref<SR_ShaderByteCode> CompileShader() override;

	private:
		SC_Ref<SR_ShaderByteCode> CompileInternal(DxcBuffer& aDxcBuffer, const char* aShaderFile, const wchar_t* aTarget);
		void InsertHeaderHLSL(std::string& aShaderCode) const;
		std::string LoadHLSL(const char* aShaderFile, std::string& aOutHLSLFile) const;
		void HandleIncludeFile(const std::string& aIncludeLine, std::string& aOutShaderCode, SC_GrowingArray<std::string>& aIncludedFiles, SC_GrowingArray<std::string>& aInsertedSemantics) const;
		bool HandleSemantics(std::ifstream& aShaderFile, const std::string& aLineOfCode, std::string& aOutShaderCode, SC_GrowingArray<std::string>& aInsertedSemantics) const;
		void WriteByteCode(IDxcBlob* aByteCode, void* aHLSLCodeBuffer, uint aHLSLCodeBufferSize);
		SC_Ref<SR_ShaderByteCode> FindCompiledByteCode(DxcBuffer& aDxcBuffer);

		IDxcCompiler3* myCompiler;
		IDxcUtils* myUtils;
		IDxcIncludeHandler* myIncludeHandler;
	};
}
#endif