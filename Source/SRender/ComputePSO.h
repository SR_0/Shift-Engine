#pragma once
#include "SC_Resource.h"
#include <string>

namespace Shift
{
	struct SComputePSODesc
	{
		std::string myComputeShader;
	};
	class CComputePSO : public SC_Resource
	{
	public:
		CComputePSO();
		CComputePSO(const CComputePSO& aOther);
		~CComputePSO();

		void operator=(const CComputePSO& aOther);

		SComputePSODesc myProperties;
		SC_Handle myPipelineState;
	};

	uint SC_Hash(const SComputePSODesc& aDesc);
	uint SC_Hash(const CComputePSO& aPSO);
}