#pragma once

namespace Shift
{
	class SR_CommandList;
	class SR_RenderThread;
	class SR_RenderThreadPool
	{
	public:
		SR_RenderThreadPool(uint aNumThreads);
		~SR_RenderThreadPool();

		SC_Ref<SR_Waitable> PostRenderTask(std::function<void()> aTask, uint aWaitMode = SR_WaitMode_CPU);
		SC_Ref<SR_Waitable> PostComputeTask(std::function<void()> aTask, uint aWaitMode = SR_WaitMode_CPU);
		SC_Ref<SR_Waitable> PostCopyTask(std::function<void()> aTask, uint aWaitMode = SR_WaitMode_CPU);

		bool Flush(bool aBlock);
		bool IsIdle() const;

		bool WaitPop(std::function<void()>& aTaskOut);

	private:
		SC_Ref<SR_Waitable> PushTask(std::function<void()> aTask, uint aWaitMode, SR_QueueType aContextType);

		SC_GrowingArray<SR_RenderThread*> myThreads;
		SC_ThreadSafeQueue<std::function<void()>> myQueueData;

		SC_ThreadSafeQueue<SC_Ref<SR_CommandList>> myPendingCLs[SR_QueueType::SR_QueueType_MaxQueues];
	};
}
