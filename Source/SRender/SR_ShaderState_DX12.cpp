#include "SRender_Precompiled.h"
#include "SR_ShaderState_DX12.h"

namespace Shift
{
	SR_ShaderState_DX12::SR_ShaderState_DX12()
	{
	}

	SR_ShaderState_DX12::~SR_ShaderState_DX12()
	{
		for (auto& pso : myPipelineStates)
			pso.second->Release();
	}

	uint SR_ShaderState_DX12::GetHash() const
	{
		return uint();
	}

	ID3D12PipelineState* SR_ShaderState_DX12::GetPSO(const SR_PSOKey& aKey)
	{
		if (myIsCompute)
			return myComputePipelineState;

		auto index = myPipelineStates.find(aKey);
		if (index == myPipelineStates.end())
			return nullptr;

		return myPipelineStates[aKey];
	}
}