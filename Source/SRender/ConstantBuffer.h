#pragma once

namespace Shift
{
	class SR_Buffer;
	class SR_BufferView;
	class CConstantBuffer
	{
	public:
		CConstantBuffer();
		~CConstantBuffer();

		SR_Buffer* GetBuffer() const;

		uint mySize;
		SC_Handle myData;
	private:
		SR_BufferView* myBufferView;
	};
}
