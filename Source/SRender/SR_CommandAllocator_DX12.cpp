#include "SRender_Precompiled.h"
#include "SR_CommandAllocator_DX12.h"

#if ENABLE_DX12
#include "SR_CommandListPool_DX12.h"
#include "SR_GraphicsDevice_DX12.h"
#include "SR_GraphicsQueueManager_DX12.h"

namespace Shift
{
	SR_CommandAllocator_DX12::SR_CommandAllocator_DX12(D3D12_COMMAND_LIST_TYPE aType, SR_CommandListPool_DX12* aPool)
		: myType(aType)
		, myAllocator(nullptr)
		, myPool(aPool)
		, myCommandsSinceResetCount(0)
		, myCommandsPerResetLimit(20)
		, myCommandListsActive(0)
		, myCommandListsAlive(0)
		, myFence(0)
		, myFenceContext(SR_QueueType_Render)
	{
		if (aType == D3D12_COMMAND_LIST_TYPE_DIRECT)
			myCommandsPerResetLimit = 500;
		else if (aType == D3D12_COMMAND_LIST_TYPE_COMPUTE)
			myCommandsPerResetLimit = 100;

		SR_GraphicsDevice_DX12* device = SR_GraphicsDevice_DX12::GetDX12Device();
		ID3D12Device* nativeDevice = device->GetNativeDevice();

		HRESULT hr = S_OK;
		hr = nativeDevice->CreateCommandAllocator(myType, SR_IID_PPV_ARGS(&myAllocator));
		if (FAILED(hr))
		{
			SC_ERROR_LOG("Failed to create CommandAllocator");
			assert(false);
		}

		SC_Atomic_Increment(myPool->myAllocatorCount);
		//TODO: Set name
	}
	SR_CommandAllocator_DX12::~SR_CommandAllocator_DX12()
	{
		if (myAllocator)
			myAllocator->Release();
	}
	bool SR_CommandAllocator_DX12::BeginCommandList()
	{
		if (IsFilled())
			return false;

		assert(SC_Atomic_CompareExchange(myCommandListsActive, 1, 0));
		SC_Atomic_Increment(myCommandListsAlive);

		return true;
	}
	void SR_CommandAllocator_DX12::Reset()
	{
		assert(myCommandsSinceResetCount > 0);
		assert(IsIdle());

		//S_LOG("Resetting allocator (%p)", this);
		HRESULT hr = myAllocator->Reset();
		if (FAILED(hr))
		{
			SC_ERROR_LOG("Failed to reset CommandAllocator");
			assert(false);
		}

		myLastFrameOpened = SC_Timer::GetGlobalFrameIndex();
		myCommandsSinceResetCount = 0;
	}
	bool SR_CommandAllocator_DX12::IsFilled() const
	{
		return myCommandsSinceResetCount > myCommandsPerResetLimit;
	}
	bool SR_CommandAllocator_DX12::IsIdle()
	{
		if (myCommandListsActive || myCommandListsAlive)
			return false;

		if (SR_GraphicsQueueManager::IsPending(myFence, myFenceContext))
			return false;

		myFence = 0;
		return true;
	}
}
#endif