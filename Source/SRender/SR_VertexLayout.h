#pragma once
#include <string>

namespace Shift
{
	struct SVertexLayoutElement
	{
		enum EInputClassification
		{
			INPUT_PER_VERTEX_DATA,
			INPUT_PER_INSTANCE_DATA,
		};

		std::string mySemanticName;
		uint mySemanticIndex;
		SR_Format myFormat;
		uint myInputSlot;
		uint myAlignedByteOffset;
		EInputClassification myInputSlotClass;
		uint myInstanceDataStepRate;
	};

	class CVertexLayout
	{
		friend class CGraphicsDevice_DX11;
		friend class SR_GraphicsDevice_DX12;
		friend class CGraphicsDevice_Vulkan;

	public:
		CVertexLayout();
		~CVertexLayout();

	private:
		SC_GrowingArray<SVertexLayoutElement> myElements;
	};

}