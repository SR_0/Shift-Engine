#pragma once
#include "SR_RenderStateDescs.h"
#include "SR_PSOKey.h"

namespace Shift
{
	struct SR_ShaderStateDesc
	{
		SR_ShaderStateDesc()
			: myIsCompute(false)
			, myTopology(SR_Topology_TriangleList)
		{
			SC_Fill<SR_ShaderByteCode*>(myShaderByteCodes, SR_ShaderType_COUNT, nullptr);
		}

		SR_PSOKey GetKey()
		{
			SR_PSOKey out;

			out.myTopologyKey = SC_Hash(&myTopology, sizeof(myTopology));
			out.myDepthStateKey = SC_Hash(&myDepthStateDesc, sizeof(myDepthStateDesc));
			out.myRasterStateKey = SC_Hash(&myRasterizerStateDesc, sizeof(myRasterizerStateDesc));
			out.myBlendStateKey = SC_Hash(&myBlendStateDesc, sizeof(myBlendStateDesc));
			out.myRenderTargetsKey = SC_Hash(&myRenderTargetFormats, sizeof(myRenderTargetFormats));

			return out;
		}

		SR_ShaderByteCode* myShaderByteCodes[SR_ShaderType_COUNT];

		SR_Topology myTopology;
		SR_DepthStateDesc myDepthStateDesc;
		SR_RasterizerStateDesc myRasterizerStateDesc;
		SR_BlendStateDesc myBlendStateDesc;
		SR_RenderTargetFormats myRenderTargetFormats;

		bool myIsCompute;
	};

	class SR_ShaderState : public SR_Resource
	{
	public:
		SR_ShaderState();
		virtual ~SR_ShaderState();

		virtual uint GetHash() const = 0;

	protected:
		std::unordered_map<uint, SR_ShaderStateDesc> myProperties;
		bool myIsCompute : 1;
	};
}
