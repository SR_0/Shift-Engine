#include "SRender_Precompiled.h"
#include "SR_GraphicsQueueManager_DX12.h"

#if ENABLE_DX12
#include "SR_CommandListPool_DX12.h"
#include "SR_CommandList_DX12.h"
#include "SR_CommandAllocator_DX12.h"
#include "SR_GraphicsDevice_DX12.h"
#include "SR_GraphicsContext_DX12.h"

namespace Shift
{
	SR_GraphicsQueueManager_DX12::SR_GraphicsQueueManager_DX12()
	{
		Init_Internal();
	}
	SR_GraphicsQueueManager_DX12::~SR_GraphicsQueueManager_DX12()
	{
		for (int i = 0; i < SR_QueueType_MaxQueues; ++i)
			myQueues[i]->Release();
	}
	ID3D12CommandQueue* SR_GraphicsQueueManager_DX12::GetQueue(const SR_QueueType& aType) const
	{
		return myQueues[aType];
	}
	uint64 SR_GraphicsQueueManager_DX12::GetTimestampFreq(const SR_QueueType& aType) const
	{
		uint64 returnVal = 1;
		HRESULT res = myQueues[aType]->GetTimestampFrequency(&returnVal);
		if (FAILED(res))
			SC_ERROR_LOG("Could not retrieve Timestamp Frequency.");

		return returnVal;
	}
	void SR_GraphicsQueueManager_DX12::Init_Internal()
	{
		SR_GraphicsDevice_DX12* deviceDX12 = static_cast<SR_GraphicsDevice_DX12*>(SR_GraphicsDevice::GetDevice());
		ID3D12Device* device = deviceDX12->GetNativeDevice();

		D3D12_COMMAND_QUEUE_DESC cqDesc = {};

		HRESULT hr = S_OK;
		// Render Queue
		cqDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
		hr = device->CreateCommandQueue(&cqDesc, SR_IID_PPV_ARGS(&myQueues[SR_QueueType_Render]));
		if (FAILED(hr))
		{
			return;
		}
		myQueues[SR_QueueType_Render]->SetName(L"Graphics Queue");
		myCmdListPools[SR_QueueType_Render] = new SR_CommandListPool_DX12(cqDesc.Type, "Command List Pool [Render]");

		// Compute queue
		cqDesc.Type = D3D12_COMMAND_LIST_TYPE_COMPUTE;
		hr = device->CreateCommandQueue(&cqDesc, SR_IID_PPV_ARGS(&myQueues[SR_QueueType_Compute]));
		if (FAILED(hr))
		{
			return;
		}
		myQueues[SR_QueueType_Compute]->SetName(L"Compute Queue");
		myCmdListPools[SR_QueueType_Compute] = new SR_CommandListPool_DX12(cqDesc.Type, "Command List Pool [Compute]");

		// Copy Queue
		cqDesc.Type = D3D12_COMMAND_LIST_TYPE_COPY;
		hr = device->CreateCommandQueue(&cqDesc, SR_IID_PPV_ARGS(&myQueues[SR_QueueType_Copy]));
		if (FAILED(hr))
		{
			return;
		}
		myQueues[SR_QueueType_Copy]->SetName(L"Copy Queue");
		myCmdListPools[SR_QueueType_Copy] = new SR_CommandListPool_DX12(cqDesc.Type, "Command List Pool [Copy]");

		for (uint i = 0; i < SR_QueueType_MaxQueues; ++i)
		{
			hr = device->CreateFence(1, D3D12_FENCE_FLAG_NONE, SR_IID_PPV_ARGS(&myFences[i]));

			if (FAILED(hr))
				SC_ERROR_LOG("Could not create Fence_DX12");

			myFenceHandles[i] = CreateEvent(nullptr, true, false, nullptr);
		}
	}

	void SR_GraphicsQueueManager_DX12::Execute_Internal(QueueData& aQueueData)
	{
		SC_GrowingArray<SC_Ref<SR_CommandList_DX12>> transitionLists;
		ID3D12CommandQueue* cmdQueue = myQueues[aQueueData.myContextType];
		
		const uint count = aQueueData.myCommandLists.Count();
		if (count)
		{
			SC_GrowingArray<ID3D12CommandList*> cmdLists;
			cmdLists.PreAllocate(count);
			for (uint i = 0; i < count; ++i)
			{
				SR_CommandList_DX12* cmdList = static_cast<SR_CommandList_DX12*>(aQueueData.myCommandLists[i].Get());

				if (!cmdList)
					continue;

				if (cmdList->myPendingTransitions.size() > 0)
				{
					SC_Ref<SR_CommandList> transitionCmdList;
					GetCommandList(transitionCmdList, aQueueData.myContextType);
					transitionLists.Add(transitionCmdList);
					SR_CommandList_DX12* transitionCmdListDX12 = static_cast<SR_CommandList_DX12*>(transitionCmdList.Get());

					transitionCmdListDX12->myPendingTransitions = SC_Move(cmdList->myPendingTransitions);
					transitionCmdListDX12->FlushPendingTransitions();
					transitionCmdListDX12->UpdateResourceGlobalStates();
					transitionCmdListDX12->SetLastFence(cmdList->myFence, cmdList->myFenceContext);
					transitionCmdListDX12->FinishRecording();
					transitionCmdListDX12->Close();
					cmdLists.Add(transitionCmdListDX12->GetCommandList());
				}
				cmdList->Close();
				assert(cmdList->myState == SR_CommandList::Closed);
				//S_LOG("Executing cmd-list (%p)", aQueueData.myCommandLists[i].Get());
				cmdLists.Add(cmdList->GetCommandList());
			}

			cmdQueue->ExecuteCommandLists(cmdLists.Count(), cmdLists.GetBuffer());

			for (uint i = 0; i < count; ++i)
			{
				SR_CommandList_DX12* cmdList = static_cast<SR_CommandList_DX12*>(aQueueData.myCommandLists[i].Get());

				if (!cmdList)
					continue;

				cmdList->InactivateAndReturn();
			}
			for (SR_CommandList_DX12* transitionList : transitionLists)
			{
				transitionList->InactivateAndReturn();
			}
		}
	}

	void SR_GraphicsQueueManager_DX12::Signal_Internal(QueueData& aQueueData)
	{
		HRESULT result = S_OK;
		ID3D12CommandQueue* cmdQueue = myQueues[aQueueData.myContextType];

		ID3D12Fence* fence = myFences[aQueueData.myContextType];
		//Log("Signal fence (%p) with value: %llu", fence, aQueueData.myFenceValue);
		result = cmdQueue->Signal(fence, aQueueData.myFenceValue);
		if (FAILED(result))
		{
			SC_ERROR_LOG("DX12 CommandQueue : Error signaling fence.");
			assert(false);
		}
	}

	void SR_GraphicsQueueManager_DX12::UpdateCommandListPool_Internal()
	{
		for (uint i = 0; i < SR_QueueType::SR_QueueType_MaxQueues; ++i)
		{
			if (HasWork())
				break;

			myCmdListPools[i]->Update();
		}
	}
	uint64 SR_GraphicsQueueManager_DX12::Signal_Internal(const SR_QueueType& aQueue)
	{
		uint64 valueToSignal = myLastSignaledValue[aQueue] + 1;

		SC_Atomic_CompareExchange(myLastSignaledValue[aQueue], valueToSignal, valueToSignal - 1);
		InsertSignal_Internal(valueToSignal, aQueue);

		return valueToSignal;
	}
	void SR_GraphicsQueueManager_DX12::Signal_Internal(uint64 aFence, const SR_QueueType& aQueue)
	{
		if (aFence < myLastKnownCompletedFences[aQueue])
			return;

		InsertSignal_Internal(aFence, aQueue);
	}
	bool SR_GraphicsQueueManager_DX12::WaitForSignal_Internal(uint64 aFenceValue, const SR_QueueType& aQueue)
	{
		if (!IsPending_Internal(aFenceValue, aQueue))
			return true;

		for (;;)
		{
			if (!IsPending_Internal(aFenceValue, aQueue))
				break;

			uint64 nextWaitValue = myLastKnownCompletedFences[aQueue] + 1;
			if (nextWaitValue > aFenceValue)
			{
				assert(!IsPending_Internal(aFenceValue, aQueue));
				break;
			}

			HANDLE event = myFenceHandles[aQueue];
			ResetEvent(event);
			myFences[aQueue]->SetEventOnCompletion(nextWaitValue, event);

			HRESULT hr = WaitForSingleObject(event, INFINITE);
			if (FAILED(hr))
			{
				assert(false);
			}
		}
		return true;
	}
	bool SR_GraphicsQueueManager_DX12::IsPending_Internal(uint64 aFenceValue, const SR_QueueType& aQueue)
	{
		ID3D12Fence* fence = myFences[aQueue];

		uint64 lastKnownCompletedValue = myLastKnownCompletedFences[aQueue];

		if (aFenceValue <= lastKnownCompletedValue)
			return false;

		uint64 completedValue = fence->GetCompletedValue();

		if (completedValue > lastKnownCompletedValue)
		{
			if (completedValue == UINT64_MAX) // Device was removed
			{
				assert(false);
			}

			SC_Atomic_CompareExchange(myLastKnownCompletedFences[aQueue], completedValue, lastKnownCompletedValue);
		}

		return aFenceValue > completedValue;
	}
	uint64 SR_GraphicsQueueManager_DX12::GetNextExpectedFence_Internal(const SR_QueueType& aQueue)
	{
		uint64 valueToSignal = myLastSignaledValue[aQueue] + 1;
		SC_Atomic_CompareExchange(myLastSignaledValue[aQueue], valueToSignal, valueToSignal - 1);
		return valueToSignal;
	}
	bool SR_GraphicsQueueManager_DX12::GetCommandList_Internal(SC_Ref<SR_CommandList>& aCmdListOut, SR_QueueType aContextType)
	{
		SC_Ref<SR_CommandList_DX12> cmdList;
		cmdList = myCmdListPools[aContextType]->GetCommandList(aContextType);

		if (cmdList)
		{
			aCmdListOut = cmdList;
			return true;
		}

		return false;
	}
}
#endif