#pragma once
#include "SC_Module.h"
#include "STools_EditorShelf.h"
#include "STools_TransformGizmo.h"

class SClient_Base;
namespace Shift
{
	class SG_Camera;
	class STools_FeatureBinding;
	class STools_Editor : public SC_Module
	{
		SC_MODULE_DECL(STools_Editor);

	public:
		STools_Editor();
		~STools_Editor();

		bool Init();

		void Update() override;
		void Render() override;

		void GameExitHook();
		void AddClientHook(SClient_Base* aTargetClient);

		void RegisterTool(STools_FeatureBinding* aTool);
		void UnregisterTool(STools_FeatureBinding* aTool);

		static STools_Editor* GetInstance() { return ourInstance; }

		SG_Camera* GetCamera() { return myCamera; }
		SC_Vector2f GetViewportCursorPos() { return myLastViewportCursorPos; }

	private:
		static STools_Editor* ourInstance;

		SC_GrowingArray<SC_Ref<STools_FeatureBinding>> myTools;
		SC_GrowingArray<STools_FeatureBinding*> myRemovedTools;

		STools_EditorShelf myShelf;

		STools_TransformGizmo myGizmo;

		SC_Vector2f myLastViewportCursorPos;
		SClient_Base* myTargetClientHook;
		SG_Camera* myCamera;
	};
}