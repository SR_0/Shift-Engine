#pragma once
#include "STools_FeatureBinding.h"

namespace Shift
{
	class SG_DynamicSky;
	class STools_DynamicSky final : public STools_FeatureBinding
	{
	public:
		STools_DynamicSky(SG_DynamicSky* aSky);
		~STools_DynamicSky();

		void Render() override;

	private:
		SG_DynamicSky* myDynamicSky;
		bool myOverridePCFFilter;
	};
}

