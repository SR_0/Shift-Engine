#pragma once
#include "SC_RefCounted.h"

namespace Shift
{
	class STools_FeatureBinding : public SC_RefCounted
	{
	public:
		STools_FeatureBinding();
		virtual ~STools_FeatureBinding();

		virtual void Update();
		virtual void Render() = 0;

		void UnregisterTool();

	protected:
		STools_Editor* myEditor;
		bool myIsOpen;
	};
}