#include "STools_Precompiled.h"
#include "STools_AssetImporter.h"

#include "SG_MeshHeader.h"
#include "SG_Material.h"

#include "imguifilebrowser.h"

#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

#include <sstream>

namespace Shift
{
	STools_AssetImporter::STools_AssetImporter()
		: mySelectedMeshIdx(0)
	{
		myBrowser = new ImGui::FileBrowser();
		myBrowser->SetTitle("Asset Browser");
	}

	STools_AssetImporter::~STools_AssetImporter()
	{
	}

	void STools_AssetImporter::Render()
	{
		ImGui::Begin("Asset Importer", &myIsOpen);

		ImGui::Text("%s", mySelectedFile.c_str());

		if (ImGui::Button("..."))
		{
			mySelectedFile.clear();
			myBrowser->SetTypeFilters({ ".fbx", ".obj", ".3ds" });
			myBrowser->Open();
		}

		myBrowser->Display();

		if (mySelectedFile.empty() && myBrowser->HasSelected())
		{
			mySelectedFile = myBrowser->GetSelected().string();
			myBrowser->ClearSelected();

			SC_Stopwatch time;
			if (Import(mySelectedFile))
			{
				SC_SUCCESS_LOG("Asset Imported [%s]: import duration: %.3f ms", GetFileWithExtension(mySelectedFile).c_str(), time.Stop());
			}
			else
			{
				SC_ERROR_LOG("Failed to import asset [%s]", GetFileWithExtension(mySelectedFile).c_str());
			}
		}
		
		if (!myImportedMesh.Empty())
		{
			RenderImportedIntermediate();
		}

		ImGui::End();
	}

	void STools_AssetImporter::RenderImportedIntermediate()
	{
		uint numMeshes = myImportedMesh.Count();
		SC_GrowingArray<const char*> listOfMeshes;
		listOfMeshes.PreAllocate(numMeshes);
		for (uint i = 0; i < numMeshes; ++i)
			listOfMeshes.Add(myImportedMesh[i].myName.c_str());
		
		ImGui::NewLine();
		ImGui::Separator();
		ImGui::NewLine();
		ImGui::Text("Model: %s", myImportedModelName.c_str());
		ImGui::Text("Num Meshes in file: %i", numMeshes);

		if (numMeshes > 0)
		{
			ImGui::ListBox(" :Meshes", &mySelectedMeshIdx, listOfMeshes.GetBuffer(), numMeshes, SC_Min(listOfMeshes.Count(), 6));
			ImGui::NewLine();

			ImGui::Separator();
			ImGui::NewLine();

			if (mySelectedMeshIdx >= 0)
			{
				STools_MeshBuffer& mesh = myImportedMesh[mySelectedMeshIdx];
				ImGui::Text("Mesh Name: %s", mesh.myName.c_str());
				ImGui::Text("AABB: { min: x:%.3f y:%.3f z:%.3f }\n\t\t{ max: x:%.3f y:%.3f z:%.3f }",
					mesh.myAABB.myMin.x, mesh.myAABB.myMin.y, mesh.myAABB.myMin.z,
					mesh.myAABB.myMax.x, mesh.myAABB.myMax.y, mesh.myAABB.myMax.z);
				ImGui::Text("Num Vertices: %i", mesh.myNumVertices);
				ImGui::Text("Vertex Size: %i", mesh.myVertexStrideSize);
				ImGui::Text("Num Indices: %i", mesh.myNumIndices);

				ImGui::Text("Num Textures: %i", mesh.myTextures.Count());

				uint numTextures = mesh.myTextures.Count();
				if (numTextures > 0)
				{
					SC_GrowingArray<const char*> textures;
					textures.PreAllocate(numTextures);
					for (uint i = 0; i < numTextures; ++i)
						textures.Add(mesh.myTextures[i].c_str());

					int select = -1;
					ImGui::ListBox(" :Textures", &select, textures.GetBuffer(), numTextures, SC_Min(textures.Count(), 6));

					
				}

				ImGui::NewLine();
				mesh.myMaterialPath.reserve(256);
				ImGui::Text("Material: %s", mesh.myMaterialPath.c_str());
				ImGui::SameLine();
				if (ImGui::Button("Material..."))
				{
					mesh.myMaterialPath.clear();
					myBrowser->SetTypeFilters({ ".smtf" });
					myBrowser->Open();
				}

				if (myBrowser->HasSelected())
				{
					mesh.myMaterialPath = myBrowser->GetSelected().string();
					myBrowser->ClearSelected();
				}
			}

			if (ImGui::Button("Save"))
			{
				if (!WriteMeshFile(mySelectedFile, myImportedMesh))
				{
					SC_ERROR_LOG("Failed to save asset [%s]", GetFileWithExtension(mySelectedFile).c_str());
				}
				else
				{
					myImportedMesh.RemoveAll();
					return;
				}
			}
		}
	}

	bool STools_AssetImporter::Import(const std::string& aFile)
	{
		Assimp::Importer importer;

		unsigned int flags = aiProcess_CalcTangentSpace | aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_SortByPType | aiProcess_ConvertToLeftHanded;

		const aiScene* scene = importer.ReadFile(aFile, flags);

		if (!scene->HasMeshes())
		{
			SC_ERROR_LOG("Model-file contains no meshes [%s]", aFile.c_str());
			return false;
		}

		myImportedMesh.RemoveAll();
		ProcessAiNode(scene->mRootNode, scene, myImportedMesh);

		myImportedModelName = GetFileWithExtension(mySelectedFile);

		return true;
	}

	bool STools_AssetImporter::WriteMeshFile(const std::string& aFile, const SC_GrowingArray<STools_MeshBuffer>& aMeshes)
	{
		std::stringstream outputFileName;
		std::string modelName = GetFilename(aFile);

		outputFileName << "Assets/Imported/Models/" << modelName << ".smf";

		std::ofstream outputFile(outputFileName.str(), std::ios::binary);
		if (outputFile.good())
		{
			SG_MeshFileHeader fileHeader;
			fileHeader.myNumMeshes = aMeshes.Count();
			fileHeader.myNameSize = (uint)modelName.size();
			outputFile.write((char*)&fileHeader, sizeof(fileHeader));
			outputFile.write(modelName.c_str(), fileHeader.myNameSize);

			uint largestAlloc = 0;
			char* data = nullptr;
			for (uint i = 0, count = aMeshes.Count(); i < count; ++i)
			{
				const STools_MeshBuffer& mesh = aMeshes[i];

				SG_MeshHeader header;
				header.myNameSize = (uint)mesh.myName.size();
				header.myMaterialPathSize = (uint)mesh.myMaterialPath.size();
				header.myAABB = mesh.myAABB;
				header.myVertexStrideSize = mesh.myVertexStrideSize;
				header.myNumVertices = mesh.myNumVertices;
				header.myNumIndices = mesh.myNumIndices;
				header.myTotalVertexBufferSize = mesh.myTotalVertexBufferSize;
				header.myTotalIndexBufferSize = mesh.myTotalIndexBufferSize;

				uint meshDataSize = 0;
				meshDataSize += sizeof(SG_MeshHeader);
				meshDataSize += header.myNameSize;
				meshDataSize += header.myMaterialPathSize;
				meshDataSize += header.myTotalVertexBufferSize + header.myTotalIndexBufferSize;

				if (meshDataSize > largestAlloc)
				{
					delete[] data;
					data = new char[meshDataSize];
					largestAlloc = meshDataSize;
				}

				uint currPos = 0;
				SC_Memcpy(&data[currPos], &header, sizeof(header));
				currPos += sizeof(header);

				SC_Memcpy(&data[currPos], (void*)mesh.myName.c_str(), header.myNameSize);
				currPos += header.myNameSize;

				SC_Memcpy(&data[currPos], (void*)mesh.myMaterialPath.c_str(), header.myMaterialPathSize);
				currPos += header.myMaterialPathSize;

				SC_Memcpy(&data[currPos], mesh.myVertices, header.myTotalVertexBufferSize);
				currPos += header.myTotalVertexBufferSize;

				SC_Memcpy(&data[currPos], mesh.myIndices, header.myTotalIndexBufferSize);
				currPos += header.myTotalIndexBufferSize;

				outputFile.write(data, meshDataSize);
				delete[] mesh.myVertices;
				delete[] mesh.myIndices;
			}
			delete[] data;
		}
		else
			return false;

		outputFile.close();

		return true;
	}

	void STools_AssetImporter::ProcessAiNode(aiNode* aNode, const aiScene* aScene, SC_GrowingArray<STools_MeshBuffer>& aOutMeshes)
	{
		uint numMeshes = aNode->mNumMeshes;
		aOutMeshes.PreAllocate(numMeshes);
		for (unsigned int meshIdx = 0; meshIdx < numMeshes; ++meshIdx)
		{
			const aiMesh* mesh = aScene->mMeshes[aNode->mMeshes[meshIdx]];

			uint vertexStrideSize = sizeof(SC_Vector3f);

			bool hasUVs = mesh->HasTextureCoords(0);
			bool hasNormals = mesh->HasNormals();
			bool hasTangentsAndBitangents = mesh->HasTangentsAndBitangents();
			bool hasVertexColors = mesh->HasVertexColors(0);

			if (hasUVs)
				vertexStrideSize += sizeof(SC_Vector2f);
			if (hasNormals)
				vertexStrideSize += sizeof(SC_Vector3f);
			if (hasTangentsAndBitangents)
				vertexStrideSize += sizeof(SC_Vector3f) * 2;
			if (hasVertexColors)
				vertexStrideSize += sizeof(SC_Vector4f);

			uint vertexDataArraySize = mesh->mNumVertices * vertexStrideSize;
			uint8* vertexDataArray = new uint8[vertexDataArraySize];
			uint currentPos = 0;

			SC_AABB meshAABB;
			meshAABB.myMax = SC_Vector3f(SC_FLT_LOWEST);
			meshAABB.myMin = SC_Vector3f(SC_FLT_MAX);
			for (uint i = 0; i < mesh->mNumVertices; ++i)
			{
				SC_Vector3f position = SC_Vector3f(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z);
				SC_Memcpy(&vertexDataArray[currentPos], &position, sizeof(SC_Vector3f));
				currentPos += sizeof(SC_Vector3f);

				if (position.x < meshAABB.myMin.x)
					meshAABB.myMin.x = position.x;
				else if (position.x > meshAABB.myMax.x)
					meshAABB.myMax.x = position.x;

				if (position.y < meshAABB.myMin.y)
					meshAABB.myMin.y = position.y;
				else if (position.y > meshAABB.myMax.y)
					meshAABB.myMax.y = position.y;

				if (position.z < meshAABB.myMin.z)
					meshAABB.myMin.z = position.z;
				else if (position.z > meshAABB.myMax.z)
					meshAABB.myMax.z = position.z;

				if (hasUVs)
				{
					SC_Vector2f uv = SC_Vector2f(mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y);
					SC_Memcpy(&vertexDataArray[currentPos], &uv, sizeof(SC_Vector2f));
					currentPos += sizeof(SC_Vector2f);
				}
				if (hasNormals)
				{
					SC_Vector3f normal = SC_Vector3f(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z);
					SC_Memcpy(&vertexDataArray[currentPos], &normal, sizeof(SC_Vector3f));
					currentPos += sizeof(SC_Vector3f);
				}
				if (hasTangentsAndBitangents)
				{
					SC_Vector3f data[2];
					data[0] = SC_Vector3f(mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z);
					data[1] = SC_Vector3f(mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z);
					SC_Memcpy(&vertexDataArray[currentPos], data, sizeof(SC_Vector3f) * 2);
					currentPos += (sizeof(SC_Vector3f) * 2);
				}
				if (hasVertexColors)
				{
					SC_Vector4f color = SC_Vector4f(mesh->mColors[0][i].r, mesh->mColors[0][i].g, mesh->mColors[0][i].b, mesh->mColors[0][i].a);
					SC_Memcpy(&vertexDataArray[currentPos], &color, sizeof(SC_Vector4f));
					currentPos += sizeof(SC_Vector4f);
				}
			}

			const uint numIndices = mesh->mNumFaces * 3; // Assume 3 indices per face.
			uint* indices = new uint[numIndices];
			uint indexNum = 0;
			for (uint i = 0; i < mesh->mNumFaces; ++i)
			{
				const aiFace& face = mesh->mFaces[i];
				assert(face.mNumIndices == 3 && "Mesh isn't triangulated.");

				for (uint index = 0; index < face.mNumIndices; ++index)
					indices[indexNum++] = face.mIndices[index];
			}


			SC_GrowingArray<std::string> textures;
			const aiMaterial* material = aScene->mMaterials[mesh->mMaterialIndex];
			aiString path;
			for (uint i = 0; i < AI_TEXTURE_TYPE_MAX; ++i)
			{
				aiReturn texFound = material->GetTexture((aiTextureType)i, 0, &path);
				if (texFound != AI_SUCCESS)
					continue;

				std::string texturePath = GetFileWithExtension(std::string(path.data));

				textures.Add(texturePath);
			}

			STools_MeshBuffer& outMesh = aOutMeshes.Add();
			outMesh.myAABB = meshAABB;
			outMesh.myVertices = vertexDataArray;
			outMesh.myNumVertices = mesh->mNumVertices;
			outMesh.myVertexStrideSize = vertexStrideSize;
			outMesh.myIndices = indices;
			outMesh.myNumIndices = numIndices;
			outMesh.myTotalVertexBufferSize = vertexDataArraySize;
			outMesh.myTotalIndexBufferSize = (sizeof(uint) * outMesh.myNumIndices);
			outMesh.myName = mesh->mName.C_Str();
			outMesh.myTextures = SC_Move(textures);
		}
		
		for (unsigned int n = 0; n < aNode->mNumChildren; ++n)
		{
			ProcessAiNode(aNode->mChildren[n], aScene, aOutMeshes);
		}
	}
}