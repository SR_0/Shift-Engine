#include "STools_Precompiled.h"
#include "STools_MaterialEditor.h"
#include "SG_Material.h"

#include "imguifilebrowser.h"

namespace Shift
{
	STools_MaterialEditor::STools_MaterialEditor()
	{
		myBrowser = new ImGui::FileBrowser();
		myBrowser->SetTitle("Asset Browser");
	}

	STools_MaterialEditor::~STools_MaterialEditor()
	{
	}
	void STools_MaterialEditor::Render()
	{
		ImGui::Begin("Material Editor", &myIsOpen);

		if (ImGui::Button("Open..."))
		{
			mySelectedFile.clear();
			myBrowser->SetTypeFilters({ ".smtf" });
			myBrowser->Open();
		}

		myBrowser->Display();

		if (mySelectedFile.empty() && myBrowser->HasSelected())
		{
			mySelectedFile = myBrowser->GetSelected().string();
			myBrowser->ClearSelected();
		}
		ImGui::NewLine();

		myMaterialDesc.myVertexShader.reserve(256);
		ImGui::InputText(" :Vertex Shader", myMaterialDesc.myVertexShader.data(), 256);
		myMaterialDesc.myPixelShader.reserve(256);
		ImGui::InputText(" :Pixel Shader", myMaterialDesc.myPixelShader.data(), 256);

		ImGui::Checkbox("Is Opaque", &myMaterialDesc.myIsOpaque);
		ImGui::Checkbox("Needs Forward", &myMaterialDesc.myNeedsForward);
		ImGui::Checkbox("Needs GBuffer", &myMaterialDesc.myNeedsGBuffer);

		uint numTextures = myMaterialDesc.myTextures.Count();
		if (numTextures > 0)
		{ 
			SC_GrowingArray<const char*> textures;
			textures.PreAllocate(numTextures);
			for (uint i = 0; i < numTextures; ++i)
				textures.Add(myMaterialDesc.myTextures[i].c_str());

			int select = -1;
			ImGui::ListBox(" :Textures", &select, textures.GetBuffer(), numTextures, SC_Min(textures.Count(), 6));


		}

		ImGui::NewLine();
		if (ImGui::Button("Save"))
		{
		}



		ImGui::End();
	}
}