#include "STools_Precompiled.h"
#include "STools_Editor.h"

#include "imgui_node_editor.h"

#include "STools_FeatureBinding.h"
#include "STools_AssetRepository.h"
#include "STools_LogWindow.h"
#include "STools_DynamicSky.h"
#include "STools_GraphicsWorld.h"
#include "STools_AssetImporter.h"
#include "STools_ScreenToWorld.h"

#include "SClient_Base.h"

#include "SG_View.h"
#include "SG_Camera.h"

#include "SR_GraphicsDevice.h"

#include "SC_Profiler.h"
#include "STools_MaterialEditor.h"

namespace ed = ax::NodeEditor;

static ed::EditorContext* g_Context = nullptr;

namespace Shift
{
	STools_Editor* STools_Editor::ourInstance = nullptr;

	STools_Editor::STools_Editor()
		: myTargetClientHook(nullptr)
	{
		if (ourInstance)
			assert(false && "Only one Editor instance is allowed.");

		ourInstance = this;
	}

	STools_Editor::~STools_Editor()
	{
		ourInstance = nullptr;
	}

	bool STools_Editor::Init()
	{
		bool result = true;
		if (myTargetClientHook)
			result = myTargetClientHook->Init();

		myCamera = myTargetClientHook->GetCamera();

		//g_Context = ed::CreateEditor();

		RegisterTool(new STools_AssetRepository());
		RegisterTool(new STools_LogWindow());
		RegisterTool(new STools_DynamicSky(myTargetClientHook->GetGraphicsWorld()->mySky));
		RegisterTool(new STools_GraphicsWorld(myTargetClientHook->GetGraphicsWorld()));

		return result;
	}
	void STools_Editor::Update()
	{
		for (STools_FeatureBinding* tool : myRemovedTools)
			myTools.RemoveCyclic(tool);

		myRemovedTools.RemoveAll();

		for (auto& tool : myTools)
		{
			tool->Update();
		}

		//myGizmo.Interact();

		SC_CPU_PROFILER_FUNCTION();
		if (myTargetClientHook)
			myTargetClientHook->Update();
	}
	void STools_Editor::GameExitHook()
	{
	}
	SC_PRAGMA_DEOPTIMIZE
	void STools_Editor::Render()
	{
		SG_View* clientView = myTargetClientHook->GetView();
		
		SR_Texture* viewportTexture = clientView->GetLastFinishedFrame();
		SC_Vector2f resolution = SC_EngineInterface::GetResolution();

		SC_Vector2f cursorPos;

		bool fullscreen = true;
		bool open = true;
		
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
		if (fullscreen)
		{
			ImGuiViewport* viewport = ImGui::GetMainViewport();
			ImGui::SetNextWindowPos(viewport->Pos);
			ImGui::SetNextWindowSize(viewport->Size);
			ImGui::SetNextWindowViewport(viewport->ID);
			window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
			window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
		}
		
		ImGui::Begin("DockSpace", &open, window_flags);
		ImGuiID dockspace_id = ImGui::GetID("DockSpaceBackground");
		ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), 0);
		
		if (ImGui::BeginMenuBar())
		{
			if (ImGui::BeginMenu("File"))
			{
				if (ImGui::MenuItem("Exit", "Alt+F4"))
					SC_EngineInterface::Exit();
				ImGui::EndMenu();
			}

			if (ImGui::BeginMenu("Window"))
			{
				if (ImGui::MenuItem("Import Asset"))
					RegisterTool(new STools_AssetImporter());
				if (ImGui::MenuItem("Asset Repository"))
					RegisterTool(new STools_AssetRepository());
				if (ImGui::MenuItem("Log"))
					RegisterTool(new STools_LogWindow());
				if (ImGui::MenuItem("Dynamic Sky"))
					RegisterTool(new STools_DynamicSky(myTargetClientHook->GetGraphicsWorld()->mySky));
				if (ImGui::MenuItem("Graphics World"))
					RegisterTool(new STools_GraphicsWorld(myTargetClientHook->GetGraphicsWorld()));
				if (ImGui::MenuItem("Material Editor"))
					RegisterTool(new STools_MaterialEditor());

				ImGui::EndMenu();
			}
		
			ImGui::EndMenuBar();
		}
		
		ImGui::Begin("Viewport");
		ImVec2 viewportSize = SR_ImGUI::GetCurrentWindowClientSize();
		ImGui::Image(viewportTexture, viewportSize);
		cursorPos = SR_ImGUI::GetCurrentRelativeCursorPos();
		ImGui::End(); // Viewport
		
		for (auto& tool : myTools)
		{
			tool->Render();
		}
		
		ImGui::End(); // DockSpace
		
		ImGui::Begin("Profiling");
		ImGui::Text("FPS: %i (Delta: %.6f ms)", SC_Timer::GetFPS(), SC_Timer::GetDeltaTime() * 1000.f);
		uint64 memoryUsed = SC_Memory::GetMemoryUsage();
		ImGui::Text("Memory: %llu kB (%llu MB)", memoryUsed / KB, memoryUsed / MB);
		ImGui::Text("VRAM: %u MB", uint(SR_GraphicsDevice::GetUsedVRAM()));
		ImGui::Text("Viewport Cursor: x:%.3f y:%.3f", cursorPos.x, cursorPos.y);
		
#if ENABLE_PROFILER
		auto& profilerData = SC_Profiler::Get().GetData();
		for (auto& section : profilerData)
		{
			ImGui::Text("%s", section.first.c_str());
		
			std::sort(section.second.begin(), section.second.end());
			for (auto& event : section.second)
			{
				ImGui::Text("\t%06.3f ms \t: %s", event.myTimeElapsedMS, event.myTag.c_str());
			}
			section.second.RemoveAll();
		}
#endif	
		
		ImGui::End(); // Profiling
		myLastViewportCursorPos = cursorPos;

		//myGizmo.Render();
	}

	void STools_Editor::AddClientHook(SClient_Base* aTargetClient)
	{
		myTargetClientHook = aTargetClient;
	}
	void STools_Editor::RegisterTool(STools_FeatureBinding* aTool)
	{
		myTools.Add(aTool);
	}
	void STools_Editor::UnregisterTool(STools_FeatureBinding* aTool)
	{
		myRemovedTools.Add(aTool);
	}
}