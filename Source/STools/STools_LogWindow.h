#pragma once
#include "STools_FeatureBinding.h"

namespace Shift
{
	class STools_LogWindow final : public STools_FeatureBinding
	{
	public:
		STools_LogWindow();
		~STools_LogWindow();

		void Update() override;
		void Render() override;

	private:
		void TryOpenLogFile();
		void OnFileChanged(const std::string& aFile, const EFileEvent&);

		SC_GrowingArray<std::string> mySavedLogMessages;
		std::ifstream myLogFile;
	};

}