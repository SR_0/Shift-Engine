#pragma once
#include "STools_AssetTypes.h"
#include "STools_FeatureBinding.h"

namespace Shift
{
	class STools_AssetRepository final : public STools_FeatureBinding
	{
	public:
		STools_AssetRepository();
		~STools_AssetRepository();

		void Render() override;

	private:

		void OnDirectoryChanged(const SC_GrowingArray<std::string>& aFiles, const EFileEvent&);

		std::unordered_map<std::string, SC_GrowingArray<std::string>> myAssets;
	};
}
