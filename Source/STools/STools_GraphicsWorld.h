#pragma once
#include "STools_FeatureBinding.h"
namespace Shift
{
	class SG_World;
	class STools_GraphicsWorld final : public STools_FeatureBinding
	{
	public:
		STools_GraphicsWorld(SG_World* aWorld);
		~STools_GraphicsWorld();

		void Update() override;
		void Render() override;

	private:
		bool myEnableTerrain;
		SG_World* myWorld;
	};
}

