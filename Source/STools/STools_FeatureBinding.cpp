#include "STools_Precompiled.h"
#include "STools_FeatureBinding.h"

namespace Shift
{
	STools_FeatureBinding::STools_FeatureBinding()
		: myEditor(STools_Editor::GetInstance())
		, myIsOpen(true)
	{
	}

	STools_FeatureBinding::~STools_FeatureBinding()
	{
	}

	void STools_FeatureBinding::Update()
	{
		if (myIsOpen == false)
			UnregisterTool();
	}

	void STools_FeatureBinding::UnregisterTool()
	{
		myEditor->UnregisterTool(this);
	}
}