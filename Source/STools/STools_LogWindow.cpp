#include "STools_Precompiled.h"
#include "STools_LogWindow.h"

namespace Shift
{
	STools_LogWindow::STools_LogWindow()
	{
		//std::string inFileName(GetFilename(CEnginePaths::GetExecutableName()) + ".log");
		//auto onChange = [&](const std::string& aFile, const EFileEvent& aEvent)
		//{
		//	OnFileChanged(aFile, aEvent);
		//};
		//CEngineInterface::WatchFile(inFileName, onChange);
	}

	STools_LogWindow::~STools_LogWindow()
	{
		if (myLogFile.is_open())
			myLogFile.close();
	}

	void STools_LogWindow::Update()
	{
		STools_FeatureBinding::Update();

		mySavedLogMessages.RemoveAll();
		TryOpenLogFile();

		// TODO: only update when file changed.
		if (myLogFile.is_open())
		{
			myLogFile.clear();
			myLogFile.seekg(0);
			std::string logMsg;
			while (std::getline(myLogFile, logMsg))
			{
				mySavedLogMessages.Add(logMsg);
			}
		}
	}

	void STools_LogWindow::Render()
	{
		ImGui::Begin("Log", &myIsOpen);
		
		for (std::string& logText : mySavedLogMessages)
		{
			ImVec4 color(1.0f, 1.0f, 1.0f, 1.0f);
			if (logText.find("[ERROR]") != std::string::npos)
				color = ImVec4(1.0f, 0.0f, 0.0f, 1.0f);
			else if (logText.find("[WARNING]") != std::string::npos)
				color = ImVec4(1.0f, 1.0f, 0.0f, 1.0f);
			else if (logText.find("[SUCCESS]") != std::string::npos)
				color = ImVec4(0.0f, 1.0f, 0.0f, 1.0f);

			ImGui::TextColored(color, logText.c_str());
		}
		ImGui::End();
	}
	void STools_LogWindow::TryOpenLogFile()
	{
		std::string inFileName(GetFilename(SC_EnginePaths::GetExecutableName()) + ".log");
		myLogFile.open(inFileName);
		if (!myLogFile.is_open())
		{
			SC_ERROR_LOG("Couldn't open log file.");
			myLogFile.close();
		}
	}
	void STools_LogWindow::OnFileChanged(const std::string& /*aFile*/, const EFileEvent&)
	{
		
	}
}