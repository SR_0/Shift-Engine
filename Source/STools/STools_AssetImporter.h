#pragma once
#include "STools_FeatureBinding.h"

namespace ImGui
{
	class FileBrowser;
}

struct aiNode;
struct aiScene;

namespace Shift
{
	struct STools_MeshBuffer
	{
		SC_GrowingArray<std::string> myTextures;
		std::string myName;
		std::string myMaterialPath;

		SC_AABB myAABB;
		uint myVertexStrideSize;
		uint myNumVertices;
		uint myNumIndices;
		uint myTotalVertexBufferSize;
		uint myTotalIndexBufferSize;
		uint8* myVertices;
		uint* myIndices;
	};

	class STools_AssetImporter final : public STools_FeatureBinding
	{
	public:
		STools_AssetImporter();
		~STools_AssetImporter();

		void Render() override;

	private:
		void RenderImportedIntermediate();

		bool Import(const std::string& aFile);
		bool WriteMeshFile(const std::string& aFile, const SC_GrowingArray<STools_MeshBuffer>& aMeshes);
		void ProcessAiNode(aiNode* aNode, const aiScene* aScene, SC_GrowingArray<STools_MeshBuffer>& aOutMeshes);

		std::string mySelectedFile;
		SC_UniquePtr<ImGui::FileBrowser> myBrowser;
		SC_GrowingArray<STools_MeshBuffer> myImportedMesh;

		// Imported Intermediate
		std::string mySelectedMaterialFile;
		std::string myImportedModelName;
		int mySelectedMeshIdx;
		int mySelectedTextureIdx;
	};

}