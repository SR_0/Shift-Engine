#pragma once
#include "STools_FeatureBinding.h"
#include "SG_Material.h"

namespace ImGui
{
	class FileBrowser;
}

namespace Shift
{
	class STools_MaterialEditor final : public STools_FeatureBinding
	{
	public:
		STools_MaterialEditor();
		~STools_MaterialEditor();

		void Render() override;

	private:
		SC_UniquePtr<ImGui::FileBrowser> myBrowser;
		std::string mySelectedFile;
		SG_MaterialDesc2 myMaterialDesc;
	};
}
