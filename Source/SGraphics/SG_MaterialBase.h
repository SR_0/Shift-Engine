#pragma once
#include "SR_GraphicsDefinitions.h"
#include "GraphicsPSO.h"
namespace Shift
{
	class SR_ShaderState;

	struct SG_MaterialBaseDesc
	{
		SG_MaterialBaseDesc()
			: myBlendState(EBlendState::Disabled)
			, myTopology(SR_Topology_TriangleList)
			, myIsOpaque(true)
			, myNumRenderTargets(0)
			, myWriteDepth(true)
		{}
		std::string myVertexShader;
		std::string myPixelShader;
		std::string myShaderCode;

		uint myNumRenderTargets;

		SR_RasterizerStateDesc myRasterizerStateDesc;
		EBlendState myBlendState;
		SR_Topology myTopology;

		bool myWriteDepth;
		bool myIsOpaque : 1;
	};

	class SG_MaterialBase
	{
	public:
		SG_MaterialBase();
		~SG_MaterialBase();

		void CreatePSO(const SG_MaterialBaseDesc& aDesc);
		bool BindPSO() const;

		uint GetHash() const;

	private:
		uint myHash;
		CGraphicsPSO* myPSO;
		SC_Ref<SR_ShaderState> myShader;
	};
}

