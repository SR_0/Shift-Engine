#include "SGraphics_Precompiled.h"
#include "SG_Raytracer.h"

namespace Shift
{
	SG_Raytracer::SG_Raytracer()
	{
	}

	SG_Raytracer::~SG_Raytracer()
	{
	}
	void SG_Raytracer::Init()
	{
	}
	void SG_Raytracer::BuildStructure(SG_RenderData& /*aPrepareData*/)
	{
		// Gather geometrydata
		// Build raytracing structure
		// Optimize structure for tracing.
	}
	void SG_Raytracer::Light()
	{
		// Dispatch lights into structure to gather light-data.
		// 1. Shadows
		// 2. Direct Light
		// 3. AO
		// 4. Indirect Light
		// 5. Reflections
	}
}