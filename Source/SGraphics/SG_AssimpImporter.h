#pragma once


struct aiNode;
struct aiScene;
struct aiMaterial;
namespace Shift
{
	class SG_Model;
	class CAssimpImporter
	{
	public:
		CAssimpImporter();
		~CAssimpImporter();

		bool Import(SG_Model& aModelOut, const char* aFile, bool aShouldInvert);
	private:
		void WriteMeshFile(const SG_Model& aModel, const char* aMeshName);
		void ProcessAiNode(SG_Model& aModelOut, aiNode* aNode, const aiScene* aScene, SG_Model* aParent);
		void GenerateMaterial(SG_Model& aModelOut, const aiMaterial* aMaterial);
	};
}