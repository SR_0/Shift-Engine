#include "SGraphics_Precompiled.h"
#include "SG_MeshInstance.h"
#include "SG_MeshHeader.h"
#include "SG_Mesh.h"
#include "SG_MaterialFactory.h"
#include "SG_Camera.h"

namespace Shift
{
	SG_MeshInstance::SG_MeshInstance(SG_Mesh* aMesh)
		: myMeshTemplate(aMesh)
	{
		CalcBoundingBox();
	}

	SG_MeshInstance::~SG_MeshInstance()
	{
	}

	SC_Ref<SG_MeshInstance> SG_MeshInstance::Create(const SG_MeshHeader& aHeader, void* aVertexData, void* aIndexData, const char* aMaterialPath, const char* aName)
	{
		SC_Ref<SG_Mesh> mesh = SG_Mesh::Create(aHeader, aVertexData, aIndexData, aName);
		SC_Ref<SG_MeshInstance> meshInstance = new SG_MeshInstance(mesh);

		if (aMaterialPath)
			meshInstance->myMaterial = SG_MaterialFactory::GetCreateMaterial(aMaterialPath);

		return meshInstance;
	}

	void SG_MeshInstance::SetTransform(const SC_Matrix44& aTransform)
	{
		myTransform = aTransform;
		CalcBoundingBox();
	}

	const SC_Matrix44& SG_MeshInstance::GetTransform() const
	{
		return myTransform;
	}

	const SC_AABB& SG_MeshInstance::GetBoundingBox() const
	{
		return myBoundingBox;
	}
	bool SG_MeshInstance::Cull(const SG_Camera& aCamera)
	{
		if (aCamera.GetFrustum().Intersects(myBoundingBox))
			return true;

		return false;
	}
	void SG_MeshInstance::CalcBoundingBox()
	{
		const SC_AABB& aabb = myMeshTemplate->GetAABB();
		myBoundingBox.myMin = aabb.myMin * myTransform;
		myBoundingBox.myMax = aabb.myMax * myTransform;
	}
}