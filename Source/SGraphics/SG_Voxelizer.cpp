#include "SGraphics_Precompiled.h"
#include "SG_Voxelizer.h"
#include "SG_RenderData.h"
#include "SG_Model.h"
#include "SG_Material.h"
#include "SG_Shadows.h"

namespace Shift
{
	CGraphicsVoxelizer::CGraphicsVoxelizer()
	{
	}
	CGraphicsVoxelizer::~CGraphicsVoxelizer()
	{
	}
	void CGraphicsVoxelizer::Init()
	{
		//CGraphicsDevice* device = CGraphicsDevice::GetDevice();
		//
		//mySettings.myVoxelCenter = float3(0.f, 0.f, 0.f);
		//mySettings.myVoxelSize = 0.25f;
		//mySettings.myVoxelTextureSize = 64.f;
		//
		//STextureDesc sceneDesc;
		//sceneDesc.myDimension = SR_Dimension_Texture3D;
		//sceneDesc.myFlags = SR_ResourceFlag_AllowWrites;
		//sceneDesc.myFormat = SFormat_R32_Uint;
		//sceneDesc.myWidth = mySettings.myVoxelTextureSize;
		//sceneDesc.myHeight = mySettings.myVoxelTextureSize;
		//sceneDesc.myDepth = mySettings.myVoxelTextureSize;
		//sceneDesc.myMips = 1;

		//if (!device->CreateTexture(sceneDesc, &myVoxelizedScene))
		//{
		//	S_ERROR_LOG("Could not create voxel texture.");
		//	return;
		//}
		//
		//sceneDesc.myFormat = SFormat_RGBA8_Unorm;
		//sceneDesc.myMips = (uint16)log2(mySettings.myVoxelTextureSize);
		//if (!device->CreateTexture(sceneDesc, &myVoxelizedScene_Float4))
		//{
		//	S_ERROR_LOG("Could not create voxel texture.");
		//	return;
		//}
		//
		//sceneDesc.myFormat = SFormat_RGB10A2_Unorm;
		//if (!device->CreateTexture(sceneDesc, &myVoxelizedNormals))
		//{
		//	S_ERROR_LOG("Could not create voxel texture.");
		//	return;
		//}
		//
		//sceneDesc.myDimension = SR_Dimension_Texture2D;
		//sceneDesc.myFlags = SR_ResourceFlag_AllowRenderTarget;
		//sceneDesc.myFormat = SFormat_RGBA8_Unorm;
		//sceneDesc.myWidth = mySettings.myVoxelTextureSize;
		//sceneDesc.myHeight = mySettings.myVoxelTextureSize;
		//sceneDesc.myDepth = 1.0f;
		//sceneDesc.myMips = 1;
		//
		//if (!device->CreateTexture(sceneDesc, &myVoidRT))
		//{
		//	S_ERROR_LOG("Could not create voxel void target.");
		//	return;
		//}

#if !IS_FINAL_BUILD
		//sceneDesc.myWidth = device->GetResolution().x;
		//sceneDesc.myHeight = device->GetResolution().y;
		//if (!device->CreateTexture(sceneDesc, &myDebugRT))
		//{
		//	S_ERROR_LOG("Could not create voxel debug target.");
		//	return;
		//}
#endif
		{
			SGraphicsPSODesc desc;
			desc.myVertexShader = "Shaders/Object_VoxelizeVS.hlsl";
			desc.myPixelShader = "Shaders/Object_VoxelizePS.hlsl";
			desc.myGeometryShader = "Shaders/Object_VoxelizeGS.hlsl";
			desc.topology = SR_Topology_TriangleList;
			myVoxelizeShader = SR_ShaderStateCache::Get().GetPSO(desc);
		}
#if !IS_FINAL_BUILD
		{
			SGraphicsPSODesc desc;
			desc.myVertexShader = "Shaders/Object_VoxelInstancedVS.hlsl";
			desc.myPixelShader = "Shaders/Object_VoxelPS.hlsl";
			desc.myGeometryShader = "Shaders/Object_VoxelGS.hlsl";
			desc.myRenderTargetFormats.myNumColorFormats = 1;
			desc.myRenderTargetFormats.myColorFormats[0] = SR_Format_RGBA8_Unorm;
			desc.topology = SR_Topology_PointList;
			myDebugVoxels = SR_ShaderStateCache::Get().GetPSO(desc);
		}
#endif
		{
			SComputePSODesc desc;
			desc.myComputeShader = "Shaders/VoxelSceneClear.hlsl";
			myClearShader = SR_ShaderStateCache::Get().GetPSO(desc);
		}
		{
			SComputePSODesc desc;
			desc.myComputeShader = "Shaders/VoxelSceneRelight.hlsl";
			myRelightShader = SR_ShaderStateCache::Get().GetPSO(desc);
		}
		{
			SComputePSODesc desc;
			desc.myComputeShader = "Shaders/ConvertTexture3D_UintToFloat4.hlsl";
			myConvertTextureShader = SR_ShaderStateCache::Get().GetPSO(desc);
		}
		SVertexBufferDesc instanceBufferDesc;
		instanceBufferDesc.myNumVertices = 1;
		instanceBufferDesc.myStride = sizeof(SVertex_ObjectInputInstance);
		instanceBufferDesc.myVertexData = nullptr;
		//device->CreateVertexBuffer(instanceBufferDesc, &myInstanceBuffer);

		myCamera.InitAsOrthogonal(SC_Vector2f(mySettings.myVoxelTextureSize, mySettings.myVoxelTextureSize), 0.0f, mySettings.myVoxelTextureSize);
	}
	void CGraphicsVoxelizer::UpdateSettings(const SVoxelizerSettings& aSettings)
	{
		SC_UNUSED(aSettings);
	}
	const SVoxelizerSettings& CGraphicsVoxelizer::GetSettings() const
	{
		return mySettings;
	}
	SVoxelizerSettings& CGraphicsVoxelizer::GetSettings()
	{
		return mySettings;
	}
	void CGraphicsVoxelizer::RenderVoxels(const SG_RenderData& aRenderData, SG_Shadows* aShadowRenderer)
	{
		if (!myVoxelizeShader)
			return;

		SE_PROFILER_FUNCTION();

		//CGraphicsDevice* device = CGraphicsDevice::GetDevice();
		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();
		{
			SE_PROFILER_FUNCTION_TAGGED("Clear VoxelTexture");
			//ctx->BindTextureRW(myVoxelizedScene, 0);
			ctx->BindComputePSO(*myClearShader);
			ctx->Dispatch((uint)mySettings.myVoxelTextureSize, (uint)mySettings.myVoxelTextureSize, (uint)mySettings.myVoxelTextureSize);
		}


		{
			SE_PROFILER_FUNCTION_TAGGED("Render VoxelTexture");
			const float f = 0.5f / mySettings.myVoxelSize;
			SC_Vector3f camPos = aRenderData.myMainCamera.GetPosition();
			SC_Vector3f center = SC_Vector3f(floorf(camPos.x * f) / f, floorf(camPos.y * f) / f, floorf(camPos.z * f) / f);

			mySettings.myVoxelCenter = center;

			SViewport viewport;
			viewport.width = mySettings.myVoxelTextureSize;
			viewport.height = mySettings.myVoxelTextureSize;
			viewport.maxDepth = 1.0f;
			ctx->SetViewport(viewport);

			SScissorRect scissor;
			scissor.right = (long)viewport.width;
			scissor.bottom = (long)viewport.height;
			ctx->SetScissorRect(scissor);

			//ctx->Transition(myVoidRT.GetBuffer(), SR_ResourceState_RenderTarget);
			//ctx->SetRenderTargets(&myVoidRT, 1, device->GetDepthTexture());

			//ctx->Transition(myVoxelizedScene.GetBuffer(), SR_ResourceState_UnorderedAccess);

			SC_GrowingArray<SVertex_ObjectInputInstance> instanceData;
			for (uint i = 0; i < aRenderData.myOpaqeObjects.Count(); ++i)
			{
				const SG_RenderObject& object = aRenderData.myOpaqeObjects[i];
				if (!object.myModel)
					continue;

				SVertex_ObjectInputInstance& data = instanceData.Add();
				data.mat0 = SC_Vector4f(object.myTransform.my11, object.myTransform.my12, object.myTransform.my13, object.myTransform.my14);
				data.mat1 = SC_Vector4f(object.myTransform.my21, object.myTransform.my22, object.myTransform.my23, object.myTransform.my24);
				data.mat2 = SC_Vector4f(object.myTransform.my31, object.myTransform.my32, object.myTransform.my33, object.myTransform.my34);
				data.mat3 = SC_Vector4f(object.myTransform.my41, object.myTransform.my42, object.myTransform.my43, object.myTransform.my44);
				data.instanceColor = SC_Vector4f(1.0f, 1.0f, 1.0f, 1.0f);

				ctx->UpdateBufferData(sizeof(SVertex_ObjectInputInstance), instanceData.Count(), instanceData.GetBuffer(), myInstanceBuffer);

				SC_GrowingArray<CVertexBuffer> vBuffers;
				vBuffers.Add(CVertexBuffer());
				vBuffers.Add(myInstanceBuffer);
				for (uint meshIndex = 0; meshIndex < object.myModel->myMeshes.Count(); ++meshIndex)
				{
					ctx->BindConstantBuffer(&mySettings, sizeof(mySettings), 0);

					SC_GrowingArray<SC_Ref<SR_Texture>>& textures = object.myModel->myMaterials[meshIndex]->GetTextures();
					ctx->BindTexture(textures[0], 0);

					//ctx->BindTextureRW(myVoxelizedScene, 0);
					//ctx->BindTextureRW(myVoxelizedNormals, 1);

					vBuffers[0] = (object.myModel->myMeshes[meshIndex].myVertexBuffer);
					ctx->SetVertexBuffer(0, vBuffers);
					ctx->SetIndexBuffer(object.myModel->myMeshes[meshIndex].myIndexBuffer);

					ctx->BindGraphicsPSO(*myVoxelizeShader);
					ctx->DrawIndexedInstanced(object.myModel->myMeshes[meshIndex].myIndexBuffer.GetNumIndices(), instanceData.Count(), 0, 0, 0);
				}
				instanceData.RemoveAll();

			}
		}
		{
			SE_PROFILER_FUNCTION_TAGGED("Unpack VoxelTexture");
				//ctx->BindTexture(myVoxelizedScene, 0);
			//ctx->BindTextureRW(myVoxelizedScene_Float4, 0);
			ctx->BindComputePSO(*myConvertTextureShader);
			ctx->Dispatch(std::max(uint(mySettings.myVoxelTextureSize / 4), 1u), std::max(uint(mySettings.myVoxelTextureSize / 4), 1u), std::max(uint(mySettings.myVoxelTextureSize / 4), 1u));
		}
		RelightVoxels(aRenderData, aShadowRenderer);

		{
			SE_PROFILER_FUNCTION_TAGGED("Generate Voxel Mips");
			//ctx->GenerateMips(myVoxelizedScene_Float4);
			//ctx->GenerateMips(myVoxelizedNormals);
		}
	}
	void CGraphicsVoxelizer::DebugDraw(const SG_RenderData& aRenderData)
	{
#if !IS_FINAL_BUILD
		SR_GraphicsDevice* device = SR_GraphicsDevice::GetDevice();
		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();

		SE_PROFILER_FUNCTION();

		ctx->SetTopology(SR_Topology_PointList);

		SViewport viewport;
		viewport.width = device->GetResolution().x;
		viewport.height = device->GetResolution().y;
		viewport.maxDepth = 1.0f;
		ctx->SetViewport(viewport);

		SScissorRect scissor;
		scissor.right = (long)viewport.width;
		scissor.bottom = (long)viewport.height;
		ctx->SetScissorRect(scissor);

		//ctx->Transition(myDebugRT.GetBuffer(), SR_ResourceState_RenderTarget);
		//ctx->SetRenderTargets(&myDebugRT, 1, device->GetDepthTexture());

		ctx->ClearDepthTarget(true);
		ctx->ClearRenderTarget(0, SC_Vector4f(0.f, 0.f, 0.f, 1.f), false);

		struct CameraConstants
		{
			SC_Matrix44 proj;
			SC_Matrix44 view;
			SC_Matrix44 model;
			SC_Float3 cameraPosition;
		} constants;

		constants.proj = aRenderData.myMainCamera.GetProjection();
		constants.view = aRenderData.myMainCamera.GetView();
		constants.cameraPosition = aRenderData.myMainCamera.GetPosition();

		ctx->BindConstantBuffer(&constants, sizeof(constants), 0);
		ctx->BindConstantBuffer(&mySettings, sizeof(mySettings), 1);

		//ctx->BindTexture(myVoxelizedScene_Float4, 0);

		uint gridSize = (uint)pow(mySettings.myVoxelTextureSize, 3);

		ctx->BindGraphicsPSO(*myDebugVoxels);
		ctx->Draw(gridSize, 0);
#else
		SC_UNUSED(aRenderData);
#endif
	}
	void CGraphicsVoxelizer::BindVoxelScene()
	{
		//CGraphicsContext* ctx = CGraphicsContext::GetCurrent();
		//ctx->BindTexture(myVoxelizedScene_Float4, TEXSLOT_VOXEL_SCENE);
	}
	void CGraphicsVoxelizer::AddGUI()
	{
		ImGui::SliderFloat("VoxelSize", &mySettings.myVoxelSize, 0.03125f, 1.0f, "%.5f");
	}
	void CGraphicsVoxelizer::RelightVoxels(const SG_RenderData& aRenderData, SG_Shadows* aShadowRenderer)
	{
		aShadowRenderer;
		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();

		SE_PROFILER_FUNCTION();

		struct CameraConstants
		{
			SC_Matrix44 shadowProj;
			SC_Matrix44 shadowView;
		} cameraConstants;
		//cameraConstants.shadowProj = aShadowRenderer->GetShadowProjection();
		//cameraConstants.shadowView = aShadowRenderer->GetShadowView();

		struct LightConstants
		{
			SC_Float3 SkyLightColor;
			float SkyIntensity;
			SC_Float3 SkyLightToDirection;
			float AmbientLightIntensity;
			SC_Float3 PointLightColor;
			float PointLightRange;
			SC_Float3 PointLightPos;
			float PointLightIntensity;

		} lightConstants;
		lightConstants.SkyLightColor = SC_Vector3f(1.0f, 1.0f, 1.0f);
		lightConstants.SkyIntensity = 1.0f;
		lightConstants.SkyLightToDirection = aRenderData.mySun.GetToDirection();
		lightConstants.AmbientLightIntensity = 1.0f;
		lightConstants.PointLightPos = aRenderData._PL.PointLightPos;
		lightConstants.PointLightColor = aRenderData._PL.PointLightColor;
		lightConstants.PointLightRange = aRenderData._PL.PointLightRange;
		lightConstants.PointLightIntensity = aRenderData._PL.PointLightIntensity;

		ctx->BindConstantBuffer(&cameraConstants, sizeof(cameraConstants), 0);
		ctx->BindConstantBuffer(&lightConstants, sizeof(lightConstants), 1);
		ctx->BindConstantBuffer(&mySettings, sizeof(mySettings), 2);


		//ctx->BindTextureRW(myVoxelizedScene_Float4, 0);
		//ctx->BindTexture(myVoxelizedNormals, 0);

		//ctx->BindTexture(aShadowRenderer->GetShadowBuffer(), 1);

		ctx->BindComputePSO(*myRelightShader);
		ctx->Dispatch(std::max(uint(mySettings.myVoxelTextureSize / 8), 1u), std::max(uint(mySettings.myVoxelTextureSize / 8), 1u), std::max(uint(mySettings.myVoxelTextureSize / 8), 1u));
	}
}