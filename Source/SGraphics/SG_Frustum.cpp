#include "SGraphics_Precompiled.h"
#include "SG_Frustum.h"

namespace Shift
{
	SG_Frustum::SG_Frustum()
	{
	}
	SG_Frustum::~SG_Frustum()
	{
	}
	void SG_Frustum::Update(const SC_Matrix44& aView, const SC_Matrix44& aProj)
	{
		const SC_Matrix44 viewProj = aView * aProj; 

		myPlanes[SG_FrustumPlane_Left].x = viewProj.my14 + viewProj.my11;
		myPlanes[SG_FrustumPlane_Left].y = viewProj.my24 + viewProj.my21;
		myPlanes[SG_FrustumPlane_Left].z = viewProj.my34 + viewProj.my31;
		myPlanes[SG_FrustumPlane_Left].w = viewProj.my44 + viewProj.my41;
		myPlanes[SG_FrustumPlane_Left].Normalize();

		myPlanes[SG_FrustumPlane_Right].x = viewProj.my14 - viewProj.my11;
		myPlanes[SG_FrustumPlane_Right].y = viewProj.my24 - viewProj.my21;
		myPlanes[SG_FrustumPlane_Right].z = viewProj.my34 - viewProj.my31;
		myPlanes[SG_FrustumPlane_Right].w = viewProj.my44 - viewProj.my41;
		myPlanes[SG_FrustumPlane_Right].Normalize();

		myPlanes[SG_FrustumPlane_Top].x = viewProj.my14 - viewProj.my12;
		myPlanes[SG_FrustumPlane_Top].y = viewProj.my24 - viewProj.my22;
		myPlanes[SG_FrustumPlane_Top].z = viewProj.my34 - viewProj.my32;
		myPlanes[SG_FrustumPlane_Top].w = viewProj.my44 - viewProj.my42;
		myPlanes[SG_FrustumPlane_Top].Normalize();

		myPlanes[SG_FrustumPlane_Bottom].x = viewProj.my14 + viewProj.my12;
		myPlanes[SG_FrustumPlane_Bottom].y = viewProj.my24 + viewProj.my22;
		myPlanes[SG_FrustumPlane_Bottom].z = viewProj.my34 + viewProj.my32;
		myPlanes[SG_FrustumPlane_Bottom].w = viewProj.my44 + viewProj.my42;
		myPlanes[SG_FrustumPlane_Bottom].Normalize();

		myPlanes[SG_FrustumPlane_Near].x = viewProj.my13;
		myPlanes[SG_FrustumPlane_Near].y = viewProj.my23;
		myPlanes[SG_FrustumPlane_Near].z = viewProj.my33;
		myPlanes[SG_FrustumPlane_Near].w = viewProj.my43;
		myPlanes[SG_FrustumPlane_Near].Normalize();

		myPlanes[SG_FrustumPlane_Far].x = viewProj.my14 - viewProj.my13;
		myPlanes[SG_FrustumPlane_Far].y = viewProj.my24 - viewProj.my23;
		myPlanes[SG_FrustumPlane_Far].z = viewProj.my34 - viewProj.my33;
		myPlanes[SG_FrustumPlane_Far].w = viewProj.my44 - viewProj.my43;
		myPlanes[SG_FrustumPlane_Far].Normalize();
	}

	bool SG_Frustum::Intersects(const SC_AABB& aAABB) const
	{
		bool intersects = true;
		for (uint i = 0; i < SG_FrustumPlane_Plane_Count; ++i)
		{
			SC_Vector3f axisVert;
			if (myPlanes[i].x < 0.0f)
				axisVert.x = aAABB.myMin.x;
			else
				axisVert.x = aAABB.myMax.x;

			if (myPlanes[i].y < 0.0f)
				axisVert.y = aAABB.myMin.y;
			else
				axisVert.y = aAABB.myMax.y;

			if (myPlanes[i].z < 0.0f)
				axisVert.z = aAABB.myMin.z;
			else
				axisVert.z = aAABB.myMax.z;

			if (myPlanes[i].XYZ().Dot(axisVert) + myPlanes[i].w < 0.0f)
			{
				intersects = false;
				break;
			}
		}

		return intersects;
	}
}