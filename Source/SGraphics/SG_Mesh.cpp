#include "SGraphics_Precompiled.h"
#include "SG_Mesh.h"
#include "SG_MeshHeader.h"

namespace Shift
{
	std::unordered_map<uint, SC_Ref<SG_Mesh>> SG_Mesh::ourCache;

	SG_Mesh::SG_Mesh()
		: myVertexStride(0)
		, myNumVertices(0)
		, myNumIndices(0)
	{
	}

	SG_Mesh::~SG_Mesh()
	{
	}
	SC_PRAGMA_DEOPTIMIZE
	SC_Ref<SG_Mesh> SG_Mesh::Create(const SG_MeshHeader& aHeader, void* aVertexData, void* aIndexData, const char* aName)
	{
		uint hash = 0;
		hash += SC_Hash(&aHeader, sizeof(aHeader));
		if (aVertexData)
			hash += SC_Hash(aVertexData, aHeader.myTotalVertexBufferSize);
		if (aIndexData)
			hash += SC_Hash(aIndexData, aHeader.myTotalIndexBufferSize);
		if (aName)
			hash += SC_Hash(aName, aHeader.myNameSize);

		if (ourCache.find(hash) != ourCache.end())
			return ourCache[hash];

		SC_Ref<SG_Mesh> mesh = new SG_Mesh();

		mesh->myVertexBufferData = new uint8[aHeader.myTotalVertexBufferSize];
		SC_Memcpy(mesh->myVertexBufferData, aVertexData, aHeader.myTotalVertexBufferSize);
		mesh->myNumVertices = aHeader.myNumVertices;
		mesh->myVertexStride = aHeader.myVertexStrideSize;

		mesh->myIndexBufferData = new uint[aHeader.myTotalIndexBufferSize];
		SC_Memcpy(mesh->myIndexBufferData, aIndexData, aHeader.myTotalIndexBufferSize);
		mesh->myNumIndices = aHeader.myNumIndices;

		mesh->myBoundingBox = aHeader.myAABB;
		mesh->myName = aName;

		mesh->Init();

		ourCache[hash] = mesh;
		return mesh;
	}

	void SG_Mesh::Init()
	{
		SR_GraphicsDevice* device = SR_GraphicsDevice::GetDevice();

		SR_BufferDesc vertexBufferDesc;
		vertexBufferDesc.myBindFlag = SBindFlag_VertexBuffer;
		vertexBufferDesc.mySize = myVertexStride * myNumVertices;
		myVertexBuffer = device->_CreateBuffer(vertexBufferDesc, myVertexBufferData, "Mesh VertexBuffer");

		SR_BufferDesc indexBufferDesc;
		indexBufferDesc.myBindFlag = SBindFlag_IndexBuffer;
		indexBufferDesc.mySize = sizeof(uint) * myNumIndices;
		myIndexBuffer = device->_CreateBuffer(indexBufferDesc, myIndexBufferData, "Mesh IndexBuffer");
	}

	SR_Buffer* SG_Mesh::GetVertexBuffer()
	{
		return myVertexBuffer;
	}

	SR_Buffer* SG_Mesh::GetIndexBuffer()
	{
		return myIndexBuffer;
	}

	void SG_Mesh::SetVertexBufferData(uint8* aVertexData, uint aNumVertices)
	{
		myVertexBufferData = aVertexData;
		myNumVertices = aNumVertices;
	}

	void SG_Mesh::SetVertexStride(uint aVertexStride)
	{
		myVertexStride = aVertexStride;
	}

	void SG_Mesh::SetIndexBufferData(uint* aIndexData, uint aNumIndices)
	{
		myIndexBufferData = aIndexData;
		myNumIndices = aNumIndices;
	}
}