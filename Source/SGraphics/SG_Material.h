#pragma once

#include "SG_MaterialBase.h"
#include "SG_RenderType.h"

#include "SR_Texture.h"

namespace Shift
{
	using MaterialHandle = unsigned int;
	class SR_ShaderState;

	struct SG_MaterialDesc
	{
		SG_MaterialDesc()
			: myUseConstantAttributes(nullptr)
			, myReceiveShadows(true)
			, myCastShadows(false)
			, myWireframe(false)
			, myIsOpaque(true)
		{}

		uint SC_Hash() const;
		uint GetSize() const;

		static constexpr uint MaxNumTextures = 32;

		std::string myFilename;

		std::string myVertexShader;
		std::string myPixelShader;

		std::vector<std::string> myTextures;

		bool myUseConstantAttributes : 1;
		bool myReceiveShadows : 1;
		bool myCastShadows : 1;
		bool myWireframe : 1;
		bool myIsOpaque : 1;
	};

	class SG_Material
	{
		friend class SG_MaterialFactory;
	public:
		SG_Material();
		~SG_Material();

		void Init(const SG_MaterialDesc& aDesc);
		bool Bind();
		bool BindDepthTest();

		void SetColor(SC_Float3 aColor);
		void SetRoughness(float aRoughness);
		void SetMetallic(float aMetallic);

		uint GetHash() const;

		SC_GrowingArray<SC_Ref<SR_Texture>>& GetTextures();

		static constexpr const char* ourMaterialExtension = ".smtf";
		static const std::string ourMaterialDirectory;

	private:
		void Init_Internal(const SG_MaterialDesc& aDesc);
		void InitFromFile_Internal(const SG_MaterialDesc& aDesc);

		uint SC_Hash();
		bool Serialize();

	private:
		SG_MaterialBase myBase;
		SG_MaterialBase myBaseDepthTest;
		SC_GrowingArray<SC_Ref<SR_Texture>> myTextures;
		SG_MaterialDesc myProperties;

		SC_Float3 myColor; // Diffuse Color
		SC_Float3 myARM; // Ambient Occlusion, Roughness, Metallic

		uint myHash;

		bool myInitialized : 1;
	};

	struct SG_MaterialDesc2
	{
		SG_MaterialDesc2()
			: myIsOpaque(true)
			, myNeedsGBuffer(true)
			, myNeedsForward(false)
		{}

		void FromFile(const char* aFilePath);

		uint GetHash() const
		{
			uint hash = 0;
			for (uint i = 0; i < myTextures.Count(); ++i)
				hash += SC_Hash(myTextures[i].c_str(), (uint)(sizeof(char) * myTextures[i].length()));

			hash += SC_Hash(myVertexShader.c_str(), (uint)(sizeof(char) * myVertexShader.length()));
			hash += SC_Hash(myPixelShader.c_str(), (uint)(sizeof(char) * myPixelShader.length()));
			hash += SC_Hash(&myIsOpaque, (uint)(sizeof(bool) * 3));

			return hash;
		}

		SC_GrowingArray<std::string> myTextures;
		std::string myVertexShader;
		std::string myPixelShader;

		bool myIsOpaque;
		bool myNeedsForward;
		bool myNeedsGBuffer;
	};

	class SG_Material2 : public SC_Resource
	{
		friend class SG_MaterialFactory;
	public:
		SG_Material2();
		~SG_Material2();

		void Bind(const SG_RenderType& aRenderType);

	private:
		SC_Ref<SR_ShaderState> myShaders[SG_RenderType_COUNT];
		SC_GrowingArray<SC_Ref<SR_Texture>> myTextures;
		SG_MaterialDesc2 myProperties;
	};
}
