#pragma once
#include "SC_Resource.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"

struct aiNode;
struct aiScene;
struct aiMaterial;
namespace Shift
{
	class SG_Material;
	class SG_MeshInstance;
	class SG_Camera;
	class SG_Model : public SC_Resource
	{
		friend class CAssimpImporter;
		friend class SG_ModelLoader;
		friend class SG_World;
	public:
		struct Bones
		{
		};
		struct ModelBuffer
		{
			~ModelBuffer()
			{
			}
			std::string myName;

			SC_AABB myAABB;
			uint myMaterialHash;
			Bones myBones;
			uint myVertexSize;
			uint myVertexByteOffset;
			uint myNumVertices;
			uint myNumIndices;
			uint myIndexByteOffset;
			uint myBufferByteSize;
			uint8* myBuffer;
			bool myIsActive;

			CVertexBuffer myVertexBuffer;
			CIndexBuffer myIndexBuffer;
		};

		SG_Model();
		SG_Model(const char* aFilePath, bool aShouldInvert = false);

		~SG_Model();

		static SC_Ref<SG_Model> Create(const char* aFilePath);

		void FromFile(const char* aFilePath, bool aShouldInvert = false);
		void FromHashedFile();

		void Render();
		void DebugDrawAABB();

		bool Cull(const SG_Camera& aCamera);

		uint GetHash() const;
		
		SC_GrowingArray<ModelBuffer> myMeshes;
		SC_GrowingArray<SG_Material*> myMaterials;

		SC_Matrix44 myTransform;
	private:
		std::vector<SG_Model> myChildren;
		SC_GrowingArray<SC_Ref<SG_MeshInstance>> _myMeshes;
		std::string myName;
		uint myHash;
		SG_Model* myParent;
	};

	class SG_Model2
	{
	public:

		SC_Matrix44 myTransform;
	private:
		SC_GrowingArray<SC_Ref<SG_MeshInstance>> myMeshes;
		std::string myName;
	};
}