#include "SGraphics_Precompiled.h"
#include "SG_MaterialBase.h"

namespace Shift
{
	SG_MaterialBase::SG_MaterialBase()
		: myPSO(nullptr)
	{
	}


	SG_MaterialBase::~SG_MaterialBase()
	{
	}

	void SG_MaterialBase::CreatePSO(const SG_MaterialBaseDesc& aDesc)
	{
		SGraphicsPSODesc psoDesc;
		psoDesc.myDepthStateDesc.myDepthComparisonFunc = SR_ComparisonFunc_GreaterEqual;
		psoDesc.myDepthStateDesc.myWriteDepth = aDesc.myWriteDepth;
		psoDesc.myRasterizerStateDesc = aDesc.myRasterizerStateDesc;

		if (aDesc.myNumRenderTargets > 0)
		{
			psoDesc.myRenderTargetFormats.myNumColorFormats = static_cast<uint8>(aDesc.myNumRenderTargets);
			if (aDesc.myIsOpaque)
			{
				psoDesc.myRenderTargetFormats.myColorFormats[0] = SR_Format_RGBA8_Unorm; // Color
				psoDesc.myRenderTargetFormats.myColorFormats[1] = SR_Format_RG11B10_Float; // Normals
				psoDesc.myRenderTargetFormats.myColorFormats[2] = SR_Format_RGBA8_Unorm; // ARM
				psoDesc.myRenderTargetFormats.myColorFormats[3] = SR_Format_RGBA8_Unorm; // Emissive + Fog

				for (uint i = 0; i < 4; ++i)
				{
					psoDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].mySrcBlend = SR_BlendMode_One;
					psoDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].myDstBlend = SR_BlendMode_Zero;
					psoDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].mySrcBlendAlpha = SR_BlendMode_One;
					psoDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].myDstBlendAlpha = SR_BlendMode_Zero;
				}
			}
			else
			{
				psoDesc.myRenderTargetFormats.myColorFormats[0] = SR_Format_RGBA8_Unorm; // Color
				psoDesc.myBlendStateDesc.myRenderTagetBlendDescs[0].mySrcBlend = SR_BlendMode_One;
				psoDesc.myBlendStateDesc.myRenderTagetBlendDescs[0].myDstBlend = SR_BlendMode_Zero;
				psoDesc.myBlendStateDesc.myRenderTagetBlendDescs[0].mySrcBlendAlpha = SR_BlendMode_One;
				psoDesc.myBlendStateDesc.myRenderTagetBlendDescs[0].myDstBlendAlpha = SR_BlendMode_Zero;
			}
		}

		psoDesc.topology = aDesc.myTopology;
		psoDesc.myVertexShader = aDesc.myVertexShader;

		//if (aDesc.myShaderCode)
		//{
		//	CPixelShader* ps = new CPixelShader();
		//	if (CGraphicsDevice::GetDevice()->CreatePixelShader(aDesc.myShaderCode, ps))
		//		psoDesc.ps = ps;
		//	else
		//		delete ps;
		//}
		//else
		psoDesc.myPixelShader = aDesc.myPixelShader;
		myPSO = SR_ShaderStateCache::Get().GetPSO(psoDesc);
		if (!myPSO)
		{
			SC_ERROR_LOG("Couldn't create PSO!");
		}
		myHash += Shift::SC_Hash(psoDesc.myRenderTargetFormats.myNumColorFormats);
		myHash += Shift::SC_Hash(psoDesc.myRenderTargetFormats.myColorFormats[0]);
		myHash += Shift::SC_Hash(psoDesc.myRenderTargetFormats.myColorFormats[1]);
		myHash += Shift::SC_Hash(psoDesc.myRenderTargetFormats.myColorFormats[2]);
		myHash += Shift::SC_Hash(psoDesc.myRenderTargetFormats.myColorFormats[3]);
		myHash += Shift::SC_Hash(psoDesc.myRenderTargetFormats.myColorFormats[4]);
		myHash += Shift::SC_Hash(psoDesc.myRenderTargetFormats.myColorFormats[5]);
		myHash += Shift::SC_Hash(psoDesc.myRenderTargetFormats.myColorFormats[6]);
		myHash += Shift::SC_Hash(psoDesc.myRenderTargetFormats.myColorFormats[7]);
		myHash += Shift::SC_Hash(psoDesc.topology);
	}

	bool SG_MaterialBase::BindPSO() const
	{
		if (myPSO != nullptr && myPSO->IsLoaded())
		{
			if (SR_GraphicsContext::GetCurrent()->GetBoundPSO() != myPSO)
			{
				SR_GraphicsContext::GetCurrent()->SetTopology(myPSO->myProperties.topology);
				SR_GraphicsContext::GetCurrent()->BindGraphicsPSO(*myPSO);
			}
			return true;
		}
		return false;
	}
	uint SG_MaterialBase::GetHash() const
	{
		return myHash;
	}
}