#include "SGraphics_Precompiled.h"
#include "SG_ModelLoader.h"
#include "SG_Model.h"
#include "SG_MeshHeader.h"
#include "SG_MeshInstance.h"
#include "SG_Mesh.h"
#include "SG_Material.h"

#include <fstream>
#include <sstream>

namespace Shift
{
	const std::string SG_ModelLoader::ourModelsDirectory = "Models/";

	SG_ModelLoader::SG_ModelLoader()
	{
		std::filesystem::create_directory(ourModelsDirectory);
	}
	SG_ModelLoader::~SG_ModelLoader()
	{
	}
	void SG_ModelLoader::Export(const SG_Model& aModel)
	{
		std::stringstream stream;
		stream << ourModelsDirectory << std::hex << aModel.GetHash() << ourModelExtension;

		std::ofstream outputFile(stream.str(), std::ios::binary);
		if (outputFile.good())
		{
			FileHeader fileHeader;
			fileHeader.myNumMeshes = aModel.myMeshes.Count();
			fileHeader.myNameSize = (uint)aModel.myName.size();
			outputFile.write((char*)&fileHeader, sizeof(fileHeader));
			outputFile.write(aModel.myName.c_str(), fileHeader.myNameSize);

			uint largestAlloc = 0;
			char* data = nullptr;
			for (uint i = 0; i < fileHeader.myNumMeshes; ++i)
			{
				const SG_Model::ModelBuffer& mesh = aModel.myMeshes[i];


				uint fileSize = 0;
				fileSize += mesh.myBufferByteSize;
				fileSize += sizeof(MeshHeader);

				if (fileSize > largestAlloc)
				{
					delete[] data;
					data = new char[fileSize];
					largestAlloc = fileSize;
				}

				MeshHeader header;
				header.myAABB = mesh.myAABB;
				header.myVertexStructSize = mesh.myVertexSize;
				header.myVertexStartOffset = mesh.myVertexByteOffset;
				header.myNumVertices = mesh.myNumVertices;
				header.myIndexStartOffset = mesh.myIndexByteOffset;
				header.myNumIndices = mesh.myNumIndices;
				header.myMeshByteSize = mesh.myBufferByteSize;
				header.myMaterialHash = mesh.myMaterialHash;


				SC_Memcpy(&data[0], &header, sizeof(header));
				SC_Memcpy(&data[sizeof(header)], mesh.myBuffer, header.myMeshByteSize);

				outputFile.write(data, fileSize);
			}
			delete[] data;
		}
		outputFile.close();

	}
	bool SG_ModelLoader::Import(SG_Model& aModelOut)
	{
		std::stringstream stream;
		stream << ourModelsDirectory << std::hex << aModelOut.GetHash() << ourModelExtension;

		std::ifstream inputFile(stream.str(), std::ios::binary);
		if (inputFile.good())
		{
			// Optimize
			// Read the entire file once into memory.
			// Then process on-chip data for model-loading.

			uint64 currFilePos = 0;
			FileHeader fileHeader;
			char* headerPtr = reinterpret_cast<char*>(&fileHeader);
			inputFile.read(headerPtr, sizeof(fileHeader));
			currFilePos += sizeof(fileHeader);

			const uint nameByteSize = sizeof(char) * fileHeader.myNameSize;
			char* name = new char[fileHeader.myNameSize + 1];
			inputFile.seekg(currFilePos);
			inputFile.read(name, nameByteSize);
			currFilePos += nameByteSize;

			name[fileHeader.myNameSize] = '\0';
			aModelOut.myName = name;
			delete[] name;

			aModelOut.myMeshes.PreAllocate(fileHeader.myNumMeshes);

			uint largestAlloc = 0;
			char* data = nullptr;
			for (uint i = 0; i < fileHeader.myNumMeshes; ++i)
			{
				MeshHeader header;
				char* meshHeaderPtr = reinterpret_cast<char*>(&header);
				inputFile.seekg(currFilePos);
				inputFile.read(meshHeaderPtr, sizeof(header));
				currFilePos += sizeof(header);

				if (header.myMeshByteSize > largestAlloc)
				{
					delete[] data;
					data = new char[header.myMeshByteSize];
					largestAlloc = header.myMeshByteSize;
				}
				inputFile.seekg(currFilePos);
				inputFile.read(data, header.myMeshByteSize);
				currFilePos += header.myMeshByteSize;

				char* meshBuffer = new char[header.myMeshByteSize];

				SC_Memcpy(meshBuffer, data, header.myMeshByteSize);

				SG_Model::ModelBuffer& mesh = aModelOut.myMeshes.Add();
				mesh.myBuffer = (uint8*)meshBuffer;
				mesh.myAABB = header.myAABB;
				mesh.myVertexSize = header.myVertexStructSize;
				mesh.myVertexByteOffset = header.myVertexStartOffset;
				mesh.myNumVertices = header.myNumVertices;
				mesh.myIndexByteOffset = header.myIndexStartOffset;
				mesh.myNumIndices = header.myNumIndices;
				mesh.myBufferByteSize = header.myMeshByteSize;

				SVertexBufferDesc vbDesc;
				vbDesc.myVertexData = mesh.myBuffer;
				vbDesc.myNumVertices = mesh.myNumVertices;
				vbDesc.myStride = mesh.myVertexSize;
				SR_GraphicsDevice::GetDevice()->CreateVertexBuffer(vbDesc, &mesh.myVertexBuffer);

				SR_GraphicsDevice::GetDevice()->CreateIndexBuffer(reinterpret_cast<uint*>(mesh.myBuffer + mesh.myIndexByteOffset), mesh.myNumIndices, &mesh.myIndexBuffer);

				std::stringstream materialPath;
				materialPath << SG_Material::ourMaterialDirectory << std::hex << header.myMaterialHash << SG_Material::ourMaterialExtension;

				SG_MaterialDesc matDesc;
				matDesc.myFilename = materialPath.str();
				if (!std::string(matDesc.myFilename).empty())
				{
					SG_Material*& mat = aModelOut.myMaterials.Add();
					mat = new SG_Material();
					mat->Init(matDesc);
				}
			}
			delete[] data;

			return true;
		}

		inputFile.close();

		return false;
	}

	SC_Ref<SG_Model> SG_ModelLoader::Import2(const char* aFilePath)
	{
		std::ifstream inputFile(aFilePath, std::ios::binary);
		if (inputFile.is_open())
		{
			SC_Ref<SG_Model> model = new SG_Model();

			SG_MeshFileHeader fileHeader;
			char* fileHeaderPtr = (char*)(&fileHeader);
			inputFile.read(fileHeaderPtr, sizeof(fileHeader));

			std::string modelName;
			modelName.resize(fileHeader.myNameSize);
			inputFile.read(modelName.data(), fileHeader.myNameSize);

			char* vertexBufferData = nullptr;
			char* indexBufferData = nullptr;
			uint vertexDataBufferSize = 0;
			uint indexDataBufferSize = 0;

			model->myMeshes.PreAllocate(fileHeader.myNumMeshes);
			for (uint i = 0; i < fileHeader.myNumMeshes; ++i)
			{
				SG_MeshHeader meshHeader;
				char* meshHeaderPtr = (char*)(&meshHeader);
				inputFile.read(meshHeaderPtr, sizeof(meshHeader));

				char* meshNameBuffer = new char[meshHeader.myNameSize + 1];
				meshNameBuffer[meshHeader.myNameSize] = '\0';
				char* materialPathBuffer = new char[meshHeader.myMaterialPathSize + 1];
				materialPathBuffer[meshHeader.myMaterialPathSize] = '\0';

				// Allocate space for mesh data
				if (vertexDataBufferSize < meshHeader.myTotalVertexBufferSize)
				{
					vertexBufferData = new char[meshHeader.myTotalVertexBufferSize];
					vertexDataBufferSize = meshHeader.myTotalVertexBufferSize;
				}
				if (indexDataBufferSize < meshHeader.myTotalIndexBufferSize)
				{
					indexBufferData = new char[meshHeader.myTotalIndexBufferSize];
					indexDataBufferSize = meshHeader.myTotalIndexBufferSize;
				}

				// Read mesh name
				inputFile.read(meshNameBuffer, meshHeader.myNameSize);
				inputFile.read(materialPathBuffer, meshHeader.myMaterialPathSize);

				// Read mesh data
				inputFile.read(vertexBufferData, meshHeader.myTotalVertexBufferSize);
				inputFile.read(indexBufferData, meshHeader.myTotalIndexBufferSize);

				model->_myMeshes.Add(SG_MeshInstance::Create(meshHeader, vertexBufferData, indexBufferData, materialPathBuffer, meshNameBuffer));
				delete[] meshNameBuffer;
				delete[] materialPathBuffer;
			}

			inputFile.close();
			return model;
		}
		else
			return nullptr;
	}

	bool SG_ModelLoader::ReadModelName(const char* aFile, std::string& aOutName)
	{
		if (GetFileExtension(aFile) != "smf")
			return false;

		std::ifstream inputFile(aFile, std::ios::binary);
		if (inputFile.good())
		{
			uint64 currFilePos = 0;
			FileHeader fileHeader;
			char* headerPtr = reinterpret_cast<char*>(&fileHeader);
			inputFile.read(headerPtr, sizeof(fileHeader));
			currFilePos += sizeof(fileHeader);

			const uint nameByteSize = sizeof(char) * fileHeader.myNameSize;
			char* name = new char[fileHeader.myNameSize + 1];
			inputFile.seekg(currFilePos);
			inputFile.read(name, nameByteSize);
			currFilePos += nameByteSize;

			name[fileHeader.myNameSize] = '\0';
			aOutName = name;
			return true;
		}

		return false;
	}
	bool SG_ModelLoader::LoadModelFromFile(SG_Model& aModelOut)
	{
		aModelOut;
		return false;
	}
}