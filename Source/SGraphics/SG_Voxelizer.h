#pragma once
#include "VertexBuffer.h"
#include "SG_Camera.h"

namespace Shift
{
	struct SVoxelizerSettings
	{
		SC_Float3 myVoxelCenter;
		float myVoxelTextureSize;
		float myVoxelSize; // Voxel Size in meters
	};

	class SG_RenderData;
	class CGraphicsPSO;
	class CComputePSO;
	class SG_Model;
	class SG_Shadows;

	class CGraphicsVoxelizer
	{
	public: 
		CGraphicsVoxelizer();
		~CGraphicsVoxelizer();

		void Init();

		const SVoxelizerSettings& GetSettings() const;
		SVoxelizerSettings& GetSettings();

		void UpdateSettings(const SVoxelizerSettings& aSettings);
		void RenderVoxels(const SG_RenderData& aRenderData, SG_Shadows* aShadowRenderer);
		void DebugDraw(const SG_RenderData& aRenderData);
		void BindVoxelScene();

		void AddGUI();

	private:
		void RelightVoxels(const SG_RenderData& aRenderData, SG_Shadows* aShadowRenderer);

		SG_Camera myCamera;
		SVoxelizerSettings mySettings;
		CVertexBuffer myInstanceBuffer;

		// Shaders
		CGraphicsPSO* myVoxelizeShader;
		CGraphicsPSO* myDebugVoxels;
		CComputePSO* myClearShader;
		CComputePSO* myRelightShader;
		CComputePSO* myConvertTextureShader;

		// Textures
		//CTexture myVoxelizedScene;
		//CTexture myVoxelizedScene_Float4;
		//CTexture myVoxelizedNormals;
		//CTexture myVoidRT;
		//CTexture myDebugRT;
	};
}
