#include "SGraphics_Precompiled.h"
#include "SG_PostEffects.h"
#include "SG_View.h"

namespace Shift
{
	static SG_PostEffects::Shared* ourSharedCache = nullptr;

	uint locDispatchSize(uint aSize, uint aNumElements)
	{
		uint dispatchSize = aNumElements / aSize;
		dispatchSize += aNumElements % aSize > 0 ? 1 : 0;
		return dispatchSize;
	}

	uint ReductionSize = 16;

	SG_PostEffects::SG_PostEffects()
	{
	}

	SG_PostEffects::~SG_PostEffects()
	{
	}

	void SG_PostEffects::Init(SC_Vector2f aResolution)
	{
		if (ourSharedCache == nullptr)
		{
			ourSharedCache = new SG_PostEffects::Shared();
			ourSharedCache->Load();
		}


		uint width = (uint)aResolution.x;
		uint height = (uint)aResolution.y;

		while (width > 1 || height > 1)
		{
			width = locDispatchSize(ReductionSize, width);
			height = locDispatchSize(ReductionSize, height);

			SR_TextureResource& resource = myLuminanceReductionTargets.Add();
			resource.myCreateTexture = true;
			resource.myCreateRWTexture = true;
			resource.myTextureFormat = SR_Format_R32_Float;
			resource.Update(SC_Vector2f((float)width, (float)height), "Luminance Reduction");
		}
	}
	void SG_PostEffects::Render(SG_View* aView, SR_Texture* aInputTexture)
	{
		aView;
		aInputTexture;
		// Do all post processing here

		// Copy output texture into View -> PostProcessedOutput
	}

	void SG_PostEffects::CalculateLuminance(SG_View* aView, SR_Texture* aInputTexture)
	{
		SE_PROFILER_FUNCTION_TAGGED("Luminance");
		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();

		ctx->BindTextureRW(myLuminanceReductionTargets[0].myTextureRW, 0);
		ctx->BindTexture(aInputTexture, 0);
		ctx->BindComputePSO(*ourSharedCache->myInitialLumReductionShader);

		uint dispatchX = (uint)myLuminanceReductionTargets[0].myTextureBuffer->GetProperties().myWidth;
		uint dispatchY = (uint)myLuminanceReductionTargets[0].myTextureBuffer->GetProperties().myHeight;
		ctx->Dispatch(dispatchX, dispatchY, 1);

		for (uint i = 1, count = myLuminanceReductionTargets.Count(); i < count; ++i)
		{
			if (i == myLuminanceReductionTargets.Count() - 1)
			{
				struct Constants
				{
					float TimeDelta;
					bool EnableAdaptation;
				} constants;
				constants.TimeDelta = SC_Timer::GetGlobalDeltaTime();
				constants.EnableAdaptation = true;

				ctx->BindConstantBuffer(&constants, sizeof(constants), 0);
				ctx->BindComputePSO(*ourSharedCache->myFinalLumReductionShader);
			}
			else
				ctx->BindComputePSO(*ourSharedCache->myLumReductionShader);

			ctx->BindTextureRW(myLuminanceReductionTargets[i].myTextureRW, 0);
			ctx->BindTexture(myLuminanceReductionTargets[i - 1].myTexture, 0);

			dispatchX = (uint)myLuminanceReductionTargets[i].myTextureBuffer->GetProperties().myWidth;
			dispatchY = (uint)myLuminanceReductionTargets[i].myTextureBuffer->GetProperties().myHeight;
			ctx->Dispatch(dispatchX, dispatchY, 1);

			if (i == myLuminanceReductionTargets.Count() - 1)
				aView->myFrameResources.myAvgLuminance = myLuminanceReductionTargets[i];
		}
	}

	void SG_PostEffects::Bloom(SR_Texture* /*aTextureInOut*/)
	{
	}

	void SG_PostEffects::Tonemap(SR_Texture* /*aFramebufferTexture*/, SR_Texture* /*aBloomTexture*/)
	{
	}

	SG_PostEffects::Shared::Shared()
		: myTonemapPSO(nullptr)
		, myBloomLuminancePSO(nullptr)
		, myBloomBlurPSO(nullptr)
		, myCopyPSO(nullptr)
		, myAddPSO(nullptr)
	{
	}

	SG_PostEffects::Shared::~Shared()
	{
		delete myTonemapPSO;
		delete myBloomLuminancePSO;
		delete myBloomBlurPSO;
		delete myCopyPSO;
		delete myAddPSO;
	}

	void SG_PostEffects::Shared::Load()
	{
		{
			SComputePSODesc csDesc;
			csDesc.myComputeShader = "Shaders/InitLuminanceReduction.hlsl";
			myInitialLumReductionShader = SR_ShaderStateCache::Get().GetPSO(csDesc);
		}
		{
			SComputePSODesc csDesc;
			csDesc.myComputeShader = "Shaders/LuminanceReduction.hlsl";
			myLumReductionShader = SR_ShaderStateCache::Get().GetPSO(csDesc);
		}
		{
			SComputePSODesc csDesc;
			csDesc.myComputeShader = "Shaders/FinalLuminanceReduction.hlsl";
			myFinalLumReductionShader = SR_ShaderStateCache::Get().GetPSO(csDesc);
		}
		//{
		//	myBloomLuminancePSO = new CComputePSO();
		//
		//	SComputePSODesc csDesc;
		//	csDesc.cs = device->CreateComputeShader("Shaders/Bloom_Luminance.hlsl", "main");
		//	device->CreateComputePSO(csDesc, myBloomLuminancePSO);
		//}
		//{
		//	myBloomBlurPSO = new CComputePSO();
		//
		//	SComputePSODesc csDesc;
		//	csDesc.cs = device->CreateComputeShader("Shaders/Bloom_Blur.hlsl", "main");
		//	device->CreateComputePSO(csDesc, myBloomBlurPSO);
		//}
		//{
		//	myCopyPSO = new CComputePSO();
		//
		//	SComputePSODesc csDesc;
		//	csDesc.cs = device->CreateComputeShader("Shaders/Copy.hlsl", "main");
		//	device->CreateComputePSO(csDesc, myCopyPSO);
		//}
		//{
		//	myAddPSO = new CComputePSO();
		//
		//	SComputePSODesc csDesc;
		//	csDesc.cs = device->CreateComputeShader("Shaders/Bloom_Add.hlsl", "main");
		//	device->CreateComputePSO(csDesc, myAddPSO);
		//}
	}
}