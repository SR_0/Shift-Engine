#pragma once

namespace Shift
{
	enum ELightType
	{
		Directional = 0,
		Point,
		Spot,
	};

	class SG_Light
	{
	public:
		SG_Light() = delete;
		virtual ~SG_Light() {}

		ELightType myType;
	protected:
		SG_Light(const ELightType& aType) : myType(aType) {}
	};
}