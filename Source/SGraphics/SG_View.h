#pragma once

#include "SC_RefCounted.h"

#include "SG_RenderData.h"
#include "SG_Camera.h"
#include "SG_FrameResources.h"
#include "SG_ViewSettings.h"
#include "SG_PostEffects.h"

#include "VertexBuffer.h"
#include "IndexBuffer.h"

namespace Shift
{
	class SG_World;
	struct SViewPrivate;
	class CComputePSO;
	class CGraphicsPSO;
	class SG_Shadows;

	enum EViewDataAccess
	{
		SViewData_Read,
		SViewData_Write,
		SViewData_Idle,

		SViewData_Count
	};

	class SG_View : public SC_RefCounted
	{
		friend class CRenderer;
	public:
		SG_View(SG_World* aWorld);
		~SG_View();

		void Prepare();
		void Render();

		void SetWorld(SG_World* aWorld);
		void SetCamera(const SG_Camera& aCamera);

		SG_RenderData& GetPrepareData();
		const SG_RenderData& GetRenderData() const;
		SG_RenderData& GetRenderData_Mutable();

		SR_Texture* GetLastFinishedFrame();
		SR_Texture* GetShadowTexture();

		SG_FrameResources myFrameResources;
		SG_World* myWorld;
	private:

		void StartPrepare();
		void EndPrepare();

		void StartRender();
		void EndRender();

		struct SSAO
		{
			SC_Ref<SR_Texture> mySSAORand;
			CComputePSO* mySSAO;
			CComputePSO* mySSAOBlur;
			SC_Ref<SR_ShaderState> myShader;

			float mySSAOSamplingRadius;
			float mySSAOScale;
			float mySSAOBias;
			float mySSAOIntensity;

			bool myEnableSSAO;
		};

		struct Lighting
		{
			CComputePSO* myLightingShader; 
			SC_Ref<SR_ShaderState> myShader;
		};

		struct PostFX
		{
			CVertexBuffer myFullscreenQuadBuffer;
			CIndexBuffer myFullscreenQuadIBuffer;

			CComputePSO* myTonemapPSO;
			CComputePSO* myBloomLuminancePSO;
			CComputePSO* myBloomBlurPSO;
			CComputePSO* myCopyPSO;
			CComputePSO* myAddPSO;
			CGraphicsPSO* myAAPSO;
			SC_Ref<SR_ShaderState> myFXAAShader;
			SC_Ref<SR_ShaderState> myTonemapShader;

			float myBloomThreshold;
			float myBloomIntensity;

			float myExposure;


			bool myUseBloom;
			bool myUseFXAA;
		};

	private:

		void Initialize();

		void UpdateViewSettings();

		typedef void (SG_View::* RenderFunc)();
		void PostRenderTask(SC_Ref<SR_Waitable>& aEventOut, RenderFunc aTask, bool aEnabled = true);
		void PostComputeTask(SC_Ref<SR_Waitable>& aEventOut, RenderFunc aTask, bool aEnabled = true);
		void PostCopyTask(SC_Ref<SR_Waitable>& aEventOut, RenderFunc aTask, bool aEnabled = true);

		void Render_StartFrame();
		void Render_SkyProbes();
		void Render_Depth();
		void Render_Shadows();
		void Render_GBuffer();
		void Render_AmbientOcclusion();
		void Render_Lights();
		void Render_Forward();
		void Render_PostFX();
		void Render_UI();
		void Render_Compose();

		void RenderSky();
		
		void RenderDebugPrimitives();

		SG_Shadows* myShadows;

		SG_RenderData myViewData[SViewData_Count];
		uint myCurrentViewDataIndex;

		SC_GrowingArray<SC_Ref<SR_WaitEvent>> myEventsThisFrame;
		SSAO mySSAO;
		Lighting myLighting;
		PostFX myPostFX;
		SG_ViewSettings mySettings;
		SG_Camera myCamera;
		SG_PostEffects myPostEffects;

		SViewPrivate* myPrivate;
		volatile uint64 myCurrentFrameIndex;

		////////////////
		CVertexBuffer myInstanceBuffer;
		SR_Buffer* myTimestampBuffer;

	};

}