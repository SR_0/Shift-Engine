#pragma once
#include "SC_AABB.h"

namespace Shift
{
	struct SG_MeshHeader
	{
		SC_AABB myAABB;
		uint myMaterialPathSize;
		uint myNameSize;
		uint myVertexStrideSize;
		uint myNumVertices;
		uint myNumIndices;
		uint myTotalVertexBufferSize;
		uint myTotalIndexBufferSize;
	};
	struct SG_MeshFileHeader
	{
		uint myNumMeshes;
		uint myNameSize;
	};
}