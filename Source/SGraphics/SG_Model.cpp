#include "SGraphics_Precompiled.h"
#include "SG_Model.h"

#include "SG_Material.h"
#include "SG_ModelLoader.h"
#include "SG_AssimpImporter.h"
#include "SG_Camera.h"


namespace Shift
{
	SG_Model::SG_Model()
		: myParent(nullptr)
	{
	}
	SG_Model::SG_Model(const char* aFilePath, bool aShouldInvert)
	{
		FromFile(aFilePath, aShouldInvert);
	}

	SG_Model::~SG_Model()
	{
		for (SG_Material* material : myMaterials)
			delete material;
	}

	SC_Ref<SG_Model> SG_Model::Create(const char* aFilePath)
	{
		aFilePath;
		static SG_ModelLoader locLoader;

		SC_Ref<SG_Model> model = new SG_Model();

		return model;
	}

	void SG_Model::FromFile(const char* aFilePath, bool aShouldInvert)
	{
		std::string file(aFilePath);
		myHash = Shift::SC_Hash(file.c_str(), (uint)file.size());

		SG_ModelLoader exporter;

		SC_Stopwatch timer;
		timer.Start();
		if (!exporter.Import(*this))
		{
			CAssimpImporter importer;
			importer.Import(*this, aFilePath, aShouldInvert);

			myName = GetFilename(aFilePath);

			exporter.Export(*this);
		}
		
		SC_LOG("Loading model [%s] took %f ms.", aFilePath, timer.Stop());
	}

	void SG_Model::Render()
	{
		// Process Children First

		for (auto& child : myChildren)
		{
			child.Render();
		}

		// Process Myself

	}

	void SG_Model::DebugDrawAABB()
	{
		SC_Vector3f corners[8];
		for (auto& mesh : myMeshes)
		{
			SC_AABB aabb = mesh.myAABB;
			aabb.myMin = aabb.myMin * myTransform;
			aabb.myMax = aabb.myMax * myTransform;

			// Near face
			corners[0] = { aabb.myMin };
			corners[1] = { aabb.myMin.x, aabb.myMax.y, aabb.myMin.z };
			corners[2] = { aabb.myMax.x, aabb.myMax.y, aabb.myMin.z };
			corners[3] = { aabb.myMax.x, aabb.myMin.y, aabb.myMin.z };

			// Far face
			corners[4] = { aabb.myMin.x, aabb.myMin.y, aabb.myMax.z };
			corners[5] = { aabb.myMin.x, aabb.myMax.y, aabb.myMax.z };
			corners[6] = { aabb.myMax };
			corners[7] = { aabb.myMax.x, aabb.myMin.y, aabb.myMax.z };

			// Near Face
			SG_PRIMITIVE_DRAW_LINE3D_COLORED(corners[0], corners[1], SC_Color(1.0f, 1.0f, 0.0f, 1.0f));
			SG_PRIMITIVE_DRAW_LINE3D_COLORED(corners[1], corners[2], SC_Color(1.0f, 1.0f, 0.0f, 1.0f));
			SG_PRIMITIVE_DRAW_LINE3D_COLORED(corners[2], corners[3], SC_Color(1.0f, 1.0f, 0.0f, 1.0f));
			SG_PRIMITIVE_DRAW_LINE3D_COLORED(corners[3], corners[0], SC_Color(1.0f, 1.0f, 0.0f, 1.0f));

			// Far face
			SG_PRIMITIVE_DRAW_LINE3D_COLORED(corners[4], corners[5], SC_Color(1.0f, 1.0f, 0.0f, 1.0f));
			SG_PRIMITIVE_DRAW_LINE3D_COLORED(corners[5], corners[6], SC_Color(1.0f, 1.0f, 0.0f, 1.0f));
			SG_PRIMITIVE_DRAW_LINE3D_COLORED(corners[6], corners[7], SC_Color(1.0f, 1.0f, 0.0f, 1.0f));
			SG_PRIMITIVE_DRAW_LINE3D_COLORED(corners[7], corners[4], SC_Color(1.0f, 1.0f, 0.0f, 1.0f));

			// Connecting near and far
			SG_PRIMITIVE_DRAW_LINE3D_COLORED(corners[0], corners[4], SC_Color(1.0f, 1.0f, 0.0f, 1.0f));
			SG_PRIMITIVE_DRAW_LINE3D_COLORED(corners[1], corners[5], SC_Color(1.0f, 1.0f, 0.0f, 1.0f));
			SG_PRIMITIVE_DRAW_LINE3D_COLORED(corners[2], corners[6], SC_Color(1.0f, 1.0f, 0.0f, 1.0f));
			SG_PRIMITIVE_DRAW_LINE3D_COLORED(corners[3], corners[7], SC_Color(1.0f, 1.0f, 0.0f, 1.0f));
		}
	}

	bool SG_Model::Cull(const SG_Camera& aCamera)
	{
		bool isCompletelyInactive = true;
		for (auto& mesh : myMeshes)
		{
			SC_AABB aabb = mesh.myAABB;
			aabb.myMin = aabb.myMin * myTransform;
			aabb.myMax = aabb.myMax * myTransform;

			if (aCamera.GetFrustum().Intersects(aabb))
			{
				mesh.myIsActive = true;
				isCompletelyInactive = false;
			}
			else
				mesh.myIsActive = false;
		}

		return !isCompletelyInactive;
	}

	uint SG_Model::GetHash() const
	{
		return myHash;
	}
}