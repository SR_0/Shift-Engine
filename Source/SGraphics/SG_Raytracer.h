#pragma once

namespace Shift
{
	class SG_RenderData;
	class SG_Raytracer
	{
	public: 
		SG_Raytracer();
		~SG_Raytracer();

		void Init();
		void BuildStructure(SG_RenderData& aPrepareData);
		void Light();

	private:
		// Shaders to run tracing
		// References to scene/render-data
		// Trace settings
	};
}
