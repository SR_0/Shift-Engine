#include "SGraphics_Precompiled.h"
#include "SG_RenderQueue.h"

namespace Shift
{
	SG_RenderQueueBase::SG_RenderQueueBase()
		: myIsPrepared(false)
	{
	}

	SG_RenderQueueBase::~SG_RenderQueueBase()
	{
	}

	void SG_RenderQueueBase::Prepare()
	{
		SG_RenderQueueItem* items = myQueueItems.GetBuffer();
		uint numItemsToProcess = myQueueItems.Count();

		while (numItemsToProcess)
		{
			SG_RenderQueueItem& firstItem = *items;
			uint instanceCount = 1;

			if (firstItem.myVertexBuffer != nullptr &&
				firstItem.myIndexBuffer != nullptr)
			{
				for (uint i = 1; i < numItemsToProcess; ++i)
				{
					SG_RenderQueueItem& item = items[i];
					if (/*item.myRasterizeState != firstItem.myRasterizerState ||*/
						item.myVertexBuffer != firstItem.myVertexBuffer ||
						item.myIndexBuffer != firstItem.myIndexBuffer ||
						item.myShader != firstItem.myShader ||
						item.myLodIndex != firstItem.myLodIndex ||
						instanceCount == SC_UINT16_MAX)
						break;

					++instanceCount;
				}
			}

			firstItem.myNumInstances = instanceCount;
			items += instanceCount;
			numItemsToProcess -= instanceCount;
		}

		myIsPrepared = true;
	}

	void SG_RenderQueueBase::Render()
	{
		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();

		for (SG_RenderQueueItem& renderItem : myQueueItems)
		{

			if (renderItem.myVertexBuffer != nullptr &&
				renderItem.myIndexBuffer != nullptr)
			{
				// ctx->SetBlendState();
				// ctx->SetRasterizerState();
				// ctx->SetDepthStencilState();

				// ctx->SetBufferRef(); // Instance specific data

				ctx->SetShaderState(renderItem.myShader);

				ctx->SetVertexBuffer(0, &renderItem.myVertexBuffer, 1);
				ctx->SetIndexBuffer(renderItem.myIndexBuffer);

				ctx->SetTopology(SR_Topology_TriangleList);
				ctx->DrawIndexedInstanced(renderItem.myNumIndices, renderItem.myNumInstances, 0, 0, 0);
			}
			// Change states when relevant
			//ctx->BindGraphicsPSO(*renderItem.myShader);

			//if (nextItem.Material != currentItem.Material)
			//if (nextItem.DepthState != currentItem.DepthState)
			//if (nextItem.RasterState != currentItem.RasterState)
			//if (nextItem.Mesh != currentItem.Mesh)
			//{
			//	ctx->UpdateBufferData(InstanceBufferData);
			//	ctx->BindVertexBuffer();
			//	ctx->BindIndexBuffer();
			//
			//	ctx->BindGraphicsPSO();
			//
			//	ctx->DrawIndexedInstanced(0,0,0,0,0); // Draw all instances of this setup
			//}
			// Bind object specifics for each instance
		}
	}

	void SG_RenderQueueBase::Clear()
	{
		myQueueItems.RemoveAll();
		myIsPrepared = false;
	}
}