#include "SGraphics_Precompiled.h"
#include "SG_RenderData.h"
#include "SG_Material.h"
#include "SG_Model.h"

namespace Shift
{
	SG_RenderData::SG_RenderData()
		: myDeltaTime_Render(0.f)
		, myDeltaTime_Logic(0.f)
		, myFramerate_Render(0)
		, myFramerate_Logic(0)
		, myIsUpdated(false)
	{
	}

	SG_RenderData::SG_RenderData(const SG_RenderData& aRenderData)
	{
		*this = aRenderData;
	}

	void SG_RenderData::operator=(const SG_RenderData& aRenderData)
	{
		myOpaqeObjects = aRenderData.myOpaqeObjects;
		myTransparentObjects = aRenderData.myTransparentObjects;
		mySky = aRenderData.mySky;
		myDeltaTime_Render = aRenderData.myDeltaTime_Render;
		myDeltaTime_Logic = aRenderData.myDeltaTime_Logic;
		myFramerate_Render = aRenderData.myFramerate_Render;
		myFramerate_Logic = aRenderData.myFramerate_Logic;

		myRenderShadowsEvent = aRenderData.myRenderShadowsEvent;
		myRenderSkyProbeEvent = aRenderData.myRenderSkyProbeEvent;
		myRenderGBufferEvent = aRenderData.myRenderGBufferEvent;
		myRenderSSAOEvent = aRenderData.myRenderSSAOEvent;
		myRenderLightsEvent = aRenderData.myRenderLightsEvent;
		myRenderForwardEvent = aRenderData.myRenderForwardEvent;
		myRenderPostFXEvent = aRenderData.myRenderPostFXEvent;
		myRenderUIEvent = aRenderData.myRenderUIEvent;
		myRenderComposeEvent = aRenderData.myRenderComposeEvent;
		myEvents = aRenderData.myEvents;
	}

	SG_RenderData::~SG_RenderData()
	{
	}

	void SG_RenderData::Clear()
	{
		myQueues.Clear();

		for (uint i = 0; i < SG_Shadows::ourNumShadowCascades; ++i)
			myShadowLightOccluders[i].RemoveAll(); 

		myOpaqeObjects.RemoveAll();
		myTransparentObjects.RemoveAll();
		myDeltaTime_Render = (0.f);
		myDeltaTime_Logic = (0.f);
		myFramerate_Render = (0);
		myFramerate_Logic = (0);
		mySky = SG_PrimitiveRenderObject();

		mySetViewRefsEvent.Reset();
		myRenderShadowsEvent.Reset();
		myRenderSkyProbeEvent.Reset();
		myRenderGBufferEvent.Reset();
		myRenderSSAOEvent.Reset();
		myRenderLightsEvent.Reset();
		myRenderForwardEvent.Reset();
		myRenderPostFXEvent.Reset();
		myRenderUIEvent.Reset();
		myRenderComposeEvent.Reset();
		myEvents.RemoveAll();

		myPrepareFuture.Reset();
		myPrepareSettings.Reset();
		myPrepareReady.Reset();

		myIsUpdated = false;
	}

	bool SG_RenderObject::operator<(const SG_RenderObject& aOther)
	{
		if (myMaterial->GetHash() < aOther.myMaterial->GetHash() ||
			(myMaterial->GetHash() == aOther.myMaterial->GetHash()) && myModel->GetHash() < aOther.myModel->GetHash())
			return true;

		return false;
	}
}