#pragma once
#include "SC_Resource.h"

namespace Shift
{
	class SG_Mesh;
	class SG_Camera;
	class SG_Material2;
	struct SG_MeshHeader;
	class SG_MeshInstance : public SC_Resource
	{
	public:
		SG_MeshInstance(SG_Mesh* aMesh);
		~SG_MeshInstance();

		static SC_Ref<SG_MeshInstance> Create(const SG_MeshHeader& aHeader, void* aVertexData, void* aIndexData, const char* aMaterialPath = nullptr, const char* aName = nullptr);

		void SetTransform(const SC_Matrix44& aTransform);
		const SC_Matrix44& GetTransform() const;
		const SC_AABB& GetBoundingBox() const;

		bool Cull(const SG_Camera& aCamera);

		SC_Ref<SG_Mesh> myMeshTemplate;
	private:
		void CalcBoundingBox();

		SC_Ref<SG_Material2> myMaterial;
		SC_Matrix44 myTransform;
		SC_AABB myBoundingBox;

	};
}
