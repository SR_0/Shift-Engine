#include "SGraphics_Precompiled.h"
#include "SG_World.h"
#include "SG_View.h"
#include "SG_SceneGraph.h"
#include "SG_ViewManager.h"
#include "SG_Terrain.h"
#include "SG_MeshInstance.h"
#include "SG_Mesh.h"

#include "SR_GraphicsDevice.h"

namespace Shift
{
	SG_World::SG_World()
		: mySkyWindowOpen(true)
		, myQuadTree({ 2048.f, 2048.f}, 32)
		, myDebugDrawAABBs(false)
		, myLockCullingCamera(false)
		, myWasCameraLocked(false)
	{
	}


	SG_World::~SG_World()
	{
		for (uint i = 0; i < myViews.Count(); ++i)
		{
			if (SG_ViewManager* vm = SC_EngineInterface::GetViewManager())
				vm->RemoveView(myViews[i]);
		}
	}

	bool SG_World::Init()
	{
		mySphere.FromFile("Sphere.obj", true);

		SGraphicsPSODesc desc;
		SR_GraphicsDevice* device = SR_GraphicsDevice::GetDevice();
		desc.myVertexShader = "Shaders/SkyVS.hlsl";
		desc.myPixelShader = "Shaders/SkyPS.hlsl";
		desc.myRenderTargetFormats.myNumColorFormats = 1;
		desc.myRenderTargetFormats.myColorFormats[0] = SR_Format_RGBA8_Unorm;
		desc.topology = Shift::SR_Topology_TriangleList;

		mySkyPSO = SR_ShaderStateCache::Get().GetPSO(desc);

		_mySkyDiffuseTexture = device->GetCreateTexture("skyDiffuseHDR.dds");
		_mySkySpecularTexture = device->GetCreateTexture("skySpecularHDR.dds");
		_mySkyBRDFTexture = device->GetCreateTexture("skyBrdf.dds");


		//if (!Init_Objects())
		//	return false;

		mySun.SetDirection({0.1f, -1.f, 0.1f});
		mySky = new SG_DynamicSky();
		if (mySky)
			mySky->Init();

		myTerrain = new SG_Terrain();

		SG_TerrainInitData terrainInitData;
		terrainInitData.myResolution = { 1024.f, 1024.f };

		myTerrain->Init(terrainInitData);

		return true;
	}

	SC_Ref<SG_View> SG_World::CreateView()
	{
		SC_Ref<SG_View> newView = myViews.Add(new SG_View(this));
		SC_EngineInterface::GetViewManager()->AddView(newView);
		return newView;
	}

	bool SG_World::PrepareView(SG_View* aView)
	{
		if (!aView)
			return false;

		SG_RenderData& prepareData = aView->GetPrepareData();

		if (myLockCullingCamera)
		{
			if (!myWasCameraLocked)
			{
				myWasCameraLocked = true;
				myLockedCullingCamera = prepareData.myMainCamera;
			}
		}
		else
			myWasCameraLocked = false;

		const SG_Camera& camera = (myLockCullingCamera) ? myLockedCullingCamera : prepareData.myMainCamera;


		// Do visibility-checks, sorting and other preparation.

		// Fill the prepareData with the data needed to render this particular view.
		if (mySkyPSO && mySkyPSO->IsLoaded())
		{
			prepareData.mySky.myPSO = mySkyPSO;
			prepareData.mySky.myVertexBuffer = &mySphere.myMeshes[0].myVertexBuffer;
			prepareData.mySky.myIndexBuffer = &mySphere.myMeshes[0].myIndexBuffer;
			prepareData.mySky.myTransform.SetPosition(camera.GetPosition());
			prepareData.mySky.myTransform.Scale({ 10.f, 10.f, 10.f });
			prepareData.mySky.myTexture = _mySkyDiffuseTexture;
			prepareData.mySky.myTexture2 = _mySkySpecularTexture;
			prepareData.mySky.myTexture3 = _mySkyBRDFTexture;

			prepareData.mySun = mySun;

			// Add other data to send for rendering
			// Eg. renderable GameObjects, lights,
			// environmental data...
		}

		for (SG_Model* model : myModels)
		{
			if (model->Cull(camera))
			{
				SG_RenderObject& renderObj = prepareData.myOpaqeObjects.Add();
				renderObj.myTransform = model->myTransform;
				renderObj.myTransform.Transpose();
				renderObj.myModel = model;

				for (uint cascade = 0; cascade < SG_Shadows::ourNumShadowCascades; ++cascade) // Shoulld cull with its own camera.
				{
					prepareData.myShadowLightOccluders[cascade].Add(renderObj);
				}

			}

			if (myDebugDrawAABBs)
			{
				if (model->Cull(camera))
					model->DebugDrawAABB();
			}

			
		}

		for (SG_MeshInstance* mesh : myMeshes)
		{
			if (mesh->Cull(camera))
			{
				SG_RenderQueueItem& renderItem = prepareData.myQueues.myGBuffers->myQueueItems.Add();
				renderItem.myVertexBuffer = mesh->myMeshTemplate->GetVertexBuffer();
				renderItem.myIndexBuffer = mesh->myMeshTemplate->GetIndexBuffer();
				renderItem.myNumIndices = renderItem.myIndexBuffer->GetProperties().mySize / sizeof(uint);

				const SC_Matrix44& meshTransform = mesh->GetTransform();
				const SC_Matrix44& cameraTransform = camera.GetTransform();

				float distanceToCamera = (meshTransform.GetPosition() - cameraTransform.GetPosition()).Length();
				renderItem.mySortDistance = distanceToCamera;
				renderItem.myTransform = meshTransform;
				renderItem.myShader = mesh->myMeshTemplate->myShaders[SG_RenderType_GBuffer];
			}

			for (uint cascade = 0; cascade < SG_Shadows::ourNumShadowCascades; ++cascade)
			{
				if (mesh->Cull(prepareData.myShadowLightCameras[cascade]))
				{
					SG_RenderObject& renderObj = prepareData.myShadowLightOccluders[cascade].Add();
					renderObj.myMeshInstance = mesh;
				}
			}
		}

		return true;
	}

	void SG_World::AddModel(SG_Model* aModel)
	{
		myModels.Add(aModel);
	}

	void SG_World::AddMeshes(SG_Model* aModel)
	{
		for (SG_MeshInstance* mesh : aModel->_myMeshes)
			myMeshes.Add(mesh);
	}

	void SG_World::AddMesh(SG_MeshInstance* aMesh)
	{
		myMeshes.Add(aMesh);
	}

	void SG_World::RemoveModel(SG_Model* aModel)
	{
		myModels.RemoveCyclic(aModel);
	}

	void SG_World::AddLight()
	{
	}

	void SG_World::Update()
	{
		mySky->Update(0.f);
		// Update player
		// Stream GameObjects
		// Update Terrain

		mySun.SetDirection(-(mySky->GetToSunDirection()));
	}
}