#include "SGraphics_Precompiled.h"
#include "SG_Material.h"
#include "SG_MaterialFactory.h"

#include <sstream>
namespace Shift
{
	const std::string SG_Material::ourMaterialDirectory = "Materials/";

	SG_Material::SG_Material()
		: myColor(0.5f, 0.5f, 0.5f)
		, myARM(1.0f, 0.5f, 0.0f)
		, myInitialized(false)
	{
	}


	SG_Material::~SG_Material()
	{
		if (myInitialized)
		{
			SG_MaterialFactory::RemoveCacheRef(myHash);
		}
	}
	void SG_Material::Init(const SG_MaterialDesc& aDesc)
	{
		SG_MaterialFactory::GetOrCreateMaterialFromCache(aDesc, this);
	}

	bool SG_Material::Bind()
	{

		if (!myBase.BindPSO())
			return false;
		for (uint i = 0; i < myTextures.Count(); ++i)
		{
			SR_GraphicsContext::GetCurrent()->BindTexture(myTextures[i], i);
		}
		return true;
	}
	bool SG_Material::BindDepthTest()
	{
		myBaseDepthTest.BindPSO();
		return true;
	}
	void SG_Material::SetColor(SC_Float3 aColor)
	{
		myColor = aColor;
	}
	void SG_Material::SetRoughness(float aRoughness)
	{
		myARM.y = aRoughness;
	}
	void SG_Material::SetMetallic(float aMetallic)
	{
		myARM.z = aMetallic;
	}
	uint SG_Material::GetHash() const
	{
		return myHash;
	}
	SC_GrowingArray<SC_Ref<SR_Texture>>& SG_Material::GetTextures()
	{
		return myTextures;
	}
	void SG_Material::Init_Internal(const SG_MaterialDesc& aDesc)
	{
		myProperties = aDesc;
		SC_Hash();

		std::stringstream stream;
		stream << SG_Material::ourMaterialDirectory << std::hex << GetHash() << SG_Material::ourMaterialExtension;
		std::ofstream outputFile(stream.str());

		if (outputFile.good())
		{
			Json material;
			material["IsOpaque"] = aDesc.myIsOpaque;
			material["PixelShader"] = aDesc.myPixelShader;
			material["VertexShader"] = aDesc.myVertexShader;
			material["Wireframe"] = aDesc.myWireframe;
			for (size_t i = 0; i < aDesc.myTextures.size(); ++i)
			{
				material["Textures"][i] = aDesc.myTextures[i];
			}

			outputFile << material;
		}
		outputFile.close();

		SG_MaterialBaseDesc baseDesc;
		baseDesc.myBlendState = EBlendState::Disabled;
		baseDesc.myIsOpaque = aDesc.myIsOpaque;
		baseDesc.myPixelShader = aDesc.myPixelShader;
		baseDesc.myVertexShader = aDesc.myVertexShader;
		baseDesc.myTopology = SR_Topology_TriangleList;
		baseDesc.myNumRenderTargets = (aDesc.myIsOpaque) ? 4 : 1;
		baseDesc.myWriteDepth = true;

		SG_MaterialBaseDesc baseDepthDesc(baseDesc);
		baseDepthDesc.myVertexShader = "Shaders/Object_DepthTestVS.hlsl";
		baseDepthDesc.myPixelShader = "Shaders/Object_DepthTest.hlsl";
		baseDepthDesc.myNumRenderTargets = 0;
		baseDepthDesc.myRasterizerStateDesc.mySlopedScaleDepthBias = -2.0f;
		baseDepthDesc.myRasterizerStateDesc.myDepthBias = -100;
		baseDepthDesc.myRasterizerStateDesc.myEnableDepthClip = false;

		myBase.CreatePSO(baseDesc);
		myBaseDepthTest.CreatePSO(baseDepthDesc);

		myTextures.PreAllocate((uint)aDesc.myTextures.size());

		for (auto& tex : aDesc.myTextures)
		{
			myTextures.Add(SR_GraphicsDevice::GetDevice()->GetCreateTexture(tex.c_str()));
		}
		myInitialized = true;
	}
	void SG_Material::InitFromFile_Internal(const SG_MaterialDesc& aDesc)
	{
		if (aDesc.myFilename.empty())
		{
			SC_ERROR_LOG("Material is missing filename");
			return;
		}

		myProperties = aDesc;
		SC_Hash();

		std::ifstream i(aDesc.myFilename);
		if (i.good())
		{
			Json material;
			i >> material;

			////Load all material attributes here.
			//std::string includes;
			//includes.append("#include \"");
			//includes.append(material["shaderincludes"]);
			//includes.append("\"\n");
			//std::string shaderCode = material["shaderdefinition"];
			//shaderCode.append(material["shadercode"]);
			//shaderCode.insert(0, includes);
			//
			//uint hash = Shift::Hash(shaderCode.c_str(), (uint)shaderCode.size());
			//
			//CreateDirectory(L"..\\Data\\Shaders\\Cache\\", 0);
			//std::string shaderOut("..\\Data\\Shaders\\Cache\\");
			//shaderOut.append(Shift::GetFilename(aDesc.myFilename));
			//shaderOut.append("_");
			//shaderOut.append(std::to_string(hash));
			//shaderOut.append(".sshcache");
			//std::ofstream out(shaderOut);
			//if (out.good())
			//{
			//	out << shaderCode;
			//	out.close();
			//}

			SG_MaterialBaseDesc baseDesc;
			baseDesc.myBlendState = EBlendState::Disabled;
			baseDesc.myIsOpaque = material["IsOpaque"];
			baseDesc.myPixelShader = material["PixelShader"].get<std::string>().c_str();
			baseDesc.myVertexShader = material["VertexShader"].get<std::string>().c_str();
			baseDesc.myTopology = SR_Topology_TriangleList;
			baseDesc.myNumRenderTargets = (baseDesc.myIsOpaque) ? 4 : 1;
			baseDesc.myWriteDepth = true;
			//baseDesc.myShaderCode = shaderCode.c_str();

			SG_MaterialBaseDesc baseDepthDesc(baseDesc);
			baseDepthDesc.myVertexShader = "Shaders/Object_DepthTestVS.hlsl";
			baseDepthDesc.myPixelShader = "Shaders/Object_DepthTest.hlsl";
			baseDepthDesc.myNumRenderTargets = 0;
			baseDepthDesc.myRasterizerStateDesc.mySlopedScaleDepthBias = -2.0f;
			baseDepthDesc.myRasterizerStateDesc.myEnableDepthClip = false;

			myBase.CreatePSO(baseDesc);
			myBaseDepthTest.CreatePSO(baseDepthDesc); 

			uint numTextures = (uint)material["Textures"].size();
			myTextures.PreAllocate(numTextures);

			for (uint texIdx = 0; texIdx < numTextures; ++texIdx)
			{
				myTextures.Add(SR_GraphicsDevice::GetDevice()->GetCreateTexture(material["Textures"][texIdx].get<std::string>().c_str()));
			}
			myInitialized = true;
			
			i.close();
		}
		else
			SC_ERROR_LOG("Invalid filename [%s]", aDesc.myFilename.c_str());
	}
	uint SG_Material::SC_Hash()
	{
		myHash = myProperties.SC_Hash();
		return myHash;
	}
	bool SG_Material::Serialize()
	{
		return false;
	}
	uint SG_MaterialDesc::SC_Hash() const
	{
		uint hash = 0;

		if (!myFilename.empty())
		{
			std::string filename(myFilename);
			filename = filename.substr(0, filename.rfind(SG_Material::ourMaterialExtension));
			filename = filename.substr(filename.rfind('/') + 1);
			sscanf_s(filename.c_str(), "%x", &hash);
		}
		else
		{
			hash += Shift::SC_Hash(myCastShadows);
			hash += Shift::SC_Hash(myIsOpaque);
			hash += Shift::SC_Hash(myReceiveShadows);
			hash += Shift::SC_Hash(myUseConstantAttributes);
			hash += Shift::SC_Hash(myWireframe);

			if (!myVertexShader.empty())
				hash += Shift::SC_Hash(myVertexShader.c_str(), (uint)myVertexShader.length());

			if (!myPixelShader.empty())
				hash += Shift::SC_Hash(myPixelShader.c_str(), (uint)myPixelShader.length());


			for (const auto& texture : myTextures)
			{
				hash += Shift::SC_Hash(texture.c_str(), (uint)texture.size());
			}
		}
		return hash;
	}
	uint SG_MaterialDesc::GetSize() const
	{
		//uint texturesSize = sizeof(std::vector<std::string>) + (sizeof(std::string) * myTextures.size());
		//uint vertexShaderSize = strlen(myVertexShader);
		//uint pixelShaderSize = strlen(myPixelShader);
		//uint fileNameSize = strlen(myFilename);
		//
		//return texturesSize + vertexShaderSize + pixelShaderSize + fileNameSize + 1;
		return 0;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	SG_Material2::SG_Material2()
	{
	}

	SG_Material2::~SG_Material2()
	{
	}

	void SG_Material2::Bind(const SG_RenderType& aRenderType)
	{
		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();

		ctx->SetShaderState(myShaders[aRenderType]);
	}
}