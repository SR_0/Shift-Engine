#pragma once
#include "SR_TextureResource.h"

namespace Shift
{
	class SR_Texture;
	class CComputePSO;
	class CGraphicsPSO;
	class SG_View;

	enum SG_TonemapType
	{
		Reinhard,
		Uncharted,
		BurgessDawson,
	};

	struct SG_PostEffectsSettings
	{
		float myExposure;
		SG_TonemapType myTonemapType;
		bool myUseTonemap;

		float myBloomIntensity;
		float myBloomThreshold;
		bool myUseBloom;

		bool myUseGlare : 1;
		bool myUseFXAA : 1;
	};


	class SG_PostEffects
	{
	public:
		struct Shared
		{
		public:
			Shared();
			~Shared();

			void Load();

			CComputePSO* myTonemapPSO;
			CComputePSO* myBloomLuminancePSO;
			CComputePSO* myBloomBlurPSO;
			CComputePSO* myCopyPSO;
			CComputePSO* myAddPSO;

			CComputePSO* myInitialLumReductionShader;
			CComputePSO* myLumReductionShader;
			CComputePSO* myFinalLumReductionShader;

		private:
			bool myIsLoaded;
		};

		SG_PostEffects();
		~SG_PostEffects();

		void Init(SC_Vector2f aResolution);
		void Render(SG_View* aView, SR_Texture* aInputTexture);

		void CalculateLuminance(SG_View* aView, SR_Texture* aInputTexture);

	private:
		//void ChromaticAbberation();
		void Bloom(SR_Texture* aTextureInOut);
		void Tonemap(SR_Texture* aFramebufferTexture, SR_Texture* aBloomTexture);


	private:
		SC_GrowingArray<SR_TextureResource> myLuminanceReductionTargets;

		SG_PostEffectsSettings mySettings;
	};
}

