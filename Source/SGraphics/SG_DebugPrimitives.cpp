#include "SGraphics_Precompiled.h"
#include "SG_DebugPrimitives.h"

#if ENABLE_DEBUG_PRIMITIVES
#include "SG_View.h"

namespace Shift
{
	SG_DebugPrimitives* SG_DebugPrimitives::ourInstance = nullptr;

	static const SC_Vector4f locCubeVertices[] = {
		SC_Vector4f(-0.5f,  0.5f, -0.5f, 1.0f),
		SC_Vector4f( 0.5f,  0.5f, -0.5f, 1.0f),
		SC_Vector4f(-0.5f, -0.5f, -0.5f, 1.0f),
		SC_Vector4f( 0.5f, -0.5f, -0.5f, 1.0f),
		SC_Vector4f(-0.5f,  0.5f,  0.5f, 1.0f),
		SC_Vector4f( 0.5f,  0.5f,  0.5f, 1.0f),
		SC_Vector4f(-0.5f, -0.5f,  0.5f, 1.0f),
		SC_Vector4f( 0.5f, -0.5f,  0.5f, 1.0f)
	};

	static const uint locCubeIndices[] =
	{
		0, 1, 2,   // side 1
		2, 1, 3,
		4, 0, 6,   // side 2
		6, 0, 2,
		7, 5, 6,   // side 3
		6, 5, 4,
		3, 1, 7,   // side 4
		7, 1, 5,
		4, 5, 0,   // side 5
		0, 5, 1,
		3, 7, 2,   // side 6
		2, 7, 6,
	};

	SC_Vector4f GetTransformedCubeVertex(const SC_Matrix44& aTransform, uint aIndex)
	{
		return locCubeVertices[aIndex] * aTransform;
	}

	void SG_DebugPrimitives::Create()
	{
		if (!ourInstance)
			ourInstance = new SG_DebugPrimitives();
	}

	void SG_DebugPrimitives::Destroy()
	{
		delete ourInstance;
	}

	void SG_DebugPrimitives::Clear()
	{
		{
			SC_MutexLock lock(myLinesMutex);
			myLineVertices3D.RemoveAll();
		}
		{
			SC_MutexLock lock(myTriangleMutex);
			myTriangleVertices3D.RemoveAll();
		}
	}

	void SG_DebugPrimitives::DrawLine3D(const SC_Vector3f& aStartPos, const SC_Vector3f& aEndPos)
	{
		DrawLine3D(aStartPos, aEndPos, { 1.0f, 1.0f, 1.0f, 1.0f });
	}

	void SG_DebugPrimitives::DrawLine3D(const SC_Vector3f& aStartPos, const SC_Vector3f& aEndPos, const SC_Vector4f& aColor)
	{
		DrawLine3D(aStartPos, aEndPos, aColor, aColor);
	}

	void SG_DebugPrimitives::DrawLine3D(const SC_Vector3f& aStartPos, const SC_Vector3f& aEndPos, const SC_Vector4f& aStartColor, const SC_Vector4f& aEndColor)
	{
		SG_PrimitiveVertex startVert;
		startVert.myPosition = aStartPos;
		startVert.myColor = aStartColor;

		SG_PrimitiveVertex endVert;
		endVert.myPosition = aEndPos;
		endVert.myColor = aEndColor;

		SC_MutexLock lock(myLinesMutex);
		myLineVertices3D.Add(startVert);
		myLineVertices3D.Add(endVert);
	}

	void SG_DebugPrimitives::DrawTriangle3D(const SC_Vector3f& aCorner1, const SC_Vector3f& aCorner2, const SC_Vector3f& aCorner3, const SC_Vector4f& aColor)
	{
		SG_PrimitiveVertex firstCorner;
		firstCorner.myPosition = aCorner1;
		firstCorner.myColor = aColor;

		SG_PrimitiveVertex secondCorner;
		secondCorner.myPosition = aCorner2;
		secondCorner.myColor = aColor;

		SG_PrimitiveVertex thirdCorner;
		thirdCorner.myPosition = aCorner3;
		thirdCorner.myColor = aColor;

		SC_MutexLock lock(myTriangleMutex);
		myTriangleVertices3D.Add(firstCorner);
		myTriangleVertices3D.Add(secondCorner);
		myTriangleVertices3D.Add(thirdCorner);
	}

	void SG_DebugPrimitives::DrawBox3D(const SC_Matrix44& aTransform, const SC_Vector4f& aColor)
	{
		SG_PrimitiveVertex vertices[36];
		for (uint i = 0; i < 36; ++i)
		{
			vertices[i].myPosition = GetTransformedCubeVertex(aTransform, locCubeIndices[i]);
			vertices[i].myColor = aColor;
		}

		SC_MutexLock lock(myTriangleMutex);
		myTriangleVertices3D.AddN(vertices, 36);
	}

	void SG_DebugPrimitives::RenderPrimitives(SG_View* aView)
	{
		RenderLines(aView);
		RenderTriangles(aView);
	}

	SG_DebugPrimitives::SG_DebugPrimitives()
	{
		SVertexBufferDesc vBufferDesc;
		vBufferDesc.myNumVertices = 1;
		vBufferDesc.myStride = sizeof(SG_PrimitiveVertex);
		vBufferDesc.myVertexData = nullptr;
		SR_GraphicsDevice::GetDevice()->CreateVertexBuffer(vBufferDesc, &myLineVertexBuffer);
		SR_GraphicsDevice::GetDevice()->CreateVertexBuffer(vBufferDesc, &myTriangleVertexBuffer);

		SGraphicsPSODesc shaderDesc;
		shaderDesc.myVertexShader = "Shaders/Primitives_Simple3D_VS.hlsl";
		shaderDesc.myPixelShader = "Shaders/Primitives_Simple3D_PS.hlsl";
		shaderDesc.topology = SR_Topology_LineList;
		shaderDesc.myDepthStateDesc.myDepthComparisonFunc = SR_ComparisonFunc_GreaterEqual;
		shaderDesc.myRasterizerStateDesc.myFaceCull = SR_RasterizerFaceCull_None;
		shaderDesc.myRenderTargetFormats.myNumColorFormats = 1;
		shaderDesc.myRenderTargetFormats.myColorFormats[0] = SR_Format_RGBA8_Unorm;
		myShader = SR_GraphicsDevice::GetDevice()->CreateGraphicsPSO(shaderDesc);
	}

	SG_DebugPrimitives::~SG_DebugPrimitives()
	{
		delete myShader;
	}

	void SG_DebugPrimitives::RenderLines(SG_View* aView)
	{
		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();
		uint lineVertexCount = 0;
		{
			SC_MutexLock lock(myLinesMutex);
			if (myLineVertices3D.Empty())
				return;

			lineVertexCount = myLineVertices3D.Count();
			ctx->UpdateBufferData(sizeof(SG_PrimitiveVertex), lineVertexCount, myLineVertices3D.GetBuffer(), myLineVertexBuffer);
		}

		ctx->SetTopology(SR_Topology_LineList);

		ctx->SetVertexBuffer(0, myLineVertexBuffer);

		struct ViewConstants
		{
			SC_Matrix44 myViewProjection;
		} viewConstants;

		const SG_Camera& camera = aView->GetRenderData().myMainCamera;
		SC_Matrix44 view = camera.GetView();
		SC_Matrix44 projection = camera.GetProjection();

		viewConstants.myViewProjection = view * projection;
		ctx->BindConstantBuffer(&viewConstants, sizeof(viewConstants), 0);

		ctx->BindGraphicsPSO(*myShader);
		ctx->Draw(lineVertexCount, 0);
	}

	void SG_DebugPrimitives::RenderTriangles(SG_View* aView)
	{
		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();
		uint triangleVertexCount = 0;
		{
			SC_MutexLock lock(myTriangleMutex);
			if (myTriangleVertices3D.Empty())
				return;

			triangleVertexCount = myTriangleVertices3D.Count();
			ctx->UpdateBufferData(sizeof(SG_PrimitiveVertex), triangleVertexCount, myTriangleVertices3D.GetBuffer(), myTriangleVertexBuffer);
		}

		ctx->SetTopology(SR_Topology_TriangleList);

		ctx->SetVertexBuffer(0, myTriangleVertexBuffer);

		struct ViewConstants
		{
			SC_Matrix44 myViewProjection;
		} viewConstants;

		const SG_Camera& camera = aView->GetRenderData().myMainCamera;
		SC_Matrix44 view = camera.GetView();
		SC_Matrix44 projection = camera.GetProjection();

		viewConstants.myViewProjection = view * projection;
		ctx->BindConstantBuffer(&viewConstants, sizeof(viewConstants), 0);

		ctx->BindGraphicsPSO(*myShader);
		ctx->Draw(triangleVertexCount, 0);
	}
}
#endif