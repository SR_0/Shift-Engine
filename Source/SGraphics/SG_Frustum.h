#pragma once

namespace Shift
{

	class SG_Frustum
	{
	public:
		enum SG_FrustumPlane
		{
			SG_FrustumPlane_Near = 0,
			SG_FrustumPlane_Right,
			SG_FrustumPlane_Left,
			SG_FrustumPlane_Bottom,
			SG_FrustumPlane_Top,
			SG_FrustumPlane_Far,
			SG_FrustumPlane_Plane_Count,
		};

		SG_Frustum();
		~SG_Frustum();

		void Update(const SC_Matrix44& aView, const SC_Matrix44& aProj);

		bool Intersects(const SC_AABB& aAABB) const;

	private:
		SC_Vector4f myPlanes[SG_FrustumPlane_Plane_Count];
	};
}

