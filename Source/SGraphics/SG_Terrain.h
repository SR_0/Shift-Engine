#pragma once
namespace Shift
{
	class SG_RenderData;
	class CGraphicsPSO;
	struct SG_TerrainInitData
	{
		SG_TerrainInitData()
			: myResolution(256.f, 256.f)
			, mySectorSize(1.f)
		{}
		SC_Vector2f myResolution;
		float mySectorSize;
	};

	class SG_Terrain
	{
	public:
		SG_Terrain();
		~SG_Terrain();

		bool Init(const SG_TerrainInitData& aInitData);
		void Render(const SG_RenderData& aRenderData, bool aOnlyDepth = false);

	private:
		enum { Depth, Color };

		SC_Ref<SR_Texture> myHeightMap;
		SC_Ref<SR_Texture> myNormalMap;
		CVertexBuffer myVertexBuffer;
		CIndexBuffer myIndexBuffer;
		CGraphicsPSO* myShader;
		CGraphicsPSO* myDepthShader;
		SC_Ref<SR_ShaderState> myShaders[2];
	};

}