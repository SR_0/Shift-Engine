#include "SGraphics_Precompiled.h"
#include "SG_Terrain.h"

#include "SR_ShaderCompiler.h"

namespace Shift
{
	SG_Terrain::SG_Terrain()
	{
	}

	SG_Terrain::~SG_Terrain()
	{
	}

	bool SG_Terrain::Init(const SG_TerrainInitData& aInitData)
	{
		// Create lowest LOD mesh.
		// Tesselate for detail
		// Use heightmap
		//

		SR_GraphicsDevice* device = SR_GraphicsDevice::GetDevice();
		SR_ShaderCompiler* shaderCompiler = device->GetShaderCompiler();

		myHeightMap = device->GetCreateTexture("ShiftEngine/Textures/HeightMapGen.png");
		myNormalMap = device->GetCreateTexture("ShiftEngine/Textures/HeightMapGenNorm.png");

		SGraphicsPSODesc psoDesc;
		psoDesc.myVertexShader = "Shaders/Object_TerrainVS.hlsl";
		psoDesc.myPixelShader = "Shaders/Object_TerrainPS.hlsl";
		psoDesc.topology = SR_Topology_TriangleList;
		psoDesc.myDepthStateDesc.myDepthComparisonFunc = SR_ComparisonFunc_GreaterEqual;
		psoDesc.myDepthStateDesc.myWriteDepth = true;
		psoDesc.myRenderTargetFormats.myNumColorFormats = 4;
		psoDesc.myRenderTargetFormats.myColorFormats[0] = SR_Format_RGBA8_Unorm; // Color
		psoDesc.myRenderTargetFormats.myColorFormats[1] = SR_Format_RG11B10_Float; // Normals
		psoDesc.myRenderTargetFormats.myColorFormats[2] = SR_Format_RGBA8_Unorm; // ARM
		psoDesc.myRenderTargetFormats.myColorFormats[3] = SR_Format_RGBA8_Unorm; // Emissive + Fog

		for (uint i = 0; i < 4; ++i)
		{
			psoDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].mySrcBlend = SR_BlendMode_One;
			psoDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].myDstBlend = SR_BlendMode_Zero;
			psoDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].mySrcBlendAlpha = SR_BlendMode_One;
			psoDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].myDstBlendAlpha = SR_BlendMode_Zero;
		}
		myShader = SR_ShaderStateCache::Get().GetPSO(psoDesc);

		SR_ShaderStateDesc colorShaderDesc;
		colorShaderDesc.myTopology = SR_Topology_TriangleList;
		colorShaderDesc.myDepthStateDesc.myDepthComparisonFunc = SR_ComparisonFunc_GreaterEqual;
		colorShaderDesc.myDepthStateDesc.myWriteDepth = true;
		colorShaderDesc.myRenderTargetFormats.myNumColorFormats = 4;
		colorShaderDesc.myRenderTargetFormats.myColorFormats[0] = SR_Format_RGBA8_Unorm; // Color
		colorShaderDesc.myRenderTargetFormats.myColorFormats[1] = SR_Format_RG11B10_Float; // Normals
		colorShaderDesc.myRenderTargetFormats.myColorFormats[2] = SR_Format_RGBA8_Unorm; // ARM
		colorShaderDesc.myRenderTargetFormats.myColorFormats[3] = SR_Format_RGBA8_Unorm; // Emissive + Fog

		for (uint i = 0; i < 4; ++i)
		{
			colorShaderDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].mySrcBlend = SR_BlendMode_One;
			colorShaderDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].myDstBlend = SR_BlendMode_Zero;
			colorShaderDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].mySrcBlendAlpha = SR_BlendMode_One;
			colorShaderDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].myDstBlendAlpha = SR_BlendMode_Zero;
		}

		colorShaderDesc.myShaderByteCodes[SR_ShaderType_Vertex] = shaderCompiler->CompileShaderFromFile("ShiftEngine/Shaders/SGraphics/TEMP_SHADERS/ObjectTerrainVS.ssf", SR_ShaderType_Vertex);
		colorShaderDesc.myShaderByteCodes[SR_ShaderType_Pixel] = shaderCompiler->CompileShaderFromFile("ShiftEngine/Shaders/SGraphics/TEMP_SHADERS/ObjectTerrainPS.ssf", SR_ShaderType_Pixel);
		myShaders[Color] = device->CreateShaderState(colorShaderDesc);

		SR_ShaderStateDesc depthShaderDesc;
		depthShaderDesc.myTopology = SR_Topology_TriangleList;

		depthShaderDesc.myRasterizerStateDesc.myDepthBias = -100;
		depthShaderDesc.myRasterizerStateDesc.mySlopedScaleDepthBias = -8.0;
		depthShaderDesc.myRasterizerStateDesc.myEnableDepthClip = false;
		depthShaderDesc.myShaderByteCodes[SR_ShaderType_Vertex] = shaderCompiler->CompileShaderFromFile("ShiftEngine/Shaders/SGraphics/TEMP_SHADERS/ObjectTerrainVS.ssf", SR_ShaderType_Vertex);
		depthShaderDesc.myShaderByteCodes[SR_ShaderType_Pixel] = shaderCompiler->CompileShaderFromFile("ShiftEngine/Shaders/SGraphics/TEMP_SHADERS/ObjectDepthTestPS.ssf", SR_ShaderType_Pixel);
		myShaders[Depth] = device->CreateShaderState(depthShaderDesc);

		psoDesc.myVertexShader = "Shaders/Object_TerrainDepthVS.hlsl";
		psoDesc.myPixelShader = "Shaders/Object_DepthTest.hlsl";
		psoDesc.topology = SR_Topology_TriangleList;
		psoDesc.myRasterizerStateDesc.myDepthBias = -100;
		psoDesc.myRasterizerStateDesc.mySlopedScaleDepthBias = -8.0;
		psoDesc.myRasterizerStateDesc.myEnableDepthClip = false;
		myDepthShader = SR_ShaderStateCache::Get().GetPSO(psoDesc);

		SC_GrowingArray<SVertex_Terrain> vertices;
		SC_GrowingArray<uint> indices; 

		SC_Vector2f uvIncrements = { 1.0f / aInitData.myResolution.x, 1.0f / aInitData.myResolution.y };

		int halfResY = (int)(aInitData.myResolution.y * 0.5f);
		int halfResX = (int)(aInitData.myResolution.x * 0.5f);

		for (uint y = 0; y < aInitData.myResolution.y; ++y)
		{
			for (uint x = 0; x < aInitData.myResolution.x; ++x)
			{
				SVertex_Terrain& vertex = vertices.Add();
				vertex.myPosition = {x * aInitData.mySectorSize - halfResX, 0.0f, y * aInitData.mySectorSize - halfResY, 1.0f };
				vertex.myUV = { x * uvIncrements.x, y * uvIncrements.y };
			}
		}
		
		for (uint y = 0; y < aInitData.myResolution.y - 1; ++y)
		{
			for (uint x = 0; x < aInitData.myResolution.x - 1; ++x)
			{
				indices.Add(uint(x + (y * aInitData.myResolution.x)));									//current vertex
				indices.Add(uint(x + (y * aInitData.myResolution.x) + aInitData.myResolution.x));			//current vertex + row
				indices.Add(uint(x + (y * aInitData.myResolution.x) + aInitData.myResolution.x + 1));		//current vertex + row + one right

				indices.Add(uint(x + (y * aInitData.myResolution.x)));									//current vertex
				indices.Add(uint(x + (y * aInitData.myResolution.x) + aInitData.myResolution.x + 1));		//current vertex + row + one right
				indices.Add(uint(x + (y * aInitData.myResolution.x) + 1));								//current vertex + one right
			}
		}

		SVertexBufferDesc vbDesc;
		vbDesc.myNumVertices = vertices.Count();
		vbDesc.myStride = sizeof(SVertex_Terrain);
		vbDesc.myVertexData = vertices.GetBuffer();
		device->CreateVertexBuffer(vbDesc, &myVertexBuffer);

		//SR_BufferDesc bufferDescV;
		//bufferDescV.mySize = sizeof(SVertex_Terrain) * vertices.Count();
		//bufferDescV.myBindFlag = SR_BindFlag_VertexBuffer;
		//
		//SC_Ref<SR_Buffer> bufferV = device->_CreateBuffer(bufferDescV, vertices.GetBuffer(), "Test VertexBuffer");


		device->CreateIndexBuffer(indices.GetBuffer(), indices.Count(), &myIndexBuffer);

		return false;
	}
	void SG_Terrain::Render(const SG_RenderData&, bool aOnlyDepth)
	{
		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();

		ctx->BindTexture(myHeightMap, 0);
		ctx->BindTexture(myNormalMap, 1);

		ctx->SetIndexBuffer(myIndexBuffer);
		ctx->SetVertexBuffer(0, myVertexBuffer);
		if (!aOnlyDepth)
			ctx->BindGraphicsPSO(*myShader);
		else
			ctx->BindGraphicsPSO(*myDepthShader);

		ctx->DrawIndexed(myIndexBuffer.GetNumIndices(), 0, 0);

	}
}