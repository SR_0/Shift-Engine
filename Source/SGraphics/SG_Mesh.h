#pragma once
#include "SC_Resource.h"
#include "SC_AABB.h"
#include "SG_RenderType.h"

namespace Shift
{
	using MeshHandle = unsigned int;
	class SR_Buffer;
	class SR_ShaderState;
	struct SG_MeshHeader;
	class SG_Mesh : public SC_Resource
	{
	public:
		SG_Mesh();
		~SG_Mesh();

		static SC_Ref<SG_Mesh> Create(const SG_MeshHeader& aHeader, void* aVertexData, void* aIndexData, const char* aName = nullptr);

		void Init();

		const SC_AABB& GetAABB() const { return myBoundingBox; }
		SR_Buffer* GetVertexBuffer();
		SR_Buffer* GetIndexBuffer();

		void SetVertexBufferData(uint8* aVertexData, uint aNumVertices);
		void SetVertexStride(uint aVertexStride);
		void SetIndexBufferData(uint* aIndexData, uint aNumIndices);

		SC_Ref<SR_ShaderState> myShaders[SG_RenderType_COUNT];
	protected:
		std::string myName;
		SC_AABB myBoundingBox;
		SC_Ref<SR_Buffer> myVertexBuffer;
		SC_Ref<SR_Buffer> myIndexBuffer;

		uint myVertexStride;
		uint myNumVertices;
		uint myNumIndices;
		SC_UniquePtr<uint8> myVertexBufferData;
		SC_UniquePtr<uint> myIndexBufferData;
		// Buffers (per LOD)
		// Vertex-data (per LOD)
		// Index-data (per LOD)
		// Bounds (sphere + aabb)
		// Shaders to use
	private:
		static std::unordered_map<uint, SC_Ref<SG_Mesh>> ourCache;
	};

}