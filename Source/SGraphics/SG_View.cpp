#include "SGraphics_Precompiled.h"
#include "SG_View.h"
#include "SG_World.h"
#include "SG_Shadows.h"
#include "SG_Terrain.h"
#include "SG_DrawSubmitStructs.h"
#include "SG_Material.h"

#include "SR_WaitEvent.h"
#include "SR_SwapChain.h"
#include "SR_ViewConstants.h"
#include "SR_ResourceRefs.h"

#include "SR_ShaderCompiler_DX12.h"

namespace Shift
{
	struct SViewPrivate
	{
		SViewPrivate() {}
		~SViewPrivate() {}

		std::mutex myDataAccessLock;
	};

	SG_View::SG_View(SG_World* aWorld)
		: myCurrentFrameIndex(0)
		, myCurrentViewDataIndex(0)
	{


		myWorld = aWorld;
		myPrivate = new SViewPrivate;

		Initialize();

		SR_ShaderCompiler_DX12* compiler = new SR_ShaderCompiler_DX12();
		
		SC_Ref<SR_ShaderByteCode> bc = compiler->CompileShaderFromFile("ShiftEngine/Shaders/SGraphics/Light/Light_Compute.ssf", SR_ShaderType_Compute);
		
		SR_ShaderStateDesc ssdesc;
		ssdesc.myShaderByteCodes[SR_ShaderType_Compute] = bc;
		ssdesc.myIsCompute = true;
		
		SC_Ref<SR_ShaderState> ss = SR_GraphicsDevice::GetDevice()->CreateShaderState(ssdesc);

	}

	SG_View::~SG_View()
	{
		delete myShadows;
		delete myTimestampBuffer;
		delete myPrivate;
	}

	void SG_View::Prepare()
	{
		SG_RenderData& prepareData = GetPrepareData();
		prepareData.Clear();

		prepareData.myMainCamera = myCamera;
		for (uint i = 0; i < SG_Shadows::ourNumShadowCascades; ++i)
			prepareData.myShadowLightCameras[i] = myShadows->GetCSMCamera(i);

		prepareData.myPrepareFuture = SC_CreateFrameTask(myWorld, &SG_World::PrepareView, this);
		//prepareData.myPrepareSettings = SC_CreateFrameTask(this, &SG_View::UpdateViewSettings);
		//myWorld->PrepareView(this);
		UpdateViewSettings();

		prepareData.myIsUpdated = true;
		//SignalReady(SViewData_Write);
	}

	void SG_View::Render()
	{
		SC_CPU_PROFILER_FUNCTION();
		SG_RenderData& renderData = GetRenderData_Mutable();

		//bool isUpdated = renderData.myIsUpdated;
		//while (!isUpdated)
		//{
		//	SC_Sleep(1);
		//	SignalReady(SViewData_Read);
		//	isUpdated = GetRenderData_Mutable().myIsUpdated;
		//}
		renderData.myPrepareSettings.GetResult();

		PostRenderTask(renderData.mySetViewRefsEvent, &SG_View::Render_StartFrame);
		PostRenderTask(renderData.myRenderShadowsEvent, &SG_View::Render_Shadows, true);
		PostRenderTask(renderData.myRenderSkyProbeEvent, &SG_View::Render_SkyProbes, true);
		PostRenderTask(renderData.myRenderGBufferEvent, &SG_View::Render_GBuffer, mySettings.myRenderGBuffers);
		PostRenderTask(renderData.myRenderSSAOEvent, &SG_View::Render_AmbientOcclusion, mySettings.myEnableSSAO);
		PostRenderTask(renderData.myRenderLightsEvent, &SG_View::Render_Lights, mySettings.myRenderDeferredLights);

		//PostRenderTask(forwardEvent, &CView::Render_Forward, false);

		PostRenderTask(renderData.myRenderPostFXEvent, &SG_View::Render_PostFX, mySettings.myRenderPostFX);
		PostRenderTask(renderData.myRenderUIEvent, &SG_View::Render_UI, true);
		PostRenderTask(renderData.myRenderComposeEvent, &SG_View::Render_Compose, mySettings.myRenderUI);
		

		for (auto& event : renderData.myEvents)
		{
			event->myEventCPU.Wait();
		}
		SR_RenderInterface::ourLastViewTaskFence = renderData.myEvents.GetLast()->myFence;

		EndRender();
	}

	void SG_View::SetWorld(SG_World* aWorld)
	{
		myWorld = aWorld;
	}

	void SG_View::SetCamera(const SG_Camera& aCamera)
	{
		myCamera = aCamera;
	}

	SG_RenderData& SG_View::GetPrepareData()
	{
		return myViewData[myCurrentViewDataIndex];
	}

	const SG_RenderData& SG_View::GetRenderData() const
	{
		return myViewData[myCurrentViewDataIndex];
	}

	SG_RenderData& SG_View::GetRenderData_Mutable()
	{
		return myViewData[myCurrentViewDataIndex];
	}

	SR_Texture* SG_View::GetLastFinishedFrame()
	{
		const uint8 historyIndex = uint8(myCurrentFrameIndex % 4);
		return myFrameResources.myScreenHistory[historyIndex].myTexture;
	}

	SR_Texture* SG_View::GetShadowTexture()
	{
		return myShadows->GetSunShadowMap();
	}

	void SG_View::StartPrepare()
	{
	}

	void SG_View::EndPrepare()
	{
	}

	void SG_View::StartRender()
	{
	}

	void SG_View::EndRender()
	{
		myCurrentViewDataIndex = (myCurrentViewDataIndex == SViewData_Count - 1) ? 0 : ++myCurrentViewDataIndex;
	}

	void SG_View::Initialize()
	{
		SR_GraphicsDevice* device = SR_GraphicsDevice::GetDevice();
		const SC_Vector2f& framebufferRes = device->GetResolution();
		const SC_Vector2f& halfFramebufferRes = framebufferRes * 0.5f;
		const SC_Vector2f& quarterFramebufferRes = halfFramebufferRes * 0.5f;

		myFrameResources.myGBuffer.Update();

		myFrameResources.myDepth.myCreateRenderTarget = true;
		myFrameResources.myDepth.myCreateTexture = true;
		myFrameResources.myDepth.myTextureBufferFormat = SR_Format_D32_Float;
		myFrameResources.myDepth.myTextureFormat = SR_Format_R32_Float;
		myFrameResources.myDepth.Update(framebufferRes, "DepthBuffer");

		myFrameResources.myAmbientOcclusion.myCreateTexture = true;
		myFrameResources.myAmbientOcclusion.myCreateRWTexture = true;
		myFrameResources.myAmbientOcclusion.myTextureFormat = SR_Format_R32_Float;
		myFrameResources.myAmbientOcclusion.Update(framebufferRes, "Ambient Occlusion");

		myFrameResources.myFullscreenHDR.myCreateTexture = true;
		myFrameResources.myFullscreenHDR.myCreateRWTexture = true;
		myFrameResources.myFullscreenHDR.myCreateRenderTarget = true;
		myFrameResources.myFullscreenHDR.myTextureFormat = SR_Format_RGBA16_Float;
		myFrameResources.myFullscreenHDR.Update(framebufferRes, "Fullscreen HDR Texture");

		myFrameResources.myFullscreen.myCreateTexture = true;
		myFrameResources.myFullscreen.myCreateRWTexture = true;
		myFrameResources.myFullscreen.myCreateRenderTarget = true;
		myFrameResources.myFullscreen.myTextureFormat = SR_Format_RGBA8_Unorm;
		myFrameResources.myFullscreen.Update(framebufferRes, "Fullscreen Texture");

		myFrameResources.myFullscreen2.myCreateTexture = true;
		myFrameResources.myFullscreen2.myCreateRWTexture = true;
		myFrameResources.myFullscreen2.myCreateRenderTarget = true;
		myFrameResources.myFullscreen2.myTextureFormat = SR_Format_RGBA8_Unorm;
		myFrameResources.myFullscreen2.Update(framebufferRes, "Fullscreen Texture 2");

		myFrameResources.myScreenHistory[0].myCreateTexture = true;
		myFrameResources.myScreenHistory[0].myTextureFormat = SR_Format_RGBA8_Unorm;
		myFrameResources.myScreenHistory[0].Update(framebufferRes, "Screen History Texture");

		myFrameResources.myScreenHistory[1].myCreateTexture = true;
		myFrameResources.myScreenHistory[1].myTextureFormat = SR_Format_RGBA8_Unorm;
		myFrameResources.myScreenHistory[1].Update(framebufferRes, "Screen History Texture");

		myFrameResources.myScreenHistory[2].myCreateTexture = true;
		myFrameResources.myScreenHistory[2].myTextureFormat = SR_Format_RGBA8_Unorm;
		myFrameResources.myScreenHistory[2].Update(framebufferRes, "Screen History Texture");

		myFrameResources.myScreenHistory[3].myCreateTexture = true;
		myFrameResources.myScreenHistory[3].myTextureFormat = SR_Format_RGBA8_Unorm;
		myFrameResources.myScreenHistory[3].Update(framebufferRes, "Screen History Texture");

		///// Bloom

		myFrameResources.myBloomLuminance.myCreateTexture = true;
		myFrameResources.myBloomLuminance.myCreateRWTexture = true;
		myFrameResources.myBloomLuminance.myTextureFormat = SR_Format_RGBA16_Float;
		myFrameResources.myBloomLuminance.Update(halfFramebufferRes, "Bloom Luminance");

		myFrameResources.myBloomBlur0.myCreateTexture = true;
		myFrameResources.myBloomBlur0.myCreateRWTexture = true;
		myFrameResources.myBloomBlur0.myTextureFormat = SR_Format_RGBA16_Float;
		myFrameResources.myBloomBlur0.Update(halfFramebufferRes, "Bloom Blur 0");

		myFrameResources.myBloomBlur1.myCreateTexture = true;
		myFrameResources.myBloomBlur1.myCreateRWTexture = true;
		myFrameResources.myBloomBlur1.myTextureFormat = SR_Format_RGBA16_Float;
		myFrameResources.myBloomBlur1.Update(halfFramebufferRes, "Bloom Blur 1");

		myFrameResources.myBloomFinal.myCreateTexture = true;
		myFrameResources.myBloomFinal.myCreateRWTexture = true;
		myFrameResources.myBloomFinal.myTextureFormat = SR_Format_RGBA16_Float;
		myFrameResources.myBloomFinal.Update(halfFramebufferRes, "Bloom Final");

		myFrameResources.myBloomLuminanceQuarter.myCreateTexture = true;
		myFrameResources.myBloomLuminanceQuarter.myCreateRWTexture = true;
		myFrameResources.myBloomLuminanceQuarter.myTextureFormat = SR_Format_RGBA16_Float;
		myFrameResources.myBloomLuminanceQuarter.Update(quarterFramebufferRes, "Bloom Luminance Quarter");

		myFrameResources.myBloomBlurQuarter0.myCreateTexture = true;
		myFrameResources.myBloomBlurQuarter0.myCreateRWTexture = true;
		myFrameResources.myBloomBlurQuarter0.myTextureFormat = SR_Format_RGBA16_Float;
		myFrameResources.myBloomBlurQuarter0.Update(quarterFramebufferRes, "Bloom Blur Quarter 0");

		myFrameResources.myBloomBlurQuarter1.myCreateTexture = true;
		myFrameResources.myBloomBlurQuarter1.myCreateRWTexture = true;
		myFrameResources.myBloomBlurQuarter1.myTextureFormat = SR_Format_RGBA16_Float;
		myFrameResources.myBloomBlurQuarter1.Update(quarterFramebufferRes, "Bloom Blur Quarter 1");

		SR_TextureBufferDesc skyProbeDesc;
		skyProbeDesc.myDimension = SR_Dimension_TextureCube;
		skyProbeDesc.myFlags |= SR_ResourceFlag_AllowWrites;
		skyProbeDesc.myWidth = 256.f;
		skyProbeDesc.myHeight = 256.f;
		skyProbeDesc.myMips = 1;
		skyProbeDesc.myFormat = SR_Format_RGBA16_Float;

		myFrameResources.mySkyProbeTextureBuffer = device->CreateTextureBuffer(skyProbeDesc);

		SR_TextureDesc skyProbeCubeDesc;
		skyProbeCubeDesc.myFormat = SR_Format_RGBA16_Float;
		myFrameResources.mySkyProbeTexture = device->CreateTexture(skyProbeCubeDesc, myFrameResources.mySkyProbeTextureBuffer);

		SR_TextureDesc skyProbeRWDesc;
		skyProbeRWDesc.myFormat = SR_Format_RGBA16_Float;
		skyProbeRWDesc.myArraySize = 1;
		myFrameResources.mySkyProbeTextureRW = device->CreateRWTexture(skyProbeRWDesc, myFrameResources.mySkyProbeTextureBuffer);

		SVertexBufferDesc quadBuffer;
		quadBuffer.myNumVertices = 4;
		quadBuffer.myStride = sizeof(SVertex_Simple);
		quadBuffer.myVertexData = (SC_Handle)globalQuadVertexList;
		device->CreateVertexBuffer(quadBuffer, &myPostFX.myFullscreenQuadBuffer);
		device->CreateIndexBuffer(globalQuadIndexList, static_cast<uint>(6), &myPostFX.myFullscreenQuadIBuffer);

		SR_ShaderCompiler* shaderCompiler = device->GetShaderCompiler();

		{
			SGraphicsPSODesc desc;
			desc.myVertexShader = "Shaders/FullscreenQuadVS.hlsl";
			desc.myPixelShader = "Shaders/PostFX_AA.hlsl";
			desc.myRenderTargetFormats.myNumColorFormats = 1;
			desc.myRenderTargetFormats.myColorFormats[0] = SR_Format_RGBA8_Unorm;
			desc.topology = SR_Topology_TriangleList;
			myPostFX.myAAPSO = SR_ShaderStateCache::Get().GetPSO(desc);

			//SR_ShaderStateDesc sDesc;
			//sDesc.myRenderTargetFormats.myNumColorFormats = 1;
			//sDesc.myRenderTargetFormats.myColorFormats[0] = SR_Format_RGBA8_Unorm;
			//sDesc.myTopology = SR_Topology_TriangleList;
			//sDesc.myShaderByteCodes[SR_ShaderType_Vertex] = shaderCompiler->CompileShaderFromFile("ShiftEngine/Shaders/SGraphics/PostFX/PostFX_QuadVS.ssf", SR_ShaderType_Vertex);
			//sDesc.myShaderByteCodes[SR_ShaderType_Pixel] = shaderCompiler->CompileShaderFromFile("ShiftEngine/Shaders/SGraphics/PostFX/FXAA.ssf", SR_ShaderType_Pixel);
			//
			//myPostFX.myFXAAShader = device->CreateShaderState(sDesc);

		}
		{
			SComputePSODesc csDesc;
			csDesc.myComputeShader = "Shaders/HDR_Tonemap.hlsl";
			myPostFX.myTonemapPSO = SR_ShaderStateCache::Get().GetPSO(csDesc);

			SR_ShaderStateDesc sDesc;
			sDesc.myIsCompute = true;
			sDesc.myShaderByteCodes[SR_ShaderType_Compute] = shaderCompiler->CompileShaderFromFile("ShiftEngine/Shaders/SGraphics/PostFX/Tonemap/Tonemap.ssf", SR_ShaderType_Compute);
			myPostFX.myTonemapShader = device->CreateShaderState(sDesc);
		}
		{
			SComputePSODesc csDesc;
			csDesc.myComputeShader = "Shaders/Bloom_Luminance.hlsl";
			myPostFX.myBloomLuminancePSO = SR_ShaderStateCache::Get().GetPSO(csDesc);
		}
		{
			SComputePSODesc csDesc;
			csDesc.myComputeShader = "Shaders/Bloom_Blur.hlsl";
			myPostFX.myBloomBlurPSO = SR_ShaderStateCache::Get().GetPSO(csDesc);
		}
		{
			SComputePSODesc csDesc;
			csDesc.myComputeShader = "Shaders/Copy.hlsl";
			myPostFX.myCopyPSO = SR_ShaderStateCache::Get().GetPSO(csDesc);
		}
		{
			SComputePSODesc csDesc;
			csDesc.myComputeShader = "Shaders/Bloom_Add.hlsl";
			myPostFX.myAddPSO = SR_ShaderStateCache::Get().GetPSO(csDesc);
		}

		myPostFX.myUseFXAA = false;
		myPostFX.myUseBloom = true;
		myPostFX.myExposure = 1.0f;
		myPostFX.myBloomIntensity = 0.75f;
		myPostFX.myBloomThreshold = 0.01f;

		SVertexBufferDesc instanceBufferDesc;
		instanceBufferDesc.myNumVertices = 1;
		instanceBufferDesc.myStride = sizeof(SVertex_ObjectInputInstance);
		instanceBufferDesc.myVertexData = nullptr;
		device->CreateVertexBuffer(instanceBufferDesc, &myInstanceBuffer);

		SComputePSODesc ssaoDesc;
		ssaoDesc.myComputeShader = "Shaders/SSAO.hlsl";
		mySSAO.mySSAO = SR_ShaderStateCache::Get().GetPSO(ssaoDesc);

		SR_ShaderStateDesc ssaoDesc2;
		ssaoDesc2.myIsCompute = true;
		ssaoDesc2.myShaderByteCodes[SR_ShaderType_Compute] = shaderCompiler->CompileShaderFromFile("ShiftEngine/Shaders/SGraphics/SSAO.ssf", SR_ShaderType_Compute);
		mySSAO.myShader = device->CreateShaderState(ssaoDesc2);

		ssaoDesc.myComputeShader = "Shaders/Compute_Blur.hlsl";
		mySSAO.mySSAOBlur = SR_ShaderStateCache::Get().GetPSO(ssaoDesc);

		mySSAO.mySSAORand = device->GetCreateTexture("_Random.dds");

		mySSAO.mySSAOSamplingRadius = 0.2f;
		mySSAO.mySSAOScale = 2.0f;
		mySSAO.mySSAOBias = 0.3f;
		mySSAO.mySSAOIntensity = 3.f;
		mySSAO.myEnableSSAO = true;


		SComputePSODesc lightDesc;
		lightDesc.myComputeShader = "Shaders/Light_Compute.hlsl";
		myLighting.myLightingShader = SR_ShaderStateCache::Get().GetPSO(lightDesc);

		SR_ShaderStateDesc lightDesc2;
		lightDesc2.myIsCompute = true;
		lightDesc2.myShaderByteCodes[SR_ShaderType_Compute] = shaderCompiler->CompileShaderFromFile("ShiftEngine/Shaders/SGraphics/Light/Light_Compute.ssf", SR_ShaderType_Compute);
		myLighting.myShader = device->CreateShaderState(lightDesc2);


		SR_BufferDesc timestampsDesc;
		timestampsDesc.myBindFlag = SBindFlag_Buffer;
		timestampsDesc.myCPUAccess = SR_AccessCPU_Map_Read;
		timestampsDesc.mySize = 24 * sizeof(uint64);
		myTimestampBuffer = device->CreateBuffer(timestampsDesc, "Timestamp Buffer");

		myShadows = new SG_Shadows();
		myShadows->Init();

		myPostEffects.Init(framebufferRes);

	}
	void SG_View::UpdateViewSettings()
	{
		mySettings.myRenderDepth = true;
		mySettings.myRenderGBuffers = true;
		mySettings.myRenderDeferredLights = true;
		mySettings.myRenderForward = true;
		mySettings.myRenderPostFX = true;
		mySettings.myRenderUI = true;
		mySettings.myRenderSky = true;
		
		mySettings.myEnableSSAO = mySSAO.myEnableSSAO;
	}
	void SG_View::PostRenderTask(SC_Ref<SR_Waitable>& aEventOut, RenderFunc aTask, bool aEnabled)
	{
		if (!aEnabled)
			return;

		SC_Ref<SR_Waitable> event = SR_RenderInterface::PostRenderTask(std::bind(aTask, this));
		GetRenderData_Mutable().myEvents.Add(event);
		aEventOut = event;
	}
	void SG_View::PostComputeTask(SC_Ref<SR_Waitable>& aEventOut, RenderFunc aTask, bool aEnabled)
	{
		if (!aEnabled)
			return;

		aEventOut = SR_RenderInterface::PostComputeTask(std::bind(aTask, this));
	}

	void SG_View::PostCopyTask(SC_Ref<SR_Waitable>& aEventOut, RenderFunc aTask, bool aEnabled)
	{
		if (!aEnabled)
			return;

		aEventOut = SR_RenderInterface::PostCopyTask(std::bind(aTask, this));
	}

	void SG_View::Render_StartFrame()
	{
		const SG_RenderData& renderData = GetRenderData();
		const SG_Camera& camera = renderData.myMainCamera;

		const SC_Matrix44& view = camera.GetView();
		const SC_Matrix44& projection = camera.GetProjection();
		const SC_Matrix44& inverseView = camera.GetInverseView();
		const SC_Matrix44& inverseProjection = camera.GetInverseProjection();

		SR_ViewConstants constants;
		constants.myWorldToClip = view * projection;
		constants.myWorldToCamera = view;
		constants.myClipToWorld = inverseProjection * inverseView;
		constants.myClipToCamera = inverseProjection;
		constants.myCameraToClip = projection;
		constants.myCameraToWorld = inverseView;
		constants.myCameraPosition = camera.GetPosition();
		constants.myResolution = SC_EngineInterface::GetResolution();

		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();
		ctx->BindConstantBuffer(&constants, sizeof(SR_ViewConstants), SR_ConstantBufferRef::SR_ConstantBufferRef_ViewConstants);
	}

	void SG_View::Render_SkyProbes()
	{
		const SG_RenderData& renderData = GetRenderData();
		renderData.myPrepareFuture.GetResult();
		SR_RenderInterface::ourPresentEvent->myEventCPU.Wait();
		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();
		ctx->InsertWait(renderData.mySetViewRefsEvent);
		SE_PROFILER_FUNCTION();
		SC_CPU_PROFILER_FUNCTION();
		SG_Camera camera;
		camera.InitAsPerspective({256.f, 256.f}, 90.f, 1.f, 100.f);
		SC_Vector3f targets[6] =
		{
			SC_Vector3f(1, 0, 0),
			SC_Vector3f(-1, 0, 0),
			SC_Vector3f(0, 1, 0),
			SC_Vector3f(0, -1, 0),
			SC_Vector3f(0, 0, 1),
			SC_Vector3f(0, 0, -1),
		};

		for (uint i = 0; i < 6; ++i)
		{
			SC_Vector3f up = { 0.0f, 1.0f, 0.0f };
			if (i == 2)
				up = { 0.0f, 0.0f, -1.0f };
			else if (i == 3)
				up = { 0.0f, 0.0f, 1.0f };

			camera.LookAt({0.0f, 0.0f, 0.0f}, targets[i], up);
			myWorld->mySky->RenderSkyProbeFace(camera, myFrameResources.mySkyProbeTextureRW, i);
		}
	}

	void SG_View::Render_Depth()
	{
		SC_LOG("Render_Depth");
	}

	void SG_View::Render_Shadows()
	{
		const SG_RenderData& renderData = GetRenderData();
		renderData.myPrepareFuture.GetResult();
		SR_RenderInterface::ourPresentEvent->myEventCPU.Wait();
		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();
		ctx->InsertWait(renderData.mySetViewRefsEvent);
		SE_PROFILER_FUNCTION();
		SC_CPU_PROFILER_FUNCTION();
		//myShadows->SetShadowConstants();
		myShadows->RenderShadows(this);
	}

	void SG_View::Render_GBuffer()
	{
		const SG_RenderData& renderData = GetRenderData();
		renderData.myPrepareFuture.GetResult();
		SR_RenderInterface::ourPresentEvent->myEventCPU.Wait();

		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();
		SR_GraphicsDevice* device = SR_GraphicsDevice::GetDevice();
		ctx->InsertWait(renderData.mySetViewRefsEvent);

		SE_PROFILER_FUNCTION();
		SC_CPU_PROFILER_FUNCTION();

		ctx->EndQuery(SQueryType_Timestamp, 2);
		{
			SC_Pair<SR_ResourceState, SR_TrackedResource*> pairs[4];
			pairs[0] = SC_Pair(SR_ResourceState_RenderTarget, myFrameResources.myGBuffer.myColors.myTextureBuffer);
			pairs[1] = SC_Pair(SR_ResourceState_RenderTarget, myFrameResources.myGBuffer.myNormals.myTextureBuffer);
			pairs[2] = SC_Pair(SR_ResourceState_RenderTarget, myFrameResources.myGBuffer.myMaterials.myTextureBuffer);
			pairs[3] = SC_Pair(SR_ResourceState_RenderTarget, myFrameResources.myGBuffer.myEmissive.myTextureBuffer);
			ctx->Transition(pairs, 4);
		}
		SR_RenderTarget* gbufferTargets[4];
		gbufferTargets[0] = myFrameResources.myGBuffer.myColors.myRenderTarget;
		gbufferTargets[1] = myFrameResources.myGBuffer.myNormals.myRenderTarget;
		gbufferTargets[2] = myFrameResources.myGBuffer.myMaterials.myRenderTarget;
		gbufferTargets[3] = myFrameResources.myGBuffer.myEmissive.myRenderTarget;

		ctx->ClearDepthTarget(myFrameResources.myDepth.myRenderTarget, 0.0f, 0, 0);
		ctx->ClearRenderTargets(gbufferTargets, 4, SC_Vector4f(0.0f, 0.0f, 0.0f, 0.f));
		ctx->SetRenderTargets(gbufferTargets, 4, myFrameResources.myDepth.myRenderTarget, 0);

		SViewport viewport;
		viewport.topLeftX = 0;
		viewport.topLeftY = 0;
		viewport.width = float(device->GetResolution().x);
		viewport.height = float(device->GetResolution().y);
		viewport.minDepth = 0.f;
		viewport.maxDepth = 1.0f;

		SScissorRect scissor;
		scissor.left = 0;
		scissor.top = 0;
		scissor.right = long(device->GetResolution().x);
		scissor.bottom = long(device->GetResolution().y);

		ctx->SetViewport(viewport);
		ctx->SetScissorRect(scissor);


		struct Constants
		{
			SC_Matrix44 proj;
			SC_Matrix44 view;
			SC_Matrix44 model;
			SC_Float3 cameraPosition;
		} constants;

		constants.proj = renderData.myMainCamera.GetProjection();
		constants.view = renderData.myMainCamera.GetView();
		constants.model = renderData.mySky.myTransform;
		constants.cameraPosition = renderData.myMainCamera.GetPosition();


		if (myWorld->myTerrain)
		{
			ctx->BindConstantBuffer(&constants, sizeof(constants), 0);
			myWorld->myTerrain->Render(renderData);
		}

		if (renderData.myOpaqeObjects.Count() == 0)
			return;

		SC_GrowingArray<SVertex_ObjectInputInstance> instanceData;
		uint currentModel = renderData.myOpaqeObjects[0].myModel->GetHash();

		for (uint i = 0; i < renderData.myOpaqeObjects.Count(); ++i)
		{
			const SG_RenderObject& object = renderData.myOpaqeObjects[i];
			if (!object.myModel)
			{
				continue;
			}

			SVertex_ObjectInputInstance& data = instanceData.Add();
			data.mat0 = SC_Vector4f(object.myTransform.my11, object.myTransform.my12, object.myTransform.my13, object.myTransform.my14);
			data.mat1 = SC_Vector4f(object.myTransform.my21, object.myTransform.my22, object.myTransform.my23, object.myTransform.my24);
			data.mat2 = SC_Vector4f(object.myTransform.my31, object.myTransform.my32, object.myTransform.my33, object.myTransform.my34);
			data.mat3 = SC_Vector4f(object.myTransform.my41, object.myTransform.my42, object.myTransform.my43, object.myTransform.my44);
			data.instanceColor = SC_Vector4f(1.0f, 1.0f, 1.0f, 1.0f);

			const SG_RenderObject* nextObj = nullptr;
			if (i + 1 < renderData.myOpaqeObjects.Count())
				nextObj = &renderData.myOpaqeObjects[i + 1];

			//if (!nextObj || currentMaterial != nextObj->myMaterial->GetHash() || currentModel != nextObj->myModel->GetHash())
			{
				ctx->UpdateBufferData(sizeof(SVertex_ObjectInputInstance), instanceData.Count(), instanceData.GetBuffer(), myInstanceBuffer);

				SC_GrowingArray<CVertexBuffer> vBuffers;
				vBuffers.Add(CVertexBuffer());
				vBuffers.Add(myInstanceBuffer);
				for (uint meshIndex = 0; meshIndex < object.myModel->myMeshes.Count(); ++meshIndex)
				{
					if (!object.myModel->myMeshes[meshIndex].myIsActive)
						continue;

					ctx->BindConstantBuffer(&constants, sizeof(constants), 0);
					if (object.myModel->myMaterials[meshIndex]->Bind())
					{
						vBuffers[0] = (object.myModel->myMeshes[meshIndex].myVertexBuffer);
						ctx->SetVertexBuffer(0, vBuffers);
						ctx->SetIndexBuffer(object.myModel->myMeshes[meshIndex].myIndexBuffer);

						ctx->DrawIndexedInstanced(object.myModel->myMeshes[meshIndex].myIndexBuffer.GetNumIndices(), instanceData.Count(), 0, 0, 0);
					}
				}
				instanceData.RemoveAll();

				currentModel = object.myModel->GetHash();
			}

		}
		//if (renderData.mySky.myPSO)
		//{
		//	ctx->SetVertexBuffer(0, *renderData.mySky.myVertexBuffer);
		//	ctx->SetIndexBuffer(*renderData.mySky.myIndexBuffer);
		//	ctx->BindTexture(renderData.mySky.myTexture2, 0);
		//	ctx->BindConstantBuffer(&constants, sizeof(constants), 0);
		//	float ambLightIntensity = 1.0f;
		//	ctx->BindConstantBuffer(&ambLightIntensity, sizeof(ambLightIntensity), 1);
		//	ctx->BindGraphicsPSO(*renderData.mySky.myPSO);
		//	ctx->DrawIndexed(renderData.mySky.myIndexBuffer->GetNumIndices(), 0, 0);
		//}
		ctx->EndQuery(SQueryType_Timestamp, 3);

		ctx->EndQuery(SQueryType_Timestamp, 16);

		//myVoxelizer->RenderVoxels(renderData, myShadowRenderer);
		//if (myVoxelizeSceneCounter > 3)
		//{
		//	SC_Vector3f camPos = renderData.myMainCamera.GetPosition();
		//	if ((camPos - myVoxelizeCameraPrevPos).Length() > 0)
		//	{
		//		myVoxelizeCameraPrevPos = camPos;
		//		myVoxelizer->RenderVoxels(renderData);
		//	}
		//	myVoxelizeSceneCounter = 0;
		//}
		//++myVoxelizeSceneCounter;

		ctx->EndQuery(SQueryType_Timestamp, 17);
		{
			SC_Pair<SR_ResourceState, SR_TrackedResource*> pairs[4];
			pairs[0] = SC_Pair(SR_ResourceState_NonPixelSRV | SR_ResourceState_PixelSRV, myFrameResources.myGBuffer.myColors.myTextureBuffer);
			pairs[1] = SC_Pair(SR_ResourceState_NonPixelSRV | SR_ResourceState_PixelSRV, myFrameResources.myGBuffer.myNormals.myTextureBuffer);
			pairs[2] = SC_Pair(SR_ResourceState_NonPixelSRV | SR_ResourceState_PixelSRV, myFrameResources.myGBuffer.myMaterials.myTextureBuffer);
			pairs[3] = SC_Pair(SR_ResourceState_NonPixelSRV | SR_ResourceState_PixelSRV, myFrameResources.myGBuffer.myEmissive.myTextureBuffer);
			ctx->Transition(pairs, 4);
		}
	}
	void SG_View::Render_AmbientOcclusion()
	{
		const SG_RenderData& renderData = GetRenderData();
		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();
		SR_GraphicsDevice* device = SR_GraphicsDevice::GetDevice();

		ctx->InsertWait(renderData.myRenderGBufferEvent);

		SE_PROFILER_FUNCTION();
		SC_CPU_PROFILER_FUNCTION();

		ctx->Transition(SR_ResourceState_UnorderedAccess, myFrameResources.myAmbientOcclusion.myTextureRW->GetTextureBuffer());

		struct SSAO_Settings
		{
			SC_Matrix44 WorldToCamera;
			SC_Matrix44 InvProj;
			float SamplingRadius;
			float Scale;
			float Bias;
			float Intensity;
		} ssaoSettings;
		ssaoSettings.WorldToCamera = renderData.myMainCamera.GetView();
		ssaoSettings.InvProj = renderData.myMainCamera.GetInverseProjection();
		ssaoSettings.SamplingRadius = mySSAO.mySSAOSamplingRadius;
		ssaoSettings.Scale = mySSAO.mySSAOScale;
		ssaoSettings.Bias = mySSAO.mySSAOBias;
		ssaoSettings.Intensity = mySSAO.mySSAOIntensity;

		SViewport viewport;
		viewport.topLeftX = 0;
		viewport.topLeftY = 0;
		viewport.width = float(device->GetResolution().x);
		viewport.height = float(device->GetResolution().y);
		viewport.minDepth = 0.f;
		viewport.maxDepth = 1.0f;

		SScissorRect scissor;
		scissor.left = 0;
		scissor.top = 0;
		scissor.right = long(device->GetResolution().x);
		scissor.bottom = long(device->GetResolution().y);
		ctx->SetScissorRect(scissor);
		ctx->SetViewport(viewport);

		ctx->BindConstantBuffer(&ssaoSettings, sizeof(ssaoSettings), 0);
		ctx->BindTexture(myFrameResources.myDepth.myTexture, TEXSLOT_SCENE_DEPTH);
		ctx->BindTexture(myFrameResources.myGBuffer.myNormals.myTexture, TEXSLOT_GBUFFER_NORMALS);
		ctx->BindTexture(mySSAO.mySSAORand, TEXSLOT_SSAO_RAND);
		ctx->BindTextureRW(myFrameResources.myAmbientOcclusion.myTextureRW, 0);
		ctx->BindComputePSO(*mySSAO.mySSAO);

		const SC_Vector2f fullResolution = SC_EngineInterface::GetResolution();
		ctx->EndQuery(SQueryType_Timestamp, 4);
		ctx->Dispatch((uint)ceil(fullResolution.x / 8.f), (uint)ceil(fullResolution.y / 8.f), 1);

		//ctx->BindTextureRW(myFrameResources.myAmbientOcclusion.myTextureRW, 0);
		//ctx->BindComputePSO(*mySSAO.mySSAOBlur);
		//ctx->Dispatch((uint)ceil(fullResolution.x / 8.f), (uint)ceil(fullResolution.y / 8.f), 1);
		ctx->EndQuery(SQueryType_Timestamp, 5);

		ctx->Transition(SR_ResourceState_NonPixelSRV, myFrameResources.myAmbientOcclusion.myTextureRW->GetTextureBuffer());
	}

	void SG_View::Render_Lights()
	{
		const SG_RenderData& renderData = GetRenderData();

		// Bind G-buffer for scene-data
		// Draw geometry-based on light-type (Quad for directional, sphere for point etc.)
		// Use appropriate shader based on light-type.
		// Later on Raytrace these lights

		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();

		ctx->InsertWait(renderData.myRenderShadowsEvent);
		ctx->InsertWait(renderData.myRenderSkyProbeEvent);
		ctx->InsertWait(renderData.myRenderGBufferEvent);
		ctx->InsertWait(renderData.myRenderSSAOEvent);

		SE_PROFILER_FUNCTION();
		SC_CPU_PROFILER_FUNCTION();
		ctx->EndQuery(SQueryType_Timestamp, 6);

		const SC_Float2 fullResolution = SC_Vector2f((float)SC_EngineInterface::GetResolution().x, (float)SC_EngineInterface::GetResolution().y);
		ctx->Transition(SR_ResourceState_NonPixelSRV, myFrameResources.myDepth.myTextureBuffer);
		ctx->Transition(SR_ResourceState_UnorderedAccess, myFrameResources.myFullscreenHDR.myTextureBuffer);

		if (mySettings.myRenderSky)
			RenderSky();

		struct CameraConstants
		{
			SC_Matrix44 invProj;
			SC_Matrix44 invView;
			SC_Matrix44 shadowWorldToClip[SG_Shadows::ourNumShadowCascades];
			SC_Vector4f ShadowSampleRotation;
			SC_Float3 cameraPosition;
			float pcfFilter;
		} cameraConstants;
		cameraConstants.invProj = renderData.myMainCamera.GetInverseProjection();
		cameraConstants.invView = renderData.myMainCamera.GetInverseView();

		for (uint i = 0; i < SG_Shadows::ourNumShadowCascades; ++i)
		{
			cameraConstants.shadowWorldToClip[i] = myShadows->GetShadowProjection(i);
		}
		cameraConstants.ShadowSampleRotation = SC_Vector4f(1.0f, 0.0, 0.0, 1.0);
		cameraConstants.cameraPosition = renderData.myMainCamera.GetPosition();
		cameraConstants.pcfFilter = myShadows->GetPCFFilterOffset();

		struct LightConstants
		{
			SC_Float3 SkyLightColor;
			float SkyIntensity;
			SC_Float3 SkyLightToDirection;
			float AmbientLightIntensity;
			SC_Float3 PointLightColor;
			float PointLightRange;
			SC_Float3 PointLightPos;
			float PointLightIntensity;

		} lightConstants;
		lightConstants.SkyLightColor = myWorld->mySky->mySunLightColor;
		lightConstants.SkyIntensity = 1.0f; // myWorld->mySky->mySunIntensity;
		lightConstants.SkyLightToDirection = myWorld->mySky->GetToSunDirection();
		lightConstants.AmbientLightIntensity = 0.75f;
		lightConstants.PointLightPos = renderData._PL.PointLightPos;
		lightConstants.PointLightColor = renderData._PL.PointLightColor;
		lightConstants.PointLightRange = renderData._PL.PointLightRange;
		lightConstants.PointLightIntensity = 0.f;// renderData._PL.PointLightIntensity;

		ctx->BindConstantBuffer(&cameraConstants, sizeof(cameraConstants), CBSLOT_SHADOW_CONSTANTS);
		ctx->BindConstantBuffer(&lightConstants, sizeof(lightConstants), 1);


		/// TEMP
		struct SVoxelizerSettings
		{
			SC_Float3 myVoxelCenter;
			float myVoxelTextureSize = 0;
			float myVoxelSize = 0; // Voxel Size in meters
		} voxelSettings;
		ctx->BindConstantBuffer(&voxelSettings, sizeof(voxelSettings), 2);
		///

		ctx->BindTexture(myFrameResources.myDepth.myTexture, TEXSLOT_SCENE_DEPTH);
		ctx->BindTexture(myFrameResources.myGBuffer.myColors.myTexture, TEXSLOT_GBUFFER_COLOR);
		ctx->BindTexture(myFrameResources.myGBuffer.myNormals.myTexture, TEXSLOT_GBUFFER_NORMALS);
		ctx->BindTexture(myFrameResources.myGBuffer.myMaterials.myTexture, TEXSLOT_GBUFFER_ARM);
		ctx->BindTexture(myFrameResources.myGBuffer.myEmissive.myTexture, TEXSLOT_GBUFFER_EMISSION);
		//myVoxelizer->BindVoxelScene();

		if (myFrameResources.mySkyProbeTexture)
			//ctx->BindTexture(renderData.mySky.myTexture, TEXSLOT_SKY_DIFFUSE);
			ctx->BindTexture(myFrameResources.mySkyProbeTexture, TEXSLOT_SKY_DIFFUSE);
		else
			ctx->BindTexture(SR_RenderInterface::ourBlack4x4, TEXSLOT_SKY_DIFFUSE);
		if (myFrameResources.mySkyProbeTexture)
			//ctx->BindTexture(renderData.mySky.myTexture2, TEXSLOT_SKY_SPECULAR);
			ctx->BindTexture(myFrameResources.mySkyProbeTexture, TEXSLOT_SKY_SPECULAR);
		else
			ctx->BindTexture(SR_RenderInterface::ourBlack4x4, TEXSLOT_SKY_SPECULAR);
		if (renderData.mySky.myTexture3)
			ctx->BindTexture(renderData.mySky.myTexture3, TEXSLOT_SKY_BRDF);
		else
			ctx->BindTexture(SR_RenderInterface::ourBlack4x4, TEXSLOT_SKY_BRDF);

		ctx->BindTexture(myShadows->GetSunShadowMap(), TEXSLOT_SUN_SHADOWMAP_CSM);
		ctx->BindTexture(myShadows->GetShadowNoiseTexture(), TEXSLOT_SUN_SHADOWMAP_NOISE);
		ctx->BindTexture((mySettings.myEnableSSAO) ? myFrameResources.myAmbientOcclusion.myTexture : SR_RenderInterface::ourWhite4x4, 7);
		ctx->BindTextureRW(myFrameResources.myFullscreenHDR.myTextureRW, 0);
		ctx->BindComputePSO(*myLighting.myLightingShader);
		ctx->Dispatch((uint)ceil(fullResolution.x / 8.f), (uint)ceil(fullResolution.y / 8.f), 1);

		ctx->Transition(SR_ResourceState_DepthWrite, myFrameResources.myDepth.myTextureBuffer);
		ctx->Transition(SR_ResourceState_NonPixelSRV, myFrameResources.myFullscreenHDR.myTextureBuffer);

		ctx->EndEvent();
		ctx->EndQuery(SQueryType_Timestamp, 7);
	}

	void SG_View::Render_Forward()
	{
	}

	void SG_View::Render_PostFX()
	{
		const SG_RenderData& renderData = GetRenderData();
		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();
		ctx->InsertWait(renderData.myRenderLightsEvent);

		SE_PROFILER_FUNCTION();
		SC_CPU_PROFILER_FUNCTION();
		ctx->EndQuery(SQueryType_Timestamp, 8);

		const SC_Float2 fullResolution = SC_Vector2f((float)SC_EngineInterface::GetResolution().x, (float)SC_EngineInterface::GetResolution().y);
		const SC_Float2 halfResolution = SC_Vector2f(fullResolution.x * 0.5f, fullResolution.y * 0.5f);

		struct ScreenConstants
		{
			float ExposureBias;
			float BloomIntensity;
		} screenConstants;
		screenConstants.ExposureBias = myPostFX.myExposure;
		screenConstants.BloomIntensity = myPostFX.myBloomIntensity;

		if (myPostFX.myUseBloom)
		{
			// Bloom Luminance
			{
				SE_PROFILER_FUNCTION_TAGGED("Bloom Luminance");

				struct BloomSettings
				{
					float BloomThreshold;
				} bloomSettings;
				bloomSettings.BloomThreshold = myPostFX.myBloomThreshold;

				ctx->Transition(SR_ResourceState_UnorderedAccess, myFrameResources.myBloomLuminance.myTextureBuffer);

				ctx->BindTexture(myFrameResources.myFullscreenHDR.myTexture, 0);
				ctx->BindTextureRW(myFrameResources.myBloomLuminance.myTextureRW, 0); // Outputs half-size texture
				ctx->BindConstantBuffer(&bloomSettings, sizeof(bloomSettings), 0);

				ctx->BindComputePSO(*myPostFX.myBloomLuminancePSO);
				ctx->Dispatch((uint)ceil(halfResolution.x / 8.f), (uint)ceil(halfResolution.y / 8.f), 1);
				ctx->Transition(SR_ResourceState_NonPixelSRV, myFrameResources.myBloomLuminance.myTextureBuffer);

				// Downscale to quarter
				ctx->BindTexture(myFrameResources.myBloomLuminance.myTexture, 0);
				ctx->BindTextureRW(myFrameResources.myBloomLuminanceQuarter.myTextureRW, 0);

				ctx->BindComputePSO(*myPostFX.myCopyPSO);
				ctx->Dispatch((uint)ceil((halfResolution.x * 0.5f) / 8.f), (uint)ceil((halfResolution.y * 0.5f) / 8.f), 1);
				ctx->Transition(SR_ResourceState_NonPixelSRV, myFrameResources.myBloomLuminanceQuarter.myTextureBuffer);

			}
			// Bloom Blur
			{
				SE_PROFILER_FUNCTION_TAGGED("Bloom Blur");

				struct BloomSettings
				{
					SC_Float2 BlurDirection;
				} bloomSettings;
				bloomSettings.BlurDirection = SC_Vector2f(1.0f, 0.0f);

				ctx->BindTexture(myFrameResources.myBloomLuminance.myTexture, 0);
				ctx->BindTexture(myFrameResources.myBloomLuminanceQuarter.myTexture, 1);
				ctx->BindTextureRW(myFrameResources.myBloomBlur0.myTextureRW, 0);
				ctx->BindTextureRW(myFrameResources.myBloomBlurQuarter0.myTextureRW, 1);
				ctx->BindConstantBuffer(&bloomSettings, sizeof(bloomSettings), 0);

				ctx->BindComputePSO(*myPostFX.myBloomBlurPSO);
				ctx->Dispatch((uint)ceil(halfResolution.x / 8.f), (uint)ceil(halfResolution.y / 8.f), 1);

				ctx->Transition(SR_ResourceState_NonPixelSRV, myFrameResources.myBloomBlur0.myTextureBuffer);
				ctx->Transition(SR_ResourceState_NonPixelSRV, myFrameResources.myBloomBlurQuarter0.myTextureBuffer);

				ctx->BindTexture(myFrameResources.myBloomBlur0.myTexture, 0);
				ctx->BindTexture(myFrameResources.myBloomBlurQuarter0.myTexture, 1);

				ctx->BindTextureRW(myFrameResources.myBloomBlur1.myTextureRW, 0);
				ctx->BindTextureRW(myFrameResources.myBloomBlurQuarter1.myTextureRW, 1);

				bloomSettings.BlurDirection = SC_Vector2f(0.0f, 1.0f);
				ctx->BindConstantBuffer(&bloomSettings, sizeof(bloomSettings), 0);

				ctx->BindComputePSO(*myPostFX.myBloomBlurPSO);
				ctx->Dispatch((uint)ceil(halfResolution.x / 8.f), (uint)ceil(halfResolution.y / 8.f), 1);

				// Run blur again before add!!!
				ctx->Transition(SR_ResourceState_UnorderedAccess, myFrameResources.myBloomBlur0.myTextureBuffer);
				ctx->Transition(SR_ResourceState_UnorderedAccess, myFrameResources.myBloomBlurQuarter0.myTextureBuffer);

				ctx->BindTexture(myFrameResources.myBloomBlur1.myTexture, 0);
				ctx->BindTexture(myFrameResources.myBloomBlurQuarter1.myTexture, 1);

				ctx->BindTextureRW(myFrameResources.myBloomBlur0.myTextureRW, 0);
				ctx->BindTextureRW(myFrameResources.myBloomBlurQuarter0.myTextureRW, 1);

				bloomSettings.BlurDirection = SC_Vector2f(1.0f, 0.0f);
				ctx->BindConstantBuffer(&bloomSettings, sizeof(bloomSettings), 0);

				ctx->BindComputePSO(*myPostFX.myBloomBlurPSO);
				ctx->Dispatch((uint)ceil(halfResolution.x / 8.f), (uint)ceil(halfResolution.y / 8.f), 1);

				ctx->BindTexture(myFrameResources.myBloomBlur0.myTexture, 0);
				ctx->BindTexture(myFrameResources.myBloomBlurQuarter0.myTexture, 1);

				ctx->BindTextureRW(myFrameResources.myBloomBlur1.myTextureRW, 0);
				ctx->BindTextureRW(myFrameResources.myBloomBlurQuarter1.myTextureRW, 1);

				bloomSettings.BlurDirection = SC_Vector2f(0.0f, 1.0f);
				ctx->BindConstantBuffer(&bloomSettings, sizeof(bloomSettings), 0);

				ctx->BindComputePSO(*myPostFX.myBloomBlurPSO);
				ctx->Dispatch((uint)ceil(halfResolution.x / 8.f), (uint)ceil(halfResolution.y / 8.f), 1);


				// Add blurred textures
				SC_Pair<SR_ResourceState, SR_TrackedResource*> pairs[3];
				pairs[0] = SC_Pair(SR_ResourceState_NonPixelSRV, myFrameResources.myBloomBlur1.myTextureBuffer);
				pairs[1] = SC_Pair(SR_ResourceState_NonPixelSRV, myFrameResources.myBloomBlurQuarter1.myTextureBuffer);
				pairs[2] = SC_Pair(SR_ResourceState_UnorderedAccess, myFrameResources.myBloomFinal.myTextureBuffer);

				ctx->Transition(pairs, 3);

				ctx->BindTexture(myFrameResources.myBloomBlur1.myTexture, 0);
				ctx->BindTexture(myFrameResources.myBloomBlurQuarter1.myTexture, 1);
				ctx->BindTextureRW(myFrameResources.myBloomFinal.myTextureRW, 0);

				ctx->BindComputePSO(*myPostFX.myAddPSO);
				ctx->Dispatch((uint)ceil(halfResolution.x / 8.f), (uint)ceil(halfResolution.y / 8.f), 1);

				ctx->Transition(SR_ResourceState_NonPixelSRV, myFrameResources.myBloomFinal.myTextureBuffer);

				ctx->EndQuery(SQueryType_Timestamp, 9);
			}
		}
		// Tonemap
		{
			SE_PROFILER_FUNCTION_TAGGED("Tonemap");
			// Abstract into a PostFX-renderer
			//
			myPostEffects.CalculateLuminance(this, myFrameResources.myFullscreenHDR.myTexture);

			ctx->EndQuery(SQueryType_Timestamp, 10);
			ctx->Transition(SR_ResourceState_UnorderedAccess, myFrameResources.myFullscreen.myTextureBuffer);

			ctx->BindTexture(myFrameResources.myFullscreenHDR.myTexture, 0);
			ctx->BindTexture(myPostFX.myUseBloom ? myFrameResources.myBloomFinal.myTexture : SR_RenderInterface::ourBlack4x4, 1);
			ctx->BindTexture(myFrameResources.myAvgLuminance.myTexture, 2);
			ctx->BindTextureRW(myFrameResources.myFullscreen.myTextureRW, 0);
			ctx->BindConstantBuffer(&screenConstants, sizeof(screenConstants), 0);
			//
			ctx->BindComputePSO(*myPostFX.myTonemapPSO);
			ctx->Dispatch((uint)ceil(fullResolution.x / 8.f), (uint)ceil(fullResolution.y / 8.f), 1);

			ctx->EndQuery(SQueryType_Timestamp, 11);
		}

		ctx->EndQuery(SQueryType_Timestamp, 12);
		if (myPostFX.myUseFXAA)
		{
			SE_PROFILER_FUNCTION_TAGGED("FXAA");

			SR_GraphicsDevice* device = SR_GraphicsDevice::GetDevice();
			
			ctx->Transition(SR_ResourceState_GenericRead, myFrameResources.myFullscreen.myTextureBuffer);
			ctx->Transition(SR_ResourceState_RenderTarget, myFrameResources.myFullscreen2.myTextureBuffer);
			SViewport viewport;
			viewport.topLeftX = 0;
			viewport.topLeftY = 0;
			viewport.width = float(device->GetResolution().x);
			viewport.height = float(device->GetResolution().y);
			viewport.minDepth = 0.f;
			viewport.maxDepth = 1.0f;

			SScissorRect scissor;
			scissor.left = 0;
			scissor.top = 0;
			scissor.right = long(device->GetResolution().x);
			scissor.bottom = long(device->GetResolution().y);

			ctx->SetScissorRect(scissor);
			ctx->SetViewport(viewport);
			ctx->SetVertexBuffer(0, myPostFX.myFullscreenQuadBuffer);
			ctx->SetIndexBuffer(myPostFX.myFullscreenQuadIBuffer);
			SR_RenderTarget* rt = myFrameResources.myFullscreen2.myRenderTarget;
			ctx->SetRenderTargets(&rt, 1, nullptr, 0);
			ctx->SetTopology(SR_Topology_TriangleList);
			ctx->BindTexture(myFrameResources.myFullscreen.myTexture, 0);
			ctx->BindGraphicsPSO(*myPostFX.myAAPSO);
			ctx->DrawIndexed(myPostFX.myFullscreenQuadIBuffer.GetNumIndices(), 0, 0);
		}
		else
		{
			ctx->CopyTexture(myFrameResources.myFullscreen2.myTextureBuffer, myFrameResources.myFullscreen.myTextureBuffer);
		}
		ctx->EndQuery(SQueryType_Timestamp, 13);
		ctx->ResolveQuery(SQueryType_Timestamp, 0, 18, myTimestampBuffer);

		// Motion Blur
		// Temporal AA
	}

	void SG_View::Render_UI()
	{
		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();
		const SG_RenderData& renderData = GetRenderData();

		ctx->InsertWait(renderData.myRenderPostFXEvent);

		SE_PROFILER_FUNCTION_TAGGED("UI");
		SC_CPU_PROFILER_FUNCTION();

		SR_GraphicsDevice* device = SR_GraphicsDevice::GetDevice();

		SViewport viewport;
		viewport.topLeftX = 0;
		viewport.topLeftY = 0;
		viewport.width = float(device->GetResolution().x);
		viewport.height = float(device->GetResolution().y);
		viewport.minDepth = 0.f;
		viewport.maxDepth = 1.0f;

		SScissorRect scissor;
		scissor.left = 0;
		scissor.top = 0;
		scissor.right = long(device->GetResolution().x);
		scissor.bottom = long(device->GetResolution().y);

		ctx->SetScissorRect(scissor);
		ctx->SetViewport(viewport);

		ctx->Transition(SR_ResourceState_RenderTarget, myFrameResources.myFullscreen2.myTextureBuffer);
		SR_RenderTarget* rt = myFrameResources.myFullscreen2.myRenderTarget;
		ctx->SetRenderTargets(&rt, 1, myFrameResources.myDepth.myRenderTarget, 0);

		RenderDebugPrimitives();
	}

	void SG_View::Render_Compose()
	{
		SR_GraphicsContext* ctx = SR_GraphicsContext::GetCurrent();
		const SG_RenderData& renderData = GetRenderData();

		ctx->InsertWait(renderData.myRenderUIEvent);

		SE_PROFILER_FUNCTION_TAGGED("Compose");
		SC_CPU_PROFILER_FUNCTION();

		const uint8 historyIndex = uint8(myCurrentFrameIndex % 4);

		ctx->CopyTexture(myFrameResources.myScreenHistory[historyIndex].myTextureBuffer, myFrameResources.myFullscreen2.myTextureBuffer);
		ctx->Transition(SR_ResourceState_GenericRead, myFrameResources.myScreenHistory[historyIndex].myTextureBuffer);

		SC_Atomic_Increment(myCurrentFrameIndex);
	}

	void SG_View::RenderSky()
	{
		SE_PROFILER_FUNCTION_TAGGED("Render Sky");
		myWorld->mySky->Render(this);
	}

	void SG_View::RenderDebugPrimitives()
	{
		SE_PROFILER_FUNCTION_TAGGED("Render Debug Primitives");
#if ENABLE_DEBUG_PRIMITIVES
		SG_DebugPrimitives::Get()->RenderPrimitives(this);
#endif
	}
}