#include "SGraphics_Precompiled.h"
#include "SG_MaterialFactory.h"

#include "SR_ShaderCompiler.h"

#include <sstream>

namespace Shift
{
	static const std::string locMaterialDir("Materials/");

	namespace CMaterialFactor_Private 
	{
		struct Entry
		{
			SG_Material myMaterial;
			uint myRefCount = 0;
		};
		static std::map<uint, Entry> myMaterials;
	}

	std::string HashToHexString(uint aHash) 
	{
		std::stringstream stream;
		stream << std::hex << "0x" << aHash;
		std::string result(stream.str());
		return result;
	}

	SG_MaterialFactory* SG_MaterialFactory::ourInstance = nullptr;

	SG_MaterialFactory::SG_MaterialFactory()
	{
		std::filesystem::create_directory("Assets/Generated/Materials/");
	}


	SG_MaterialFactory::~SG_MaterialFactory()
	{

	}
	void SG_MaterialFactory::GetOrCreateMaterialFromCache(const SG_MaterialDesc& aDesc, SG_Material* aMaterial)
	{
		uint hash = aDesc.SC_Hash();
		const auto& material = CMaterialFactor_Private::myMaterials.find(hash);

		if (material == CMaterialFactor_Private::myMaterials.end())
		{
			if (aDesc.myFilename.empty())
				CMaterialFactor_Private::myMaterials[hash].myMaterial.Init_Internal(aDesc);
			else
				CMaterialFactor_Private::myMaterials[hash].myMaterial.InitFromFile_Internal(aDesc);
		}

		++CMaterialFactor_Private::myMaterials[hash].myRefCount;
		SG_Material& cachedMaterial = CMaterialFactor_Private::myMaterials[hash].myMaterial;
		aMaterial->myARM = cachedMaterial.myARM;
		aMaterial->myBase = cachedMaterial.myBase;
		aMaterial->myBaseDepthTest = cachedMaterial.myBaseDepthTest;
		aMaterial->myColor = cachedMaterial.myColor;
		aMaterial->myProperties = cachedMaterial.myProperties;
		aMaterial->myTextures = cachedMaterial.myTextures;
		aMaterial->SC_Hash();

		//if (!MaterialFileCacheExists(hash))
		//	ExportMaterial(aMaterial);
	}
	void SG_MaterialFactory::RemoveCacheRef(uint aHash)
	{
		const auto& material = CMaterialFactor_Private::myMaterials.find(aHash);
		if (material != CMaterialFactor_Private::myMaterials.end())
		{
			--material->second.myRefCount;
			if (material->second.myRefCount == 0)
			{
				CMaterialFactor_Private::myMaterials.erase(material);
			}
		}
	}

	SC_Ref<SG_Material2> SG_MaterialFactory::GetCreateMaterial(const char* /*aPath*/)
	{
		SG_MaterialDesc2 desc;

		// Read desc from material file.

		return GetCreateMaterial(desc);
	}

	SC_Ref<SG_Material2> SG_MaterialFactory::GetCreateMaterial(const SG_MaterialDesc2& aDesc)
	{
		uint materialHash = aDesc.GetHash();
		if (ourInstance->myCache.find(materialHash) != ourInstance->myCache.end())
			return ourInstance->myCache[materialHash];

		SR_GraphicsDevice* device = SR_GraphicsDevice::GetDevice();
		SR_ShaderCompiler* shaderCompiler = device->GetShaderCompiler();
		SC_Ref<SG_Material2> material = new SG_Material2();

		SC_Ref<SR_ShaderByteCode> vertexShader = shaderCompiler->CompileShaderFromFile(aDesc.myVertexShader.c_str(), SR_ShaderType_Vertex);

		if (aDesc.myIsOpaque)
		{
			// Depth shader
			SR_ShaderStateDesc shaderStateDesc;
			shaderStateDesc.myTopology = SR_Topology_TriangleList;
			shaderStateDesc.myDepthStateDesc.myDepthComparisonFunc = SR_ComparisonFunc_GreaterEqual;
			shaderStateDesc.myDepthStateDesc.myWriteDepth = true;
			shaderStateDesc.myRasterizerStateDesc.mySlopedScaleDepthBias = -2.0f;
			shaderStateDesc.myRasterizerStateDesc.myDepthBias = -100;
			shaderStateDesc.myRasterizerStateDesc.myEnableDepthClip = false;
			shaderStateDesc.myRenderTargetFormats.myNumColorFormats = 0;

			shaderStateDesc.myShaderByteCodes[SR_ShaderType_Vertex] = vertexShader;
			shaderStateDesc.myShaderByteCodes[SR_ShaderType_Pixel] = shaderCompiler->CompileShaderFromFile("ShiftEngine/Shaders/SGraphics/TEMP_SHADERS/ObjectDepthTestPS.ssf", SR_ShaderType_Pixel);

			material->myShaders[SG_RenderType_Depth] = device->CreateShaderState(shaderStateDesc);
		}

		if (aDesc.myNeedsForward)
		{
			// Color shader
			SR_ShaderStateDesc shaderStateDesc;
			shaderStateDesc.myShaderByteCodes[SR_ShaderType_Vertex] = vertexShader;
			shaderStateDesc.myShaderByteCodes[SR_ShaderType_Pixel] = shaderCompiler->CompileShaderFromFile(aDesc.myPixelShader.c_str(), SR_ShaderType_Pixel);
			material->myShaders[SG_RenderType_Color] = device->CreateShaderState(shaderStateDesc);
		}

		if (aDesc.myNeedsGBuffer)
		{
			// GBuffer shader
			SR_ShaderStateDesc shaderStateDesc;
			shaderStateDesc.myDepthStateDesc.myDepthComparisonFunc = SR_ComparisonFunc_GreaterEqual;
			shaderStateDesc.myDepthStateDesc.myWriteDepth = true;
			shaderStateDesc.myRenderTargetFormats.myNumColorFormats = 4;
			shaderStateDesc.myRenderTargetFormats.myColorFormats[0] = SR_Format_RGBA8_Unorm; // Color
			shaderStateDesc.myRenderTargetFormats.myColorFormats[1] = SR_Format_RG11B10_Float; // Normals
			shaderStateDesc.myRenderTargetFormats.myColorFormats[2] = SR_Format_RGBA8_Unorm; // ARM
			shaderStateDesc.myRenderTargetFormats.myColorFormats[3] = SR_Format_RGBA8_Unorm; // Emissive + Fog

			for (uint i = 0; i < 4; ++i)
			{
				shaderStateDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].mySrcBlend = SR_BlendMode_One;
				shaderStateDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].myDstBlend = SR_BlendMode_Zero;
				shaderStateDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].mySrcBlendAlpha = SR_BlendMode_One;
				shaderStateDesc.myBlendStateDesc.myRenderTagetBlendDescs[i].myDstBlendAlpha = SR_BlendMode_Zero;
			}

			shaderStateDesc.myShaderByteCodes[SR_ShaderType_Vertex] = vertexShader;
			shaderStateDesc.myShaderByteCodes[SR_ShaderType_Pixel] = shaderCompiler->CompileShaderFromFile(aDesc.myPixelShader.c_str(), SR_ShaderType_Pixel);
			material->myShaders[SG_RenderType_GBuffer] = device->CreateShaderState(shaderStateDesc);
		}

		// Textures
		material->myTextures.PreAllocate(aDesc.myTextures.Count());
		for (uint i = 0; i < aDesc.myTextures.Count(); ++i)
			material->myTextures.Add(device->GetCreateTexture(aDesc.myTextures[i].c_str()));

		ourInstance->myCache[materialHash] = material;
		return material;
	}

	void SG_MaterialFactory::ExportMaterial(SG_Material* /*aMaterial*/)
	{
		//std::string hex = HashToHexString(aMaterial->GetHash());
		//std::string materialPath(locMaterialDir + hex + ".smat");
		//
		//uint8* data = reinterpret_cast<uint8*>(&aMaterial->myProperties);
		//uint size = aMaterial->myProperties.GetSize();
		//
		//std::ofstream outStream(materialPath, std::ios::binary);
		//if (outStream.is_open())
		//{
		//
		//}
	}
	void SG_MaterialFactory::ImportMaterial(uint /*aHash*/, SG_Material* /*aOutMaterial*/)
	{
	}
	bool SG_MaterialFactory::MaterialFileCacheExists(uint /*aHash*/)
	{
		//std::string hex = HashToHexString(aHash);
		//std::string materialPath(locMaterialDir + hex + ".smat");
		//
		//if (std::filesystem::exists(materialPath))
		//	return true;
		//
		return false;
	}
}
