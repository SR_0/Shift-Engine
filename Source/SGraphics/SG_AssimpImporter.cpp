#include "SGraphics_Precompiled.h"
#include "SG_AssimpImporter.h"

#include "SG_Model.h"
#include "SG_Material.h"

#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

namespace Shift
{
	CAssimpImporter::CAssimpImporter()
	{
	}
	CAssimpImporter::~CAssimpImporter()
	{
	}
	bool CAssimpImporter::Import(SG_Model& aModelOut, const char* aFile, bool aShouldInvert)
	{
		Assimp::Importer importer;

		unsigned int flags = aiProcess_CalcTangentSpace | aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_SortByPType | aiProcess_ConvertToLeftHanded;

		if (aShouldInvert)
			flags &= ~aiProcess_FlipWindingOrder;

		const aiScene* scene = importer.ReadFile(aFile, flags);

		if (!scene)
		{
			SC_ERROR_LOG(importer.GetErrorString());
			return false;
		}

		if (scene->HasMaterials())
		{
			//GenerateMaterials();
		}
		if (!scene->HasMeshes())
		{
			SC_ERROR_LOG("Model-file contains no meshes [%s]", aFile);
			return false;
		}
		ProcessAiNode(aModelOut, scene->mRootNode, scene, nullptr);

		WriteMeshFile(aModelOut, aFile);

		return true;
	}

	void CAssimpImporter::WriteMeshFile(const SG_Model& aModel, const char* aMeshName)
	{
		aModel;
		aMeshName;
	}

	void CAssimpImporter::ProcessAiNode(SG_Model& aModelOut, aiNode* aNode, const aiScene* aScene, SG_Model* aParent)
	{
		aModelOut.myParent = aParent;
		uint numMeshes = aNode->mNumMeshes;
		if (numMeshes > 0)
		{
			aModelOut.myMeshes.PreAllocate(aModelOut.myMeshes.Count() + numMeshes);
			for (unsigned int n = 0; n < numMeshes; ++n)
			{
				const aiMesh* mesh = aScene->mMeshes[aNode->mMeshes[n]];
				const aiMaterial* aiMaterial = aScene->mMaterials[mesh->mMaterialIndex];
				GenerateMaterial(aModelOut, aiMaterial);
				uint materialHash = aModelOut.myMaterials.GetLast()->GetHash();
				const uint numIndices = mesh->mNumFaces * 3; // Assume 3 indices per face.
				uint totalBufferSize = ((mesh->mNumVertices * sizeof(SVertex_Simple)) + (numIndices * sizeof(uint)));

				// Vertices
				SC_AABB meshAABB;
				meshAABB.myMax = SC_Vector3f(SC_FLT_LOWEST);
				meshAABB.myMin = SC_Vector3f(SC_FLT_MAX);
				SVertex_Simple* vertexList = new SVertex_Simple[mesh->mNumVertices];
				for (uint i = 0; i < mesh->mNumVertices; ++i)
				{
					vertexList[i].position = SC_Vector4f(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z, 1.0f);
					
					if (vertexList[i].position.x < meshAABB.myMin.x)
						meshAABB.myMin.x = vertexList[i].position.x;
					else if(vertexList[i].position.x > meshAABB.myMax.x)
						meshAABB.myMax.x = vertexList[i].position.x;

					if (vertexList[i].position.y < meshAABB.myMin.y)
						meshAABB.myMin.y = vertexList[i].position.y;
					else if (vertexList[i].position.y > meshAABB.myMax.y)
						meshAABB.myMax.y = vertexList[i].position.y;

					if (vertexList[i].position.z < meshAABB.myMin.z)
						meshAABB.myMin.z = vertexList[i].position.z;
					else if (vertexList[i].position.z > meshAABB.myMax.z)
						meshAABB.myMax.z = vertexList[i].position.z;

					if (mesh->HasTextureCoords(0))
					{
						const aiVector3D& uv = mesh->mTextureCoords[0][i];
						vertexList[i].uv = SC_Vector2f(uv.x, uv.y);
					}
					else
						vertexList[i].uv = SC_Vector2f(0.5f, 0.5f);
					if (mesh->HasNormals())
					{
						const aiVector3D& normal = mesh->mNormals[i];
						vertexList[i].normal = SC_Vector3f(normal.x, normal.y, normal.z);
					}
					else
						vertexList[i].normal = SC_Vector3f(0.0f, 0.0f, 0.0f);
					if (mesh->HasTangentsAndBitangents())
					{
						const aiVector3D& tangent = mesh->mTangents[i];
						vertexList[i].tangent = SC_Vector3f(tangent.x, tangent.y, tangent.z);
						const aiVector3D& binormal = mesh->mBitangents[i];
						vertexList[i].binormal = SC_Vector3f(binormal.x, binormal.y, binormal.z);
					}
					else
					{
						vertexList[i].tangent = SC_Vector3f(0.0f, 0.0f, 0.0f);
						vertexList[i].binormal = SC_Vector3f(0.0f, 0.0f, 0.0f);
					}
					if (mesh->HasVertexColors(0))
					{
						const aiColor4D& color = mesh->mColors[0][i];
						vertexList[i].color = SC_Vector4f(color.r, color.g, color.b, color.a);
					}
					else
						vertexList[i].color = SC_Vector4f(1.0f, 1.0f, 1.0f, 1.0f);
				}
				// Indices
				uint* indices = new uint[numIndices];
				uint indexNum = 0;
				for (uint i = 0; i < mesh->mNumFaces; ++i)
				{
					const aiFace& face = mesh->mFaces[i];
					assert(face.mNumIndices == 3 && "Mesh isn't triangulated.");

					for (uint index = 0; index < face.mNumIndices; ++index)
					{
						indices[indexNum++] = face.mIndices[index];
					}
				}
				SG_Model::ModelBuffer& modelBuf = aModelOut.myMeshes.Add();
				modelBuf.myAABB = meshAABB;
				modelBuf.myName = mesh->mName.C_Str();
				modelBuf.myMaterialHash = materialHash;
				modelBuf.myBuffer = new uint8[totalBufferSize];
				modelBuf.myBufferByteSize = totalBufferSize;
				modelBuf.myVertexByteOffset = 0;
				modelBuf.myVertexSize = sizeof(SVertex_Simple);
				modelBuf.myNumVertices = mesh->mNumVertices;
				memcpy(modelBuf.myBuffer, vertexList, mesh->mNumVertices * sizeof(SVertex_Simple));

				modelBuf.myIndexByteOffset = (mesh->mNumVertices * sizeof(SVertex_Simple));
				modelBuf.myNumIndices = numIndices;
				memcpy(modelBuf.myBuffer + modelBuf.myIndexByteOffset, indices, numIndices * sizeof(uint));

				delete[] indices;
				delete[] vertexList;

				SVertexBufferDesc vbDesc2;
				vbDesc2.myVertexData = modelBuf.myBuffer;
				vbDesc2.myNumVertices = modelBuf.myNumVertices;
				vbDesc2.myStride = modelBuf.myVertexSize;
				SR_GraphicsDevice::GetDevice()->CreateVertexBuffer(vbDesc2, &modelBuf.myVertexBuffer);

				std::vector<uint> ilist;
				uint* meshIndices = reinterpret_cast<uint*>(modelBuf.myBuffer + modelBuf.myIndexByteOffset);
				for (uint i = 0; i < modelBuf.myNumIndices; ++i)
				{
					ilist.push_back(meshIndices[i]);
				}
				SR_GraphicsDevice::GetDevice()->CreateIndexBuffer(ilist, &modelBuf.myIndexBuffer);
			}
		}

		if (aNode->mNumChildren > 0)
		{
			if (numMeshes == 0)
			{
				for (unsigned int n = 0; n < aNode->mNumChildren; ++n)
				{
					ProcessAiNode(aModelOut, aNode->mChildren[n], aScene, aParent);
				}
			}
			else
			{
				aModelOut.myChildren.resize(aNode->mNumChildren);
				for (unsigned int n = 0; n < aNode->mNumChildren; ++n)
				{
					ProcessAiNode(aModelOut.myChildren[n], aNode->mChildren[n], aScene, &aModelOut);
				}
			}

		}
	}
	void CAssimpImporter::GenerateMaterial(SG_Model& aModelOut, const aiMaterial* aMaterial)
	{
		if (aMaterial == nullptr)
		{
			SC_ERROR_LOG("Null material referenced when importing Model");
			return;
		}

		aiReturn texFound = AI_SUCCESS;
		aiString path;
		std::string filePath;

		SG_MaterialDesc matDesc;
		std::string tex[6];

		// Sponza Texture layout
		// 1 = Diffuse
		// 3 = Metallic
		// 5 = Normal
		// 7 = Roughness
		// 8 = Alpha Mask

		// Shift Texture Layout
		// 0 = Diffuse
		// 1 = Normal
		// 2 = AO
		// 3 = Roughness
		// 4 = Metallic
		// 5 = AlphaMask

		static std::string Defaults[6] =
		{
			std::string("DefaultTextures/Grey4x4.dds"),
			std::string("DefaultTextures/Normal4x4.dds"),
			std::string("DefaultTextures/White4x4.dds"),
			std::string("DefaultTextures/Grey4x4.dds"),
			std::string("DefaultTextures/Black4x4.dds"),
			std::string("DefaultTextures/White4x4.dds")
		};

		for (uint i = 0; i < AI_TEXTURE_TYPE_MAX; ++i)
		{
			texFound = aMaterial->GetTexture((aiTextureType)i, 0, &path);
			if (texFound != AI_SUCCESS)
				continue;

			uint texIndex = 0;
			switch (i)
			{
			case 1:
				texIndex = 0;
				break;
			case 3:
				texIndex = 4;
				break;
			case 5:
				texIndex = 1;
				break;
			case 7:
				texIndex = 3;
				break;
			case 8:
				texIndex = 5;
				break;
			}

			filePath = std::string(path.data);

			const size_t last_slash_idx = filePath.find_last_of("\\/");
			if (std::string::npos != last_slash_idx)
				filePath.erase(0, last_slash_idx + 1);

			filePath = std::string("SponzaPBR/") + filePath;

			tex[texIndex] = filePath.c_str();

		}
		for (uint t = 0; t < 6; ++t)
		{
			if (tex[t].empty())
			{
				matDesc.myTextures.push_back(Defaults[t]);
			}
			else
			{
				matDesc.myTextures.push_back(tex[t]);
			}
		}

		matDesc.myVertexShader = "Shaders/Object_SimpleInstancedVS.hlsl";
		matDesc.myPixelShader = "Shaders/Object_SimplePS.hlsl";
		matDesc.myIsOpaque = true;
		matDesc.myUseConstantAttributes = false;

		SG_Material*& mat = aModelOut.myMaterials.Add();
		mat = new SG_Material();
		mat->Init(matDesc);
	}
}