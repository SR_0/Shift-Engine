#pragma once
#include "SC_RefCounted.h"

#include "SG_Model.h"
#include "SG_DirectionalLight.h"
#include "SG_DynamicSky.h"
#include "SG_Camera.h"

#include "SC_QuadTree.h"

#include "GraphicsPSO.h"

namespace Shift
{
	class SG_View;
	class SG_RenderData;
	class CEntityManager;
	class CGameObject;
	class SG_Terrain;

	class SG_World : public SC_RefCounted
	{
	public:
		bool myDebugDrawAABBs;
		bool myLockCullingCamera;

		SG_World();
		~SG_World();

		bool Init();
		void Update();

		// Adds a new view to this world and returns a pointer.
		SC_Ref<SG_View> CreateView();
		bool PrepareView(SG_View* aView);

		void AddModel(SG_Model* aModel);
		void AddMeshes(SG_Model* aModel);
		void AddMesh(SG_MeshInstance* aMesh);
		void RemoveModel(SG_Model* aModel);
		void AddLight();
		void AddEffect();


		SC_Ref<SG_DynamicSky> mySky;
		SC_UniquePtr<SG_Terrain> myTerrain;

	private:

		// Quadtree here for spacial data.
		// Terrain lives here
		// Weather-controller lies here (sky, clouds, fog, daylight, timeofday etc.)
		// Particles controlled from here.

		SC_QuadTree<SC_Ref<SG_Model>> myQuadTree;

		SC_GrowingArray<SC_Ref<SG_View>> myViews;
		SC_GrowingArray<SG_Model*> myModels;
		SC_GrowingArray<SC_Ref<SG_MeshInstance>> myMeshes;
		SC_GrowingArray<SG_Light*> myLights;

		CGraphicsPSO* mySkyPSO;
		SR_Texture mySkyDiffuseTexture;
		SR_Texture mySkySpecularTexture;
		SR_Texture mySkyBRDFTexture;
		SC_Ref<SR_Texture> _mySkyDiffuseTexture;
		SC_Ref<SR_Texture> _mySkySpecularTexture;
		SC_Ref<SR_Texture> _mySkyBRDFTexture;
		SG_Model mySphere;
		SG_DirectionalLight mySun;

		SG_Camera myLockedCullingCamera;

		// Tools
		SC_Vector3f myLightDirection;
		bool mySkyWindowOpen;
		bool myWasCameraLocked;
	};

}