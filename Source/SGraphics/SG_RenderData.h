#pragma once
#include "SG_Camera.h"
#include "SG_DirectionalLight.h"
#include "SG_DrawSubmitStructs.h"
#include "SG_Shadows.h"
#include "SG_RenderQueues.h"

namespace Shift
{
	class SR_WaitEvent;
	class SG_RenderData
	{
	public:
		SG_RenderData();
		SG_RenderData(const SG_RenderData& aRenderData);
		void operator=(const SG_RenderData& aRenderData);
		~SG_RenderData();

		void Clear();

		SG_CameraRenderQueues myQueues;

		SC_GrowingArray<SG_RenderObject> myShadowLightOccluders[SG_Shadows::ourNumShadowCascades];
		SC_GrowingArray<SG_RenderObject> myOpaqeObjects;
		SC_GrowingArray<SG_RenderObject> myTransparentObjects;
		SC_GrowingArray<SG_Light*> myLights;
		SG_PrimitiveRenderObject mySky;
		SG_DirectionalLight mySun;
		SG_PointLightObject _PL;

		SC_Ref<SR_Waitable> mySetViewRefsEvent;
		SC_Ref<SR_Waitable> myRenderShadowsEvent;
		SC_Ref<SR_Waitable> myRenderSkyProbeEvent;
		SC_Ref<SR_Waitable> myRenderGBufferEvent;
		SC_Ref<SR_Waitable> myRenderSSAOEvent;
		SC_Ref<SR_Waitable> myRenderLightsEvent;
		SC_Ref<SR_Waitable> myRenderForwardEvent;
		SC_Ref<SR_Waitable> myRenderPostFXEvent;
		SC_Ref<SR_Waitable> myRenderUIEvent;
		SC_Ref<SR_Waitable> myRenderComposeEvent;
		SC_GrowingArray<SC_Ref<SR_Waitable>> myEvents;

		SC_Future<bool> myPrepareFuture;
		SC_Future<void> myPrepareSettings;
		SC_Future<void> myPrepareReady;

		SG_Camera myShadowLightCameras[SG_Shadows::ourNumShadowCascades];
		SG_Camera myMainCamera;
		float myDeltaTime_Render;
		float myDeltaTime_Logic;
		uint myFramerate_Render;
		uint myFramerate_Logic;

		bool myIsUpdated : 1;
	};
}