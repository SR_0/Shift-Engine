#pragma once
#include "SG_Material.h"
namespace Shift
{
	class SG_MaterialFactory
	{
	public:
		SG_MaterialFactory();
		~SG_MaterialFactory();

		static void GetOrCreateMaterialFromCache(const SG_MaterialDesc& aDesc, SG_Material* aMaterial);
		static void RemoveCacheRef(uint aHash);

		static SC_Ref<SG_Material2> GetCreateMaterial(const char* aPath);
		static SC_Ref<SG_Material2> GetCreateMaterial(const SG_MaterialDesc2& aDesc);

	private:
		static void ExportMaterial(SG_Material* aMaterial);
		static void ImportMaterial(uint aHash, SG_Material* aOutMaterial);
		static bool MaterialFileCacheExists(uint aHash);

		static SG_MaterialFactory* ourInstance;

		std::unordered_map<uint, SC_Ref<SG_Material2>> myCache;
	};
}

