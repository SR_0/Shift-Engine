#pragma once
#include "SR_GBuffer.h"

namespace Shift
{
	struct SG_FrameResources
	{
		SR_GBuffer myGBuffer;

		SR_TextureResource myDepth;
		SR_TextureResource myAmbientOcclusion;

		SR_TextureResource myFullscreen;
		SR_TextureResource myFullscreen2;
		SR_TextureResource myFullscreenHDR;
		SR_TextureResource myQuarterScreen;

		// TODO: Move these into PostEffects class
		SR_TextureResource myBloomLuminance;
		SR_TextureResource myBloomLuminanceQuarter;
		SR_TextureResource myBloomBlur0;
		SR_TextureResource myBloomBlurQuarter0;
		SR_TextureResource myBloomBlur1;
		SR_TextureResource myBloomBlurQuarter1;
		SR_TextureResource myBloomFinal;

		SR_TextureResource myAvgLuminance;

		SC_Ref<SR_TextureBuffer> mySkyProbeTextureBuffer;
		SC_Ref<SR_Texture> mySkyProbeTexture;
		SC_Ref<SR_Texture> mySkyProbeTextureRW;

		SR_TextureResource myScreenHistory[4];
	};
}
