Texture2D Source : register(t0);
Texture2D Destination : register(t1);
RWTexture2D Output : register(u0);

SR_ConstantBuffer Constants : SR_ConstantBuffer0
{
    float2 myMinUV;
    float2 myMaxUV;
    float2 myTexelSizeAndUVScale;
    float2 myScaleFilter;
    int2 myDestOffset;
    float myStrength;
}

float4 SampleSource(float2 aUV)
{
    return SR_SampleLod0(SR_Sampler_Linear_Wrap, Source, clamp(aUV * myTexelSizeAndUVScale, myMinUV, myMaxUV));
}

[numthreads(8,8,1)]
void main(uint3 threadIndex : SV_DispatchThreadId)
{
    float2 uv = float2(threadIndex.xy) + float2(0.5, 0.5);

    float x0 = -myScaleFilter.x;
    float x1 = 0.0;
    float x2 = myScaleFilter.x;
    float y0 = -myScaleFilter.y;
    float y1 = 0.0;
    float y2 = myScaleFilter.y;

    float4 sample00 = SampleSource(uv + float2(x0, y0));
    float4 sample01 = SampleSource(uv + float2(x0, y1));
    float4 sample02 = SampleSource(uv + float2(x0, y2));
    float4 sample10 = SampleSource(uv + float2(x1, y0));
    float4 sample11 = SampleSource(uv + float2(x1, y1));
    float4 sample12 = SampleSource(uv + float2(x1, y2));
    float4 sample20 = SampleSource(uv + float2(x2, y0));
    float4 sample21 = SampleSource(uv + float2(x2, y1));
    float4 sample22 = SampleSource(uv + float2(x2, y2));

    float4 sampleValue = ((sample00 + sample20 + sample22) + ((sample10 + sample01 + sample21 + sample12) * 2.0) + (sample11 * 4.0)) / 16.0;
    float4 dest = SR_SampleLod0(SR_Sampler_Point_Wrap, Destination, int2(threadIndex.xy) + myDestOffset);

    Output[threadIndex.xy] = lerp(dest, sampleValue, myStrength);
}