#pragma warning(error: 3206)
#pragma warning(error: 3556)
#pragma warning(error: 1519)
float2 SR_Transform(float2x2 aMat, float2 aVec) { return mul(aMat, aVec); }
float3 SR_Transform(float3x3 aMat, float3 aVec) { return mul(aMat, aVec); }
float4 SR_Transform(float4x4 aMat, float4 aVec) { return mul(aMat, aVec); }
float4 SR_Transform(float4x4 aMat, float3 aVec) { return mul(aMat, float4(aVec, 1.0)); }
#define SR_Sample(sampler, texture, uv) texture.Sample(sampler, uv)
#define SR_SampleLod0(sampler, texture, uv) texture.SampleLevel(sampler, uv, 0)
#define SR_SampleLod(sampler, texture, uv, lod) texture.SampleLevel(sampler, uv, lod)
#define SR_SampleCmp(sampler, texture, uv, comperand) texture.SampleCmpLevel(sampler, uv, comperand)
#define SR_SampleCmpLod0(sampler, texture, uv, comperand) texture.SampleCmpLevelZero(sampler, uv, comperand)
#define SR_Load(res, location) res.Load(location)
#define SR_Load2(res, location) res.Load2(location)
#define SR_Load3(res, location) res.Load3(location)
#define SR_Load4(res, location) res.Load4(location)
#define SR_Sampler(name, slot) SamplerState name : register(s ## slot)
#define SR_SamplerCmp(name, slot) SamplerComparisonState name : register(s ## slot)
#define SR_Texture2D(name, slot) Texture2D name : register(t ## slot)
#define SR_TextureRW2D(name, slot) RWTexture2D<float4> name : register(u ## slot)
#define SR_TextureRWTyped2D(name, slot, type) RWTexture2D<type> name : register(u ## slot)
#define SR_TextureArray2D(name, slot, type) Texture2DArray<type> name : register(t ## slot)
#define SR_TextureArrayRW2D(name, slot, type) RWTexture2DArray<type> name : register(u ## slot)
#define SR_Texture3D(name, slot, type) Texture3D<type> name : register(t ## slot)
#define SR_TextureRW3D(name, slot, type) RWTexture3D<type> name : register(u ## slot)
#define SR_TextureCube(name, slot) TextureCube name : register(t ## slot)
#define SR_Buffer(name, slot) Buffer name : register(t ## slot)
#define SR_BufferRW(name, slot) RWBuffer name : register(u ## slot)
#define SR_ByteBuffer(name, slot) ByteAddressBuffer name : register(t ## slot)
#define SR_ByteBufferRW(name, slot) RWByteAddressBuffer name : register(u ## slot)
#define SR_StructuredBuffer(name, slot) StructuredBuffer name : register(t ## slot)
#define SR_StructuredBufferRW(name, slot) RWStructuredBuffer name : register(u ## slot)

static const float SR_Pi = 3.14159265358979323846264338328;
#define SR_TextureRef_AmbientOcclusion 41
#define SR_TextureRef_GBuffer_Optional_Emission 37
#define SR_TextureRef_GBuffer_ARM 36
#define SR_TextureRef_GBuffer_Normal 35
#define SR_TextureRef_GBuffer_Color 34
#define SR_TextureRef_Depth 32
#define SR_TextureRef_ShadowMapNoise 40
#define SR_TextureRef_ShadowMapCSM 39
#define SR_TextureRef_SkyBrdf 45
#define SR_TextureRef_SkySpecular 44
#define SR_TextureRef_SkyDiffuse 43
#define SR_SamplerRef_CmpGreater_Aniso_Clamp 11
#define SR_SamplerRef_CmpGreater_Point_Clamp 10
#define SR_SamplerRef_CmpGreater_Linear_Clamp 9
#define SR_SamplerRef_Aniso_Mirror 8
#define SR_SamplerRef_Aniso_Wrap 7
#define SR_SamplerRef_Aniso_Clamp 6
#define SR_SamplerRef_Point_Mirror 5
#define SR_SamplerRef_Point_Wrap 4
#define SR_SamplerRef_Point_Clamp 3
#define SR_SamplerRef_Linear_Mirror 2
#define SR_SamplerRef_Linear_Wrap 1
#define SR_SamplerRef_Linear_Clamp 0

// Must reflect "/Source/SRender/SR_ViewConstants.h"

cbuffer SR_ViewConstants : register(b4)
{
	struct
	{
		float4x4 myWorldToClip;
		float4x4 myClipToWorld;
	
		float4x4 myWorldToCamera;
		float4x4 myCameraToWorld;
	
		float4x4 myCameraToClip;
		float4x4 myClipToCamera;
	
		float3 myCameraPosition;
		float myUnused__;
	
		float2 myResolution;
	
	} SR_ViewConstants;
}

float3 SR_ClipToCamera(float2 aScreenCoord, float aDepth)
{
	float2 screenPos = aScreenCoord * 2.0f - 1.0f;
	screenPos.y *= -1.0f;
	float4 projectedPos = float4(screenPos, aDepth, 1.0f);
	float4 viewPosition = SR_Transform(SR_ViewConstants.myClipToCamera, projectedPos);
	return viewPosition.xyz / viewPosition.w;
}

float3 SR_ClipToWorld(float2 aScreenCoord, float aDepth)
{
	float4 viewPos = float4(SR_ClipToCamera(aScreenCoord, aDepth), 1.0f);
	float4 worldPos = SR_Transform(SR_ViewConstants.myCameraToWorld, viewPos);
	return worldPos.xyz;
}

float2 SR_PixelToUV(in const int2 aPixelCoord, in const float2 aResolution)
{
	float2 uv = (aPixelCoord.xy / aResolution);
	uv.x = aPixelCoord.x / aResolution.x + 0.5f / aResolution.x;
	uv.y = aPixelCoord.y / aResolution.y + 0.5f / aResolution.y;
	return uv;
}
struct ObjectAttributes
{
	float3 myWorldPosition;
	float3 myNormal;
	float3 myDiffuse;
	float myAO;
	float myRoughness;
	float myMetallic;
};

struct PBRAttributes
{
	float3 myFresnel;
	float myAttenuation;
	float myGGX;
	float myVisibility;
};

struct SG_Light
{
	float3 myPosition;
	float myRange;
	float3 myColor;
	float myIntensity;
};

cbuffer SR_EnvironmentConstants : register(b6)
{
	struct
	{
	    float3 myToSunDirection;
	    float mySunIntensity;
	    float3 mySunColor;
		float mySunCosLow;
		float mySunCosHigh;
		float myDirectSunFactor;
	    float myAmbientLightIntensity;
	} SR_EnvironmentConstants;
}

SR_Sampler(SR_Sampler_Linear_Clamp, SR_SamplerRef_Linear_Clamp);
SR_Sampler(SR_Sampler_Linear_Wrap, SR_SamplerRef_Linear_Wrap);
SR_Sampler(SR_Sampler_Linear_Mirror, SR_SamplerRef_Linear_Mirror);

SR_Sampler(SR_Sampler_Point_Clamp, SR_SamplerRef_Point_Clamp);
SR_Sampler(SR_Sampler_Point_Wrap, SR_SamplerRef_Point_Wrap);
SR_Sampler(SR_Sampler_Point_Mirror, SR_SamplerRef_Point_Mirror);

SR_Sampler(SR_Sampler_Aniso_Clamp, SR_SamplerRef_Aniso_Clamp);
SR_Sampler(SR_Sampler_Aniso_Wrap, SR_SamplerRef_Aniso_Wrap);
SR_Sampler(SR_Sampler_Aniso_Mirror, SR_SamplerRef_Aniso_Mirror);

SR_SamplerCmp(SR_Sampler_CmpGreater_Linear_Clamp, SR_SamplerRef_CmpGreater_Linear_Clamp);
SR_SamplerCmp(SR_Sampler_CmpGreater_Point_Clamp, SR_SamplerRef_CmpGreater_Point_Clamp);
SR_SamplerCmp(SR_Sampler_CmpGreater_Aniso_Clamp, SR_SamplerRef_CmpGreater_Aniso_Clamp);

static const float PI = 3.141592653589793f;
static const float Epsilon = 0.00001f;
static const float3 Dielectric = float3(0.04f, 0.04f, 0.04f);

// Shlick's approximation of the Fresnel factor.
float3 FresnelSchlick(float3 F0, float cosTheta)
{
	return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

// GGX/Towbridge-Reitz normal distribution function.
// Uses Disney's reparametrization of alpha = roughness^2.
float GGX(float cosLh, float roughness)
{
	float alpha = roughness * roughness;
	float alphaSq = alpha * alpha;

	float denom = (cosLh * cosLh) * (alphaSq - 1.0) + 1.0;
	return alphaSq / (PI * denom * denom);
}

// Single term for separable Schlick-GGX below.
float GASchlickG1(float cosTheta, float k)
{
	return cosTheta / (cosTheta * (1.0 - k) + k);
}

// Schlick-GGX approximation of geometric attenuation function using Smith's method.
float GASchlickGGX(float cosLi, float cosLo, float roughness)
{
	float r = roughness + 1.0;
	float k = (r * r) / 8.0; // Epic suggests using this roughness remapping for analytic lights.
	return GASchlickG1(cosLi, k) * GASchlickG1(cosLo, k);
}

float3 DirectLight(in ObjectAttributes aAttributes, float3 aToEye, float3 aToLight, float3 aLightColor, float aLightIntensity, float aAttenuation)
{
	float3 directLight = float3(0.0f, 0.0f, 0.0f);

	float3 F0 = lerp(Dielectric, aAttributes.myDiffuse, aAttributes.myMetallic);

	float3 halfVec = normalize(aToLight + aToEye);

	float nDotL = max(0.0, dot(aAttributes.myNormal, aToLight));
	float nDotC = max(0.0, dot(aAttributes.myNormal, aToEye));
	float nDotHalf = max(0.0, dot(aAttributes.myNormal, halfVec));

	float3 fresnel = FresnelSchlick(F0, max(0.0, dot(halfVec, aToEye)));
	float ndf = GGX(nDotHalf, aAttributes.myRoughness);
	float geomAttenuation = GASchlickGGX(nDotL, nDotC, aAttributes.myRoughness);

	float3 kd = lerp(float3(1, 1, 1) - fresnel, float3(0, 0, 0), aAttributes.myMetallic);

	float3 diffuseBRDF = kd * aAttributes.myDiffuse;
	float3 specularBRDF = (fresnel * ndf * geomAttenuation) / max(Epsilon, 4.0 * nDotL * nDotC);

	float3 light = (diffuseBRDF + specularBRDF) * aLightIntensity * nDotL;

	return directLight = light * aLightColor * aAttenuation;
}

SR_TextureCube(skyDiffuse, SR_TextureRef_SkyDiffuse);
SR_TextureCube(skySpecular, SR_TextureRef_SkySpecular);
SR_Texture2D(skyBrdf, SR_TextureRef_SkyBrdf);

uint QuerySkyTextureLevels()
{
	uint width, height, levels;
	skySpecular.GetDimensions(0, width, height, levels);
	return levels;
}

float3 AmbientLight(in ObjectAttributes attributes, in float3 toEye)
{
	float3 ambientLight = float3(0.0f, 0.0f, 0.0f);

	// Sample diffuse irradiance at normal direction.
	float3 irradiance = SR_SampleLod(SR_Sampler_Aniso_Wrap, skyDiffuse, attributes.myNormal, 1).rgb;

	// Calculate Fresnel term for ambient lighting.
	// Since we use pre-filtered cubemap(s) and irradiance is coming from many directions
	// use cosLo instead of angle with light's half-vector (cosLh above).
	// See: https://seblagarde.wordpress.com/2011/08/17/hello-world/
	float3 F0 = lerp(Dielectric, attributes.myDiffuse, attributes.myMetallic);
	float nDotC = max(0.0, dot(attributes.myNormal, toEye));
	float3 reflectionVec = 2.0 * nDotC * attributes.myNormal - toEye;
	float3 F = FresnelSchlick(F0, nDotC);

	// Get diffuse contribution factor (as with direct lighting).
	float3 kd = lerp(1.0 - F, 0.0, attributes.myMetallic);

	// Irradiance map contains exitant radiance assuming Lambertian BRDF, no need to scale by 1/PI here either.
	float3 diffuseIBL = kd * attributes.myDiffuse * irradiance;

	// Sample pre-filtered specular reflection environment at correct mipmap level.
	uint specularTextureLevels = QuerySkyTextureLevels();
	float3 specularIrradiance = SR_SampleLod(SR_Sampler_Aniso_Wrap, skySpecular, reflectionVec, attributes.myRoughness * specularTextureLevels).rgb;

	// Split-sum approximation factors for Cook-Torrance specular BRDF.
	float2 specularBRDF = SR_SampleLod(SR_Sampler_Linear_Clamp, skyBrdf, float2(nDotC, attributes.myRoughness), 0).rg;

	// Total specular IBL contribution.
	float3 specularIBL = (F0 * specularBRDF.x + specularBRDF.y) * specularIrradiance;

	// Total ambient lighting contribution.
	ambientLight = diffuseIBL + specularIBL;

	return ambientLight;
}

cbuffer SR_ShadowConstants : register(b5)
{
	struct
	{
		float4x4 ShadowWorldToClip[4]; // Match number with SG_Shadows::ourNumShadowCascades
		float4 ShadowSampleRotation;
		float PCFFilterOffset;
	} SR_ShadowConstants;
}

SR_TextureArray2D(ShadowMapCSM, SR_TextureRef_ShadowMapCSM, float);

#ifndef SHADOWS_CSM_NO_PCF
SR_Texture2D(ShadowMapNoise, SR_TextureRef_ShadowMapNoise);

float2 GetNoiseRotateVec(float2 aScreenSpaceCoord)
{
	float2 vec = SR_SampleLod(SR_Sampler_Point_Wrap, ShadowMapNoise, aScreenSpaceCoord / 128.0, 0).xy * 2.0 - 1.0;
	vec = SR_Transform(float2x2(SR_ShadowConstants.ShadowSampleRotation.xy, SR_ShadowConstants.ShadowSampleRotation.zw), vec);
	return vec;
}
#else
float2 GetNoiseRotateVec(float2 aScreenSpaceCoord)
{
	return float2(0.0, 0.0);
}
#endif

#ifdef SHADOWS_ENABLE_CSM_DEBUG
float3 GetCSMDebugColor(uint aIndex)
{
	float3 col[4];
	col[0] = float3(1.0, 0.0, 0.0);
	col[1] = float3(0.0, 0.0, 1.0);
	col[2] = float3(0.0, 1.0, 0.0);
	col[3] = float3(1.0, 1.0, 0.0);

	return col[aIndex];
}
#endif

float SampleCascade(float4 aShadowMapCoord)
{
	return SR_SampleCmpLod0(SR_Sampler_CmpGreater_Linear_Clamp, ShadowMapCSM, aShadowMapCoord.xyz, aShadowMapCoord.w);
}

float SampleCascadeRandom(float4 aShadowMapCoord, float2 aNoiseRotationVec, float2 aOffset)
{
	float2 rotation;
	rotation.x = aOffset.x * aNoiseRotationVec.x - aOffset.y * aNoiseRotationVec.y;
	rotation.y = aOffset.y * aNoiseRotationVec.x + aOffset.x * aNoiseRotationVec.y;

	return SampleCascade(float4(aShadowMapCoord.xy + rotation, aShadowMapCoord.zw));
}

float SampleCascade4TapPCF(float4 aShadowMapCoord, float2 aNoiseRotationVec)
{
	float sampleFactor = 0.0;	
    float pcfOffset = SR_ShadowConstants.PCFFilterOffset;
	sampleFactor = SampleCascadeRandom(aShadowMapCoord, aNoiseRotationVec, float2(-pcfOffset, -pcfOffset));
	sampleFactor += SampleCascadeRandom(aShadowMapCoord, aNoiseRotationVec, float2(-pcfOffset, pcfOffset));
	sampleFactor += SampleCascadeRandom(aShadowMapCoord, aNoiseRotationVec, float2(pcfOffset, -pcfOffset));
	sampleFactor += SampleCascadeRandom(aShadowMapCoord, aNoiseRotationVec, float2(pcfOffset, pcfOffset));
	sampleFactor += SampleCascadeRandom(aShadowMapCoord, aNoiseRotationVec, float2(0.0, 0.0));
	sampleFactor *= 0.2;
	return sampleFactor;
}

float4 GetShadowCascadePos(float3 aWorldPos, out float aShadowFadeOut)
{
	const float3 minCascade = float3(0.0, 0.0, 0.0);
	const float3 maxCascade = float3(1.0, 1.0, 1.0);
	
	float4 worldPos = float4(aWorldPos, 1.0);
	float4 cascadePos3 = SR_Transform(SR_ShadowConstants.ShadowWorldToClip[3], worldPos);
	float4 shadowPos = cascadePos3;
	const float3 midPos = float3(0.5, 0.5, 0.5);
	float finalCascadeDist = max(max(abs(shadowPos.x - midPos.x), abs(shadowPos.y - midPos.y)), abs(shadowPos.w - midPos.z));
	
	float4 cascadePos2 = SR_Transform(SR_ShadowConstants.ShadowWorldToClip[2], worldPos);
	float3 cascadeTemp = step(minCascade, cascadePos2.xyw) * step(cascadePos2.xyw, maxCascade);
	float cascadeMask2 = cascadeTemp.x * cascadeTemp.y * cascadeTemp.z;
	shadowPos = lerp(shadowPos, cascadePos2, cascadeMask2);

	float4 cascadePos1 = SR_Transform(SR_ShadowConstants.ShadowWorldToClip[1], worldPos);
	cascadeTemp = step(minCascade, cascadePos1.xyw) * step(cascadePos1.xyw, maxCascade);
	float cascadeMask1 = cascadeTemp.x * cascadeTemp.y * cascadeTemp.z;
	shadowPos = lerp(shadowPos, cascadePos1, cascadeMask1);
	
	float4 cascadePos0 = SR_Transform(SR_ShadowConstants.ShadowWorldToClip[0], worldPos);
	cascadeTemp = step(minCascade, cascadePos0.xyw) * step(cascadePos0.xyw, maxCascade);
	float cascadeMask0 = cascadeTemp.x * cascadeTemp.y * cascadeTemp.z;
	shadowPos = lerp(shadowPos, cascadePos0, cascadeMask0.x);
	
	float useFinalCascade = (1 - cascadeMask0) * (1 - cascadeMask1) * (1 - cascadeMask2);
	aShadowFadeOut = useFinalCascade * smoothstep(0.45, 0.5, finalCascadeDist);
	
	return shadowPos;
}

float SampleCSM(float3 aWorldPos, float2 aNoiseRotationVec
#ifdef SHADOWS_ENABLE_CSM_DEBUG
	, out float3 aDebugColorOut
#endif
)
{
	float shadowFade = 0.0;
	float4 shadowPos = GetShadowCascadePos(aWorldPos, shadowFade);
	
#ifdef SHADOWS_CSM_NO_PCF
	float shadowFactor = SampleCascade(shadowPos);
#else
	float shadowFactor = SampleCascade4TapPCF(shadowPos, aNoiseRotationVec);
#endif

#ifdef SHADOWS_ENABLE_CSM_DEBUG
	aDebugColorOut = GetCSMDebugColor(shadowPos.z);
#endif

	const float defaultShadowFactor = 0.0;
	shadowFactor = lerp(shadowFactor, defaultShadowFactor, shadowFade);
	
	return shadowFactor;
}

float GetShadowFactor(float3 aWorldPos, float2 aScreenSpaceCoord
#ifdef SHADOWS_ENABLE_CSM_DEBUG
	, out float3 aDebugColorOut
#endif
)
{
	float shadowFactor;

	float2 noiseRotateVec = GetNoiseRotateVec(aScreenSpaceCoord);
	shadowFactor = SampleCSM(aWorldPos, noiseRotateVec
#ifdef SHADOWS_ENABLE_CSM_DEBUG
		, aDebugColorOut
#endif
	);

	// Add additional shadowing here
	// Clouds, Distance, Parallaxing Shadows etc.

	return shadowFactor;
}

SR_Texture2D(depthTexture, SR_TextureRef_Depth);
SR_Texture2D(colorTexture, SR_TextureRef_GBuffer_Color);
SR_Texture2D(normalTexture, SR_TextureRef_GBuffer_Normal);
SR_Texture2D(armTexture, SR_TextureRef_GBuffer_ARM);
SR_Texture2D(emissionTexture, SR_TextureRef_GBuffer_Optional_Emission);
SR_Texture2D(ambientOcclusionTexture, SR_TextureRef_AmbientOcclusion);

SR_TextureRW2D(outLightTexture, 0);

[numthreads(8, 8, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
	float2 Resolution = SR_ViewConstants.myResolution;
    if (threadID.x > (uint)Resolution.x || threadID.y > (uint)Resolution.y)
        return;

	int3 sampleLocation = int3(threadID.xy, 0);
	float2 uv = SR_PixelToUV(sampleLocation.xy, Resolution);
	float depthSample = SR_Load(depthTexture, sampleLocation).r;
	float3 worldPosition = SR_ClipToWorld(uv, depthSample);

	float4 colorSample = SR_Load(colorTexture, sampleLocation);
	float3 normalSample = SR_Load(normalTexture, sampleLocation).xyz * 2.0 - 1.0;
	float3 armSample = SR_Load(armTexture, sampleLocation).rgb;
    
	float3 toCamera = normalize(SR_ViewConstants.myCameraPosition - worldPosition);
    ObjectAttributes attributes;
	attributes.myWorldPosition = worldPosition;
	attributes.myNormal = normalSample;
	attributes.myDiffuse = colorSample.rgb;
	attributes.myAO = armSample.r;
	attributes.myRoughness = armSample.g;
	attributes.myMetallic = armSample.b;

	float3 directLight = (float3)0;
    float shadowFactor = GetShadowFactor(worldPosition, uv);
    if (shadowFactor > 0.0)
	    directLight += shadowFactor * DirectLight(attributes, toCamera, SR_EnvironmentConstants.myToSunDirection, SR_EnvironmentConstants.mySunColor, SR_EnvironmentConstants.mySunIntensity, 1.0);
    
	float ambientOcclusion = SR_SampleLod0(SR_Sampler_Linear_Clamp, ambientOcclusionTexture, uv).r;
    float3 ambientLight = ambientOcclusion * AmbientLight(attributes, toCamera) * SR_EnvironmentConstants.myAmbientLightIntensity;

    float3 emissionSample = SR_Load(emissionTexture, sampleLocation).rgb;
	outLightTexture[sampleLocation.xy] = float4(directLight + ambientLight + emissionSample, 1.0);
			
}
