#include "CommonStructs.hlsli"
#include "StaticSamplers.hlsli"
#include "NormalEncode.hlsli"

Texture2D diffuseTexture : register(t0);
Texture2D normalTexture : register(t1);
Texture2D aoTexture : register(t2);
Texture2D roughnessTexture : register(t3);
Texture2D metallicTexture : register(t4);
// add emissiontexture
Texture2D maskTexture : register(t5);

GBufferOut main(in SimplePixelIn input)
{
	GBufferOut output;

	float alpha = maskTexture.Sample(Sampler_Aniso16_Wrap, input.uv).r;
	clip(alpha < 0.5 ? -1 : 1);
    
    float3 normalSample = (((normalTexture.Sample(Sampler_Aniso16_Wrap, input.uv).xyz) * 2.0) - 1.0);
    float3 normal = normalize(mul(input.tangentSpaceMatrix, normalSample));
	
	
    float4 texCol = diffuseTexture.Sample(Sampler_Aniso16_Wrap, input.uv);
	float3 arm = float3(0.0f, 0.0f, 0.0f);
	arm.r = aoTexture.Sample(Sampler_Aniso16_Wrap, input.uv).r;
	arm.g = roughnessTexture.Sample(Sampler_Aniso16_Wrap, input.uv).r;
	arm.b = metallicTexture.Sample(Sampler_Aniso16_Wrap, input.uv).r;
    output.COLOR.rgb = texCol.rgb;
	output.COLOR.a = 1.0;
	output.NORMAL = normal * 0.5 + 0.5;
	output.ARM.rgb = arm;
	output.ARM.a = 0.0f;
	output.EMISSIVE = float4(0.0f, 0.0f, 0.0f, 0.0f);
	return output;
}