#include "StaticSamplers.hlsli"
#include "Utilities.hlsli"

RWTexture2D<float4> outTexture : register(u0);

float3 BlurDirectional(int3 aUV, int2 aDirection)
{
	const uint KernelSize = 7;
	const float WeightList[KernelSize] = { 0.115528, 0.141488, 0.159786, 0.166396, 0.159786, 0.141488, 0.115528 };
	const float2 PixelOffset[KernelSize] = { float2(-3,-3), float2(-2,-2), float2(-1,-1), float2(0,0),
												float2(1,1), float2(2,2), float2(3,3) };

	float3 blur = float3(0.0f, 0.0f, 0.0f);
	for (uint i = 0; i < KernelSize; ++i)
	{
		int3 offsetUV = int3(aUV.xy + ((int)PixelOffset[i] * aDirection), 0);
		float3 resource = outTexture.Load(offsetUV).rgb;
		blur += saturate(resource * (WeightList[i]));
	}

	return blur;
}

[numthreads(8, 8, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
	float2 OutTextureResolution = GetResolution(outTexture);
	if (threadID.x <= OutTextureResolution.x && threadID.y <= OutTextureResolution.y)
	{
		int3 sampleLocation = int3(threadID.xy, 0);
		{
			float3 blur = float3(0.0f, 0.0f, 0.0f);
			int2 direction = int2(1.0, 0.0);
			blur += BlurDirectional(sampleLocation, direction);
			direction = int2(0.0, 1.0);
			blur += BlurDirectional(sampleLocation, direction);
			direction = int2(1.0, 1.0);
			blur += BlurDirectional(sampleLocation, direction);
			outTexture[sampleLocation.xy] = float4(blur, 1.0f);
		}
	}
}