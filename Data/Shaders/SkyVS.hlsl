#include "CommonStructs.hlsli"

cbuffer transformations : register(b0)
{
	float4x4 projection;
	float4x4 view;
	float4x4 model;
	float3 cameraPosition;
};
SkyPixelIn main(SimpleVertexIn inputData)
{
	SkyPixelIn output;
	float4 wPos = mul(model, float4(inputData.position.xyz, 1.0f));
	output.position = mul(view, wPos);
	output.position = mul(projection, output.position);

#if (USE_REVERSE_Z == 1)
	output.position.z = 0.f;
#else
	output.position.z = output.position.w;
#endif
	output.color = inputData.color;
	output.eyeDirection = normalize(wPos.xyz - cameraPosition);
	output.worldPosition = wPos.xyz;
	return output;
}