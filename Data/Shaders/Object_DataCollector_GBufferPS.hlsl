#include "Globals.hlsli"


/*---------------------------------------------
    CUSTOMIZATION DEFINES

ENABLE_ALPHAMASK    -   Enables alpha-masking.
*define*            -   *Comment*

----------------------------------------------*/

TEXTURE2D(albedoTexture, float4, TEXSLOT_OBJECT_ALBEDO);
TEXTURE2D(normalTexture, float4, TEXSLOT_OBJECT_NORMAL);
TEXTURE2D(roughnessTexture, float, TEXSLOT_OBJECT_ROUGHNESS);
TEXTURE2D(metallicTexture, float, TEXSLOT_OBJECT_METALLIC);
TEXTURE2D(aoTexture, float, TEXSLOT_OBJECT_AMBIENTOCCLUSION);
TEXTURE2D(emissiveTexture, float4, TEXSLOT_OBJECT_EMISSIVE);

GBufferOut main(PixelInputType input)
{
    GBufferOut output;
    const float4 albedo = albedoTexture.Sample(globalSampler_AnisoClamp, input.uv);

#ifdef ENABLE_ALPHAMASK
    const bool shouldDiscard = floor(albedo.a);
    [branch]
    if (!shouldDiscard)
    {
        discard;
    }
#endif

    const float4 normal = normalTexture.Sample(globalSampler_AnisoClamp, input.uv);
    const float roughness = roughnessTexture.Sample(globalSampler_AnisoClamp, input.uv).r;
    const float metallic = metallicTexture.Sample(globalSampler_AnisoClamp, input.uv).r;
    const float ao = aoTexture.Sample(globalSampler_AnisoClamp, input.uv).r;
    const float4 emissive = emissiveTexture.Sample(globalSampler_AnisoClamp, input.uv);


    output.NORMAL.xyz = normal.xyz;
    output.ARM.r = ao;
    output.ARM.g = roughness;
    output.ARM.b = metallic;
    output.COLOR.rgb = albedo.rgb;
    output.EMISSIVE = float4(emissive.rgb, 1.0f);

    return output;
}