#include "GeneralIncludes.hlsli"

Shift_RWTexture3D(outVoxelTextureDiffuse, 0, float4);
Shift_Texture3D(VoxelTextureNormals, 0);
Shift_Texture2D(ShadowBuffer, 1);

cbuffer CameraInfo : register(b0)
{
	float4x4 ShadowProjection;
	float4x4 ShadowView;
}
cbuffer LightInfo : register(b1)
{
	float3 LightColor;
	float Intensity;
	float3 ToLightDirection;
	float AmbientLightIntensity;
	float3 PointLightColor;
	float PointLightRange;
	float3 PointLightPos;
	float PointLightIntensity;
}

cbuffer VoxelSettings : register(b2)
{
	float3 VoxelCenter;
	float VoxelTextureSize;
	float VoxelSize;
};
uint3 WorldToVoxel(in float3 aWorldPos)
{
	return floor((((aWorldPos - VoxelCenter) / VoxelTextureSize / VoxelSize) * float3(0.5f, -0.5f, 0.5f) + 0.5f) * VoxelTextureSize);
}

float3 VoxelToWorld(in uint3 aVoxelPos)
{
	float3 worldPos = ((float3) aVoxelPos + 0.5f) / VoxelTextureSize;
	worldPos = worldPos * 2 - 1;
	worldPos.y *= -1;
	worldPos *= VoxelSize;
	worldPos *= VoxelTextureSize;
	worldPos += VoxelCenter;

	return worldPos;
}

float3 TraceToLightSource(float3 pos, float3 toLight)
{
	const float3 directionalIncrement = normalize(toLight) * VoxelSize;
	const float travelStepDistance = length(directionalIncrement);
	const float maxDistance = travelStepDistance * 100.f;
	float3 currPos = pos + directionalIncrement * 2;
	float currDistance = 0.f;

	float3 outLight = (float3)(1.f);

	while (currDistance < maxDistance)
	{
		float3 currSamplePos = WorldToVoxel(currPos);
		float4 sampleColor = outVoxelTextureDiffuse[currSamplePos];
		if (sampleColor.a >= 1.f)
			return (float3)0;

		currPos += directionalIncrement;
		currDistance += travelStepDistance;
	}
	return outLight;
}


[numthreads(8, 8, 8)]
void main(uint3 threadIndex : SV_DispatchThreadID)
{
	const float4 diffuse = outVoxelTextureDiffuse[threadIndex].rgba;
	const float3 normal = normalize(VoxelTextureNormals[threadIndex].xyz);

	float4 accumulatedColor = (float4)0.f;

	float3 voxelWorldPos = VoxelToWorld(threadIndex);

	// Sun
	float3 shadowFactor = TraceToLightSource(voxelWorldPos, ToLightDirection);
	float nDotL = max(0.0f, dot(normal, normalize(ToLightDirection)));

	float3 sunColor = diffuse.rgb * shadowFactor;
	accumulatedColor.rgb += sunColor;
	accumulatedColor.a = diffuse.a;

	outVoxelTextureDiffuse[threadIndex] = accumulatedColor;
}