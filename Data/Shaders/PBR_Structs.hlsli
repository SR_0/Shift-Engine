#ifndef _PBR_STRUCTS_H_
#define _PBR_STRUCTS_H_
static const float PI = 3.141592653589793f;
static const float Epsilon = 0.00001f;
static const float3 Dielectric = float3(0.04f, 0.04f, 0.04f);

struct ObjectAttributes
{
	float3 myWorldPosition;
	float3 myNormal;
	float3 myDiffuse;
	float myAO;
	float myRoughness;
	float myMetallic;
};

struct PBRAttributes
{
	float3 myFresnel;
	float myAttenuation;
	float myGGX;
	float myVisibility;
};

#endif