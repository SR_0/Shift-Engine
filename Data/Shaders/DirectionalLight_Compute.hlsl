#include "GeneralIncludes.hlsli"
#include "PBR.hlsli"
#include "NormalEncode.hlsli"

Shift_Texture2D(depthTexture, TEXSLOT_SCENE_DEPTH);
Shift_Texture2D(colorTexture, TEXSLOT_GBUFFER_COLOR);
Shift_Texture2D(normalTexture, TEXSLOT_GBUFFER_NORMALS);
Shift_Texture2D(armTexture, TEXSLOT_GBUFFER_ARM);
Shift_TextureCube(skyTexture, TEXSLOT_SKY);
Texture2D brdfLUT : register(t5);

RWTexture2D<float4> outLightTexture : register(u0);

cbuffer CameraInfo : register(b0)
{
	float4x4 InverseProjection;
	float4x4 InverseView;
	float3 CameraPosition;
}
cbuffer LightInfo : register(b1)
{
	float3 LightColor;
	float Intensity;
	float3 ToLightDirection;
}

uint QuerySpecularTextureLevels()
{
	uint width, height, levels;
	skyTexture.GetDimensions(0, width, height, levels);
	return levels;
}

[numthreads(32, 32, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
	float2 Resolution = GetResolution(outLightTexture);
	if (threadID.x <= Resolution.x && threadID.y <= Resolution.y)
	{
		int3 sampleLocation = int3(threadID.xy, 0);
		float4 colorSample = colorTexture.Load(sampleLocation);

		if (colorSample.a > 0.0f)
		{
			float depthSample = depthTexture.Load(sampleLocation).r;
			float2 screenPos = PixelToUV(sampleLocation, Resolution);
			float3 worldPosition = WorldPositionFromDepth(screenPos, depthSample, InverseProjection, InverseView);

			float3 normalSample = mul(InverseView, Normal_Decode(normalTexture.Load(sampleLocation).xy));
			float3 armSample = armTexture.Load(sampleLocation).xyz;

			float3 toCamera = normalize(CameraPosition - worldPosition);
			float nDotC = max(0.0, dot(normalSample, toCamera));
			float3 specReflectionVec = 2.0 * nDotC * normalSample - toCamera;

			// Direct Light

			float3 F0 = lerp(Fdielectric, colorSample.rgb, armSample.b);

			const float3 toLightDir = normalize(ToLightDirection);
			float3 radiance = Intensity;

			float3 halfVec = normalize(toLightDir + toCamera);

			float nDotL = max(0.0, dot(normalSample, toLightDir));
			float nDotHalf = max(0.0, dot(normalSample, halfVec));

			float3 fresnel = FresnelSchlick(F0, max(0.0, dot(halfVec, toCamera)));
			float ndf = GGX(nDotHalf, armSample.g);
			float geomAttenuation = GASchlickGGX(nDotL, nDotC, armSample.g);

			float3 kd = lerp(float3(1, 1, 1) - fresnel, float3(0, 0, 0), armSample.b);

			float3 diffuseBRDF = kd * colorSample.rgb;
			float3 specularBRDF = (fresnel * ndf * geomAttenuation) / max(Epsilon, 4.0 * nDotL * nDotC);

			float3 directLighting = (diffuseBRDF + specularBRDF) * radiance * nDotL;

			const float4 lightColor = float4(LightColor, 1.0f);

			float3 directLight = directLighting * lightColor;

			// Ambient 
			float3 fresnelAmbient = FresnelSchlick(F0, nDotC);
			float3 irradiance = skyTexture.SampleLevel(Sampler_Aniso16_Wrap, normalSample, 0).rgb; // This is where we accumulate the indirect light from the scene (Global Illumination)
			float3 kdAmbient = lerp(1.0f - fresnelAmbient, 0.0f, armSample.b);
			float3 diffuseIBL = kdAmbient * colorSample.rgb * irradiance;
			uint specularTextureLevels = QuerySpecularTextureLevels();
			float3 specularIrradiance = skyTexture.SampleLevel(Sampler_Aniso16_Wrap, specReflectionVec, armSample.g * specularTextureLevels).rgb;
			float2 specularBRDFAmbient = brdfLUT.SampleLevel(Sampler_Linear_Clamp, float2(nDotC, armSample.g), 0).rg;
			float3 specularIBL = (F0 * specularBRDFAmbient.x + specularBRDFAmbient.y) * specularIrradiance;
			float3 ambientLight = diffuseIBL + specularIBL;

			outLightTexture[sampleLocation.xy] = float4((ambientLight + directLight) * armSample.r, 1.0f);
		}
		else
		{
			outLightTexture[sampleLocation.xy] = colorSample;
		}
	}
}