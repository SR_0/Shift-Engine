#include "GeneralIncludes.hlsli"

Texture2D<float4> srcTexture : register(t0);

// Destination could probably be an array instead, with the constantbuffer holding the number of targets.
RWTexture2D<float4> dstTexture : register(u0);



cbuffer CB : register(b0)
{
	float2 TexelSize;	// 1.0 / dstTexture dimensions
}

[numthreads( 8, 8, 1 )]
void main(uint3 threadIndex : SV_DispatchThreadID)
{
	// If using arrays later, texelSize could be determined from the destination texture by querying the dimensions.
	float2 uv = TexelSize * (threadIndex.xy + 0.5f);
	float4 color = srcTexture.SampleLevel(Sampler_Linear_Clamp, uv, 0);
	dstTexture[threadIndex.xy] = color;
}