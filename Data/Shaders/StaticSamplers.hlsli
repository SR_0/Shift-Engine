#ifndef _STATIC_SAMPLERS_H_
#define _STATIC_SAMPLERS_H_
#include "GeneralShaderInterop.h"

#define SAMPLER(name, slot) SamplerState name : register(s ## slot);
#define SAMPLERCMP(name, slot) SamplerComparisonState name : register(s ## slot);

SAMPLER(Sampler_Linear_Clamp, SAMPLERSLOT_LINEAR_CLAMP);
SAMPLER(Sampler_Linear_Wrap, SAMPLERSLOT_LINEAR_WRAP);
SAMPLER(Sampler_Linear_Mirror, SAMPLERSLOT_LINEAR_MIRROR);

SAMPLER(Sampler_Point_Clamp, SAMPLERSLOT_POINT_CLAMP);
SAMPLER(Sampler_Point_Wrap, SAMPLERSLOT_POINT_WRAP);
SAMPLER(Sampler_Point_Mirror, SAMPLERSLOT_POINT_MIRROR);

SAMPLER(Sampler_Aniso16_Clamp, SAMPLERSLOT_ANISO16_CLAMP);
SAMPLER(Sampler_Aniso16_Wrap, SAMPLERSLOT_ANISO16_WRAP);
SAMPLER(Sampler_Aniso16_Mirror, SAMPLERSLOT_ANISO16_MIRROR);

SAMPLERCMP(Sampler_CmpGreater_Linear_Clamp, SAMPLERSLOT_CMP_LINEAR_CLAMP);
SAMPLERCMP(Sampler_CmpGreater_Point_Clamp, SAMPLERSLOT_CMP_POINT_CLAMP);
SAMPLERCMP(Sampler_CmpGreater_Aniso16_Clamp, SAMPLERSLOT_CMP_ANISO16_CLAMP);

#endif