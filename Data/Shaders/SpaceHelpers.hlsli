#ifndef _SPACEHELPERS_H_
#define _SPACEHELPERS_H_

float3 ViewPositionFromDepth(float2 xy, float depth, float4x4 inverseProj)
{
	float2 screenPos = xy * 2.0f - 1.0f;
	screenPos.y *= -1.0f;
	float4 projectedPos = float4(screenPos, depth, 1.0f);
	float4 viewPosition = mul(inverseProj, projectedPos);
	return viewPosition.xyz / viewPosition.w;
}
float3 WorldPositionFromDepth(float2 xy, float depth, float4x4 inverseProj, float4x4 inverseView)
{
	float4 viewPos = float4(ViewPositionFromDepth(xy, depth, inverseProj), 1.0f);
	float4 worldPos = mul(inverseView, viewPos);
	return worldPos.xyz;
}

#endif