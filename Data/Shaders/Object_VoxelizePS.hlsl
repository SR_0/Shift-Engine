#include "CommonStructs.hlsli"
#include "StaticSamplers.hlsli"
#include "Converters.hlsli"

Texture2D diffuseTexture : register(t0);
RWTexture3D<uint> outVoxelTextureDiffuse : register(u0);
RWTexture3D<float4> outVoxelTextureNormals : register(u1);

cbuffer VoxelSettings : register(b0)
{
	float3 VoxelCenter;
	float VoxelTextureSize;
	float VoxelSize;
};

bool Inside(const uint3 pos)
{ 
	const uint size = (uint)VoxelTextureSize;
	return (abs(pos.x) < size) && (abs(pos.y) < size) && (abs(pos.z) < size);
}

void main(in VoxelPixelIn input)
{
	const float3 diff = ((input.worldPosition.xyz - VoxelCenter) / VoxelTextureSize / VoxelSize) * float3(0.5f, -0.5f, 0.5f) + 0.5f;
	const uint3 voxelPos = floor(diff * VoxelTextureSize);

	float4 pixelColor = diffuseTexture.Sample(Sampler_Aniso16_Wrap, input.uv);
	if (Inside(voxelPos))
	{
		uint currVal = outVoxelTextureDiffuse[voxelPos];
		uint decodedColor = EncodeColor(pixelColor);
		InterlockedMax(outVoxelTextureDiffuse[voxelPos], decodedColor);

		if (currVal < decodedColor)
			outVoxelTextureNormals[voxelPos] = normalize(float4(input.worldNormal, 0.f));
	}
}