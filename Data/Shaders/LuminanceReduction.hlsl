
Texture2D<float> InputMap : register(t0);
RWTexture2D<float> OutputMap : register(u0);

cbuffer Constants : register(b0)
{
    float TimeDelta;
    bool EnableAdaptation;
};

static const float AdaptationRate = 1.0f;
static const uint NumThreads = 16;
static const uint TotalNumThreads = NumThreads * NumThreads;
groupshared float LumSamples[TotalNumThreads];

[numthreads(NumThreads, NumThreads, 1)]
void main(uint3 groupID : SV_GroupID, uint3 groupThreadID : SV_GroupThreadID, uint threadIndex : SV_GroupIndex)
{
    uint2 textureSize;
    InputMap.GetDimensions(textureSize.x, textureSize.y);

    uint2 samplePos = groupID.xy * NumThreads + groupThreadID.xy;
    samplePos = min(samplePos, textureSize - 1);

    float pixelLuminance = InputMap[samplePos];

    // Store in shared memory
    LumSamples[threadIndex] = pixelLuminance;
    GroupMemoryBarrierWithGroupSync();

    // Reduce
	[unroll]
	for(uint s = NumThreads / 2; s > 0; s >>= 1)
    {
		if(threadIndex < s)
			LumSamples[threadIndex] += LumSamples[threadIndex + s];

		GroupMemoryBarrierWithGroupSync();
	}

    if(threadIndex == 0)
    {
        #ifdef IS_FINAL_PASS
			// Perform adaptation
			float lastLum = OutputMap[uint2(0, 0)];
			float currentLum = exp(LumSamples[0] / NumThreads);
			
			// Adapt the luminance using Pattanaik's technique
			const float Tau = AdaptationRate;
			float adaptedLum = EnableAdaptation ? lastLum + (currentLum - lastLum) * (1 - exp(-TimeDelta * Tau)) : currentLum;
			OutputMap[uint2(0, 0)] = adaptedLum;
        #else
			OutputMap[groupID.xy] = LumSamples[0] / NumThreads;
        #endif
    }
}