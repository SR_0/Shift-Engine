#include "CommonStructs.hlsli"

cbuffer transformations : register(b0)
{
    float4x4 projection;
    float4x4 view;
	float4x4 model;
	float3 cameraPosition;
};
SimplePixelIn main(SimpleVertexInstanced inputData)
{
	SimpleVertexIn input = inputData.objectData;
	ObjectInputInstance instance = inputData.instanceData;

	float4x4 modelTransform = float4x4(
		inputData.instanceData.mat0,
		inputData.instanceData.mat1,
		inputData.instanceData.mat2,
		inputData.instanceData.mat3
	);

    SimplePixelIn output;
    output.position = mul(modelTransform, float4(inputData.objectData.position.xyz, 1.0f));
	output.worldPosition = output.position.xyz;
    output.position = mul(view, output.position);
    output.position = mul(projection, output.position);

    float3x3 worldWithoutTranslation = (float3x3)modelTransform;

    float3 normal = normalize(mul(worldWithoutTranslation, inputData.objectData.normal));
    float3 binormal = normalize(mul(worldWithoutTranslation, inputData.objectData.binormal));
    float3 tangent = normalize(mul(worldWithoutTranslation, inputData.objectData.tangent));

    float3x3 tangentSpaceMatrix =
    {
        tangent.x, binormal.x, normal.x,
		tangent.y, binormal.y, normal.y,
		tangent.z, binormal.z, normal.z
    };

    output.tangentSpaceMatrix = tangentSpaceMatrix;
    output.color.rgb = inputData.objectData.color.rgb;
	output.color.a = inputData.objectData.color.a;
	output.worldNormal = normal;
	output.uv = inputData.objectData.uv;
	return output;
}