#include "CommonStructs.hlsli"
#include "StaticSamplers.hlsli"

TextureCube skyTexture : register(t0);
cbuffer transformations : register(b0)
{
	float4x4 projection;
	float4x4 view;
	float4x4 model;
	float3 cameraPosition;
};
cbuffer Settings : register(b1)
{
	float Intensity;
};
SimplePixelOut main(in SkyPixelIn input)
{
	SimplePixelOut output;

	//float4 gradColor = float4(0.403f, 0.341f, 0.337f, 1.0f);
	//
	//float3 camToSkyDir = normalize(input.worldPosition - cameraPosition);
	//float d0 = saturate(dot(camToSkyDir, float3(0.0f, 1.0f, 0.0f)));
	//float4 skyColor = float4(0.f, 0.f, 0.f, 0.f);
	//
	//if (d0 > 0.0f)
	//	skyColor = skyTexture.SampleLevel(samplerState, input.eyeDirection, 0);
	//
	//float4 outputColor = lerp(gradColor, skyColor, d0);
	output.color = float4(skyTexture.SampleLevel(Sampler_Aniso16_Wrap, input.eyeDirection, 0).rgb * 2.f, 0.0f) * Intensity;
	return output;
}