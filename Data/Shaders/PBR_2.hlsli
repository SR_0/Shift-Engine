#ifndef _PBR2_H_
#define _PBR2_H_
#include "GeneralIncludes.hlsli"
#include "PBR_Structs.hlsli"


float Lambert(in ObjectAttributes attributes, in float3 toLight)
{
	// Lambert
	return (saturate(dot(attributes.myNormal, toLight)));
}

float3 Fresnel(in ObjectAttributes attributes, in float3 toEye, in float3 toLight)
{
	const float3 substance = (Dielectric - (Dielectric * attributes.myMetallic)) + (attributes.myDiffuse * attributes.myMetallic);
	const float3 halfvec = normalize(toLight + toEye);
	float LdotH = dot(toLight, halfvec);
	LdotH = pow(1.0f - saturate(LdotH), 5);
	float3 fresnel = LdotH * (1.f - substance);
	fresnel = substance + fresnel;

	return max(0.f, fresnel);
}

float3 ReflectionFresnel(in ObjectAttributes attributes, in float3 toEye)
{
	float VdotN = dot(toEye, attributes.myNormal);
	VdotN = pow(1.0f - saturate(VdotN), 5);

	const float3 substance = (Dielectric - (Dielectric * attributes.myMetallic)) + (attributes.myDiffuse * attributes.myMetallic);
	float3 refFresnel = VdotN * (1.f - substance);
	refFresnel = refFresnel / (6 - 5 * attributes.myRoughness);
	refFresnel = substance + refFresnel;

	return max(0.f, refFresnel);
}

float GGX(in ObjectAttributes attributes, in float3 toEye, in float3 toLight)
{
	const float3 halfvec = normalize(toLight + toEye);
	const float HdotN = saturate(dot(halfvec, attributes.myNormal));
	const float m = attributes.myRoughness * attributes.myRoughness;
	const float m2 = m * m;
	const float denominator = HdotN * HdotN * (m2 - 1.f) + 1.f;
	const float distribution = m2 / (PI * denominator * denominator);

	return distribution;
}

float Visibility(in ObjectAttributes attributes, in float3 toEye, in float3 toLight)
{
	const float roughnessRemapped = (attributes.myRoughness + 1.f) / 2.f;
	float alphaG2 = attributes.myRoughness * attributes.myRoughness;
	const float NdotV = saturate(dot(attributes.myNormal, toLight));
	const float NdotL = abs(dot(attributes.myNormal, toEye)) + 1e-5f;
	const float k = roughnessRemapped * roughnessRemapped * 1.7724f;
	const float G1V = NdotV * (1.f - k) + k;
	const float G1L = NdotL * (1.f - k) + k;
	const float visability = (NdotV * NdotL) / (G1V * G1L);
	return visability;
}

// Direct
float3 DirectDiffuse(in ObjectAttributes attributes, in PBRAttributes pbrData)
{
	const float3 metallicAlbedo = attributes.myDiffuse - (attributes.myDiffuse * attributes.myMetallic);
	const float3 returnColor = metallicAlbedo * pbrData.myAttenuation * (float3(1.f, 1.f, 1.f) - pbrData.myFresnel);
	return returnColor;
}
float3 DirectSpecular(in PBRAttributes pbrData)
{
	return float3(pbrData.myAttenuation * pbrData.myFresnel * pbrData.myGGX * pbrData.myVisibility);
}

float3 DirectLight(in ObjectAttributes attributes, float3 toEye, float3 toLight, float3 lightColor, float lightIntensity, float attenuation)
{
	PBRAttributes pbrAttributes;
	pbrAttributes.myAttenuation = attenuation * Lambert(attributes, toLight);
	pbrAttributes.myFresnel = Fresnel(attributes, toEye, toLight);
	pbrAttributes.myGGX = GGX(attributes, toEye, toLight);
	pbrAttributes.myVisibility = Visibility(attributes, toEye, toLight);

	float3 directLight = (DirectDiffuse(attributes, pbrAttributes) + DirectSpecular(pbrAttributes)) * lightColor * lightIntensity;
	return directLight;
}

// Ambient
Shift_TextureCube(skyTexture, TEXSLOT_SKY_DIFFUSE);
Shift_TextureCube(skySpecTexture, TEXSLOT_SKY_SPECULAR);
Shift_TextureCube(skyBrdfTexture, TEXSLOT_SKY_BRDF);
uint QuerySkyTextureLevels()
{
	uint width, height, levels;
	skyTexture.GetDimensions(0, width, height, levels);
	return levels;
}
float RoughToSPow(float roughness)
{
	const float val = (2.f / (roughness * roughness)) - 2.f;
	return val > 0 ? val : 0;
}

static const float k0 = 0.00098f;
static const float k1 = 0.9921f;
static const float fakeLysMaxSpecularPower = (2.f / (0.0014f * 0.0014f)) - 2.f;
static const float fMaxT = (exp2(-10.f / sqrt(fakeLysMaxSpecularPower)) - k0) / k1;
float GetSpecPowToMip(float fSpecPow, int nMips)
{
	const float fSmulMaxT = (exp2(-10.f / sqrt(fSpecPow)) - k0) / k1;
	return float(nMips - 1 - 0) * (1.0f - clamp(fSmulMaxT / fMaxT, 0.0f, 1.0f));
}

float3 AmbientDiffuse(ObjectAttributes attributes, float3 reflectionFresnel, uint mipCount)
{
	const float3 metallicAlbedo = attributes.myDiffuse - (attributes.myDiffuse * attributes.myMetallic);
	const float3 ambientLight = skyTexture.SampleLevel(Sampler_Aniso16_Wrap, attributes.myNormal, mipCount - 2).rgb;
	return (metallicAlbedo * ambientLight * (float3(1.f, 1.f, 1.f) - reflectionFresnel));
}

float3 AmbientSpecular(ObjectAttributes attributes, float3 reflectionFresnel, in float3 toEye, uint mipCount)
{
	const float3 reflectionVector = -reflect(toEye, attributes.myNormal);
	const float fakeLysSpecularPower = RoughToSPow(attributes.myRoughness);
	const float lysMipMap = GetSpecPowToMip(fakeLysSpecularPower, mipCount);
	const float3 ambientLight = skyTexture.SampleLevel(Sampler_Aniso16_Wrap, reflectionVector.xyz, lysMipMap).xyz;
	return (ambientLight * reflectionFresnel);
}

float3 AmbientLight(in ObjectAttributes attributes, in float3 toEye)
{
	float3 reflectionFresnel = ReflectionFresnel(attributes, toEye);
	uint mipCount = QuerySkyTextureLevels();

	float3 ambientLight = (AmbientDiffuse(attributes, reflectionFresnel, mipCount) + AmbientSpecular(attributes, reflectionFresnel, toEye, mipCount));
	return ambientLight;
}
#endif