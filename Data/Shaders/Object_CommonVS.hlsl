#include "Globals.hlsli"

PixelInputType main(ObjectInputCommon input)
{
    PixelInputType output;

    output.position = mul(input.position, global_MainCameraVP);

    return output;
}