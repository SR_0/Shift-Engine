#include "PBR.hlsli"
#include "NormalEncode.hlsli"
#include "ShadowMapCSM.hlsli"
#include "VoxelConeTracing_Common.hlsli"

Shift_Texture2D(depthTexture, TEXSLOT_SCENE_DEPTH);
Shift_Texture2D(colorTexture, TEXSLOT_GBUFFER_COLOR);
Shift_Texture2D(normalTexture, TEXSLOT_GBUFFER_NORMALS);
Shift_Texture2D(armTexture, TEXSLOT_GBUFFER_ARM);
Shift_Texture2D(emissionTexture, TEXSLOT_GBUFFER_EMISSION);
Shift_Texture2D(SSAO, 7);
Shift_Texture3D(VoxelRadiance, TEXSLOT_VOXEL_SCENE);

RWTexture2D<float4> outLightTexture : register(u0);

cbuffer LightInfo : register(b1)
{
	float3 LightColor;
	float Intensity;
	float3 ToLightDirection;
	float AmbientLightIntensity;
	float3 PointLightColor;
	float PointLightRange;
	float3 PointLightPos;
	float PointLightIntensity;
}

cbuffer VoxelSettings : register(b2)
{
	float3 VoxelCenter;
	float VoxelTextureSize;
	float VoxelSize;
};

uint3 WorldToVoxel(in float3 aWorldPos)
{
	return floor((((aWorldPos - VoxelCenter) / VoxelTextureSize / VoxelSize) * float3(0.5, -0.5, 0.5) + 0.5) * VoxelTextureSize);
}
float3 TraceToLightSource(float3 pos, float3 toLight)
{
	const float3 directionalIncrement = normalize(toLight) * VoxelSize;
	const float travelStepDistance = length(directionalIncrement);
	const float maxDistance = travelStepDistance * VoxelTextureSize;
	float3 currPos = pos + directionalIncrement * 2;
	float currDistance = 0.0;

	float3 outLight = (float3)(1.0);

	while (currDistance < maxDistance)
	{
		float3 currSamplePos = WorldToVoxel(currPos);
		float alpha = VoxelRadiance[currSamplePos].a;
		if (alpha >= 1.0)
			outLight *= 0.25;

		currPos += directionalIncrement;
		currDistance += travelStepDistance;
	}
	return outLight;
}

[numthreads(8, 8, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
	float2 Resolution = GetResolution(outLightTexture);
	if (threadID.x <= (uint)Resolution.x && threadID.y <= (uint)Resolution.y)
	{
		int3 sampleLocation = int3(threadID.xy, 0);

		float4 colorSample = colorTexture.Load(sampleLocation);
		
		if (colorSample.a > 0.0)
		{
			float depthSample = depthTexture.Load(sampleLocation).r;
			float2 screenPos = PixelToUV(sampleLocation.xy, Resolution);
			float3 worldPosition = WorldPositionFromDepth(screenPos, depthSample, InverseProjection, InverseView);
		
			float3 normalSample = normalTexture.SampleLevel(Sampler_Linear_Clamp, screenPos, 0).xyz * 2.0 - 1.0;
			float3 armSample = armTexture.SampleLevel(Sampler_Linear_Clamp, screenPos, 0).xyz;
		
			float3 toCamera = normalize(CameraPosition - worldPosition);
		
			ObjectAttributes attributes;
			attributes.myWorldPosition = worldPosition;
			attributes.myNormal = normalSample;
			attributes.myDiffuse = colorSample.rgb;
			attributes.myAO = armSample.r;
			attributes.myRoughness = armSample.g;
			attributes.myMetallic = armSample.b;
		
			float3 directLight = float3(0.0, 0.0, 0.0);
			
			// Sun
			float3 debugShadowColor = float3(0.0, 0.0, 0.0);

			float shadowFactor = GetShadowFactor(worldPosition, screenPos
#ifdef SHADOWS_ENABLE_CSM_DEBUG
				, debugShadowColor
#endif
			);

			directLight += shadowFactor * DirectLight(attributes, toCamera, normalize(ToLightDirection), LightColor, Intensity, 1.0);
#ifdef SHADOWS_ENABLE_CSM_DEBUG
			directLight += debugShadowColor * 10.0;
#endif

			// Points
			{
				float3 toLight = PointLightPos - worldPosition;
				float toLightDistance = distance(PointLightPos, worldPosition);
				toLight = normalize(toLight);
				float lightRange = PointLightRange * PointLightRange;
				float toLightDist2 = toLightDistance * toLightDistance;
		
				float linearAttenuation = saturate(1.f - (toLightDist2 / lightRange));
				float physicalAttenuation = 1.f / toLightDistance;
				float attenuation = linearAttenuation * physicalAttenuation;
		
				directLight += (DirectLight(attributes, toCamera, toLight, PointLightColor, PointLightIntensity, attenuation));
			}
			float ssao = SSAO.SampleLevel(Sampler_Linear_Clamp, screenPos, 0).x;
			float3 ambientLight = ssao * ((colorSample.rgb * 1.0f) + skyDiffuse.SampleLevel(Sampler_Linear_Clamp, reflect(-toCamera,attributes.myNormal), 0) * 0.4f);//AmbientLight(attributes, toCamera) * 8.0;

			float3 emissionSample = emissionTexture.Load(sampleLocation).rgb;
			outLightTexture[sampleLocation.xy] = float4(directLight + ambientLight + emissionSample, 1.0);
		}
	}
}