#ifndef _CONVERTERS_H_
#define _CONVERTERS_H_

float4 RGBA8ToFloat4(uint aValue)
{
	float4 res = float4(
		float((aValue & 0x000000FF)			),
		float((aValue & 0x0000FF00) >> 8u	),
		float((aValue & 0x00FF0000) >> 16u	),
		float((aValue & 0xFF000000) >> 24u	)
	);
	return res;
}

uint Float4ToRGBA8(float4 aValue)
{
	uint res =	((uint(aValue.w) & 0x000000FF) << 24U) |
				((uint(aValue.z) & 0x000000FF) << 16U) |
				((uint(aValue.y) & 0x000000FF) <<  8U) |
				(uint(aValue.x) & 0x000000FF);
	return res;
}
uint EncodeColor(in float4 color)
{
	const float __hdrRange = 10.0f;
	// normalize color to LDR
	float hdr = length(color.rgb);
	color.rgb /= hdr;

	// encode LDR color and HDR range
	uint3 iColor = uint3(color.rgb * 255.0f);
	uint iHDR = (uint) (saturate(hdr / __hdrRange) * 127);
	uint colorMask = (iHDR << 24u) | (iColor.r << 16u) | (iColor.g << 8u) | iColor.b;

	// encode alpha into highest bit
	uint iAlpha = (color.a > 0 ? 1u : 0u);
	colorMask |= iAlpha << 31u;

	return colorMask;
}

// Decode 32 bit uint into HDR color with 1 bit alpha
float4 DecodeColor(in uint colorMask)
{
	const float __hdrRange = 10.0f;
	float hdr;
	float4 color;

	hdr = (colorMask >> 24u) & 0x0000007f;
	color.r = (colorMask >> 16u) & 0x000000ff;
	color.g = (colorMask >> 8u) & 0x000000ff;
	color.b = colorMask & 0x000000ff;

	hdr /= 127.0f;
	color.rgb /= 255.0f;

	color.rgb *= hdr * __hdrRange;

	color.a = (colorMask >> 31u) & 0x00000001;

	return color;
}

#endif