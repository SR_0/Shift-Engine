#include "CommonStructs.hlsli"

cbuffer transformations : register(b0)
{
    float4x4 projection;
    float4x4 view;
    float4x4 model;
};
SimplePixelIn main(SimpleVertexIn input)
{
    SimplePixelIn output;
    output.position = mul(model, float4(input.position.xyz, 1.0f));
    output.position = mul(view, output.position);
    output.position = mul(projection, output.position);

    float3x3 worldWithoutTranslation = (float3x3) model;
    float3 normal = normalize(mul(worldWithoutTranslation, input.normal));
    float3 binormal = normalize(mul(worldWithoutTranslation, input.binormal));
    float3 tangent = normalize(mul(worldWithoutTranslation, input.tangent));
    float3x3 tangentSpaceMatrix =
    {
        tangent.x, tangent.y, tangent.z,
		binormal.x, binormal.y, binormal.z,
		normal.x, normal.y, normal.z
    };

    output.tangentSpaceMatrix = tangentSpaceMatrix;
    output.color.rgb = input.color.rgb;
	output.color.a = input.color.a;
	output.uv = input.uv;
	return output;
}