#ifndef _COMMONSTRUCTS_H_
#define _COMMONSTRUCTS_H_
#include "GeneralShaderInterop.h"

struct SimpleVertexIn
{
	float4 position : POSITION;
    float4 color : COLOR;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 binormal : BINORMAL;
	float2 uv : UV;
};

struct SimpleDepthOut
{
	float4 position : SV_POSITION;
};

struct TerrainVertex
{
	float4 position : POSITION;
	float2 uv : UV;
};

struct TerrainPixelIn
{
	float4 position : SV_POSITION;
	float3 normal : NORMAL;
	float2 uv : UV;
};

struct SimplePixelIn
{
    float3x3 tangentSpaceMatrix : TANGENTSPACEMATRIX;
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float3 worldPosition : WORLD_POSITION;
	float3 worldNormal : WORLD_NORMAL;
	float2 uv : UV;
}; 
struct VoxelGeometryIn
{
	//float3x3 tangentSpaceMatrix : TANGENTSPACEMATRIX;
	float4 worldPosition : SV_POSITION;
	float3 worldNormal : NORMAL;
	float2 uv : UV;
}; 
struct VoxelPixelIn
{
	//float3x3 tangentSpaceMatrix : TANGENTSPACEMATRIX;
	float4 position : SV_POSITION;
	float3 worldPosition : WORLD_POSITION;
	float3 worldNormal : NORMAL;
	float2 uv : UV;
};

struct VoxelDebugPixelIn
{
	float4 position : SV_POSITION;
	float4 color : TEXCOORD;
};
struct FullscreenPixelIn
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float2 uv : UV;
};

struct SkyPixelIn
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float3 worldPosition : WORLDPOS;
	float3 eyeDirection : EYEDIRECTION;
};

struct SimplePixelOut
{
	float4 color : SV_TARGET;
};

struct GBufferOut
{
    float4 COLOR : SV_TARGET0;
	float3 NORMAL : SV_TARGET1;
    float4 ARM : SV_TARGET2;
	float4 EMISSIVE : SV_TARGET3;
};

struct ObjectInputInstance
{
    float4 mat0 : INSTANCE_MAT0;
    float4 mat1 : INSTANCE_MAT1;
    float4 mat2 : INSTANCE_MAT2;
    float4 mat3 : INSTANCE_MAT3;
    float4 instanceColor : INSTANCE_COLOR;
};

struct SimpleVertexInstanced
{
	SimpleVertexIn objectData;
	ObjectInputInstance instanceData;
};

#endif