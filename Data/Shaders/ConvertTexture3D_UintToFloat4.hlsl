#include "GeneralIncludes.hlsli"
#include "Converters.hlsli"

Texture3D<uint> srcTexture : register(t0);
RWTexture3D<float4> dstTexture : register(u0);

[numthreads( 4, 4, 4 )]
void main(uint3 threadIndex : SV_DispatchThreadID)
{
	float4 output = DecodeColor(srcTexture[threadIndex.xyz]);
	dstTexture[threadIndex.xyz] = output;
}