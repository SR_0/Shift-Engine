#include "CommonStructs.hlsli"
#include "StaticSamplers.hlsli"

cbuffer transformations : register(b0)
{
    float4x4 WorldToClip;
	float3 cameraPosition;
};

Shift_Texture2D(HeightTexture, 0);
Shift_Texture2D(NormalMap, 1);

TerrainPixelIn main(TerrainVertex input)
{
    TerrainPixelIn output;
	float height = HeightTexture.SampleLevel(Sampler_Aniso16_Clamp, input.uv, 0);
    float3 normal = (float3)0;//normalize(NormalMap.SampleLevel(Sampler_Aniso16_Clamp, input.uv, 0).xyz);

	output.normal = normal;
    output.position = float4(input.position.xyz, 1.0f);
	output.position.y = height * 100.0;
    output.position = mul(WorldToClip, output.position);
	output.uv = input.uv;
	return output;
}