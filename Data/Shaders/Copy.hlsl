#include "StaticSamplers.hlsli"
#include "Utilities.hlsli"

Texture2D inTexture : register(t0);
RWTexture2D<float4> outTexture : register(u0);

[numthreads(8, 8, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
	float2 OutTextureResolution = GetResolution(outTexture);
	if (threadID.x <= OutTextureResolution.x && threadID.y <= OutTextureResolution.y)
	{
		int3 sampleLocation = int3(threadID.xy, 0);
		float2 uv = PixelToUV(sampleLocation.xy, OutTextureResolution);
		float3 pixelColor = inTexture.SampleLevel(Sampler_Linear_Clamp, uv, 0).rgb;

		outTexture[sampleLocation.xy] = float4(pixelColor, 1.0f);
	}
}