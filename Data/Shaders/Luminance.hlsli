#ifndef _LUMINANCE_H_
#define _LUMINANCE_H_

float Luminance(float3 aColor)
{
    return dot(aColor, float3(0.2126f, 0.7152f, 0.0722f));
}

#endif