#ifndef _UTILITIES_H_
#define _UTILITIES_H_
#include "Converters.hlsli"

float2 GetResolution(in const Texture2D<float4> aTexture)
{
	float width = 0.0f;
	float height = 0.0f;
	aTexture.GetDimensions(width, height);
	return float2(width, height);
}

float2 GetResolution(in const RWTexture2D<float4> aTexture)
{
	float width = 0.0f;
	float height = 0.0f;
	aTexture.GetDimensions(width, height);
	return float2(width, height);
}

float2 PixelToUV(in const int2 aPixelCoord, in const float2 aResolution)
{
	float2 uv = (aPixelCoord.xy / aResolution);
	uv.x = aPixelCoord.x / aResolution.x + 0.5f / aResolution.x;
	uv.y = aPixelCoord.y / aResolution.y + 0.5f / aResolution.y;
	return uv;
}
#endif