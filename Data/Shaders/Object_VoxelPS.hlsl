#include "CommonStructs.hlsli"
SimplePixelOut main(in VoxelDebugPixelIn input)
{
	SimplePixelOut output;
	output.color = saturate(float4(input.color.rgb, 1.f));
	return output;
}