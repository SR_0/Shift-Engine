#include "CommonStructs.hlsli"
cbuffer transformations : register(b0)
{
	float4x4 projection;
	float4x4 view;
	float4x4 model;
	float3 cameraPosition;
};
cbuffer VoxelSettings : register(b1)
{
	float3 VoxelCenter;
	float VoxelTextureSize;
	float VoxelSize;
};

inline float3 CreateCube(in uint vertexID)
{
	const uint b = 1 << vertexID;
	return float3((0x287a & b) != 0, (0x02af & b) != 0, (0x31e3 & b) != 0);
}

[maxvertexcount(14)]
void main(point VoxelDebugPixelIn input[1], inout TriangleStream<VoxelDebugPixelIn> output)
{
	float4x4 viewProjection = mul(projection, view);
	[branch]
	if (input[0].color.a > 0)
	{
		for (uint i = 0; i < 14; i++)
		{
			VoxelDebugPixelIn element;
			element.position = input[0].position;
			element.color = input[0].color;

			element.position.xyz = element.position.xyz / VoxelTextureSize * 2 - 1;
			element.position.y = -element.position.y;
			element.position.xyz *= VoxelTextureSize;
			element.position.xyz += (CreateCube(i) - float3(0, 1, 0)) * 2;
			element.position.xyz *= VoxelTextureSize * VoxelSize / VoxelTextureSize;

			element.position = mul(viewProjection, element.position);

			output.Append(element);
		}
		output.RestartStrip();
	}
}