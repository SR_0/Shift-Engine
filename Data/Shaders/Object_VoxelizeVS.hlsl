#include "CommonStructs.hlsli"

VoxelGeometryIn main(SimpleVertexInstanced inputData)
{
	SimpleVertexIn input = inputData.objectData;
	ObjectInputInstance instance = inputData.instanceData;

	float4x4 modelTransform = float4x4(
		inputData.instanceData.mat0,
		inputData.instanceData.mat1,
		inputData.instanceData.mat2,
		inputData.instanceData.mat3
		);

	VoxelGeometryIn output;
	output.worldPosition = mul(modelTransform, float4(inputData.objectData.position.xyz, 1.0f));

	float3x3 worldWithoutTranslation = (float3x3) modelTransform;

	float3 normal = mul(worldWithoutTranslation, inputData.objectData.normal.xyz);
	output.worldNormal = normal;
	output.uv = inputData.objectData.uv;
	return output;
}