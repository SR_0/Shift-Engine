#include "CommonStructs.hlsli"

FullscreenPixelIn main(SimpleVertexIn input)
{
	FullscreenPixelIn output;

	output.position = input.position;
	output.color = input.color;
	output.uv = input.uv;
	return output;
}