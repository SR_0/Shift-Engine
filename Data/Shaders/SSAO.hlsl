#include "GeneralIncludes.hlsli"

Shift_Texture2D(depthTexture, TEXSLOT_SCENE_DEPTH);
Shift_Texture2D(normalTexture, TEXSLOT_GBUFFER_NORMALS);
Shift_Texture2D(randTexture, TEXSLOT_SSAO_RAND);

Shift_RWTexture2D(outTexture, 0, float4);

cbuffer SSAO_Settings : register(b0)
{
	float4x4 WorldToCamera;
	float4x4 InverseProjection;
	float SamplingRadius;
	float Scale;
	float Bias;
	float Intensity;
}

float OcclusionSample(in float2 aCoord, in float2 aUV, in float3 aPos, in float3 aNormal)
{
	float depth = depthTexture.SampleLevel(Sampler_Aniso16_Clamp, aCoord + aUV, 0).r;
	float3 diff = (ViewPositionFromDepth(aCoord + aUV , depth, InverseProjection).xyz) - aPos;
	const float3 v = normalize(diff);
	const float d = length(diff) * Scale;
	return max(0.0, dot(aNormal, v) - Bias) * (1.0 / (1.0 + d)) * Intensity;
}

[numthreads(32, 32, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
	float2 Resolution = GetResolution(outTexture);
	if (threadID.x <= Resolution.x && threadID.y <= Resolution.y)
	{
		int3 sampleLocation = int3(threadID.xy, 0);
		outTexture[sampleLocation.xy] = (float4)1.f;
		float2 uv = PixelToUV(sampleLocation, Resolution);
		float3 normalSample = normalTexture.Load(sampleLocation) * 2.0 - 1.0;
		normalSample = normalize(mul((float3x3)WorldToCamera, normalSample));
		
		
		float depthSample = depthTexture.Load(sampleLocation).r;
		float3 viewPosition = ViewPositionFromDepth(uv, depthSample, InverseProjection);
		float2 rand = randTexture.SampleLevel(Sampler_Linear_Wrap, Resolution * uv / float(64.f), 0) * 2.0f - 1.0f;

		float accumulatedOcclusion = 0.0f;
		float radius = SamplingRadius / viewPosition.z;
		const float2 direction[4] = { float2(1,0), float2(-1,0), float2(0,1), float2(0,-1) };

		int iterations = 4;
		for (int j = 0; j < iterations; ++j)
		{
			float2 coord1 = reflect(direction[j], rand) * radius;
			float2 coord2 = float2(coord1.x * 0.707 - coord1.y * 0.707, coord1.x * 0.707 + coord1.y * 0.707);

			accumulatedOcclusion += OcclusionSample(uv, coord1 * 0.25,	viewPosition, normalSample);
			accumulatedOcclusion += OcclusionSample(uv, coord2 * 0.5,	viewPosition, normalSample);
			accumulatedOcclusion += OcclusionSample(uv, coord1 * 0.75,	viewPosition, normalSample);
			accumulatedOcclusion += OcclusionSample(uv, coord2,			viewPosition, normalSample);
		}

		accumulatedOcclusion /= iterations * 4.f;
		outTexture[sampleLocation.xy] = (float4)(1 - accumulatedOcclusion);
	}
}