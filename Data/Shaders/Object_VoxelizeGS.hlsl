#include "CommonStructs.hlsli"

#define ENABLE_SOFTWARE_CONSERVATIVE_RASTER 0

cbuffer VoxelSettings : register(b0)
{
	float3 VoxelCenter;
	float VoxelTextureSize;
	float VoxelSize;
};

[maxvertexcount(3)]
void main(triangle VoxelGeometryIn input[3], inout TriangleStream<VoxelPixelIn> outputStream)
{
	VoxelPixelIn output[3];

	const float3 faceNormal = abs(input[0].worldNormal.xyz + input[1].worldNormal.xyz + input[2].worldNormal.xyz);
	uint maxFace = faceNormal[1] > faceNormal[0] ? 1 : 0;
	maxFace = faceNormal[2] > faceNormal[maxFace] ? 2 : maxFace;

	[unroll]
	for (uint i = 0; i < 3; ++i)
	{
		output[i].position = float4((input[i].worldPosition.xyz - VoxelCenter) / VoxelSize, 1.f);
		output[i].position.xyz = ((maxFace == 0) ? output[i].position.zyx : ((maxFace == 1) ? output[i].position.xzy : output[i].position.xyz));
	}
	
	// Expand triangle to get fake Conservative Rasterization:
	//float2 side0N = normalize(output[1].position.xy - output[0].position.xy);
	//float2 side1N = normalize(output[2].position.xy - output[1].position.xy);
	//float2 side2N = normalize(output[0].position.xy - output[2].position.xy);
	//output[0].position.xy += normalize(side2N - side0N);
	//output[1].position.xy += normalize(side0N - side1N);
	//output[2].position.xy += normalize(side1N - side2N);

	[unroll]
	for (uint j = 0; j < 3; j++)
	{
		output[j].position.xy /= VoxelTextureSize;
		output[j].position.zw = 1;
		output[j].worldPosition = input[j].worldPosition.xyz;
		output[j].uv = input[j].uv;
		output[j].worldNormal = input[j].worldNormal;
		outputStream.Append(output[j]);
	}

	outputStream.RestartStrip();
}