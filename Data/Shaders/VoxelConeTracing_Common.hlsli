#ifndef _VCT_COMMON_H_
#define _VCT_COMMON_H_

#define VOXEL_CONE_QUALITY (8)
#define VOXEL_CONE_QUALITY_INVERSED (1/8.f)
#define VOXEL_CONE_PI (3.14159265359f)

inline float4 ConeTrace(in Texture3D<float4> aVoxelRadiance, in float3 aPosition, in float3 aNormal, in float3 aConeDirection, in float aConeAperture,
	in float aVoxelSize, in float aVoxelTextureSize, in float3 aVoxelCenter, in uint aNumMips)
{
	float3 color = 0;
	float alpha = 0;
	float currentDistance = aVoxelSize;
	const float3 startPos = aPosition + aNormal * aVoxelSize * 2;
	const float maxDistance = 100.f * aVoxelSize;
	const float voxelSizeInverted = 1 / aVoxelSize;
	const float voxelTextureSizeInverted = 1 / aVoxelTextureSize;
	while (currentDistance < maxDistance && alpha < 1)
	{
		const float diameter = max(aVoxelSize, 2 * aConeAperture * currentDistance);
		const float mip = log2(diameter * voxelSizeInverted);

		float3 currentConePosition = startPos + aConeDirection * currentDistance;
		currentConePosition = (((((currentConePosition - aVoxelCenter) * voxelSizeInverted) * voxelTextureSizeInverted) * float3(0.5f, -0.5f, 0.5f)) + 0.5f);

		if (any(currentConePosition - saturate(currentConePosition)) || mip >= (float)aNumMips)
		{
			break;
		}

		const float4 radianceSample = aVoxelRadiance.SampleLevel(Sampler_Linear_Clamp, currentConePosition, mip);
		float a = 1 - alpha;
		color += a * radianceSample.rgb;
		alpha += a * radianceSample.a;
		currentDistance += diameter * 0.6f;
	}

	return float4(color, alpha);
}

inline float4 ConeTraceRadiance(in Texture3D<float4> aVoxelRadiance, in float3 aPosition, in float3 aNormal,
	in float aVoxelSize, in float aVoxelTextureSize, in float3 aVoxelCenter, in uint aNumMips)
{
	const float3 VCT_Cones[] =
	{
		float3(0.57735, 0.57735, 0.57735),
		float3(0.57735, -0.57735, -0.57735),
		float3(-0.57735, 0.57735, -0.57735),
		float3(-0.57735, -0.57735, 0.57735),
		float3(-0.903007, -0.182696, -0.388844),
		float3(-0.903007, 0.182696, 0.388844),
		float3(0.903007, -0.182696, 0.388844),
		float3(0.903007, 0.182696, -0.388844),
		float3(-0.388844, -0.903007, -0.182696),
		float3(0.388844, -0.903007, 0.182696),
		float3(0.388844, 0.903007, -0.182696),
		float3(-0.388844, 0.903007, 0.182696),
		float3(-0.182696, -0.388844, -0.903007),
		float3(0.182696, 0.388844, -0.903007),
		float3(-0.182696, 0.388844, 0.903007),
		float3(0.182696, -0.388844, 0.903007)
	};


	float4 radiance = 0;
	float ambientFactor = 0.f;
	[unroll]
	for (uint cone = 0; cone < VOXEL_CONE_QUALITY; ++cone)
	{
		float3 coneDirection = reflect(VCT_Cones[cone], aNormal);
		coneDirection *= (dot(coneDirection, aNormal) < 0) ? -1 : 1;
		radiance += ConeTrace(aVoxelRadiance, aPosition, aNormal, coneDirection, tan(VOXEL_CONE_PI * 0.125f), aVoxelSize, aVoxelTextureSize, aVoxelCenter, aNumMips);

	}

	ambientFactor = 1.0f - saturate(ambientFactor);

	radiance *= VOXEL_CONE_QUALITY_INVERSED;
	radiance.a = saturate(radiance.a);

	return max(0, radiance);
}

#endif