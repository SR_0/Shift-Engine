#include "GeneralIncludes.hlsli"

Texture3D<float4> srcTexture : register(t0);

// Destination could probably be an array instead, with the constantbuffer holding the number of targets.
RWTexture3D<float4> dstTexture : register(u0);



cbuffer CB : register(b0)
{
	float3 TextureResolution;
}

[numthreads( 4, 4, 4 )]
void main(uint3 threadIndex : SV_DispatchThreadID)
{
	// If using arrays later, texelSize could be determined from the destination texture by querying the dimensions.
	float3 uvw = (threadIndex.xyz / TextureResolution);
	uvw.x = threadIndex.x / TextureResolution.x + 0.5f / TextureResolution.x;
	uvw.y = threadIndex.y / TextureResolution.y + 0.5f / TextureResolution.y;
	uvw.z = threadIndex.z / TextureResolution.z + 0.5f / TextureResolution.z;
	
	float4 color = srcTexture.SampleLevel(Sampler_Linear_Clamp, uvw, 0);
	dstTexture[threadIndex.xyz] = color;
}