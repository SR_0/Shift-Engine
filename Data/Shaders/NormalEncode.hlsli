#ifndef _NORMAL_ENCODE_H_
#define _NORMAL_ENCODE_H_

float2 Normal_Encode(float3 aNormal)
{
	aNormal.xy /= dot(1, abs(aNormal));
	if (aNormal.z <= 0)
		aNormal.xy = (1 - abs(aNormal.yx)) * (aNormal.xy >= 0 ? float2(1, 1) : float2(-1, -1));
	return aNormal.xy;
}

float3 Normal_Decode(float2 aEncoded)
{
	float3 N = float3(aEncoded, 1 - dot(1, abs(aEncoded)));
	if (N.z < 0)
		N.xy = (1 - abs(N.yx)) * (N.xy >= 0 ? float2(1, 1) : float2(-1, -1));
	return normalize(N);
}
#endif