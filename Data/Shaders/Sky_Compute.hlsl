#include "GeneralIncludes.hlsli"
#include "Sky.hlsli"

TextureCube skyTexture : register(t0);
#ifdef SKY_PROBE
	RWTexture2DArray<float4> outLightTexture : register(u0);
#else
	RWTexture2D<float4> outLightTexture : register(u0);
#endif

cbuffer CameraInfo : register(b0)
{
	float4x4 InverseProjection;
	float4x4 InverseView;
	float3 CameraPosition;
	
#ifdef SKY_PROBE
	int CubeFace;
#endif
}

[numthreads(8, 8, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
	const float PI = 3.141592653;
	
#ifdef SKY_PROBE
	float2 Resolution = (float2)256.0;
#else
	float2 Resolution = GetResolution(outLightTexture);
#endif
	if (threadID.x > Resolution.x || threadID.y > Resolution.y)
		return;
	
	int3 pixelLocation = int3(threadID.xy, 0);
	float2 uv = PixelToUV(pixelLocation, Resolution);
	float3 worldPosition = WorldPositionFromDepth(uv, 1.0, InverseProjection, InverseView);
	float3 viewDir = normalize(worldPosition - CameraPosition);
	
	float4 outColor = (float4)0;
	
	outColor.rgb += CalculateSkyColor(viewDir) * 8.0;
	
#ifdef SKY_PROBE
	outLightTexture[int3(pixelLocation.xy, CubeFace)] = outColor;
#else
	outLightTexture[pixelLocation.xy] = outColor;
#endif
}