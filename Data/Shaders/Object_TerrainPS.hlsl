#include "CommonStructs.hlsli"
#include "StaticSamplers.hlsli"

Shift_Texture2D(NormalMap, 1);
GBufferOut main(in TerrainPixelIn input)
{
	GBufferOut output;
	
    float3 normal = normalize(NormalMap.SampleLevel(Sampler_Aniso16_Clamp, input.uv, 0).xzy);
	float3 arm = float3(1.0, 0.5, 0.0);
    output.COLOR.rgb = (float3) 0.5;
	output.COLOR.a = 1.0;
	output.NORMAL = float4(normal, 0.0);
	output.ARM.rgb = arm;
	output.ARM.a = 0.0f;
	output.EMISSIVE = float4(0.0, 0.0, 0.0, 0.0);
	return output;
}