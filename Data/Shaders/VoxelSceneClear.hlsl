RWTexture3D<uint> outVoxelTexture : register(u0);

[numthreads(8,8,8)]
void main(uint3 threadIndex : SV_DispatchThreadID)
{
	outVoxelTexture[threadIndex] = 0;
}