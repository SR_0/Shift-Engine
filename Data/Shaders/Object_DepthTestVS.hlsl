#include "CommonStructs.hlsli"

cbuffer transformations : register(b0)
{
	float4x4 worldToClip;
	float3 cameraPosition;
};
SimpleDepthOut main(SimpleVertexInstanced inputData)
{
	SimpleVertexIn input = inputData.objectData;
	ObjectInputInstance instance = inputData.instanceData;

	float4x4 modelTransform = float4x4(
		inputData.instanceData.mat0,
		inputData.instanceData.mat1,
		inputData.instanceData.mat2,
		inputData.instanceData.mat3
	);

	SimpleDepthOut output;
    output.position = mul(modelTransform, float4(inputData.objectData.position.xyz, 1.0f));
    output.position = mul(worldToClip, output.position);
	
	return output;
}