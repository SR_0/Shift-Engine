#include "CommonStructs.hlsli"
#include "Converters.hlsli"

Texture3D<float4> voxelTexture : register(t0);

cbuffer transformations : register(b0)
{
	float4x4 projection;
	float4x4 view;
	float4x4 model;
	float3 cameraPosition;
}; 
cbuffer VoxelSettings : register(b1)
{
	float3 VoxelCenter;
	float VoxelTextureSize;
	float VoxelSize;
};

inline uint3 GetVoxelCoords(uint index, uint size)
{
	const uint3 position = uint3
	(
		(index / (size * size)),
		((index / size) % size),
		(index % size)
	);
	return position;
}

VoxelDebugPixelIn main(uint vertexID : SV_VERTEXID)
{
	uint3 voxelCoords = GetVoxelCoords(vertexID, VoxelTextureSize);

	VoxelDebugPixelIn output;
	output.position = float4(voxelCoords, 1.f);
	output.color = voxelTexture[voxelCoords];
	return output;
}