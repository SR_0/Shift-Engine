#include "CommonStructs.hlsli"
#include "StaticSamplers.hlsli"
#include "Utilities.hlsli"

#define FXAA_PC 1
#define FXAA_HLSL_5 1
#define FXAA_GREEN_AS_LUMA 1
#define FXAA_QUALITY__PRESET 39
#include "FXAA.hlsli"

Texture2D FrameBuffer : register(t0);

SimplePixelOut main(in FullscreenPixelIn input)
{
	SimplePixelOut output;
	const float fxaaSubpix = 0.75f;
	const float fxaaEdgeThreshold = 0.166f;
	const float fxaaEdgeThresholdMin = 0.0833f;
	float2 Resolution = GetResolution(FrameBuffer);

	FxaaTex tex = { Sampler_Linear_Clamp, FrameBuffer };
	output.color = FxaaPixelShader(input.uv, 0, tex, tex, tex, 1 / Resolution, 0, 0, 0, fxaaSubpix, fxaaEdgeThreshold, fxaaEdgeThresholdMin, 0, 0, 0, 0);
	return output;
}