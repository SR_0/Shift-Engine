#ifndef _PBR_H_
#define _PBR_H_
#include "GeneralIncludes.hlsli"
#include "PBR_Structs.hlsli"

// Shlick's approximation of the Fresnel factor.
float3 FresnelSchlick(float3 F0, float cosTheta)
{
	return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

// GGX/Towbridge-Reitz normal distribution function.
// Uses Disney's reparametrization of alpha = roughness^2.
float GGX(float cosLh, float roughness)
{
	float alpha = roughness * roughness;
	float alphaSq = alpha * alpha;

	float denom = (cosLh * cosLh) * (alphaSq - 1.0) + 1.0;
	return alphaSq / (PI * denom * denom);
}

// Single term for separable Schlick-GGX below.
float GASchlickG1(float cosTheta, float k)
{
	return cosTheta / (cosTheta * (1.0 - k) + k);
}

// Schlick-GGX approximation of geometric attenuation function using Smith's method.
float GASchlickGGX(float cosLi, float cosLo, float roughness)
{
	float r = roughness + 1.0;
	float k = (r * r) / 8.0; // Epic suggests using this roughness remapping for analytic lights.
	return GASchlickG1(cosLi, k) * GASchlickG1(cosLo, k);
}

float3 DirectLight(in ObjectAttributes attributes, float3 toEye, float3 toLight, float3 lightColor, float lightIntensity, float attenuation)
{
	float3 directLight = float3(0.0f, 0.0f, 0.0f);

	float3 F0 = lerp(Dielectric, attributes.myDiffuse, attributes.myMetallic);

	float3 halfVec = normalize(toLight + toEye);

	float nDotL = max(0.0, dot(attributes.myNormal, toLight));
	float nDotC = max(0.0, dot(attributes.myNormal, toEye));
	float nDotHalf = max(0.0, dot(attributes.myNormal, halfVec));

	float3 fresnel = FresnelSchlick(F0, max(0.0, dot(halfVec, toEye)));
	float ndf = GGX(nDotHalf, attributes.myRoughness);
	float geomAttenuation = GASchlickGGX(nDotL, nDotC, attributes.myRoughness);

	float3 kd = lerp(float3(1, 1, 1) - fresnel, float3(0, 0, 0), attributes.myMetallic);

	float3 diffuseBRDF = kd * attributes.myDiffuse;
	float3 specularBRDF = (fresnel * ndf * geomAttenuation) / max(Epsilon, 4.0 * nDotL * nDotC);

	float3 light = (diffuseBRDF + specularBRDF) * lightIntensity * nDotL;

	return directLight = light * lightColor * attenuation;
}

Shift_TextureCube(skyDiffuse, TEXSLOT_SKY_DIFFUSE);
Shift_TextureCube(skySpecular, TEXSLOT_SKY_SPECULAR);
Shift_Texture2D(skyBrdf, TEXSLOT_SKY_BRDF);
uint QuerySkyTextureLevels()
{
	uint width, height, levels;
	skySpecular.GetDimensions(0, width, height, levels);
	return levels;
}
float3 AmbientLight(in ObjectAttributes attributes, in float3 toEye)
{
	float3 ambientLight = float3(0.0f, 0.0f, 0.0f);

	// Sample diffuse irradiance at normal direction.
	float3 irradiance = skyDiffuse.SampleLevel(Sampler_Aniso16_Wrap, attributes.myNormal, 1).rgb;

	// Calculate Fresnel term for ambient lighting.
	// Since we use pre-filtered cubemap(s) and irradiance is coming from many directions
	// use cosLo instead of angle with light's half-vector (cosLh above).
	// See: https://seblagarde.wordpress.com/2011/08/17/hello-world/
	float3 F0 = lerp(Dielectric, attributes.myDiffuse, attributes.myMetallic);
	float nDotC = max(0.0, dot(attributes.myNormal, toEye));
	float3 reflectionVec = 2.0 * nDotC * attributes.myNormal - toEye;
	float3 F = FresnelSchlick(F0, nDotC);

	// Get diffuse contribution factor (as with direct lighting).
	float3 kd = lerp(1.0 - F, 0.0, attributes.myMetallic);

	// Irradiance map contains exitant radiance assuming Lambertian BRDF, no need to scale by 1/PI here either.
	float3 diffuseIBL = kd * attributes.myDiffuse * irradiance;

	// Sample pre-filtered specular reflection environment at correct mipmap level.
	uint specularTextureLevels = QuerySkyTextureLevels();
	float3 specularIrradiance = skySpecular.SampleLevel(Sampler_Aniso16_Wrap, reflectionVec, attributes.myRoughness * specularTextureLevels).rgb;

	// Split-sum approximation factors for Cook-Torrance specular BRDF.
	float2 specularBRDF = skyBrdf.SampleLevel(Sampler_Linear_Clamp, float2(nDotC, attributes.myRoughness), 0).rg;

	// Total specular IBL contribution.
	float3 specularIBL = (F0 * specularBRDF.x + specularBRDF.y) * specularIrradiance;

	// Total ambient lighting contribution.
	ambientLight = diffuseIBL + specularIBL;

	return ambientLight;
}
#endif