#include "PBR_2.hlsli"
#include "NormalEncode.hlsli"

Shift_Texture2D(depthTexture, TEXSLOT_SCENE_DEPTH);
Shift_Texture2D(colorTexture, TEXSLOT_GBUFFER_COLOR);
Shift_Texture2D(normalTexture, TEXSLOT_GBUFFER_NORMALS);
Shift_Texture2D(armTexture, TEXSLOT_GBUFFER_ARM);
Shift_Texture2D(emissionTexture, TEXSLOT_GBUFFER_EMISSION);
Shift_Texture2D(BRDF_LUT, 5);
Shift_Texture2D(ShadowBuffer, 6);
Shift_Texture2D(SSAO, 7);

RWTexture2D<float4> outLightTexture : register(u0);

cbuffer CameraInfo : register(b0)
{
	float4x4 InverseProjection;
	float4x4 InverseView;
	float4x4 ShadowProjection;
	float4x4 ShadowView;
	float3 CameraPosition;
}
cbuffer LightInfo : register(b1)
{
	float3 LightColor;
	float Intensity;
	float3 ToLightDirection;
	float AmbientLightIntensity;
	float3 PointLightColor;
	float PointLightRange;
	float3 PointLightPos;
	float PointLightIntensity;
}

[numthreads(32, 32, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
	float2 Resolution = GetResolution(outLightTexture);
	if (threadID.x <= Resolution.x && threadID.y <= Resolution.y)
	{
		int3 sampleLocation = int3(threadID.xy, 0);

		float4 colorSample = colorTexture.Load(sampleLocation);
		
		if (colorSample.a > 0.0f)
		{
			float depthSample = depthTexture.Load(sampleLocation).r;
			float2 screenPos = PixelToUV(sampleLocation, Resolution);
			float3 worldPosition = WorldPositionFromDepth(screenPos, depthSample, InverseProjection, InverseView);
		
			float3 normalSample = mul(InverseView, Normal_Decode(normalTexture.Load(sampleLocation).xy));
			float3 armSample = armTexture.Load(sampleLocation).xyz;
		
			float3 toCamera = normalize(CameraPosition - worldPosition);
		
			ObjectAttributes attributes;
			attributes.myWorldPosition = worldPosition;
			attributes.myNormal = normalSample;
			attributes.myDiffuse = colorSample.rgb;
			attributes.myAO = armSample.r;
			attributes.myRoughness = armSample.g;
			attributes.myMetallic = armSample.b;
		
			float3 directLight = float3(0.0, 0.0, 0.0);
			// Sun
			float4 shadowPosition = float4(worldPosition, 1.0f);
			shadowPosition = mul(ShadowView, shadowPosition);
			shadowPosition = mul(ShadowProjection, shadowPosition);
		
			float2 projectTexCoord;
			projectTexCoord.x = shadowPosition.x / shadowPosition.w / 2.0f + 0.5f;
			projectTexCoord.y = -shadowPosition.y / shadowPosition.w / 2.0f + 0.5f;
		
			if ((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
			{
				float shadowSample = ShadowBuffer.SampleLevel(Sampler_Aniso16_Clamp, projectTexCoord, 0).r;
				if (shadowSample > shadowPosition.z - 0.001f)
					directLight += DirectLight(attributes, toCamera, normalize(ToLightDirection), LightColor, Intensity, 1.0f);
			}
			// Points
			{
				float3 toLight = PointLightPos - worldPosition;
				float toLightDistance = distance(PointLightPos, worldPosition);
				toLight = normalize(toLight);
				float lightRange = PointLightRange * PointLightRange;
				float toLightDist2 = toLightDistance * toLightDistance;
		
				float linearAttenuation = saturate(1.f - (toLightDist2 / lightRange));
				float physicalAttenuation = 1.f / toLightDistance;
				float attenuation = linearAttenuation * physicalAttenuation;
		
				directLight += (DirectLight(attributes, toCamera, toLight, PointLightColor, PointLightIntensity, attenuation));
			}
			float ssao = SSAO.Load(sampleLocation).x;
			float3 ambientLight = AmbientLight(attributes, toCamera) * AmbientLightIntensity * ssao;

			float3 emissionSample = emissionTexture.Load(sampleLocation).rgb;
			outLightTexture[sampleLocation.xy] = float4(directLight + ambientLight + emissionSample, 1.0f);
		}
		else
		{
			outLightTexture[sampleLocation.xy] = colorSample;
		}
	}
}