#include "StaticSamplers.hlsli"
#include "Utilities.hlsli"
#include "Luminance.hlsli"

Texture2D frameTexture : register(t0);
RWTexture2D<float4> outTexture : register(u0);

cbuffer BloomSettings : register(b0)
{
	float BloomThreshold;
};

[numthreads(8, 8, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
	float2 ScreenResolution = GetResolution(outTexture);
	if (threadID.x <= ScreenResolution.x && threadID.y <= ScreenResolution.y)
	{
		int3 sampleLocation = int3(threadID.xy, 0);
		float2 uv = PixelToUV(sampleLocation.xy, ScreenResolution);

		float3 pixelColor = frameTexture.SampleLevel(Sampler_Linear_Clamp, uv, 0).rgb;
		float pixelLuminance = Luminance(pixelColor);

		outTexture[sampleLocation.xy] = float4(pixelColor * pixelLuminance * BloomThreshold, 1.0f);
	}
}