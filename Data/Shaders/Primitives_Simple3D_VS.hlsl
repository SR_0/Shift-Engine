
struct VertexIn
{
	float4 myPosition : POSITION;
	float4 myColor : COLOR;
};

struct VertexOut
{
	float4 myPosition : SV_Position;
	float4 myColor : COLOR;
};

cbuffer ViewConstants : register(b0)
{
	float4x4 myViewProjection;
};


VertexOut main(VertexIn input)
{
    VertexOut output;

    output.myPosition = mul(myViewProjection, float4(input.myPosition.xyz, 1.0));
	output.myColor = input.myColor;

    return output;
}