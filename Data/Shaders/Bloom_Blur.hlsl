#include "StaticSamplers.hlsli"
#include "Utilities.hlsli"

Texture2D luminanceTexture : register(t0);
Texture2D luminanceQuarterTexture : register(t1);
RWTexture2D<float4> outTexture : register(u0);
RWTexture2D<float4> outQuarterTexture : register(u1);

cbuffer BlurSettings : register(b0)
{
	float2 BlurDirection;
};

[numthreads(8, 8, 1)]
void main(uint3 threadID : SV_DispatchThreadID)
{
	float2 OutTextureResolution = GetResolution(outTexture);
	float2 QuarterTextureResolution = GetResolution(outQuarterTexture);
	if (threadID.x <= OutTextureResolution.x && threadID.y <= OutTextureResolution.y)
	{
		int3 sampleLocation = int3(threadID.xy, 0);
		const uint KernelSize = 7;
		const float WeightList[KernelSize] = { 0.115528, 0.141488, 0.159786, 0.166396, 0.159786, 0.141488, 0.115528 };
		const float2 PixelOffset[KernelSize] = { float2(-3,-3), float2(-2,-2), float2(-1,-1), float2(0,0),
													float2(1,1), float2(2,2), float2(3,3) };
		{
			float3 blur = float3(0.0f, 0.0f, 0.0f);
			float2 texelSize = (1 / OutTextureResolution);
			float2 uv = PixelToUV(sampleLocation.xy, OutTextureResolution);

			for (uint i = 0; i < KernelSize; ++i)
			{
				float2 offsetUV = uv + (texelSize * PixelOffset[i] * BlurDirection);
				float3 resource = luminanceTexture.SampleLevel(Sampler_Linear_Clamp, offsetUV, 0).rgb;
				blur += saturate(resource * (WeightList[i]));
			}

			outTexture[sampleLocation.xy] = float4(blur, 1.0f);
		}
		if (threadID.x <= QuarterTextureResolution.x && threadID.y <= QuarterTextureResolution.y)
		{
			const uint KernelSize = 13;
			const float WeightList[KernelSize] = { 0.002216, 0.008764, 0.026995, 0.064759, 0.120985, 0.176033, 0.199471, 0.176033, 0.120985, 0.064759, 0.026995, 0.008764, 0.002216 };
			const float2 PixelOffset[KernelSize] = { float2(-6,-6), float2(-5,-5), float2(-4,-4), float2(-3,-3),float2(-2,-2), float2(-1,-1), float2(0,0), float2(1,1), float2(2,2), float2(3,3), float2(4,4), float2(5,5), float2(6,6) };

			float3 quarterBlur = (float3)0;
			for (uint i = 0; i < KernelSize; ++i)
			{
				float2 texelSizeQ = (1 / QuarterTextureResolution);
				float2 quarterUV = PixelToUV(sampleLocation.xy, QuarterTextureResolution);
				float2 offsetUV = quarterUV + (texelSizeQ * PixelOffset[i] * BlurDirection);
				float3 resource = luminanceQuarterTexture.SampleLevel(Sampler_Linear_Clamp, offsetUV, 0).rgb;
				quarterBlur += saturate(resource * (WeightList[i]));
			}
			outQuarterTexture[sampleLocation.xy] = float4(quarterBlur, 1.0f);
		}
	}
}
